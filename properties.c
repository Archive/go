/* properties.c - properties for a specific document.
 *
 * Copyright (C) 1999 Chris Lahey.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include "config.h"
#include <gnome.h>
#include "properties.h"
#include "undo_if.h"

GoProperties *
go_properties_new()
{
  GoProperties *new_props = g_new( GoProperties, 1 );
  new_props->print_page_nums = FALSE;
  return new_props;
}

GoProperties *
go_properties_clone( GoProperties *props )
{
  GoProperties *new_props = g_new( GoProperties, 1 );
  *new_props = *props;
  return new_props;
}

void
go_properties_destroy( GoProperties *props )
{
  g_free( props );
}

typedef struct
{
  GoProperties *props;
  GOWindow *window;
  GtkWidget *print_page_nums;
} props_dialog_data;

static void
delete_props_dialog_data( GtkWidget *widget, gpointer data )
{
  props_dialog_data *props_data = data;
  g_free( props_data );
}

static void
props_dialog_clicked( GtkWidget *widget, gint button, gpointer data )
{
  GnomeDialog *dialog = GNOME_DIALOG( widget );
  props_dialog_data *props_data = data;

  if ( button == 0 )
    {
      props_change_param param;
      param.new_props = go_properties_new();
      param.new_props->print_page_nums =
	gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( props_data->print_page_nums ) );
      append_to_undo_tree( props_data->window->undo, UNDO_TYPE_PROPERTIES, &param );
      go_properties_destroy( param.new_props );
    }
  
  gnome_dialog_close( dialog );
}

/* Here widget should be the menu item and data should be the GOWindow
   it was invoked on, but it doesn't seem like that should be needed
   for program wide preferences. */
void
open_props_dialog( GtkWidget *widget, gpointer data )
{
  GnomeDialog *dialog = GNOME_DIALOG ( gnome_dialog_new( _( "Set properties" ),
							 GNOME_STOCK_BUTTON_OK,
							 GNOME_STOCK_BUTTON_CANCEL,
							 NULL ) );
  props_dialog_data *props_data = g_new( props_dialog_data, 1 );
  GOWindow *window = (GOWindow *) data;

  props_data->print_page_nums = gtk_check_button_new_with_label( _( "Print page numbers :" ) );
  props_data->window = window;
  props_data->props = window->props;

  gtk_box_pack_start( GTK_BOX( dialog->vbox ), props_data->print_page_nums, TRUE, TRUE, 0 );
  gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( props_data->print_page_nums ),
				props_data->props->print_page_nums );
  
  gtk_signal_connect( GTK_OBJECT( dialog ),
		      "clicked",
		      (GtkSignalFunc) props_dialog_clicked,
		      props_data );
  gtk_signal_connect( GTK_OBJECT( dialog ),
		      "destroy",
		      (GtkSignalFunc) delete_props_dialog_data,
		      props_data );
  gnome_dialog_set_parent( dialog, GTK_WINDOW( window->app ) );
  gnome_dialog_set_default( dialog, 0 );

  gtk_widget_show_all( GTK_WIDGET( dialog ) );
}
