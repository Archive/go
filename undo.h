/* undo.h - Header file containing structures for undoing and redoing.
 *
 * Copyright (C) 1998 Chris Lahey.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __UNDO_H__
#define __UNDO_H__

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

typedef struct UndoTree UndoTree;

typedef void (*UndoRedoFunc) ( gpointer tree_data, gpointer data, gboolean primary_tree );
typedef void (*UndoUndoFunc) ( gpointer tree_data, gpointer data, gboolean primary_tree );
typedef gpointer (*UndoCreateFunc) ( gpointer tree_data, gpointer data_param, gboolean primary_tree, gint *appending, gint prev_type, gpointer prev_data );
typedef void (*UndoDestroyFunc) ( gpointer tree_data, gpointer data );
typedef void (*UndoErrorFunc) ( gpointer tree_data, gchar *message, gboolean primary_tree );

typedef struct
{
  /* primary_tree is non-zero if undo_latest was called on this tree.
     It's false if it was propagated to this tree by the GList
     *other_trees.  */
  UndoRedoFunc redo;
  UndoUndoFunc undo;
  UndoCreateFunc create;
  UndoDestroyFunc destroy;
  UndoErrorFunc error;
#if 0
  void (*redo) ( gpointer tree_data, gpointer data, gboolean primary_tree ); 
  void (*undo) ( gpointer tree_data, gpointer data, gboolean primary_tree );
  gpointer (*create) ( gpointer tree_data, gpointer data_param, gboolean primary_tree, gint *appending, gint prev_type, gpointer prev_data );
  void (*destroy) ( gpointer tree_data, gpointer data );
  void (*error) ( gpointer tree_data, gchar *message, gboolean primary_tree );
#endif
} UndoCallbacks;

typedef struct
{
  UndoCallbacks *callbacks;
  gint count;
} UndoSystem;

typedef struct undo_node
{
  gint type;
  gpointer data;
  struct undo_node *prev;
  struct undo_node *next;
} undo_node;

struct UndoTree
{
  undo_node *data;
  undo_node *current;
  gint appending;
  gint appending_callback;
  gpointer *tree_data;  /* Used to be GOWindow *. */
  GList *other_trees; /* Of type UndoTree. */
  UndoSystem *system;
};

UndoSystem *undo_init( UndoCallbacks undo_types[], gint count );

void append_to_undo_tree( UndoTree *tree, gint type, gpointer data_param );
void undo_latest( UndoTree *tree, gboolean primary_tree );
void redo_latest( UndoTree *tree, gboolean primary_tree );
UndoTree *undo_tree_new( UndoSystem *system, gpointer tree_data, GList *other_trees /* Of type UndoTree.  This gets adopted. */ );
void undo_tree_destroy( UndoTree *tree );

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
