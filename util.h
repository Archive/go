/* util.h - Header file for a few utility functions for GO.
 *
 * Copyright (C) 1998 Chris Lahey.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <gtk/gtk.h>
#include <stdio.h>

gchar *append2( gchar *, gboolean, gchar *, gboolean );
int getinteger( FILE *fp );
void clear_white( FILE *fp );
gfloat getfloat( FILE *fp );
