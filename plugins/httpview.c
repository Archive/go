/* httpview.c - View current document in current Gnome URL viewer
 *
 * Copyright (C) 1999 Alex Roberts
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdio.h>
#include <gnome.h>
#include "client.h"
#include <config.h>

int main (int argc, char *argv[])
{
	client_info info = empty_info;
	selection_range range;
	int docid, contextid, pos;
	char *text;

	info.menu_location = "[Plugins]View In Browser";

	contextid = client_init (&argc, &argv, &info);
	
	  gnome_init("httpview", VERSION, argc, argv);

	
	docid = client_document_current (contextid);

	
	text = client_document_filename (docid);
	
	if (text == NULL)
		printf ("Please Save Document First.\n");
	else
		gnome_url_show (text);
		
	client_finish( contextid );

	exit (0);
}

