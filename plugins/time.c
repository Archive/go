/* time.c - time plugin.
 *
 * Copyright (C) 1999 Alex Roberts.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ---------------
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
/* Header for making plugin act as a plugin */
#include "client.h"

static gint context;
static gint docid;
static gchar *the_time;
char *out = NULL;

/* Gratiously ripped out of GIMP (app/general.c), with a fiew minor changes */
char *get_time()
{
	static char static_buf[21];
  	char *tmp;
  	time_t clock;
  	struct tm *now;
  	const char *format = NULL;
  	size_t out_length = 0;
  	

  	clock = time (NULL);
  	/*now = gmtime (&clock);*/
  	now = localtime (&clock);
	  	
  	tmp = static_buf;
	
  	/* date format derived from ISO 8601:1988 */
  	/*sprintf(tmp, "%04d-%02d-%02d%c%02d:%02d:%02d%c",
	  	now->tm_year + 1900, now->tm_mon + 1, now->tm_mday,
	  	' ',
	  	now->tm_hour, now->tm_min, now->tm_sec,
	  	'\000'); 
	
	return tmp;
	*/
	format = "%a %b %e %H:%M:%S %Z %Y";

	do
    	  {
      	    out_length += 200;
      	    out = (char *) realloc (out, out_length);
    	  }
  	while (strftime (out, out_length, format, now) == 0);

  	
  	return out;
}
  
int main( int argc, char *argv[] )
{
  client_info info = empty_info;
  
  /* Location of the plugin in the 'Plugin's' menu in gEdit */
  info.menu_location = "[Plugins]Insert Time";
  
  	      /* Initialisation of the Plugin itself, checking if being
  	         run as a plugin (ie, fromg gEdit, not from command line */	
    context = client_init( &argc, &argv, &info );

	/* The 'output' of the Plugin */
	docid = client_document_current (context);
	
	the_time = get_time();
	
	client_text_insert (docid, the_time, strlen(the_time), client_document_get_position (docid));
	
	
      client_finish( context );
 
  free (out);
  exit(0);
}
