/* word-count.c
 *
 * Copyright (C) 1998 Chris Lahey.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <string.h>
#include <config.h>
#include <gnome.h>
#include "client.h"

int main( int argc, char *argv[] )
{
  int docid;
  int length;
  int context;
  int i;
  gint char_count = 0;
  gint par_count = 0;
  gint word_count = 0;
  gboolean in_word = FALSE;
  gchar *buff, *msg;
  client_info info = empty_info;

  info.menu_location = "[Plugins]Word Count";

  context = client_init( &argc, &argv, &info );

  bindtextdomain(PACKAGE, GNOMELOCALEDIR);
  textdomain(PACKAGE);

  gnome_init("wordcount-plugin", VERSION, argc, argv);

  docid = client_document_current( context );
  length = strlen( buff = client_text_get( docid ) );

  for( i=0; i < length; i++ )
    {
      switch( buff[i] )
	{
	case ' ':
	  if ( in_word )
	    word_count ++;
	  in_word = FALSE;
	  break;
	case '\n':
	  if ( in_word )
	    word_count ++;
	  par_count ++;
	  in_word = FALSE;
	  break;
	default:
	  in_word = TRUE;
	  break;
	}
      char_count ++;
    }
  if ( in_word )
    word_count ++;
  par_count ++;
  
  msg = g_malloc0 (80);
  sprintf(msg, "Characters: %d\nWords: %d\nParagraphs: %d\n",
	   char_count, word_count, par_count );

  gnome_dialog_run_and_close ((GnomeDialog *)
		   gnome_message_box_new (msg, GNOME_MESSAGE_BOX_INFO,
			   					  GNOME_STOCK_BUTTON_OK, NULL));

  gtk_main ();
     
  client_finish( context );
  
  exit(0);
}
