#! /usr/bin/perl

die unless (chdir( "$ENV{srcdir}/editor-plugins/" ));
open LIST, "plugins.list";
while ( <LIST> )
{
    s/#.*\n?$//; next if /^\s*$/; s/\s*$//; s/^\s*//; push @entry, $_ if length;
}

open DIRLIST, "plugins.dir.list";
while ( <DIRLIST> )
{
    s/#.*\n?$//; next if /^\s*$/; s/\s*$//; s/^\s*//; push @direntry, $_ if length;    
}

open GNOMELIST, "plugins.gnome.list";
while ( <GNOMELIST> )
{
    s/#.*\n?$//; next if /^\s*$/; s/\s*$//; s/^\s*//; push @gnomeentry, $_ if length;    
}

open MAKEFILE, "> Makefile.am";
print MAKEFILE << 'EOF';
## This is a generated file.  Please see plugins.pl.
plugindir = $(libexecdir)/go/plugins

EOF

if ( $#direntry + 1 > 0 )
{
    print MAKEFILE "SUBDIRS =";
    foreach $i (@direntry)
    {
	print MAKEFILE " $i";
    }
    print MAKEFILE "\n\n";
}

print MAKEFILE ( "plugin_PROGRAMS =" );
foreach $i (@entry)
{
    print MAKEFILE ( " $i-plugin" );
}

foreach $i (@gnomeentry)
{
    print MAKEFILE ( " $i-plugin" );
}
print MAKEFILE << "EOF";


INCLUDES = \$(GTK_CFLAGS) -Wall \$(GNOME_INCLUDEDIR)
CPPFLAGS = -DGNOMELOCALEDIR=\\\"\"\$(datadir)/locale\"\\\"

EOF

foreach $i (@entry)
{
    print MAKEFILE << "EOF";

${i}_plugin_SOURCES = \\
 $i.c \\
 client.c \\
 client.h

${i}_plugin_LDADD = \$(GTK_LIBS)
EOF
}

foreach $i (@gnomeentry)
{
    print MAKEFILE << "EOF";

${i}_plugin_SOURCES = \\
 $i.c \\
 client.c \\
 client.h

${i}_plugin_LDFLAGS = \$(GTK_LIBS) \$(GNOMEUI_LIBS) \$(GNOME_LIBDIR)
EOF
}
