/* openother.c
 *
 * Copyright (C) 1998 Chris Lahey.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gtk/gtk.h>

#include "client.h"
#include <glib.h>

int main( int argc, char *argv[] )
{
  int docid;
  int length;
  int context;
  gchar *filename;
  client_info info = empty_info;

  info.menu_location = "[Plugins]Open Header|Code";
  info.suggested_accelerator = "<alt>1";
  context = client_init( &argc, &argv, &info );

  docid = client_document_current( context );
  filename = client_document_filename( docid );
  length = strlen( filename );

  if ( ! strcmp( filename + length - 2, ".c" ) )
    {
      filename[length - 1] = 'h';
    }
  else if ( ! strcmp( filename + length - 2, ".h" ) )
    {
      filename[length - 1] = 'c';
    }
  else
    {
      client_finish( context );
      exit(0);
    }

  docid = client_document_open( context, filename );
  client_document_show( docid );
  client_finish( context );
  
  exit(0);
}
