/* man.c - Man Page viewer plugin
 *
 * Copyright (C) 1999 Alex Roberts
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <config.h>
#include <gnome.h>
#include "client.h"

static GtkWidget *entry1;
static GtkWidget *entry2;
static gint context;



static void
man_get_page( GtkWidget *widget, gint button, gpointer data )
{
  if ( button == 0 )
    {
      char buff[1025];
      int fdpipe[2];
      int pid;
      char *args[2] = { NULL };
      int docid;
      int length;

      args[0] = gtk_entry_get_text( GTK_ENTRY( entry1 ) );
      args[1] = gtk_entry_get_text( GTK_ENTRY( entry2 ) );

      if( pipe( fdpipe ) == -1 )
	{
	  _exit( 1 );
	}
  
      pid = fork();
      if ( pid == 0 )
	{
	  /* New process. */
	  char *argv[4];

	  close( 1 );
	  dup( fdpipe[1] );
	  close( fdpipe[0] );
	  close( fdpipe[1] );
      
	  argv[0] = g_strdup( "man" );
	  argv[1] = args[1];
	  argv[2] = args[0];
	  argv[3] = NULL;
	  execv( "/usr/bin/man", argv );
	  /* This is only reached if something goes wrong. */
	  _exit( 1 );
	}
      close( fdpipe[1] );

      docid = client_document_new( context, "man" );
  
      length = 1;
      while( length > 0 )
	{
	  buff[ length = read( fdpipe[0], buff, 1024 ) ] = 0;
	  if( length > 0 )
	    {
	      client_text_append( docid, buff, length );
	    }
	}
      client_document_show( docid );
  
      gnome_config_push_prefix ("/Editor_Plugins/Man/");
      gnome_config_set_string ("Page", args[0]);
      gnome_config_set_string ("Section", args[1]);
      gnome_config_pop_prefix ();
      gnome_config_sync ();
  
    }
  gnome_dialog_close( GNOME_DIALOG( widget ) );
}

static void done( GtkWidget *widget, gpointer data )
{
  client_finish(context);
  gtk_main_quit();
}

int main( int argc, char *argv[] )
{
  GtkWidget *label;
  GtkWidget *hbox;
  GtkWidget *dialog;
  client_info info = empty_info;
  gchar *temp;

  info.menu_location = "[Plugins]Man Page";

  context = client_init( &argc, &argv, &info );
  
  /* gtk_init( &argc, &argv ); */
  bindtextdomain(PACKAGE, GNOMELOCALEDIR);
  textdomain(PACKAGE);

  gnome_init("manwse-plugin", VERSION, argc, argv);

  dialog = gnome_dialog_new( _("The Man Page Viewer Plugin"),
			     GNOME_STOCK_BUTTON_OK,
			     GNOME_STOCK_BUTTON_CANCEL,
			     NULL );
  gnome_dialog_set_default( GNOME_DIALOG( dialog ), 0 );
  gtk_signal_connect( GTK_OBJECT( dialog ), "clicked",
		      GTK_SIGNAL_FUNC( man_get_page ), NULL );
  gtk_signal_connect( GTK_OBJECT( dialog ), "destroy",
		      GTK_SIGNAL_FUNC( done ), NULL );
  /*  gtk_widget_set_usize (GTK_WIDGET (dialog), 353, 100); */

  hbox = gtk_hbox_new( FALSE, 0 );
  gtk_box_pack_start( GTK_BOX( GNOME_DIALOG( dialog )->vbox ), hbox,
		      TRUE, TRUE, 0 );

  label = gtk_label_new( "Page:" );
  gtk_box_pack_start( GTK_BOX( hbox ), label,
		      TRUE, TRUE, 0 );

  entry1 = gtk_entry_new();
  gtk_box_pack_start( GTK_BOX( hbox ), entry1, TRUE, TRUE, 0 );

  hbox = gtk_hbox_new( FALSE, 0 );
  gtk_box_pack_start( GTK_BOX( GNOME_DIALOG( dialog )->vbox ), hbox,
		      TRUE, TRUE, 0 );

  label = gtk_label_new( "Section:" );
  gtk_box_pack_start( GTK_BOX( hbox ), label,
		      TRUE, TRUE, 0 );

  entry2 = gtk_entry_new();
  gtk_box_pack_start( GTK_BOX( hbox ), entry2, TRUE, TRUE, 0 );


  gnome_config_push_prefix ("/Editor_Plugins/Man/");
  temp = gnome_config_get_string( "Page" );
  if ( temp )
    {
      gtk_entry_set_text ( GTK_ENTRY( entry1 ), temp );
      g_free( temp );
    }
  gnome_config_pop_prefix ();
  gnome_config_sync ();

  gtk_widget_show_all( dialog );

  gtk_main();

  exit( 0 );
}
