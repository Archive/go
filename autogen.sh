#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.
export srcdir

PKG_NAME="Go"

(test -f $srcdir/makeconfig.pl \
  && test -f $srcdir/go_plugin_api.h) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level go directory"
    exit 1
}

perl $srcdir/plugins/plugins.pl
perl $srcdir/makeconfig.pl

. $srcdir/macros/autogen.sh
