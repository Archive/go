/* font.h - functions that do things to fonts.
 *
 * Copyright (C) 1998 Chris Lahey.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __FONT_H__
#define __FONT_H__

#include <gtk/gtk.h>

void font_color_callback( GtkWidget *widget, gpointer data );
void open_font_dialog( GtkWidget *widget, gpointer data );
void open_par_format_dialog( GtkWidget *widget, gpointer data );

void set_paragraph_toolbar_settings( GtkWidget *widget, Position *a, Position *b, gpointer data );
void toolbar_fill_justified_callback( GtkWidget *widget, gpointer data );
void toolbar_left_justified_callback( GtkWidget *widget, gpointer data );
void toolbar_center_justified_callback( GtkWidget *widget, gpointer data );
void toolbar_right_justified_callback( GtkWidget *widget, gpointer data );
		    
#endif /* __FONT_H__ */
