/* print.h - Header file for printing GO files.
 *
 * Copyright (C) 1998 Chris Lahey.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */


#ifndef __PRINT_H__
#define __PRINT_H__

#include <gtk/gtk.h>

void file_print_callback( GtkWidget *widget, gpointer data );
void file_page_setup_callback( GtkWidget *widget, gpointer data );

#endif /* __PRINT_H__ */
