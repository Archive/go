/* util.c - A few utility functions for GO.
 *
 * Copyright (C) 1998 Chris Lahey.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <gtk/gtk.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "util.h"

gchar *append2( gchar *string1, gboolean del1, gchar *string2, gboolean del2)
{
  gchar *newstring = g_malloc( strlen( string1 ) + strlen( string2 ) + 1 );
  sprintf( newstring, "%s%s", string1, string2 );
  if ( del1 )
    g_free( string1 );
  if ( del2 )
    g_free( string2 );
  return newstring;
}

int getinteger( FILE *fp )
{
  gchar nextchar;
  int response = 0;
  gboolean negative = FALSE;

  while ( isspace( nextchar = fgetc( fp ) ) )
    /* empty statement */ ;

  if ( nextchar == '-' )
    {
      negative = TRUE;
      while ( isspace( nextchar = fgetc( fp ) ) )
	/* empty statement */ ;
    }

  for ( ; '0' <= nextchar && '9' >= nextchar; nextchar = fgetc( fp ) )
    {
      response *= 10;
      response += nextchar - '0';
    }
  for ( ; isspace( nextchar ); nextchar = fgetc( fp ) )
    /* empty statement */ ;
  ungetc( nextchar, fp );
  if ( negative )
    response = -response;
  return response;
}

void clear_white( FILE *fp )
{
  gint nextchar;

  while ( isspace( nextchar = fgetc( fp ) ) )
    /* empty statement */ ;
  if ( nextchar != EOF )
    ungetc( nextchar, fp );
}

gfloat getfloat( FILE *fp )
{
  gchar nextchar;
  gint response_left = 0;
  gint decimal_count = 0;
  gint response_decimal = 0;
  gfloat response;

  while ( isspace( nextchar = fgetc( fp ) ) )
    /* empty statement */ ;

  for ( ; '0' <= nextchar && '9' >= nextchar; nextchar = fgetc( fp ) )
    {
      response_left *= 10;
      response_left += nextchar - '0';
    }
  if ( nextchar == '.' )
    {
      for( decimal_count = 0, nextchar = fgetc( fp ); '0' <= nextchar && '9' >= nextchar; nextchar = fgetc( fp ), decimal_count ++ )
	{
	  response_decimal *= 10;
	  response_decimal += nextchar - '0';
	}
    }
  response = (gfloat) response_decimal;
  for ( ; decimal_count > 0; decimal_count -- )
    response /= 10.0;
  response += (gfloat) response_left;
  for ( ; isspace( nextchar ); nextchar = fgetc( fp ) )
    /* empty statement */ ;
  ungetc( nextchar, fp );
  return response;
}
