/* converter.c - Converts back and forth between utf-8 and ucs4.
 *
 * Copyright (C) 1998 Chris Lahey.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */


#include <glib.h>

/* Returns the number of unprocessed words. */
int
ucs4utf8( char *dest, guint32 *source, int length )
{
  guint32 *ucs32;
  guint8 *utf8;
  int i;
  int bytelength;

  ucs32 = source;
  utf8 = dest;
  
  for( i = 0; i < length; i++ )
    {
      if ( *ucs32 & 0x0000007f == *ucs32 )
	{
	  *utf8++ = *ucs32;
	  bytelength = 0;
	}
      else if ( *ucs32 & 0x000007ff == *ucs32 )
	{
	  *utf8++ = (*ucs32 >> 6 ) | 0xc0;
	  bytelength = 1;
	}
      else if ( *ucs32 & 0x0000ffff == *ucs32 )
	{
	  *utf8++ = (*ucs32 >> 12) | 0xe0;
	  bytelength = 2;
	}
      else if ( *ucs32 & 0x001fffff == *ucs32 )
	{
	  *utf8++ = (*ucs32 >> 18) | 0xf0;
	  bytelength = 3;
	}
      else if ( *ucs32 & 0x03ffffff == *ucs32 )
	{
	  *utf8++ = (*ucs32 >> 24) | 0xf8;
	  bytelength = 4;
	}
      else
	{
	  *utf8++ = ((*ucs32 >> 30) & 0x01 ) | 0xfc;
	  bytelength = 5;
	}
      for( ; bytelength > 0; bytelength -- )
	{
	  *utf8++ = ( (*ucs32 >> ( ( bytelength - 1 ) * 6 ) ) & 0x3f ) | 0x80;
	}
      ucs32++;
    }
  return length - i;
}

/* Returns the number of unprocessed characters. */
int
utf8ucs4( guint32 *dest, char *source, int length )
{
  guint32 *utf32;
  guint32 *ucs32;
  guint8 *utf8;
  int i;

  utf8 = source;
  ucs32 = dest;
  while( i < length && (((int)utf8) % 4) )
    {
      if( ! ( *utf8 & 0x80 ) )
	{
	  *ucs32++ = *utf8++;
	  i++;
	}
      else
	{
	  int bytelength = 0;
	  guint32 result;
	  if ( ! *utf8 & 0x40 )
	    {
	      *ucs32++ = *utf8++;
	      i++;
	    }
	  else
	    {
	      if ( ! *utf8 & 0x20 )
		{
		  result = *utf8++ & 0x1F;
		  bytelength = 2;
		}
	      else if ( ! *utf8 & 0x10 )
		{
		  result = *utf8++ & 0x0F;
		  bytelength = 3;
		}
	      else if ( ! *utf8 & 0x08 )
		{
		  result = *utf8++ & 0x07;
		  bytelength = 4;
		}
	      else if ( ! *utf8 & 0x04 )
		{
		  result = *utf8++ & 0x03;
		  bytelength = 5;
		}
	      else if ( ! *utf8 & 0x02 )
		{
		  result = *utf8++ & 0x01;
		  bytelength = 6;
		}
	      else
		{
		  result = *utf8++;
		  bytelength = 1;
		}
	      i++;
	      for ( bytelength --; bytelength > 0; bytelength -- )
		{
		  i++;
		  result <<= 6;
		  result |= *utf8++ & 0x3F;
		}
	      *ucs32++ = result;
	    }
	}
    }
  for ( i = 0; i < length; )
    {
      utf32 = (guint32 *) utf8;

      if ( length - i >= 4 && ! ( *utf32 & 0x80808080 ) )
	{
	  *ucs32++ = *utf8++;
	  *ucs32++ = *utf8++;
	  *ucs32++ = *utf8++;
	  *ucs32++ = *utf8++;
	  i += 4;
	}
      else
	{
	  while( i < length && (((int)utf8) % 4) )
	    {
	      if( ! ( *utf8 & 0x80 ) )
		{
		  *ucs32++ = *utf8++;
		  i++;
		}
	      else
		{
		  int bytelength = 0;
		  guint32 result;
		  if ( ! *utf8 & 0x40 )
		    {
		      *ucs32++ = *utf8++;
		      i++;
		    }
		  else
		    {
		      if ( ! *utf8 & 0x20 )
			{
			  result = *utf8++ & 0x1F;
			  bytelength = 2;
			}
		      else if ( ! *utf8 & 0x10 )
			{
			  result = *utf8++ & 0x0F;
			  bytelength = 3;
			}
		      else if ( ! *utf8 & 0x08 )
			{
			  result = *utf8++ & 0x07;
			  bytelength = 4;
			}
		      else if ( ! *utf8 & 0x04 )
			{
			  result = *utf8++ & 0x03;
			  bytelength = 5;
			}
		      else if ( ! *utf8 & 0x02 )
			{
			  result = *utf8++ & 0x01;
			  bytelength = 6;
			}
		      else
			{
			  result = *utf8++;
			  bytelength = 1;
			}
		      if ( i + bytelength > length )
			return length - i;
		      i++;
		      for ( bytelength --; bytelength > 0; bytelength -- )
			{
			  i++;
			  result <<= 6;
			  result |= *utf8++ & 0x3F;
			}
		      *ucs32++ = result;
		    }
		}
	    }
	}
    }
  return length - i;
}
