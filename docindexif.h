/* docindexif.h - Interface file for the docindex for go.
 *
 * Copyright (C) 1998 Chris Lahey.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __DOCINDEXIF_H__
#define __DOCINDEXIF_H__


#include "config.h"

#include "docindex.h"

#include <ctype.h>
#include <gdk/gdkprivate.h>

#include "toolbar.h"
#include "file.h"
#include "menu.h"
#include "util.h"

void open_or_raise( gchar *file_name );
void raise_if_match( gpointer data, gpointer user_data );
gboolean exit_from_go( void );

#define IDEAPATH "/.go/ideas"


struct bool_char_pair
{
  gboolean boole;
  gchar *string;
};

struct bool_char_gpointer_triple
{
  gboolean boole;
  gchar *string;
  gpointer data;
};

#endif /* __DOCINDEXIF_H__ */
