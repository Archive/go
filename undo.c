/* undo.c - Contains functions for undoing and redoing.
 *
 * Copyright (C) 1998 Chris Lahey.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include "config.h"
#include <gtk/gtk.h>
#include <stdio.h>
#include "undo.h"
#include "window.h"
#include "file.h"

#include "gtkrichtext.h"

static void delete_rest( UndoTree *system, undo_node *node );
static undo_node *undo_node_new( UndoTree *tree, gint type, gpointer data_param, undo_node *after, gboolean primary );
static gint stop_appending_callback( gpointer data );
static void undo_latest_callback( gpointer data, gpointer user_data );
static void redo_latest_callback( gpointer data, gpointer user_data );


UndoSystem *undo_init( UndoCallbacks undo_types[], gint count )
{
  UndoSystem *return_val;
  gint i;
  return_val = g_new( UndoSystem, 1 );
  return_val->count = count;
  return_val->callbacks = g_new( UndoCallbacks, return_val->count );
  for ( i = 0; i < return_val->count; i++ )
    {
      return_val->callbacks[i] = undo_types[i];
    }
  return return_val;
}

static void delete_rest( UndoTree *tree, undo_node *node )
{
  if( node )
    {
      if( node->prev )
	{
	  node->prev->next = 0;
	}
      delete_rest( tree, node->next );
      tree->system->callbacks[node->type].destroy ( tree->tree_data, node->data );
      g_free( node );
    }
}

static undo_node *undo_node_new( UndoTree *tree, gint type, gpointer data_param, undo_node *after, gboolean primary )
{
  undo_node *node = g_malloc0( sizeof( undo_node ) );
  gint appending = tree->appending;

  node->type = type;
  if ( after != NULL )
    node->data = tree->system->callbacks[node->type].create ( tree->tree_data, data_param, primary, &appending, after->type, after->data );
  else
    node->data = tree->system->callbacks[node->type].create ( tree->tree_data, data_param, primary, &appending, -1, NULL );
  node->prev = after;
  if( after )
    {
      node->next = after->next;
      after->next = node;
    }
  else
    {
      node->next = NULL;
    }
  if( node->next )
    {
      node->next->prev = node;
    }
  return node;
}

static void
add_to_other_related_trees( UndoTree *other_tree, UndoTree *tree )
{
  other_tree->other_trees = g_list_append( other_tree->other_trees, tree );
}

UndoTree *undo_tree_new( UndoSystem *system, gpointer tree_data, GList *related_trees )
{
  UndoTree *tree = g_malloc0( sizeof( UndoTree ) );

  tree->system = system;
  tree->tree_data = tree_data;

  tree->other_trees = related_trees;
  g_list_foreach( related_trees, (GFunc) add_to_other_related_trees, tree );
  
  return tree;
}

static void
remove_from_other_related_trees( UndoTree *other_tree, UndoTree *tree )
{
  other_tree->other_trees = g_list_remove( other_tree->other_trees, tree );
}

void
undo_tree_destroy( UndoTree *tree )
{
  g_list_foreach( tree->other_trees, (GFunc) remove_from_other_related_trees, tree );
  g_list_free( tree->other_trees );
  g_free( tree );
}

static gint stop_appending_callback( gpointer data )
{
  UndoTree *tree = (UndoTree *) data;

  tree->appending = 0;
  tree->appending_callback = 0;

  return FALSE;
}

static void
do_append_to_undo_tree( UndoTree *tree, gint type, gpointer data_param, gboolean primary )
{
  if( tree )
    {
      if( tree->current )
	{
	  delete_rest( tree, tree->current->next );

	  tree->current = undo_node_new( tree, type, data_param, tree->current, primary );
	  
	  if ( tree->appending_callback )
	    {
	      gtk_timeout_remove( tree->appending_callback );
	    }
	    
	  tree->appending_callback = gtk_timeout_add( 15000, stop_appending_callback,
						      (gpointer) tree );
	  tree->appending = 1;
	}
      else
	{
	  delete_rest( tree, tree->data );
	  tree->current = tree->data = undo_node_new( tree, type, data_param, NULL, primary );
	  tree->appending = 1;
	}
    }
}

static void
append_to_non_primary_undo_tree( UndoTree *tree, gint type, gpointer data_param )
{
  do_append_to_undo_tree( tree, type, data_param, FALSE );
}

void
append_to_undo_tree( UndoTree *tree, gint type, gpointer data_param )
{
  do_append_to_undo_tree( tree, type, data_param, TRUE );
}

static void
undo_latest_callback( gpointer data, gpointer user_data )
{
  undo_latest( (UndoTree *) data, FALSE );
}

void
undo_latest( UndoTree *tree, gboolean primary )
{
  if( tree )
    {
      if( tree->current )
	{
	  tree->system->callbacks[tree->current->type].undo( tree->tree_data, tree->current->data, primary );
	  tree->current = tree->current->prev;
	}
      else
	{
	  if( tree->data )
	    tree->system->callbacks[0].error( tree->tree_data, _( "No Undo information left." ), primary );
	  else
	    tree->system->callbacks[0].error( tree->tree_data, _( "No Undo information." ), primary );
	}
      tree->appending = 0;
      if ( primary )
	{
	  g_list_foreach( tree->other_trees, undo_latest_callback, NULL );
	}
    }
}

static void
redo_latest_callback( gpointer data, gpointer user_data )
{
  redo_latest( (UndoTree *) data, FALSE );
}

void
redo_latest( UndoTree *tree, gboolean primary )
{
  if( tree )
    {
      if( tree->current )
	{
	  if ( tree->current->next )
	    {
	      tree->current = tree->current->next;
	      tree->system->callbacks[tree->current->type].redo( tree->tree_data, tree->current->data, primary );
	    }
	  else
	    tree->system->callbacks[0].error( tree->tree_data, _( "No Redo information left." ), primary );
	}
      else
	{
	  if( tree->data )
	    {
	      tree->current = tree->data;
	      tree->system->callbacks[tree->current->type].redo( tree->tree_data, tree->current->data, primary );
	    }
	  else
	    tree->system->callbacks[0].error( tree->tree_data, _( "No Redo information." ), primary );
	}
      tree->appending = 0;
      if ( primary )
	{
	  g_list_foreach( tree->other_trees, redo_latest_callback, NULL );
	}
    }
}
