/* undo_if.c - Contains functions for interfacing go with the undo
 *             system.
 *
 * Copyright (C) 1998 Chris Lahey.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.  */

#include <gtk/gtk.h>
#include "undo_if.h"
#include "gtkrichtext.h"
#include "file.h"
#include "window.h"

typedef struct
{
  CharacterParams params;
  gchar *text;
  gint length;
} text_data;

typedef struct
{
  ParagraphParams params;
  GList *old_data; /* Of type text_data. */
} par_data;

typedef struct
{
  gint start;
  gint end;
  GList *old_data;
} delete_data;


typedef struct
{
  CharacterParams params;
  gint length;
} old_font_data;

typedef struct
{
  GList *old_data; /* Of type old_font_data */
  CharacterParams params; /* The new font essentially. */
  CharacterParamsValid validity;
  gint start;
  gint end;
} font_change;


typedef struct
{
  ParagraphParams params;
  gint length;
} old_par_data;

typedef struct
{
  GList *old_data; /* Of type old_par_data */
  ParagraphParams params; /* The new data. */
  ParagraphParamsValid validity;
  gint start;
  gint end;
} par_change;

typedef struct
{
  GoProperties *old_props;
  GoProperties *new_props;
} props_change;


static void delete_redo( GOWindow *window, delete_data *data, gboolean primary_tree );
static void delete_undo( GOWindow *window, delete_data *data, gboolean primary_tree );
static delete_data *delete_create( GOWindow *window, delete_param *data_param, gboolean primary_tree, gint *appending, gint prev_type, delete_data *prev_data );
static void delete_destroy( GOWindow *window, delete_data *data );
static void delete_error( GOWindow *window, gchar *message, gboolean primary_tree );

static void insert_redo( GOWindow *window, insert_param *data, gboolean primary_tree );
static void insert_undo( GOWindow *window, insert_param *data, gboolean primary_tree );
static insert_param *insert_create( GOWindow *window, insert_param *data_param, gboolean primary_tree, gint *appending, gint prev_type, insert_param *prev_data );
static void insert_destroy( GOWindow *window, insert_param *data );

static void font_redo( GOWindow *window, font_change *data, gboolean primary_tree );
static void font_undo( GOWindow *window, font_change *data, gboolean primary_tree );
static font_change *font_create( GOWindow *window, font_change_param *data_param, gboolean primary_tree, gint *appending, gint prev_type, gpointer prev_data );
static void font_destroy( GOWindow *window, font_change *data );

static void par_redo( GOWindow *window, par_change *data, gboolean primary_tree );
static void par_undo( GOWindow *window, par_change *data, gboolean primary_tree );
static par_change *par_create( GOWindow *window, par_change_param *data_param, gboolean primary_tree, gint *appending, gint prev_type, gpointer prev_data );
static void par_destroy( GOWindow *window, par_change *data );

static void props_redo( GOWindow *window, props_change *data, gboolean primary_tree );
static void props_undo( GOWindow *window, props_change *data, gboolean primary_tree );
static props_change *props_create( GOWindow *window, props_change_param *data_param, gboolean primary_tree, gint *appending, gint prev_type, gpointer prev_data );
static void props_destroy( GOWindow *window, props_change *data );

UndoSystem *undo_system;

void
go_init_undo()
{
  UndoCallbacks callbacks[ UNDO_TYPE_LAST ];
  callbacks[ UNDO_TYPE_DELETE ].redo = (UndoRedoFunc) delete_redo;
  callbacks[ UNDO_TYPE_DELETE ].undo = (UndoUndoFunc) delete_undo;
  callbacks[ UNDO_TYPE_DELETE ].create = (UndoCreateFunc) delete_create;
  callbacks[ UNDO_TYPE_DELETE ].destroy = (UndoDestroyFunc) delete_destroy;
  callbacks[ UNDO_TYPE_DELETE ].error = (UndoErrorFunc) delete_error;

  callbacks[ UNDO_TYPE_INSERT ].redo = (UndoRedoFunc) insert_redo;
  callbacks[ UNDO_TYPE_INSERT ].undo = (UndoUndoFunc) insert_undo;
  callbacks[ UNDO_TYPE_INSERT ].create = (UndoCreateFunc) insert_create;
  callbacks[ UNDO_TYPE_INSERT ].destroy = (UndoDestroyFunc) insert_destroy;
  callbacks[ UNDO_TYPE_INSERT ].error = NULL;

  callbacks[ UNDO_TYPE_FONT ].redo = (UndoRedoFunc) font_redo;
  callbacks[ UNDO_TYPE_FONT ].undo = (UndoUndoFunc) font_undo;
  callbacks[ UNDO_TYPE_FONT ].create = (UndoCreateFunc) font_create;
  callbacks[ UNDO_TYPE_FONT ].destroy = (UndoDestroyFunc) font_destroy;
  callbacks[ UNDO_TYPE_FONT ].error = NULL;

  callbacks[ UNDO_TYPE_PARAGRAPH_FORMAT ].redo = (UndoRedoFunc) par_redo;
  callbacks[ UNDO_TYPE_PARAGRAPH_FORMAT ].undo = (UndoUndoFunc) par_undo;
  callbacks[ UNDO_TYPE_PARAGRAPH_FORMAT ].create = (UndoCreateFunc) par_create;
  callbacks[ UNDO_TYPE_PARAGRAPH_FORMAT ].destroy = (UndoDestroyFunc) par_destroy;
  callbacks[ UNDO_TYPE_PARAGRAPH_FORMAT ].error = NULL;

  callbacks[ UNDO_TYPE_PROPERTIES ].redo = (UndoRedoFunc) props_redo;
  callbacks[ UNDO_TYPE_PROPERTIES ].undo = (UndoUndoFunc) props_undo;
  callbacks[ UNDO_TYPE_PROPERTIES ].create = (UndoCreateFunc) props_create;
  callbacks[ UNDO_TYPE_PROPERTIES ].destroy = (UndoDestroyFunc) props_destroy;
  callbacks[ UNDO_TYPE_PROPERTIES ].error = NULL;

  undo_system = undo_init( callbacks, UNDO_TYPE_LAST );
}


static void
reset_auto_save_system( GOWindow *window, gboolean primary )
{
  if(primary)
    {
      window->changed = TRUE;
	      
      if ( ! window->long_term_auto_save )
	{
	  window->long_term_auto_save
	    = gtk_timeout_add( 60000, long_auto_save_callback, (gpointer) window );
	}
      if ( window->auto_save )
	{
	  gtk_timeout_remove( window->auto_save );
	}
	      
      window->auto_save = gtk_timeout_add( 1000, auto_save_callback,
					   (gpointer) window );
    }
  else
    {
      if ( window->auto_save )
	gtk_timeout_remove( window->auto_save );
      if ( window->long_term_auto_save )
	gtk_timeout_remove( window->long_term_auto_save );
      window->auto_save = window->long_term_auto_save = 0;
      window->changed = FALSE;
    }
}

static void
sometimes_freeze( GOWindow *window, gboolean primary_tree )
{
  if ( ! primary_tree )
    {
      gtk_rich_text_freeze( GTK_RICH_TEXT( window->textbox ) );
    }
}

static void
sometimes_thaw( GOWindow *window, gboolean primary_tree )
{
  if ( ! primary_tree )
    {
      gtk_rich_text_thaw( GTK_RICH_TEXT( window->textbox ) );
    }
}



typedef struct
{
  GOWindow *window;
  gint first_par;
  gint position;
} insert_paragraph_data;

static void
insert_text( text_data *text_dat, insert_paragraph_data *data )
{
  GOWindow *window = data->window;
  GtkRichText *text = GTK_RICH_TEXT( window->textbox );
  gchar *chars = text_dat->text;
  gint length = text_dat->length;

  gtk_rich_text_insert( text, text_dat->params.font, NULL, NULL, chars, length );
  data->position += length;
}

static void
insert_paragraph( par_data *paragraph, insert_paragraph_data *data )
{
  GOWindow *window = data->window;
  GtkRichText *text = GTK_RICH_TEXT( window->textbox );
  ParagraphParamsValid validity = paragraph_params_all_valid;
  
  if ( data->first_par == 0 )
    {
      gtk_rich_text_insert( text, NULL, NULL, NULL, "\n", 1 );
      data->position += 1;
    }

  gtk_rich_text_set_location_paragraph_params( text, data->position, &(paragraph->params), &(validity) );

  g_list_foreach( paragraph->old_data, (GFunc) insert_text, data );
  
  data->first_par = 0;
}

typedef struct
{
  delete_data *data;
  par_data *current_paragraph;
} delete_create_temp;

static void
delete_create_return_text( gchar *chars, gint length, CharacterParams *params, gpointer data )
{
  delete_create_temp *temp_data = data;
  text_data *new_text = g_new( text_data, 1 );

  new_text->text = g_malloc( length + 1 );
  strncpy( new_text->text, chars, length );
  new_text->text[ length ] = 0;
  new_text->length = length;
  new_text->params = *params;
  gtk_rich_text_font_ref( new_text->params.font );  

  temp_data->current_paragraph->old_data = g_list_append( temp_data->current_paragraph->old_data, new_text );
}

static void
delete_create_start_paragraph( ParagraphParams *params, gpointer data )
{
  delete_create_temp *temp_data = data;
  par_data *new_par = g_new( par_data, 1 );
  
  new_par->params = *params;
  new_par->old_data = NULL;
  
  temp_data->data->old_data = g_list_append( temp_data->data->old_data, new_par );
  temp_data->current_paragraph = new_par;
}

static void
delete_delete_text( text_data *text, gpointer user_data )
{
  g_free( text->text );
  gtk_rich_text_font_unref( text->params.font );
  g_free( text );
}

static void
delete_delete_paragraph( par_data *par, gpointer user_data )
{
  g_list_foreach( par->old_data, (GFunc) delete_delete_text, NULL );
  g_free( par );
}

static void
delete_redo( GOWindow *window, delete_data *data, gboolean primary_tree )
{
  sometimes_freeze( window, primary_tree );
  
  gtk_rich_text_set_point( GTK_RICH_TEXT( window->textbox ), data->start ); 
  gtk_rich_text_forward_delete( GTK_RICH_TEXT( window->textbox ), data->end - data->start );
  
  reset_auto_save_system( window, primary_tree );
  sometimes_thaw( window, primary_tree );
}

static void
delete_undo( GOWindow *window, delete_data *data, gboolean primary_tree )
{
  insert_paragraph_data temp_data;
  
  sometimes_freeze( window, primary_tree );

  gtk_rich_text_set_point( GTK_RICH_TEXT( window->textbox ), data->start );
  
  temp_data.window = window;
  temp_data.first_par = 1;
  temp_data.position = data->start;
  g_list_foreach( data->old_data, (GFunc) insert_paragraph, &temp_data );
  
  reset_auto_save_system( window, primary_tree );
  sometimes_thaw( window, primary_tree );
}

static delete_data *
delete_create( GOWindow *window, delete_param *data_param, gboolean primary_tree, gint *appending, gint prev_type, delete_data *prev_data )
{
  delete_data *data = g_new( delete_data, 1 );
  GtkRichTextTraversalCallbacks callbacks;
  delete_create_temp temp_data;

  callbacks.return_text = delete_create_return_text;
  callbacks.start_paragraph = delete_create_start_paragraph;
  
  data->start = data_param->start;
  data->end = data_param->end;
  data->old_data = NULL;

  temp_data.data = data;
  temp_data.current_paragraph = NULL;

  gtk_rich_text_traverse_partial( GTK_RICH_TEXT( window->textbox ), data->start, data->end, &callbacks, &temp_data );
  
  if ( ! primary_tree )
    delete_redo( window, data, primary_tree );
  
  if ( *appending &&
       prev_data &&
       prev_type == UNDO_TYPE_DELETE && 
       ( prev_data->start == data_param->start ||
	 prev_data->start == data_param->end )
       )
    *appending = 1;
  else
    *appending = 0;

  reset_auto_save_system( window, primary_tree );

  return data;
}

static void
delete_destroy( GOWindow *window, delete_data *data )
{
  g_list_foreach( data->old_data, (GFunc) delete_delete_paragraph, NULL );
  g_free( data );
}

static void
delete_error( GOWindow *window, gchar *message, gboolean primary_tree )
{
  gtk_statusbar_push( GTK_STATUSBAR( window->status ), window->contextid, message );
}


static void
insert_redo( GOWindow *window, insert_param *data, gboolean primary_tree )
{
  gint length = data->length;
  sometimes_freeze( window, primary_tree );
  gtk_rich_text_set_point( GTK_RICH_TEXT( window->textbox ), data->location );
  gtk_rich_text_insert( GTK_RICH_TEXT( window->textbox ), NULL, NULL, NULL, data->chars, length );
  reset_auto_save_system( window, primary_tree );
  sometimes_thaw( window, primary_tree );
}

static void
insert_undo( GOWindow *window, insert_param *data, gboolean primary_tree )
{
  gint length = data->length;
  sometimes_freeze( window, primary_tree );
  gtk_rich_text_set_point( GTK_RICH_TEXT( window->textbox ), data->location );
  gtk_rich_text_forward_delete( GTK_RICH_TEXT( window->textbox ), length );
  reset_auto_save_system( window, primary_tree );
  sometimes_thaw( window, primary_tree );
}

static insert_param *
insert_create( GOWindow *window, insert_param *data_param, gboolean primary_tree, gint *appending, gint prev_type, insert_param *prev_data )
{
  gint length = data_param->length;
  gchar *temp = g_malloc( length + 1 );
  insert_param *data = g_new( insert_param, 1 );

  *data = *data_param;

  strncpy( temp, data->chars, length );
  temp[ length ] = 0;
  data->chars = temp;

  /* If this is not the primary window, insert the given characters. */
  if ( ! primary_tree )
    insert_redo( window, data_param, primary_tree );

  /* Set whether to append this record or not. */
  if ( prev_type == UNDO_TYPE_INSERT &&
       *appending &&
       prev_data &&
       prev_data->location + prev_data->length == data_param->location )
    *appending = 1;
  else
    *appending = 0;

  reset_auto_save_system( window, primary_tree );
  return data;
}

static void
insert_destroy( GOWindow *window, insert_param *data )
{
  g_free( data->chars );
  g_free( data );
}



typedef struct
{
  font_change *data;
  gboolean first_paragraph;
} font_create_temp;

static void
font_create_return_text( gchar *chars, gint length, CharacterParams *params, gpointer data )
{
  font_create_temp *temp_data = data;
  old_font_data *old_data = g_new( old_font_data, 1 );
  old_data->params = *params;
  gtk_rich_text_font_ref( old_data->params.font );
  old_data->length = length;

  temp_data->data->old_data = g_list_append( temp_data->data->old_data, old_data );
}

static void
font_create_start_paragraph( ParagraphParams *params, gpointer data )
{
  font_create_temp *temp_data = data;
  if ( ! temp_data->first_paragraph )
    {
      old_font_data *old_data = g_new( old_font_data, 1 );
      old_data->params.font = gtk_rich_text_font_new( "fixed" );
      gtk_rich_text_font_ref( old_data->params.font );
      old_data->length = 1;
      
      temp_data->data->old_data = g_list_append( temp_data->data->old_data, old_data );
      temp_data->first_paragraph = FALSE;
    }
}
static void
set_font( old_font_data *font_dat, insert_paragraph_data *data )
{
  GOWindow *window = data->window;
  GtkRichText *text = GTK_RICH_TEXT( window->textbox );
  CharacterParamsValid validity = character_params_all_valid;

  gtk_rich_text_set_range_character_params( text,
					    data->position,
					    data->position + font_dat->length,
					    &(font_dat->params),
					    &validity );
  data->position += font_dat->length;
}

static void
font_redo( GOWindow *window, font_change *data, gboolean primary_tree )
{
  sometimes_freeze( window, primary_tree );
  gtk_rich_text_set_range_character_params( GTK_RICH_TEXT( window->textbox ),
					    data->start,
					    data->end,
					    &(data->params),
					    &(data->validity) );
  reset_auto_save_system( window, primary_tree );
  sometimes_thaw( window, primary_tree );
}

static void
font_undo( GOWindow *window, font_change *data, gboolean primary_tree )
{
  insert_paragraph_data temp_data;
  
  sometimes_freeze( window, primary_tree );
  
  temp_data.position = data->start;
  temp_data.window = window;
  g_list_foreach( data->old_data, (GFunc) set_font, &temp_data );
  
  reset_auto_save_system( window, primary_tree );
  sometimes_thaw( window, primary_tree );
}

static font_change *
font_create( GOWindow *window, font_change_param *data_param, gboolean primary_tree, gint *appending, gint prev_type, gpointer prev_data )
{
  font_change *data = g_new( font_change, 1 );
  GtkRichTextTraversalCallbacks callbacks;
  font_create_temp temp_data;

  callbacks.return_text = font_create_return_text;
  callbacks.start_paragraph = font_create_start_paragraph;
  
  data->params = data_param->params;
  data->validity = data_param->validity;
  data->start = data_param->start;
  data->end = data_param->end;
  data->old_data = NULL;

  temp_data.data = data;
  temp_data.first_paragraph = FALSE;
  
  gtk_rich_text_traverse_partial( GTK_RICH_TEXT( window->textbox ), data->start, data->end, &callbacks, &temp_data );

  if ( ! primary_tree )
    font_redo( window, data, primary_tree );
  
  *appending = 0;

  reset_auto_save_system( window, primary_tree );
  return data;
}

static void
font_data_unref(gpointer data, gpointer extra_data)
{
  old_font_data *old_data = data;
  gtk_rich_text_font_unref( old_data->params.font );
}

static void
font_destroy( GOWindow *window, font_change *data )
{
  g_list_foreach( data->old_data, font_data_unref, NULL );
  g_list_foreach( data->old_data, (GFunc) g_free, NULL );
  g_list_free( data->old_data );
  g_free( data );
}


typedef struct
{
  par_change *data;
  old_par_data *current_paragraph;
} par_create_temp;

static void
par_create_return_text( gchar *chars, gint length, CharacterParams *params, gpointer data )
{
  par_create_temp *temp_data = data;
  temp_data->current_paragraph->length += length;
}

static void
par_create_start_paragraph( ParagraphParams *params, gpointer data )
{
  par_create_temp *temp_data = data;
  old_par_data *old_data = g_new( old_par_data, 1 );
  old_data->params = *params;
  old_data->length = 0;

  if ( temp_data->current_paragraph )
    temp_data->current_paragraph->length ++;

  temp_data->data->old_data = g_list_append( temp_data->data->old_data, old_data );
  temp_data->current_paragraph = old_data;
}

static void
set_par_params( old_par_data *par_dat, insert_paragraph_data *data )
{
  GOWindow *window = data->window;
  GtkRichText *text = GTK_RICH_TEXT( window->textbox );
  ParagraphParamsValid validity = paragraph_params_all_valid;

  gtk_rich_text_set_location_paragraph_params( text,
					       data->position,
					       &(par_dat->params),
					       &validity );
  data->position += par_dat->length;
}

static void
par_redo( GOWindow *window, par_change *data, gboolean primary_tree )
{
  sometimes_freeze( window, primary_tree );
  gtk_rich_text_set_range_paragraph_params( GTK_RICH_TEXT( window->textbox ),
					    data->start,
					    data->end,
					    &(data->params),
					    &(data->validity) );
  reset_auto_save_system( window, primary_tree );
  sometimes_thaw( window, primary_tree );
}

static void
par_undo( GOWindow *window, par_change *data, gboolean primary_tree )
{
  insert_paragraph_data temp_data;
  
  sometimes_freeze( window, primary_tree );
  
  temp_data.position = data->start;
  temp_data.window = window;
  g_list_foreach( data->old_data, (GFunc) set_par_params, &temp_data );
  
  reset_auto_save_system( window, primary_tree );
  sometimes_thaw( window, primary_tree );
}

static par_change *
par_create( GOWindow *window, par_change_param *data_param, gboolean primary_tree, gint *appending, gint prev_type, gpointer prev_data )
{
  par_change *data = g_new( par_change, 1 );
  GtkRichTextTraversalCallbacks callbacks;
  par_create_temp temp_data;

  callbacks.return_text = par_create_return_text;
  callbacks.start_paragraph = par_create_start_paragraph;
  
  data->params = data_param->params;
  data->validity = data_param->validity;
  data->start = data_param->start;
  data->end = data_param->end;
  data->old_data = NULL;

  temp_data.data = data;
  temp_data.current_paragraph = NULL;
  
  gtk_rich_text_traverse_partial( GTK_RICH_TEXT( window->textbox ), data->start, data->end, &callbacks, &temp_data );
  
  if ( ! primary_tree )
    par_redo( window, data, primary_tree );
  
  *appending = 0;

  reset_auto_save_system( window, primary_tree );
  return data;
}


static void
par_destroy( GOWindow *window, par_change *data )
{
  g_list_foreach( data->old_data, (GFunc) g_free, NULL );
  g_list_free( data->old_data );
  g_free( data );
}

static void
props_redo( GOWindow *window, props_change *data, gboolean primary_tree )
{
  go_properties_destroy( window->props );
  window->props = go_properties_clone( data->new_props );
  
  reset_auto_save_system( window, primary_tree );
}

static void
props_undo( GOWindow *window, props_change *data, gboolean primary_tree )
{
  go_properties_destroy( window->props );
  window->props = go_properties_clone( data->old_props );
  
  reset_auto_save_system( window, primary_tree );
}

static props_change *
props_create( GOWindow *window, props_change_param *data_param, gboolean primary_tree, gint *appending, gint prev_type, gpointer prev_data )
{
  props_change *data = g_new( props_change, 1 );
  
  data->old_props = go_properties_clone( window->props );
  data->new_props = go_properties_clone( data_param->new_props );
  
  props_redo( window, data, primary_tree );
  
  *appending = 0;

  reset_auto_save_system( window, primary_tree );
  return data;
}

static void
props_destroy( GOWindow *window, props_change *data )
{
  go_properties_destroy( data->old_props );
  go_properties_destroy( data->new_props );
  g_free( data );
}

void undo_callback( GtkWidget *widget, gpointer data )
{
  GOWindow *window = data;
  undo_latest( window->undo, TRUE );
}

void redo_callback( GtkWidget *widget, gpointer data )
{
  GOWindow *window = data;
  redo_latest( window->undo, TRUE );
}
