/* undo_if.h - Header file containing structures for interfacing go
 *             with the undo system.
 *
 * Copyright (C) 1998 Chris Lahey.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.  */

#ifndef __UNDO_IF_H__
#define __UNDO_IF_H__

#include "window.h"
#include "gtkrichtext.h"
#include "undo.h"

extern UndoSystem *undo_system;

#define UNDO_TYPE_DELETE 0
#define UNDO_TYPE_INSERT 1
#define UNDO_TYPE_FONT 2
#define UNDO_TYPE_PARAGRAPH_FORMAT 3
#define UNDO_TYPE_PROPERTIES 4
#define UNDO_TYPE_LAST 5

typedef struct
{
  gint start;
  gint end;
} delete_param;

typedef struct
{
  gint location;
  gchar *chars;
  gint length;
} insert_param;

typedef struct
{
  CharacterParams params; /* The new font essentially. */
  CharacterParamsValid validity;
  gint start;
  gint end;
} font_change_param;

typedef struct
{
  ParagraphParams params; /* The new data. */
  ParagraphParamsValid validity;
  gint start;
  gint end;
} par_change_param;

typedef struct
{
  GoProperties *new_props;
} props_change_param;

void undo_callback( GtkWidget *widget, gpointer data );
void redo_callback( GtkWidget *widget, gpointer data );
void go_init_undo( void );

#endif /* __UNDO_IF_H__ */
