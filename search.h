/* main.h - Touch it to force a recompile.
 *
 * Copyright (C) 1998 Chris Lahey.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __SEARCH_H__
#define __SEARCH_H__

#include "window.h"

typedef struct search_dialog
{
  GtkWidget *dialog;
  GtkWidget *entry;
  GtkWidget *case_sens;
  GOWindow *window;
} search_dialog;

typedef struct regexp_dialog
{
  GtkWidget *dialog;
  GtkWidget *entry;
  GtkWidget *case_sens;
  GOWindow *window;
} regexp_dialog;

typedef struct search_replace_dialog
{
  GtkWidget *dialog;
  GtkWidget *entry;
  GtkWidget *new_entry;
  GtkWidget *case_sens;
  GOWindow *window;
} search_replace_dialog;

typedef struct regexp_replace_dialog
{
  GtkWidget *dialog;
  GtkWidget *entry;
  GtkWidget *new_entry;
  GtkWidget *case_sens;
  GOWindow *window;
} regexp_replace_dialog;

void search_search_callback( GtkWidget *widget, gpointer data );
void search_regexp_callback( GtkWidget *widget, gpointer data );
void search_search_replace_callback( GtkWidget *widget, gpointer data );
void search_regexp_replace_callback( GtkWidget *widget, gpointer data );


#endif /* __SEARCH_H__ */
