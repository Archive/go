/* ruler.c:  Creates the ruler for the top of go.
 *
 * Copyright (C) 1999 Chris Lahey.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

/* #define AA_RULER 1 */

#include "window.h"
#include "gtkrichtext.h"

static GdkImlibImage *
load_image( gchar *title )
{
#if AA_RULER
  return gnome_canvas_load_alpha( title );
#else
  return gdk_imlib_load_image( title );
#endif
}

static gint
first_pos( GtkRichText *text )
{
  Position first = text->cursor_first ? text->cursor : text->selection_end;
  return gtk_rich_text_position_to_int( text, first );
}

static gint
second_pos( GtkRichText *text )
{
  Position second = text->cursor_first ? text->selection_end : text->cursor;
  return gtk_rich_text_position_to_int( text, second );
}

static void
set_indentation ( GoRuler *ruler )
{
  GOWindow *window = ruler->window;
  ParagraphParams params;
  ParagraphParamsValid validity = paragraph_params_none_valid;
  validity.first_indent_valid = TRUE;
  validity.left_indent_valid = TRUE;
  validity.right_indent_valid = TRUE;

  params.first_indent = ruler->first_indent_val - ruler->left_indent_val;
  params.left_indent = ruler->left_indent_val;
  params.right_indent = ruler->right_indent_val;
  
  gtk_rich_text_set_selection_paragraph_params ( GTK_RICH_TEXT( window->textbox ),
						 &params,
						 &validity );
}

static void
add_marks( GoRuler *ruler, gint end )
{
  gint i;
  GnomeCanvasPoints *points;
  GnomeCanvasItem *item;
  for ( i = (ruler->ruler_extent / 9) ; i < (end / 9); i++ )
    {
      if ( i == 0 )
	continue;
      if ( i % 8 == 0 )
	{
	  gchar *text = g_strdup_printf( "%d", i / 8 );
	  item = gnome_canvas_item_new (gnome_canvas_root (GNOME_CANVAS (ruler->canvas) ),
					gnome_canvas_text_get_type (),
					"text", text,
					"x", (double) i * 9,
					"y", (double) 7.5,
					"anchor", GTK_ANCHOR_CENTER,
					NULL);
	  gnome_canvas_item_lower_to_bottom( item );
	  g_free( text );
	  continue;
	}
      if ( i % 4 == 0 )
	{
	  points = gnome_canvas_points_new( 2 );
	  points->coords[0] = i * 9;
	  points->coords[2] = i * 9;
	  points->coords[1] = 4;
	  points->coords[3] = 11;
	  item = gnome_canvas_item_new (gnome_canvas_root (GNOME_CANVAS (ruler->canvas) ),
					gnome_canvas_line_get_type (),
					"points", points,
					"width_pixels", 1,
					NULL);
	  gnome_canvas_item_lower_to_bottom( item );
	  gnome_canvas_points_unref( points );
	  continue;
	}
      points = gnome_canvas_points_new( 2 );
      points->coords[0] = i * 9;
      points->coords[2] = i * 9;
      points->coords[1] = 6;
      points->coords[3] = 9;      
      item = gnome_canvas_item_new (gnome_canvas_root (GNOME_CANVAS (ruler->canvas) ),
				    gnome_canvas_line_get_type (),
				    "points", points,
				    "width_pixels", 1,
				    NULL);
      gnome_canvas_item_lower_to_bottom( item );
      gnome_canvas_points_unref( points );
    }
  ruler->ruler_extent = end;
}

static gint
ruler_motion_timeout ( gpointer data )
{
  GoRuler *ruler = (GoRuler *) data;
  set_indentation( ruler );
  ruler->motion_callback = 0;
  return FALSE;
}

static gint
ruler_item_event (GnomeCanvasItem *item, GdkEvent *event, gpointer data)
{
  static int dragging;
  static gdouble x;
  double item_x, item_y;
  gdouble new_x;
  GoRuler *ruler = (GoRuler *) data;
  gboolean is_indentation = FALSE;

  /* set item_[xy] to the event x,y position in the parent's item-relative coordinates */
  item_x = event->button.x;
  item_y = event->button.y;
  gnome_canvas_item_w2i (item->parent, &item_x, &item_y);

  switch (event->type) {
  case GDK_BUTTON_PRESS:
    switch (event->button.button) {
    case 1:
      x = item_x;
      gnome_canvas_item_grab (item,
			      GDK_POINTER_MOTION_MASK | GDK_BUTTON_RELEASE_MASK,
			      NULL,
			      event->button.time);
      dragging = TRUE;
      break;

    case 2:
      break;

    case 3:
      break;

    default:
      break;
    }

    break;

  case GDK_MOTION_NOTIFY:
    if (dragging && (event->motion.state & GDK_BUTTON1_MASK))
      {
	new_x = item_x;
	
	if ( item == ruler->right_indent )
	  {
	    /* The logic here for setting right_indent_val is backwards
	       since right_indent_val is the distance from the right
	       edge. */
#if 0
	    if ( (int)(ruler->right_indent_val + new_x - x) % 9 == 1 )
	      {
		new_x ++;
	      }
	    if ( (int)(ruler->right_indent_val + new_x - x) % 9 == 8 )
	      {
		new_x --;
	      }
#endif
	    new_x += ((int)(ruler->right_indent_val - (new_x - x) + 4 ) % 9 ) - 4;
	    if ( ruler->right_indent_val - (new_x - x) < 0 )
	      {
		new_x = ruler->right_indent_val + x;
	      }
	    if ( new_x - x != 0 )
	      {
		is_indentation = TRUE;
		ruler->right_indent_val -= new_x - x;
		gnome_canvas_item_set( ruler->right_indent,
				       "x", (gdouble) ( GTK_WIDGET( ruler->canvas )->allocation.width - ruler->right_indent_val ),
				       NULL);
		gnome_canvas_item_request_update( ruler->right_indent );  
	      }
	  }
	if ( item == ruler->left_indent )
	  {
#if 0
	    if ( (int)(ruler->left_indent_val + new_x - x) % 9 == 1 )
	      {
		new_x --;
	      }
	    if ( (int)(ruler->left_indent_val + new_x - x) % 9 == 8 )
	      {
		new_x ++;
	      }
#endif
	    new_x -= ((int)(ruler->left_indent_val + new_x - x + 4 ) % 9 ) - 4;
	    if ( ruler->left_indent_val + new_x - x < 0 )
	      {
		new_x = x - ruler->left_indent_val;
	      }
	    if ( new_x - x != 0 )
	      {
		is_indentation = TRUE;
		ruler->left_indent_val += new_x - x;
		gnome_canvas_item_set( ruler->left_indent,
				       "x", (gdouble) ( ruler->left_indent_val ),
				       NULL);
		gnome_canvas_item_request_update( ruler->left_indent );
	      
		gnome_canvas_item_set( ruler->first_and_left_indent,
				       "x", (gdouble) ( ruler->left_indent_val ),
				       NULL);
		gnome_canvas_item_request_update( ruler->first_and_left_indent );
	      }
	  }
	if ( item == ruler->first_indent )
	  {
#if 0
	    if ( (int)(ruler->first_indent_val + new_x - x) % 9 == 1 )
	      {
		new_x --;
	      }
	    if ( (int)(ruler->first_indent_val + new_x - x) % 9 == 8 )
	      {
		new_x ++;
	      }
#endif
	    new_x -= ((int)(ruler->first_indent_val + new_x - x + 4) % 9 ) - 4;
	    if ( ruler->first_indent_val + new_x - x < 0 )
	      {
		new_x = x - ruler->left_indent_val;
	      }
	    if ( new_x - x != 0 )
	      {
		is_indentation = TRUE;
		ruler->first_indent_val += new_x - x;
		gnome_canvas_item_set( ruler->first_indent,
				       "x", (gdouble) ( ruler->first_indent_val ),
				       NULL);
		gnome_canvas_item_request_update( ruler->first_indent );
	      }
	  }
	if ( item == ruler->first_and_left_indent )
	  {
#if 0
	    if ( (int)(ruler->left_indent_val + new_x - x) % 9 == 1 )
	      {
		new_x --;
	      }
	    if ( (int)(ruler->left_indent_val + new_x - x) % 9 == 8 )
	      {
		new_x ++;
	      }
#endif
	    new_x -= ((int)(ruler->left_indent_val + new_x - x + 4) % 9 ) - 4;
	    if ( ruler->left_indent_val + new_x - x < 0 )
	      {
		new_x = x - ruler->left_indent_val;
	      }
	    if ( new_x - x != 0 )
	      {
		is_indentation = TRUE;
		ruler->first_indent_val += new_x - x;
		ruler->left_indent_val += new_x - x;
		gnome_canvas_item_set( ruler->first_indent,
				       "x", (gdouble) ( ruler->first_indent_val ),
				       NULL);
		gnome_canvas_item_request_update( ruler->first_indent );
		
		gnome_canvas_item_set( ruler->left_indent,
				       "x", (gdouble) ( ruler->left_indent_val ),
				       NULL);
		gnome_canvas_item_request_update( ruler->left_indent );
		
		gnome_canvas_item_set( ruler->first_and_left_indent,
				       "x", (gdouble) ( ruler->left_indent_val ),
				       NULL);
		gnome_canvas_item_request_update( ruler->first_and_left_indent );
	      }
	  }
	x = new_x;
	
	if ( is_indentation )
	  {
	    if ( ruler->motion_callback == 0 )
	      {
		ruler->motion_callback = gtk_idle_add( ruler_motion_timeout,
						       (gpointer) ruler );
	      }
	  }
      }
    break;
    
  case GDK_BUTTON_RELEASE:
    {
      GOWindow *window = ruler->window;
      ParagraphParams params;
      ParagraphParamsValid validity = paragraph_params_none_valid;
      validity.first_indent_valid = TRUE;
      validity.left_indent_valid = TRUE;
      validity.right_indent_valid = TRUE;
      
      params.first_indent = ruler->first_indent_val - ruler->left_indent_val;
      params.left_indent = ruler->left_indent_val;
      params.right_indent = ruler->right_indent_val;
      set_par_params_callback( window, first_pos( GTK_RICH_TEXT( window->textbox ) ), second_pos( GTK_RICH_TEXT( window->textbox ) ), &params, &validity );
    }
    gnome_canvas_item_ungrab (item, event->button.time);
    dragging = FALSE;
    break;

  default:
    break;
  }

  return FALSE;
}

static void
setup_item (GnomeCanvasItem *item, gpointer data)
{
	gtk_signal_connect (GTK_OBJECT (item), "event",
			    (GtkSignalFunc) ruler_item_event,
			    data);
}

static void
ruler_configure_event( GtkWidget *widget,
		       GtkAllocation *allocation,
		       gpointer data )
{
  GnomeCanvas *canvas = GNOME_CANVAS( widget );
  GoRuler *ruler = (GoRuler *) data;
  gnome_canvas_item_set( ruler->right_indent,
			 "x", (gdouble) ( allocation->width - ruler->right_indent_val ),
			 NULL);
  gnome_canvas_item_request_update( ruler->right_indent );
  gnome_canvas_set_scroll_region( ruler->canvas, 0, 0,
				  allocation->width, allocation->height );
  if ( ((gint)allocation->width + 36 ) > ruler->ruler_extent )
    {
      add_marks( ruler, allocation->width + 36);
    }
}

static void
ruler_destroy( GtkWidget *widget, gpointer data )
{
  GnomeCanvas *canvas = GNOME_CANVAS( widget );
  GoRuler *ruler = (GoRuler *) data;
  g_free( ruler );
}

static void
place_indents( GoRuler *ruler )
{
  gnome_canvas_item_set( ruler->first_indent,
			 "x", (gdouble) ( ruler->first_indent_val ),
			 NULL);
  gnome_canvas_item_request_update( ruler->first_indent );
  
  gnome_canvas_item_set( ruler->left_indent,
			 "x", (gdouble) ( ruler->left_indent_val ),
			 NULL);
  gnome_canvas_item_request_update( ruler->left_indent );
  
  gnome_canvas_item_set( ruler->first_and_left_indent,
			 "x", (gdouble) ( ruler->left_indent_val ),
			 NULL);
  gnome_canvas_item_request_update( ruler->first_and_left_indent );
  
  gnome_canvas_item_set( ruler->right_indent,
			 "x", (gdouble) ( GTK_WIDGET( ruler->canvas )->allocation.width - ruler->right_indent_val ),
			 NULL);
  gnome_canvas_item_request_update( ruler->right_indent );  
}

static void
update_selection( GtkWidget *widget, Position *start, Position *end, gpointer data )
{
  GoRuler *ruler = (GoRuler *) data;
  GOWindow *window = ruler->window;
  ParagraphParams params;
  ParagraphParamsValid validity;
  gtk_rich_text_get_selection_paragraph_params ( GTK_RICH_TEXT( window->textbox ),
						 &params,
						 &validity );

  ruler->first_indent_val = params.first_indent + params.left_indent;
  ruler->left_indent_val = params.left_indent;
  ruler->right_indent_val = params.right_indent;
  place_indents( ruler );
}


static void
ruler_realize( GtkWidget *widget, GoRuler *ruler )
{
  GtkStyle *style;
  style = gtk_style_copy (gtk_widget_get_style (widget));
  style->bg[GTK_STATE_NORMAL] = style->white;
  gtk_widget_set_style (widget, style);
  
  gdk_window_set_background (widget->window,
			     &widget->style->bg[GTK_STATE_NORMAL]);
}

GoRuler *
create_ruler( GOWindow *window )
{
  GoRuler *ruler;
  GdkImlibImage *image;

  ruler = g_new( GoRuler, 1 );

  ruler->window = window;
  
#if AA_RULER
  gtk_widget_push_visual( gdk_rgb_get_visual() );
  gtk_widget_push_colormap( gdk_rgb_get_cmap() );
  ruler->canvas = GNOME_CANVAS( gnome_canvas_new_aa() );
  gtk_widget_pop_visual();
  gtk_widget_pop_colormap();
#else
  gtk_widget_push_visual( gdk_imlib_get_visual() );
  gtk_widget_push_colormap( gdk_imlib_get_colormap() );
  ruler->canvas = GNOME_CANVAS( gnome_canvas_new() );
  gtk_widget_pop_visual();
  gtk_widget_pop_colormap();
#endif
  /*  
  void                gdk_imlib_get_image_border(GdkImlibImage * image, GdkImlibBorder * border);
  */
  gnome_canvas_set_scroll_region( ruler->canvas, 0, 0, 0, 15 );

  gtk_widget_set_usize( GTK_WIDGET( ruler->canvas ), 0, 15 );

  ruler->ruler_extent = 0;

  ruler->first_indent_val = 0;
  ruler->left_indent_val = 0;
  ruler->right_indent_val = 0;

  image = load_image( DATADIR "/go/ruler_first.png" );
  ruler->first_indent
    =gnome_canvas_item_new (gnome_canvas_root (GNOME_CANVAS (ruler->canvas) ),
			    gnome_canvas_image_get_type (),
			    "image", image,
			    "x", (double) 0,
			    "y", (double) 0,
			    "width", (double) 5,
			    "height", (double) 5,
			    "anchor", GTK_ANCHOR_NW,
			    NULL);

  image = load_image( DATADIR "/go/ruler_left.png" );
  ruler->left_indent
    =gnome_canvas_item_new (gnome_canvas_root (GNOME_CANVAS (ruler->canvas) ),
			    gnome_canvas_image_get_type (),
			    "image", image,
			    "x", (double) 0,
			    "y", (double) 10,
			    "width", (double) 5,
			    "height", (double) 5,
			    "anchor", GTK_ANCHOR_NW,
			    NULL);

  image = load_image( DATADIR "/go/ruler_right.png" );
  ruler->right_indent
    =gnome_canvas_item_new (gnome_canvas_root (GNOME_CANVAS (ruler->canvas) ),
			    gnome_canvas_image_get_type (),
			    "image", image,
			    "x", (double) 50,
			    "y", (double) 0,
			    "width", (double) 7,
			    "height", (double) 15,
			    "anchor", GTK_ANCHOR_NE,
			    NULL);

  image = load_image( DATADIR "/go/ruler_first_and_left.png" );
  ruler->first_and_left_indent
    =gnome_canvas_item_new (gnome_canvas_root (GNOME_CANVAS (ruler->canvas) ),
			    gnome_canvas_image_get_type (),
			    "image", image,
			    "x", (double) 0,
			    "y", (double) 6,
			    "width", (double) 7,
			    "height", (double) 3,
			    "anchor", GTK_ANCHOR_NW,
			    NULL);
  
  setup_item( ruler->first_indent, ruler );
  setup_item( ruler->left_indent, ruler );
  setup_item( ruler->right_indent, ruler );
  setup_item( ruler->first_and_left_indent, ruler );

  gtk_signal_connect( GTK_OBJECT( ruler->canvas ),
		      "size_allocate",
		      GTK_SIGNAL_FUNC( ruler_configure_event ),
		      (gpointer) ruler );

  gtk_signal_connect( GTK_OBJECT( ruler->canvas ),
		      "destroy",
		      GTK_SIGNAL_FUNC( ruler_destroy ),
		      (gpointer) ruler );

  gtk_signal_connect( GTK_OBJECT( ruler->window->textbox ),
		      "selection_changed",
		      GTK_SIGNAL_FUNC( update_selection ),
		      (gpointer) ruler );

  gtk_signal_connect( GTK_OBJECT( ruler->canvas ),
		      "realize",
		      GTK_SIGNAL_FUNC( ruler_realize ),
		      (gpointer) ruler );

  ruler->motion_callback = 0;

  return ruler;
}
