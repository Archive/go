/* search.c - search facilities for the GO.
 *
 * Copyright (C) 1998 Chris Lahey.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include "config.h"
#include <gtk/gtk.h>
#include <stdio.h>
#include <ctype.h>
#include <sys/types.h>
#include <regex.h>
#include "search.h"
#include "main.h"
#include "menu.h"
#include "file.h"
#include "window.h"

#include "gtkrichtext.h"

static search_dialog *create_search_dialog( GOWindow *window );
static void destroy_search_callback( GtkWidget *widget, gpointer data );
static void search_callback( GtkWidget *widget, gint button, gpointer data );

static regexp_dialog *create_regexp_dialog( GOWindow *window );
static void destroy_regexp_callback( GtkWidget *widget, gpointer data );
static void regexp_callback( GtkWidget *widget, gint button, gpointer data );

static search_replace_dialog *create_search_replace_dialog( GOWindow *window );
static void destroy_search_replace_callback( GtkWidget *widget, gpointer data );
static void search_replace_callback( GtkWidget *widget, gint button, gpointer data );

static regexp_replace_dialog *create_regexp_replace_dialog( GOWindow *window );
static void destroy_regexp_replace_callback( GtkWidget *widget, gpointer data );
static void regexp_replace_callback( GtkWidget *widget, gint button, gpointer data );

typedef struct state
{
  int position;
  struct state *next;
} state;

static search_dialog *
create_search_dialog( GOWindow *window )
{
  search_dialog *dialog = g_malloc0( sizeof( search_dialog ) );
  GtkWidget *search, *close;
  GtkWidget *hbox, *vbox;
  GtkWidget *entry_hbox;
  GtkWidget *label;

  dialog->dialog = gnome_dialog_new(_("Search"),
				    _("Search"),
				    GNOME_STOCK_BUTTON_CLOSE,
				    NULL);
  dialog->window = window;

  gnome_dialog_set_default( GNOME_DIALOG( dialog->dialog ), 0 );

  hbox = gtk_hbox_new( FALSE, 10 );
  vbox = gtk_vbox_new( FALSE, 0 );
  gtk_box_pack_start( GTK_BOX( hbox ), vbox, TRUE, TRUE, 10 );
  gtk_box_pack_start( GTK_BOX( GNOME_DIALOG( dialog->dialog )->vbox ), hbox, TRUE, TRUE, 0 );

  dialog->entry = gtk_entry_new();
  gtk_entry_set_editable( GTK_ENTRY( dialog->entry ), TRUE );
  entry_hbox = gtk_hbox_new( FALSE, 3 );
  label = gtk_label_new( _("Search text :") );
  gtk_box_pack_start( GTK_BOX( entry_hbox ), label, TRUE, FALSE, 0 );
  gtk_box_pack_start( GTK_BOX( entry_hbox ), dialog->entry, TRUE, FALSE, 0 );
  gtk_box_pack_start( GTK_BOX( vbox ), entry_hbox, TRUE, FALSE, 5 );

  dialog->case_sens = gtk_check_button_new_with_label( _("Case Sensitive") );
  gtk_toggle_button_set_state( GTK_TOGGLE_BUTTON( dialog->case_sens ), TRUE );
  gtk_box_pack_start( GTK_BOX( vbox ), dialog->case_sens, TRUE, FALSE, 5 );

  gtk_signal_connect( GTK_OBJECT( dialog->dialog ),
		      "clicked",
		      (GtkSignalFunc) search_callback,
		      dialog );
  gtk_signal_connect( GTK_OBJECT( dialog->dialog ),
		      "destroy",
		      (GtkSignalFunc) destroy_search_callback,
		      dialog );
  gtk_widget_show_all( dialog->dialog );

  return dialog;
}

static regexp_dialog *
create_regexp_dialog( GOWindow *window )
{
  regexp_dialog *dialog = g_malloc0( sizeof( regexp_dialog ) );
  GtkWidget *regexp, *close;
  GtkWidget *hbox, *vbox;
  GtkWidget *entry_hbox;
  GtkWidget *label;

  dialog->dialog = gnome_dialog_new(_("Regular Expression Search"),
				    _("Search"),
				    GNOME_STOCK_BUTTON_CLOSE,
				    NULL);
  dialog->window = window;

  gnome_dialog_set_default( GNOME_DIALOG( dialog->dialog ), 0 );

  hbox = gtk_hbox_new( FALSE, 10 );
  vbox = gtk_vbox_new( FALSE, 0 );
  gtk_box_pack_start( GTK_BOX( hbox ), vbox, TRUE, TRUE, 10 );
  gtk_box_pack_start( GTK_BOX( GNOME_DIALOG( dialog->dialog )->vbox ), hbox, TRUE, TRUE, 0 );

  dialog->entry = gtk_entry_new();
  gtk_entry_set_editable( GTK_ENTRY( dialog->entry ), TRUE );
  entry_hbox = gtk_hbox_new( FALSE, 3 );
  label = gtk_label_new( _("Regular Expression :") );
  gtk_box_pack_start( GTK_BOX( entry_hbox ), label, TRUE, FALSE, 0 );
  gtk_box_pack_start( GTK_BOX( entry_hbox ), dialog->entry, TRUE, FALSE, 0 );
  gtk_box_pack_start( GTK_BOX( vbox ), entry_hbox, TRUE, FALSE, 5 );

  dialog->case_sens = gtk_check_button_new_with_label( _("Case Sensitive") );
  gtk_toggle_button_set_state( GTK_TOGGLE_BUTTON( dialog->case_sens ), TRUE );
  gtk_box_pack_start( GTK_BOX( vbox ), dialog->case_sens, TRUE, FALSE, 5 );


  gtk_signal_connect( GTK_OBJECT( dialog->dialog ),
		      "clicked",
		      (GtkSignalFunc) regexp_callback,
		      dialog );
  gtk_signal_connect( GTK_OBJECT( dialog->dialog ),
		      "destroy",
		      (GtkSignalFunc) destroy_regexp_callback,
		      dialog );
  gtk_widget_show_all( dialog->dialog );

  return dialog;
}

static search_replace_dialog *
create_search_replace_dialog( GOWindow *window )
{
  search_replace_dialog *dialog = g_malloc0( sizeof( search_replace_dialog ) );
  GtkWidget *search, *close;
  GtkWidget *hbox, *vbox;
  GtkWidget *table;
  GtkWidget *label;
  GtkWidget *alignment;

  dialog->dialog = gnome_dialog_new(_("Search And Replace"),
				    _("Search And Replace"),
				    GNOME_STOCK_BUTTON_CLOSE,
				    NULL);
  dialog->window = window;

  gnome_dialog_set_default( GNOME_DIALOG( dialog->dialog ), 0 );

  hbox = gtk_hbox_new( FALSE, 10 );
  vbox = gtk_vbox_new( FALSE, 0 );
  gtk_box_pack_start( GTK_BOX( hbox ), vbox, TRUE, TRUE, 10 );
  gtk_box_pack_start( GTK_BOX( GNOME_DIALOG( dialog->dialog )->vbox ), hbox, TRUE, TRUE, 0 );

  table = gtk_table_new( 2, 2, FALSE );
  gtk_table_set_col_spacing( GTK_TABLE( table ), 0, 5 );

  dialog->entry = gtk_entry_new();
  gtk_entry_set_editable( GTK_ENTRY( dialog->entry ), TRUE );
  gtk_table_attach_defaults( GTK_TABLE( table ), dialog->entry, 1, 2, 0, 1 );
  label = gtk_label_new( _("Search text :") );
  alignment = gtk_alignment_new( 1, .5, 0, 1 );
  gtk_container_add( GTK_CONTAINER( alignment ), label );
  gtk_table_attach_defaults ( GTK_TABLE( table ), alignment, 0, 1, 0, 1 );

  dialog->new_entry = gtk_entry_new();
  gtk_entry_set_editable( GTK_ENTRY( dialog->new_entry ), TRUE );
  gtk_table_attach_defaults( GTK_TABLE( table ), dialog->new_entry, 1, 2, 1, 2 );
  label = gtk_label_new( _("Replacement text :") );
  alignment = gtk_alignment_new( 1, .5, 0, 1 );
  gtk_container_add( GTK_CONTAINER( alignment ), label );
  gtk_table_attach_defaults ( GTK_TABLE( table ), alignment, 0, 1, 1, 2 );

  gtk_box_pack_start( GTK_BOX( vbox ), table, TRUE, FALSE, 5 );

  dialog->case_sens = gtk_check_button_new_with_label( _("Case Sensitive") );
  gtk_toggle_button_set_state( GTK_TOGGLE_BUTTON( dialog->case_sens ), TRUE );
  gtk_box_pack_start( GTK_BOX( vbox ), dialog->case_sens, TRUE, FALSE, 5 );


  gtk_signal_connect( GTK_OBJECT( dialog->dialog ),
		      "clicked",
		      (GtkSignalFunc) search_replace_callback,
		      dialog );
  gtk_signal_connect( GTK_OBJECT( dialog->dialog ),
		      "destroy",
		      (GtkSignalFunc) destroy_search_replace_callback,
		      dialog );
  gtk_widget_show_all( dialog->dialog );

  return dialog;
}

static regexp_replace_dialog *
create_regexp_replace_dialog( GOWindow *window )
{
  regexp_replace_dialog *dialog = g_malloc0( sizeof( regexp_replace_dialog ) );
  GtkWidget *search, *close;
  GtkWidget *hbox, *vbox;
  GtkWidget *table;
  GtkWidget *label;
  GtkWidget *alignment;

  dialog->dialog = gnome_dialog_new(_("Regular Expression Search And Replace"),
				    _("Search And Replace"),
				    GNOME_STOCK_BUTTON_CLOSE,
				    NULL);
  dialog->window = window;

  gnome_dialog_set_default( GNOME_DIALOG( dialog->dialog ), 0 );

  hbox = gtk_hbox_new( FALSE, 10 );
  vbox = gtk_vbox_new( FALSE, 0 );
  gtk_box_pack_start( GTK_BOX( hbox ), vbox, TRUE, TRUE, 10 );
  gtk_box_pack_start( GTK_BOX( GNOME_DIALOG( dialog->dialog )->vbox ), hbox, TRUE, TRUE, 0 );

  table = gtk_table_new( 2, 2, FALSE );
  gtk_table_set_col_spacing( GTK_TABLE( table ), 0, 5 );

  dialog->entry = gtk_entry_new();
  gtk_entry_set_editable( GTK_ENTRY( dialog->entry ), TRUE );
  gtk_table_attach_defaults( GTK_TABLE( table ), dialog->entry, 1, 2, 0, 1 );
  label = gtk_label_new( _("Regular Expression :") );
  alignment = gtk_alignment_new( 1, .5, 0, 1 );
  gtk_container_add( GTK_CONTAINER( alignment ), label );
  gtk_table_attach_defaults ( GTK_TABLE( table ), alignment, 0, 1, 0, 1 );

  dialog->new_entry = gtk_entry_new();
  gtk_entry_set_editable( GTK_ENTRY( dialog->new_entry ), TRUE );
  gtk_table_attach_defaults( GTK_TABLE( table ), dialog->new_entry, 1, 2, 1, 2 );
  label = gtk_label_new( _("Replacement text :") );
  alignment = gtk_alignment_new( 1, .5, 0, 1 );
  gtk_container_add( GTK_CONTAINER( alignment ), label );
  gtk_table_attach_defaults ( GTK_TABLE( table ), alignment, 0, 1, 1, 2 );

  gtk_box_pack_start( GTK_BOX( vbox ), table, TRUE, FALSE, 5 );

  dialog->case_sens = gtk_check_button_new_with_label( _("Case Sensitive") );
  gtk_toggle_button_set_state( GTK_TOGGLE_BUTTON( dialog->case_sens ), TRUE );
  gtk_box_pack_start( GTK_BOX( vbox ), dialog->case_sens, TRUE, FALSE, 5 );


  gtk_signal_connect( GTK_OBJECT( dialog->dialog ),
		      "clicked",
		      (GtkSignalFunc) search_callback,
		      dialog );
  gtk_signal_connect( GTK_OBJECT( dialog->dialog ),
		      "destroy",
		      (GtkSignalFunc) destroy_search_callback,
		      dialog );
  gtk_widget_show_all( dialog->dialog );

  return dialog;
}

static void
destroy_search_callback( GtkWidget *widget, gpointer data )
{
  search_dialog *dialog = (search_dialog *) data;
  g_free( dialog );
}

static void
destroy_regexp_callback( GtkWidget *widget, gpointer data )
{
  regexp_dialog *dialog = (regexp_dialog *) data;
  g_free( dialog );
}

static void
destroy_search_replace_callback( GtkWidget *widget, gpointer data )
{
  search_replace_dialog *dialog = (search_replace_dialog *) data;
  g_free( dialog );
}

static void
destroy_regexp_replace_callback( GtkWidget *widget, gpointer data )
{
  regexp_replace_dialog *dialog = (regexp_replace_dialog *) data;
  g_free( dialog );
}

#define COMPARE( sensitive, x, y ) ( sensitive ? \
				     ( x ) == ( y ) : \
				     tolower( x ) == tolower( y ) )

static void
search_callback( GtkWidget *widget, gint button, gpointer data )
{
  gboolean found;
  search_dialog *dialog = (search_dialog *) data;
  if ( button == 0 )
    {
      gchar *text = gtk_entry_get_text( GTK_ENTRY( dialog->entry ) );
      gint position = gtk_editable_get_position( GTK_EDITABLE( dialog->window->textbox ) ) + 1;
      gint start = position;
      gchar *char_data = gtk_editable_get_chars ( GTK_EDITABLE( dialog->window->textbox ),
						  position,
						  -1 );
      gint length = gtk_rich_text_get_length( GTK_RICH_TEXT( dialog->window->textbox ) );

      state *possible = g_malloc0( sizeof( state ) );
      state *current, *previous;
      gint text_length = strlen( text );
      
      possible->position = 0;
      possible->next = 0;
      
      found = FALSE;
  
      while ( position < length && !found )
	{
	  previous = possible;
	  current = possible->next;
	  
	  if ( COMPARE( GTK_TOGGLE_BUTTON( dialog->case_sens )->active,
			char_data[ position - start ],
			text[ 0 ] ) )
	    {
	      state *temp = possible->next;
	      possible->next = g_malloc0( sizeof( state ) );
	      possible->next->position = 1;
	      possible->next->next = temp;
	      current = possible->next->next;
	      previous = possible->next;
	    }
	  
	  for ( ; current; previous = current, current = current->next )
	    {
	      if ( COMPARE( GTK_TOGGLE_BUTTON( dialog->case_sens )->active,
			    char_data[ position - start ],
			    text[ current->position ] ) )
		{
		  current->position++;
		  if ( current->position == text_length)
		    {
		      found = TRUE;
		      break;
		    }
		}
	      else
		{
		  previous->next = current->next;
		  g_free( current );
		  current = previous;
		}
	    }
	  position ++;
	}
      
      if( found )
	{
	  gtk_editable_set_position( GTK_EDITABLE( dialog->window->textbox ), position );
	  gtk_editable_select_region( GTK_EDITABLE( dialog->window->textbox ), position - text_length, position );
	}
      else
	{
	  gtk_statusbar_push( GTK_STATUSBAR( dialog->window->status ), dialog->window->contextid, _("Text not found.") );
	}
      
      previous = current = possible;
      for( ; current; previous = current )
	{
	  current = previous->next;
	  g_free( previous );
	}
    }
  else
    {
      gnome_dialog_close( GNOME_DIALOG( dialog->dialog ) );
    }
}

static void
regexp_callback( GtkWidget *widget, gint button, gpointer data )
{
  search_dialog *dialog = (search_dialog *) data;
  if ( button == 0 )
    {
      gint position = gtk_editable_get_position( GTK_EDITABLE( dialog->window->textbox ) ) + 1;
      gchar *text = gtk_entry_get_text( GTK_ENTRY( dialog->entry ) );
      gchar *char_data = gtk_editable_get_chars ( GTK_EDITABLE( dialog->window->textbox ),
						  position,
						  -1 );
      regex_t regexp;
      gboolean found;
      regmatch_t location;
      if ( char_data )
	{
	  if ( regcomp( &regexp, text,
			GTK_TOGGLE_BUTTON( dialog->case_sens )->active
			? REG_NEWLINE : REG_ICASE | REG_NEWLINE ) )
	    {
	      gtk_statusbar_push( GTK_STATUSBAR( dialog->window->status ),
				  dialog->window->contextid,
				  _("Problem with regular expression.") );
	    }
	  else
	    {
	      found = ! regexec( &regexp, char_data, 1, &location, 0 );
	  
	      if( found )
		{
		  gint text_length = location.rm_eo - location.rm_so;
	  
		  position += location.rm_eo;
	      
		  gtk_editable_set_position( GTK_EDITABLE( dialog->window->textbox ), position );
		  gtk_editable_select_region( GTK_EDITABLE( dialog->window->textbox ), position - text_length, position );
		}
	      else
		{
		  gtk_statusbar_push( GTK_STATUSBAR( dialog->window->status ), dialog->window->contextid, _("Text not found.") );
		}
	    }      
	  g_free( char_data );
	  regfree( &regexp );
	}
      else
	{
	  gtk_statusbar_push( GTK_STATUSBAR( dialog->window->status ), dialog->window->contextid, _("Text not found.") );
	}
    }
  else
    gnome_dialog_close( GNOME_DIALOG( dialog->dialog ) );
}

static void
regexp_replace_callback( GtkWidget *widget, gint button, gpointer data )
{
  regexp_replace_dialog *dialog = (regexp_replace_dialog *) data;
  if ( button == 0 )
    {
      gint position = gtk_editable_get_position( GTK_EDITABLE( dialog->window->textbox ) ) + 1;
      gchar *text = gtk_entry_get_text( GTK_ENTRY( dialog->entry ) );
      gchar *new_text = gtk_entry_get_text( GTK_ENTRY( dialog->new_entry ) );
      gint new_text_length = strlen( new_text );
      gchar *char_data = gtk_editable_get_chars ( GTK_EDITABLE( dialog->window->textbox ), position, -1 );
      regex_t regexp;
      gboolean found;
      regmatch_t *locations;

      if ( char_data )
	{
	  if ( regcomp( &regexp, text, GTK_TOGGLE_BUTTON( dialog->case_sens )->active ? REG_NEWLINE : REG_ICASE | REG_NEWLINE ) )
	    {
	      gtk_statusbar_push( GTK_STATUSBAR( dialog->window->status ), dialog->window->contextid, _("Problem with regular expression.") );      
	    }
	  else
	    {
	      locations = g_malloc0( ( regexp.re_nsub + 1 ) * sizeof( regmatch_t ) );
      
	      found = ! regexec( &regexp, char_data, regexp.re_nsub + 1, locations, 0 );
      
	      if( found )
		{
		  gint text_length = locations[0].rm_eo - locations[0].rm_so;
		  gint i;
		  gint act_new_text_length = 0;
	  
		  position += locations[0].rm_so;
		  gtk_editable_delete_text( GTK_EDITABLE( dialog->window->textbox ), position, position + text_length );
		  for ( i = 0; i < new_text_length; i++ )
		    {
		      gint which;
		      switch( new_text[i] )
			{
			case '\\':
			  i++;
			  if( i < new_text_length )
			    switch( new_text[i] )
			      {
			      case '&':
				gtk_editable_insert_text( GTK_EDITABLE( dialog->window->textbox ), char_data + locations[0].rm_so, text_length, &position );
				break;
			      case '1': case '2': case '3':
			      case '4': case '5': case '6':
			      case '7': case '8': case '9':
				which = new_text[i] - '0';
				gtk_editable_insert_text( GTK_EDITABLE( dialog->window->textbox ),
							  char_data + locations[which].rm_so,
							  locations[which].rm_eo - locations[which].rm_so,
							  &position );
				act_new_text_length += locations[which].rm_eo - locations[which].rm_so;
				break;
			      case '\\':
			      default:
				gtk_editable_insert_text( GTK_EDITABLE( dialog->window->textbox ),
							  new_text + i, 1, &position );
				act_new_text_length += 1;
				break;			
			      }
			  break;
			default:
			  gtk_editable_insert_text( GTK_EDITABLE( dialog->window->textbox ),
						    new_text + i, 1, &position );
			  act_new_text_length += 1;
			}
		    }
		  gtk_editable_set_position( GTK_EDITABLE( dialog->window->textbox ), position );
		  gtk_editable_select_region( GTK_EDITABLE( dialog->window->textbox ), position - act_new_text_length, position );
		}
	      else
		{
		  gtk_statusbar_push( GTK_STATUSBAR( dialog->window->status ), dialog->window->contextid, _("Text not found.") );
		}
	      g_free( locations );
	    }
	  g_free( char_data );
	  regfree( &regexp );
	}
      else
	{
	  gtk_statusbar_push( GTK_STATUSBAR( dialog->window->status ), dialog->window->contextid, _("Text not found.") );
	}
    }
  else
    gnome_dialog_close( GNOME_DIALOG( dialog->dialog ) );
}

static void
search_replace_callback( GtkWidget *widget, gint button, gpointer data )
{
  search_replace_dialog *dialog = (search_replace_dialog *) data;
  if ( button == 0 )
    {
      gboolean found;
      gchar *text = gtk_entry_get_text( GTK_ENTRY( dialog->entry ) );
      gchar *new_text = gtk_entry_get_text( GTK_ENTRY( dialog->new_entry ) );
      gint position = gtk_editable_get_position( GTK_EDITABLE( dialog->window->textbox ) ) + 1;
      gint start = position;
      gchar *char_data = gtk_editable_get_chars ( GTK_EDITABLE( dialog->window->textbox ),
						  position,
						  -1 );
      gint length = gtk_rich_text_get_length( GTK_RICH_TEXT( dialog->window->textbox ) );

      state *possible = g_malloc0( sizeof( state ) );
      state *current, *previous;
      gint text_length = strlen( text );
      gint new_text_length = strlen( new_text );

      possible->position = 0;
      possible->next = 0;
  
      found = FALSE;

      while ( position < length && !found )
	{
	  previous = possible;
	  current = possible->next;

	  if ( COMPARE( GTK_TOGGLE_BUTTON( dialog->case_sens )->active,
			char_data[ position - start ],
			text[ 0 ] ) )
	      {
		state *temp = possible->next;
		possible->next = g_malloc0( sizeof( state ) );
		possible->next->position = 1;
		possible->next->next = temp;
		current = possible->next->next;
		previous = possible->next;
	      }

	  for ( ; current; previous = current, current = current->next )
	    {
	      if ( COMPARE( GTK_TOGGLE_BUTTON( dialog->case_sens )->active,
			    char_data[ position - start ],
			    text[ current->position ] ) )
		{
		  current->position++;
		  if ( current->position == text_length)
		    {
		      found = TRUE;
		      break;
		    }
		}
	      else
		{
		  previous->next = current->next;
		  g_free( current );
		  current = previous;
		}
	    }
	  position ++;
	}

      if( found )
	{
	  position = position - text_length;      
	  gtk_editable_delete_text( GTK_EDITABLE( dialog->window->textbox ), position, position + text_length );
	  gtk_editable_insert_text( GTK_EDITABLE( dialog->window->textbox ), new_text, new_text_length, &position );
	  gtk_editable_set_position( GTK_EDITABLE( dialog->window->textbox ), position );
	  gtk_editable_select_region( GTK_EDITABLE( dialog->window->textbox ), position - new_text_length, position );
	}
      else
	{
	  gtk_statusbar_push( GTK_STATUSBAR( dialog->window->status ), dialog->window->contextid, _("Text not found.") );
	}
  
      previous = current = possible;
      for( ; current; previous = current )
	{
	  current = previous->next;
	  g_free( previous );
	}
    }
  else
    gnome_dialog_close( GNOME_DIALOG( dialog->dialog ) );
}

void
search_search_callback( GtkWidget *widget, gpointer data )
{
  create_search_dialog( (GOWindow *) data );
}

void
search_regexp_callback( GtkWidget *widget, gpointer data )
{
  create_regexp_dialog( (GOWindow *) data );
}

void
search_search_replace_callback( GtkWidget *widget, gpointer data )
{
  create_search_replace_dialog( (GOWindow *) data );
}

void
search_regexp_replace_callback( GtkWidget *widget, gpointer data )
{
  create_regexp_replace_dialog( (GOWindow *) data );
}
