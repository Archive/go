/* file.h - Header file for manipulations regarding the File menu.
 *
 * Copyright (C) 1998 Chris Lahey.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __FILE_H__
#define __FILE_H__
#include "config.h"
#include <gtk/gtk.h>

#include "window.h"

typedef struct selector_window_pair
{
  GOWindow *window;
  GtkWidget *selector;
} selector_window_pair;

typedef struct list_gdk_window_pair
{
  GdkWindow *window;
  GList **list;
} list_gdk_window_pair;

void file_new_callback( GtkWidget *widget, gpointer data );
void file_open_callback( GtkWidget *widget, gpointer data );
void file_save_callback( GtkWidget *widget, gpointer data );
void file_save_as_callback( GtkWidget *widget, gpointer data );
void file_save_as_ff_callback( GtkWidget *widget, gpointer data );
void file_save_as_rtf_callback( GtkWidget *widget, gpointer data );
void file_close_callback( GtkWidget *widget, gpointer data );
gboolean delete_event_callback( GtkWidget *widget, GdkEvent *event, gpointer data );
void destroy_callback( GtkWidget *widget, gpointer data );
void file_exit_callback( GtkWidget *widget, gpointer data );
gint auto_save_callback( gpointer data );
gint long_auto_save_callback( gpointer data );
void edit_select_all_callback( GtkWidget *widget, gpointer data );
void edit_copy_callback( GtkWidget *widget, gpointer data );
void edit_cut_callback( GtkWidget *widget, gpointer data );
void edit_paste_callback( GtkWidget *widget, gpointer data );
void edit_clear_callback( GtkWidget *widget, gpointer data );
GOWindow *open_file( gchar *filename );
GOWindow *open_file_with_dimensions( gchar *filename, int x, int y, int width, int height );
GOWindow *open_file_in_position( gchar *filename, gint position );
GOWindow *open_file_with_dimensions_in_position( gchar *filename, int x, int y, int width, int height, gint position );
GOWindow *new_file( gchar *filename );
GOWindow *new_file_with_dimensions( gchar *filename, int x, int y, int width, int height );
GOWindow *new_file_in_position( gchar *filename, gint position );
GOWindow *new_file_with_dimensions_in_position( gchar *filename, int x, int y, int width, int height, gint position );
/* Returns true if successful. */
gboolean create_directory( gchar *directory );
void save_location( GOWindow *window );

#endif
