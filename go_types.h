/* go_types.h: Defines many types used in go.
 *
 * Copyright (C) 1999 Chris Lahey.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __GO_TYPES_H__
#define __GO_TYPES_H__

#include "config.h"
#include <gnome.h>
#include "undo.h"

typedef struct _GOWindow GOWindow;
typedef struct _GoRuler GoRuler;

typedef enum { GO_FILE_FORMAT_PLAIN, GO_FILE_FORMAT_GO, GO_FILE_FORMAT_RTF } GoFileFormat;

typedef struct
{
  gboolean print_page_nums;
} GoProperties;

struct _GoRuler
{
  GnomeCanvas *canvas;
  GnomeCanvasItem *first_indent;
  GnomeCanvasItem *left_indent;
  GnomeCanvasItem *right_indent;
  GnomeCanvasItem *first_and_left_indent;
  gint first_indent_val;
  gint left_indent_val;
  gint right_indent_val;
  gint motion_callback;
  gint ruler_extent;
  GOWindow *window;
};


struct _GOWindow
{
  /* The scrollbars */
  GtkWidget *vscrollbar;
  GtkWidget *hscrollbar;
  /* The Gnome app */
  GtkWidget *app;
  /* The text widget */
  GtkWidget *textbox;
  /* Embeddable hack stuff. */
  GtkWidget *canvas;
  GnomeCanvasItem *canvas_item;
  GList *embedded_list;
  /* The status bar widget */
  GtkWidget *status;
  /* The main status context id */
  guint contextid;
  gboolean changed;
  gboolean named;
  gchar *title;
  gint auto_save;
  gint long_term_auto_save;
  gint count;
  UndoTree *undo;
  gint undo_count;
  GList *windowlist;
  GoFileFormat format;
  GoRuler *ruler;
  GoProperties *props;
  gint width;
  gint height;
  gint is_editable;
};

#endif /* __GO_TYPES_H__ */
