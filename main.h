/* main.h - Declarations for the functions in main.c
 *
 * Copyright (C) 1998 Chris Lahey.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __MAIN_H__
#define __MAIN_H__

void about_callback( GtkWidget *widget, gpointer data );
void start_plugin( GtkWidget *widget, gpointer data );

#endif /* __MAIN_H__ */
