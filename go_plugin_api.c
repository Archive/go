/* go_plugin_api.c
 * Copyright (C) 1998 Chris Lahey
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>
#include "window.h"
#include "go_plugin_api.h"
#include "file.h"

#include <glib.h>

#include "gtkrichtext.h"

GList *plugins;
extern GList *windowlist;

int go_plugin_document_create( gint context, gchar *title )
{
  return GPOINTER_TO_INT( new_file ( title ) );
}

int go_plugin_document_open( gint context, gchar *title )
{
  return GPOINTER_TO_INT( open_file ( title ) );
}

gboolean go_plugin_document_close( gint docid )
{
  file_close_callback( NULL, ((GOWindow *) GINT_TO_POINTER( docid )) );
  return TRUE;
}

void go_plugin_text_append( gint docid, gchar *buffer, gint length )
{
  GtkEditable *editable = GTK_EDITABLE( ((GOWindow *) GINT_TO_POINTER( docid ))->textbox );
  int position = gtk_rich_text_get_length( GTK_RICH_TEXT( editable ) );
  gtk_editable_insert_text( editable, buffer, length, &position );
}

void go_plugin_text_insert( gint docid, gchar *buffer, gint length, gint position )
{
  GtkEditable *editable = GTK_EDITABLE( ((GOWindow *) GINT_TO_POINTER( docid ))->textbox );
  position = MIN( position, gtk_rich_text_get_length( GTK_RICH_TEXT( editable ) ) );
  gtk_editable_insert_text( editable, buffer, length, &position );
}

void go_plugin_document_show( gint docid )
{
}

int go_plugin_document_current( gint context )
{
  return context;
}

char *go_plugin_document_filename( gint docid )
{
  GOWindow *window = GINT_TO_POINTER( docid );
  return window->title;
}

char *go_plugin_text_get( gint docid )
{
  GtkEditable *editable = GTK_EDITABLE( ((GOWindow *) GINT_TO_POINTER( docid ))->textbox );
  return gtk_editable_get_chars( editable, 0, -1 );
}

gboolean go_plugin_program_quit()
{
  file_exit_callback( NULL, NULL );
  return TRUE;
}

void go_plugin_program_register( plugin_info *info )
{
  plugin_info *temp;

  temp = info;
  info = g_malloc0( sizeof( plugin_info ) );
  info->type = temp->type;
  info->menu_location = g_strdup( temp->menu_location );
  info->suggested_accelerator = g_strdup( temp->suggested_accelerator );
  info->plugin_name = g_strdup( temp->plugin_name );
  plugins = g_list_append( plugins, info );

  g_list_foreach( windowlist, add_plugin_info_to_plugin_list, info );
}

gint go_plugin_document_get_position( gint docid )
{
  GOWindow *window = GINT_TO_POINTER( docid );
  return gtk_editable_get_position( GTK_EDITABLE( window->textbox ) );
}

selection_range go_plugin_document_get_selection_range( gint docid )
{
  GOWindow *window = GINT_TO_POINTER( docid );
  GtkEditable *editable = GTK_EDITABLE( window->textbox );
  selection_range selection;
  if( editable->selection_start_pos <= editable->selection_end_pos )
    {
      selection.start = editable->selection_start_pos;
      selection.end = editable->selection_end_pos;
    }
  else
    {
      selection.start = editable->selection_end_pos;
      selection.end = editable->selection_start_pos;
    }
  return selection;
}

void go_plugin_document_set_selection_range( gint docid, selection_range range )
{
  GOWindow *window = GINT_TO_POINTER( docid );
  GtkEditable *editable = GTK_EDITABLE( window->textbox );
  gtk_editable_select_region( editable, range.start, range.end );
}

void go_plugin_text_set_selection_text( gint docid, gchar *text, gint length )
{
  GOWindow *window = GINT_TO_POINTER( docid );
  GtkEditable *editable = GTK_EDITABLE( window->textbox );
  selection_range selection;
  if( editable->selection_start_pos <= editable->selection_end_pos )
    {
      selection.start = editable->selection_start_pos;
      selection.end = editable->selection_end_pos;
    }
  else
    {
      selection.start = editable->selection_end_pos;
      selection.end = editable->selection_start_pos;
    }

  gtk_editable_delete_selection( editable );
  selection.end = selection.start;
  gtk_editable_insert_text( editable, text, length, &selection.end );
  gtk_editable_select_region( editable, selection.start, selection.end );  
}

gchar *go_plugin_text_get_selection_text( gint docid )
{
  GOWindow *window = GINT_TO_POINTER( docid );
  GtkEditable *editable = GTK_EDITABLE( window->textbox );
  if( editable->selection_start_pos <= editable->selection_end_pos )
    return gtk_editable_get_chars( editable, editable->selection_start_pos, editable->selection_end_pos );
  else
    return gtk_editable_get_chars( editable, editable->selection_end_pos, editable->selection_start_pos );
}
