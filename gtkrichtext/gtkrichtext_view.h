/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GTK_RICH_TEXT_H__
#define __GTK_RICH_TEXT_H__


#include <gdk/gdk.h>
#include <gtk/gtkwidget.h>
#include <gtk/gtkadjustment.h>
#include <gtk/gtksignal.h>
#include <gtk/gtkeditable.h>
#include <libhnj/hsjust.h>
#include "gtkparagraph.h"
#include "gtkword.h"
#include "gtkrichtextcallbacks.h"


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
#define GTK_RICH_TEXT(obj)          GTK_CHECK_CAST (obj, gtk_rich_text_get_type (), GtkRichText)
#define GTK_RICH_TEXT_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, gtk_rich_text_get_type (), GtkRichTextClass)
#define GTK_IS_RICH_TEXT(obj)       GTK_CHECK_TYPE (obj, gtk_rich_text_get_type ())

typedef struct _GtkRichText        GtkRichText;
typedef struct _GtkRichTextClass   GtkRichTextClass;
typedef struct _Position           Position;

struct _Position
{
  ParagraphPosition paragraph_ptr;
  GList *paragraph; /* Of type Paragraph */
};

struct _GtkRichText
{
  GtkEditable editable;

  gpointer draw_data;

  gint minimum_width;
  gint width;
  
  GtkAdjustment *hadj;
  GtkAdjustment *vadj;

  int xoff;
  int yoff;

  Position cursor;
  
  Position selection_end;
  gboolean selection_active;
  gboolean cursor_first;

  int cursor_timeout;
  gboolean cursor_on;

  gint ideal;
  gboolean ideal_valid;

  GtkRichTextFont *cursor_font;

  gint queuedx, queuedy;
  gint motion_callback;
  gint redraw_callback;

  gint last_button;
  gint last_count;
};

struct _GtkRichTextClass
{
  GtkEditableClass parent_class;
};


guint      gtk_rich_text_view_get_type   (void);
GtkWidget* gtk_rich_text_view_new         (GtkAdjustment *hadj,
				     GtkAdjustment *vadj);
void       gtk_rich_text_view_set_minimum_width (GtkRichTextView *rtext,
					    gint         min_width);
void       gtk_rich_text_view_set_width (GtkRichTextView *rtext,
				    gint         width);
gint       gtk_rich_text_view_get_width (GtkRichTextView *rtext);
void       gtk_rich_text_view_set_adjustments (GtkRichTextView *rtext,
					  GtkAdjustment *hadj,
					  GtkAdjustment *vadj);


/* Just stubs so far. */
void       gtk_rich_text_view_set_editable    (GtkRichTextView       *text,
					  gboolean           editable);
void       gtk_rich_text_view_set_word_wrap   (GtkRichTextView       *text,
					  gint               word_wrap);
void       gtk_rich_text_view_freeze          (GtkRichTextView       *text);
void       gtk_rich_text_view_thaw            (GtkRichTextView       *text);
void       gtk_rich_text_view_set_cursor_font (GtkRichTextView       *text,
					  GtkRichTextFont   *font);
gboolean   gtk_rich_text_view_position_equal  (GtkRichTextView       *text,
					  Position           a,
					  Position           b);

void gtk_rich_text_view_set_selection_paragraph_params (GtkRichTextView *text,
						   ParagraphParams *params,
						   ParagraphParamsValid *validity);

void gtk_rich_text_view_set_selection_character_params (GtkRichTextView *text,
						   CharacterParams *params,
						   CharacterParamsValid *validity);


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_RICH_TEXT_VIEW_H__ */
