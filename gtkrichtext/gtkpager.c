/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include <math.h>
#include <stdio.h>
#include <gtk/gtkmain.h>
#include <gtk/gtksignal.h>

#include "gtkpager.h"

#define PAGER_DEFAULT_SIZE 21

/* Forward declararations */

static void gtk_pager_class_init               (GtkPagerClass    *klass);
static void gtk_pager_init                     (GtkPager         *pager);
static void gtk_pager_destroy                  (GtkObject        *object);
static void gtk_pager_realize                  (GtkWidget        *widget);
static void gtk_pager_size_request             (GtkWidget      *widget,
						     GtkRequisition *requisition);
static void gtk_pager_size_allocate            (GtkWidget     *widget,
						     GtkAllocation *allocation);
static gint gtk_pager_expose                   (GtkWidget        *widget,
						     GdkEventExpose   *event);
static gint gtk_pager_button_press             (GtkWidget *widget, GdkEventButton *event);
static gint gtk_pager_button_release           (GtkWidget *widget, GdkEventButton *event);
static gint gtk_pager_motion_notify            (GtkWidget *widget, GdkEventMotion *event);
static void gtk_pager_update                   (GtkPager *scball);
static void gtk_pager_adjustment_value_changed (GtkAdjustment *adjustment, gpointer *data);
static void gtk_pager_adjustment_changed       (GtkAdjustment *adjustment, gpointer *data);

/* Local data */

static GtkWidgetClass *parent_class = NULL;

guint gtk_pager_get_type () {
  static guint pager_type = 0;

  if (!pager_type)
    {
      GtkTypeInfo pager_info =
      {
	"GtkPager",
	sizeof (GtkPager),
	sizeof (GtkPagerClass),
	(GtkClassInitFunc) gtk_pager_class_init,
	(GtkObjectInitFunc) gtk_pager_init,
	NULL,
	NULL,
      };

      pager_type = gtk_type_unique (gtk_widget_get_type (), &pager_info);
    }

  return pager_type;
}

static void gtk_pager_class_init (GtkPagerClass *class) {
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;

  object_class = (GtkObjectClass*) class;
  widget_class = (GtkWidgetClass*) class;

  parent_class = gtk_type_class (gtk_widget_get_type ());

  object_class->destroy = gtk_pager_destroy;

  widget_class->realize = gtk_pager_realize;
  widget_class->expose_event = gtk_pager_expose;
  widget_class->size_request = gtk_pager_size_request;
  widget_class->size_allocate = gtk_pager_size_allocate;
  widget_class->button_press_event = gtk_pager_button_press;
  widget_class->motion_notify_event = gtk_pager_motion_notify;
  widget_class->button_release_event = gtk_pager_button_release;
}

static void gtk_pager_init (GtkPager *pager) {
  pager->button = 0;
  pager->hadjust = NULL;
  pager->vadjust = NULL;
}

GtkWidget* gtk_pager_new (GtkAdjustment *hadjust, GtkAdjustment *vadjust) {
  GtkPager *pager;

  pager = gtk_type_new (gtk_pager_get_type ());

  gtk_pager_set_hadjustment (pager, hadjust);
  gtk_pager_set_vadjustment (pager, vadjust);
  pager->acceleration=20;
  pager->hmove=0;
  pager->vmove=0;

  return GTK_WIDGET (pager);
}

static void gtk_pager_destroy (GtkObject *object) {
  GtkPager *pager;

  g_return_if_fail (object != NULL);
  g_return_if_fail (GTK_IS_PAGER (object));

  pager = GTK_PAGER (object);

  if (pager->hadjust)
    gtk_object_unref (GTK_OBJECT (pager->hadjust));
  if (pager->vadjust)
    gtk_object_unref (GTK_OBJECT (pager->vadjust));

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

GtkAdjustment* gtk_pager_get_hadjustment (GtkPager *pager) {
  g_return_val_if_fail (pager != NULL, NULL);
  g_return_val_if_fail (GTK_IS_PAGER (pager), NULL);

  return pager->hadjust;
}

GtkAdjustment* gtk_pager_get_vadjustment (GtkPager *pager) {
  g_return_val_if_fail (pager != NULL, NULL);
  g_return_val_if_fail (GTK_IS_PAGER (pager), NULL);

  return pager->vadjust;
}

void gtk_pager_set_hadjustment (GtkPager *pager, GtkAdjustment *hadjust) {
  g_return_if_fail (pager != NULL);
  g_return_if_fail (GTK_IS_PAGER (pager));

  if (hadjust) {
    if (pager->hadjust) {
	gtk_signal_disconnect_by_data (GTK_OBJECT (pager->hadjust), (gpointer) pager);
	gtk_object_unref (GTK_OBJECT (pager->hadjust));
    }

    pager->hadjust = hadjust;
    gtk_object_ref (GTK_OBJECT (pager->hadjust));

    gtk_signal_connect (GTK_OBJECT (hadjust), "changed",
			(GtkSignalFunc) gtk_pager_adjustment_changed,
			(gpointer) pager);
    gtk_signal_connect (GTK_OBJECT (hadjust), "value_changed",
			(GtkSignalFunc) gtk_pager_adjustment_value_changed,
			(gpointer) pager);
  }
}

void gtk_pager_set_vadjustment (GtkPager *pager, GtkAdjustment *vadjust) {
  g_return_if_fail (pager != NULL);
  g_return_if_fail (GTK_IS_PAGER (pager));

  if (vadjust) {
    if (pager->vadjust) {
	gtk_signal_disconnect_by_data (GTK_OBJECT (pager->vadjust), (gpointer) pager);
	gtk_object_unref (GTK_OBJECT (pager->vadjust));
    }

    pager->vadjust = vadjust;
    gtk_object_ref (GTK_OBJECT (pager->vadjust));

    gtk_signal_connect (GTK_OBJECT (vadjust), "changed",
			(GtkSignalFunc) gtk_pager_adjustment_changed,
			(gpointer) pager);
    gtk_signal_connect (GTK_OBJECT (vadjust), "value_changed",
			(GtkSignalFunc) gtk_pager_adjustment_value_changed,
			(gpointer) pager);
  }
}

static void
gtk_pager_realize (GtkWidget *widget)
{
  GtkPager *pager;
  GdkWindowAttr attributes;
  gint attributes_mask;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_PAGER (widget));

  GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);
  pager = GTK_PAGER (widget);

  attributes.x = widget->allocation.x;
  attributes.y = widget->allocation.y;
  attributes.width = widget->allocation.width;
  attributes.height = widget->allocation.height;
  attributes.wclass = GDK_INPUT_OUTPUT;
  attributes.window_type = GDK_WINDOW_CHILD;
  attributes.event_mask = gtk_widget_get_events (widget) | GDK_EXPOSURE_MASK | GDK_BUTTON_PRESS_MASK | 
    GDK_BUTTON_RELEASE_MASK | GDK_POINTER_MOTION_MASK | GDK_POINTER_MOTION_HINT_MASK;
  attributes.visual = gtk_widget_get_visual (widget);
  attributes.colormap = gtk_widget_get_colormap (widget);

  attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;
  widget->window = gdk_window_new (widget->parent->window, &attributes, attributes_mask);

  widget->style = gtk_style_attach (widget->style, widget->window);

  gdk_window_set_user_data (widget->window, widget);

  gtk_style_set_background (widget->style, widget->window, GTK_STATE_ACTIVE);
}

static void gtk_pager_size_request (GtkWidget *widget, GtkRequisition *requisition) {
  requisition->width = PAGER_DEFAULT_SIZE;
  requisition->height = PAGER_DEFAULT_SIZE;
}

static void gtk_pager_size_allocate (GtkWidget *widget, GtkAllocation *allocation) {
  GtkPager *pager;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_PAGER (widget));
  g_return_if_fail (allocation != NULL);

  widget->allocation = *allocation;
  pager = GTK_PAGER (widget);

  if (GTK_WIDGET_REALIZED (widget))
    gdk_window_move_resize (widget->window, allocation->x, allocation->y,
			    allocation->width, allocation->height);
}

static gint gtk_pager_expose (GtkWidget *widget, GdkEventExpose *event) {
  /* All the variable for the placement of the miniwindow */
  gint mwwidth, mwheight, mwx, mwy;
  static gint lasthm=0, lastvm=0;
  
  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_PAGER (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);

  if (event->count > 0)
    return FALSE;
  
  gdk_window_clear_area (widget->window, 0, 0, widget->allocation.width, widget->allocation.height);

  gdk_draw_rectangle (widget->window, widget->style->fg_gc[5], 1, 0, 0, 
		      widget->allocation.width, widget->allocation.height);

  if (GTK_PAGER(widget)->button == 0) {
    lasthm=0;
    lastvm=0;
  }

  /* Code to take out the arrows, and then we can expand the mini-window even more ;) */

  /*  if (GTK_PAGER(widget)->vadjust) {
    gdk_draw_line (widget->window, widget->style->black_gc, 10, 0, 13, 3);
    gdk_draw_line (widget->window, widget->style->black_gc, 10, 0, 7, 3);
    if (GTK_PAGER(widget)->vmove < 0 || (GTK_PAGER(widget)->vmove==0 && lastvm < 0)) {
      gdk_draw_line (widget->window, widget->style->black_gc, 9, 1, 11, 1);
      gdk_draw_line (widget->window, widget->style->black_gc, 8, 2, 12, 2);
      gdk_draw_line (widget->window, widget->style->black_gc, 7, 3, 13, 3);
    }
      
    gdk_draw_line (widget->window, widget->style->black_gc, 10, 20, 13, 17);
    gdk_draw_line (widget->window, widget->style->black_gc, 10, 20, 7, 17);
    if (GTK_PAGER(widget)->vmove > 0 || (GTK_PAGER(widget)->vmove==0 && lastvm > 0)) {
      gdk_draw_line (widget->window, widget->style->black_gc, 9, 19, 11, 19);
      gdk_draw_line (widget->window, widget->style->black_gc, 8, 18, 12, 18);
      gdk_draw_line (widget->window, widget->style->black_gc, 7, 17, 13, 17);
    }

    if (GTK_PAGER(widget)->vmove != 0)
      lastvm = GTK_PAGER(widget)->vmove;
  }
  
  if (GTK_PAGER(widget)->hadjust) {
    gdk_draw_line (widget->window, widget->style->black_gc, 0, 10, 3, 13);
    gdk_draw_line (widget->window, widget->style->black_gc, 0, 10, 3, 7);
    if (GTK_PAGER(widget)->hmove < 0 || (GTK_PAGER(widget)->hmove==0 && lasthm < 0)) {
      gdk_draw_line (widget->window, widget->style->black_gc, 1, 9, 1, 11);
      gdk_draw_line (widget->window, widget->style->black_gc, 2, 8, 2, 12);
      gdk_draw_line (widget->window, widget->style->black_gc, 3, 7, 3, 13);
    }
    
    gdk_draw_line (widget->window, widget->style->black_gc, 20, 10, 17, 13);
    gdk_draw_line (widget->window, widget->style->black_gc, 20, 10, 17, 7);
    if (GTK_PAGER(widget)->hmove > 0 || (GTK_PAGER(widget)->hmove==0 && lasthm > 0)) {
      gdk_draw_line (widget->window, widget->style->black_gc, 19, 9, 19, 11);
      gdk_draw_line (widget->window, widget->style->black_gc, 18, 8, 18, 12);
      gdk_draw_line (widget->window, widget->style->black_gc, 17, 7, 17, 13);
    }

    if (GTK_PAGER(widget)->hmove != 0)
      lasthm = GTK_PAGER(widget)->hmove;
      }*/

  /* And the stuff inside them.... a window to display the current location */

  /* The new code, which draws itself larger and scales... */

  gdk_draw_rectangle (widget->window, widget->style->black_gc, 0, 0, 0, 
		      widget->allocation.width-1, widget->allocation.height-1);
  gdk_draw_rectangle (widget->window, widget->style->bg_gc[1], 1, 1, 1,
		      widget->allocation.width-2, widget->allocation.height-2);
      /*  gdk_draw_rectangle (widget->window, widget->style->white_gc, 1, 1, 1,
  widget->allocation.width-2, widget->allocation.height-2);*/
 
  if (GTK_PAGER(widget)->hadjust) {
    mwwidth = ((GTK_PAGER(widget)->hadjust->page_size /
	       (GTK_PAGER(widget)->hadjust->upper - GTK_PAGER(widget)->hadjust->lower))
	       * (widget->allocation.width-2));
    if (mwwidth <= 0)
      mwwidth=1;

    mwx = ((GTK_PAGER(widget)->hadjust->value /
	   (GTK_PAGER(widget)->hadjust->upper - GTK_PAGER(widget)->hadjust->lower)) 
	   * (widget->allocation.width - 2));
  } else {
    mwwidth = (widget->allocation.width - 2);
    mwx = 0;
  }

  if (GTK_PAGER(widget)->vadjust) {
    mwheight = ((GTK_PAGER(widget)->vadjust->page_size /
		(GTK_PAGER(widget)->vadjust->upper - GTK_PAGER(widget)->vadjust->lower)) 
		* (widget->allocation.height - 2));
    if (mwheight <= 0)
      mwheight=1;

    mwy = ((GTK_PAGER(widget)->vadjust->value /
	   (GTK_PAGER(widget)->vadjust->upper - GTK_PAGER(widget)->vadjust->lower)) * 
	   (widget->allocation.height - 2));
  } else {
    mwheight = (widget->allocation.height - 2);
    mwy = 0;
  }

  gdk_draw_rectangle (widget->window, widget->style->white_gc, 1, 1+mwx,1+mwy,mwwidth,mwheight); 
  /* gdk_draw_rectangle (widget->window, widget->style->bg_gc[1], 1, 1+mwx,1+mwy,mwwidth,mwheight); */


  /*  gdk_draw_rectangle (widget->window, widget->style->black_gc, 0, 4, 4, 12, 12);
  gdk_draw_rectangle (widget->window, widget->style->bg_gc[1], 1, 5, 5, 11, 11);
 
  if (GTK_PAGER(widget)->hadjust) {
    mwwidth = (GTK_PAGER(widget)->hadjust->page_size /
	       (GTK_PAGER(widget)->hadjust->upper - GTK_PAGER(widget)->hadjust->lower)) * 11;
    if (mwwidth <= 0)
      mwwidth=1;

    mwx = (GTK_PAGER(widget)->hadjust->value /
	   (GTK_PAGER(widget)->hadjust->upper - GTK_PAGER(widget)->hadjust->lower)) * 11;
  } else {
    mwwidth = 11;
    mwx = 0;
  }

  if (GTK_PAGER(widget)->vadjust) {
    mwheight = (GTK_PAGER(widget)->vadjust->page_size /
		(GTK_PAGER(widget)->vadjust->upper - GTK_PAGER(widget)->vadjust->lower)) * 11;
    if (mwheight <= 0)
      mwheight=1;

    mwy = (GTK_PAGER(widget)->vadjust->value /
	   (GTK_PAGER(widget)->vadjust->upper - GTK_PAGER(widget)->vadjust->lower)) * 11;
  } else {
    mwheight = 11;
    mwy = 0;
  }

  gdk_draw_rectangle (widget->window, widget->style->white_gc, 1, 5+mwx,5+mwy,mwwidth,mwheight);*/

  return FALSE;
}

static gint gtk_pager_button_press (GtkWidget *widget, GdkEventButton *event) {
  GtkPager *pager;

  pager =   GTK_PAGER (widget);

  if (event->x >= 0 && event->x <= widget->allocation.width &&
      event->y >= 0 && event->y <= widget->allocation.height) {
    pager->button=event->button;
    
    gtk_grab_add (widget);
  }

  return TRUE;
}

static gint gtk_pager_button_release (GtkWidget *widget, GdkEventButton *event) {
  GTK_PAGER (widget)->button = 0;
  gtk_grab_remove (widget);

  if (GTK_PAGER(widget)->hadjust) {
    GTK_PAGER (widget)->hmove = 0;
    gtk_signal_emit_by_name (GTK_OBJECT (GTK_PAGER(widget)->hadjust), "value_changed");
  }

  if (GTK_PAGER(widget)->vadjust) {
    GTK_PAGER (widget)->vmove = 0;
    gtk_signal_emit_by_name (GTK_OBJECT (GTK_PAGER(widget)->vadjust), "value_changed");
  }

  return TRUE;
}

static gint gtk_pager_motion_notify (GtkWidget *widget, GdkEventMotion *event) {
  GtkPager *pager;
  gint x, y, value;

  pager = GTK_PAGER (widget);

  gtk_widget_get_pointer (widget, &x, &y);

  /*  printf ("motion at %d, %d\n",x,y);*/

  pager->hmove=x-pager->hmove;
  pager->vmove=y-pager->vmove;
  
  if ((pager->hmove != 0 || pager->vmove !=0) && pager->button) {
    if (pager->hadjust) {
      value=pager->hadjust->value + pager->hmove*pager->acceleration;
      if (value < pager->hadjust->lower)
	value=pager->hadjust->lower;
      if (value > pager->hadjust->upper - pager->hadjust->page_size)
	value=pager->hadjust->upper - pager->hadjust->page_size;
      pager->hadjust->value=value;

      gtk_signal_emit_by_name (GTK_OBJECT (pager->hadjust), "value_changed");
    }
    
    if (pager->vadjust) {
      value=pager->vadjust->value + pager->vmove*pager->acceleration;
      if (value < pager->vadjust->lower)
	value=pager->vadjust->lower;
      if (value > pager->vadjust->upper - pager->vadjust->page_size)
	value=pager->vadjust->upper - pager->vadjust->page_size;
      pager->vadjust->value=value;
      
      gtk_signal_emit_by_name (GTK_OBJECT (pager->vadjust), "value_changed");
    }
  }

  pager->hmove=x;
  pager->vmove=y;

  return TRUE;
}

static void gtk_pager_update (GtkPager *scball) {
  gfloat new_hvalue;
  gfloat new_vvalue;
  
  g_return_if_fail (scball != NULL);
  g_return_if_fail (GTK_IS_PAGER (scball));

  if (scball->hadjust) {
    new_hvalue = scball->hadjust->value;
    
    if (new_hvalue < scball->hadjust->lower)
      new_hvalue = scball->hadjust->lower;
    
    if (new_hvalue > scball->hadjust->upper)
      new_hvalue = scball->hadjust->upper;
    
    if (new_hvalue != scball->hadjust->value) {
      scball->hadjust->value = new_hvalue;
      gtk_signal_emit_by_name (GTK_OBJECT (scball->hadjust), "value_changed");
    }
  }

  if (scball->vadjust) {
    new_vvalue = scball->vadjust->value;
    
    if (new_vvalue < scball->vadjust->lower)
      new_vvalue = scball->vadjust->lower;
    
    if (new_vvalue > scball->vadjust->upper)
      new_vvalue = scball->vadjust->upper;
    
    if (new_vvalue != scball->vadjust->value) {
      scball->vadjust->value = new_vvalue;
      gtk_signal_emit_by_name (GTK_OBJECT (scball->vadjust), "value_changed");
    }
  }

  gtk_widget_draw (GTK_WIDGET(scball), NULL);
}

static void gtk_pager_adjustment_value_changed (GtkAdjustment *adjustment, gpointer *data) {
  GtkPager *pager;

  g_return_if_fail (adjustment != NULL);
  g_return_if_fail (data != NULL);

  pager = GTK_PAGER (data);

  gtk_pager_update (pager);
}

static void gtk_pager_adjustment_changed (GtkAdjustment *adjustment, gpointer *data) {
  GtkPager *pager;

  g_return_if_fail (adjustment != NULL);
  g_return_if_fail (data != NULL);

  pager = GTK_PAGER (data);

  gtk_pager_update (pager);
}

