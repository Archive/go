/* gtkparagraph.h
 * Copyright (C) 1998 Chris Lahey
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GTK_PARAGRAPH_H__
#define __GTK_PARAGRAPH_H__

#include "gtkword.h"
#include <gdk/gdk.h>
#include <gdk/gdkkeysyms.h>
#include <gtk/gtkenums.h>
#include "gtkrichtextcallbacks.h"

typedef struct _Paragraph          Paragraph;
typedef struct _ParagraphPosition  ParagraphPosition;

typedef struct
{
  gint count;
  GList **words;
  gint *js;
  gint *ascents;
  gint *descents;
  gint *extra_space;
  gint *space_space;
  gint *word_counts;
  gint *tab_count;
  gint current_x_line;
  gint current_y_char;
  gint height;
} ParagraphValues;

struct _Paragraph
{
  GList *words;  /* Of type Word. */
  ParagraphValues *values;
  gboolean valuesvalid;
  gint width;
  gint length;
  gboolean length_valid;
  ParagraphParams params;
  GtkRichText *parent;
};

struct _ParagraphPosition
{
  WordPosition word_ptr;
  GList *word; /* Of type Word. */
};

ParagraphPosition gtk_paragraph_end( Paragraph *paragraph );
ParagraphPosition gtk_paragraph_start( Paragraph *paragraph );
#define gtk_paragraph_length( x ) ( (x)->length_valid ? (x)->length : gtk_paragraph_length_invalid ( x ) )
gint gtk_paragraph_length_invalid( Paragraph *paragraph );
ParagraphPosition gtk_paragraph_delete( Paragraph *paragraph, ParagraphPosition start, ParagraphPosition end );
ParagraphPosition gtk_paragraph_insert( Paragraph *paragraph, ParagraphPosition start, gchar *vals, gint length );

ParagraphPosition gtk_paragraph_insert_with_data( Paragraph *paragraph, ParagraphPosition start, gchar *vals, gint length, GtkRichTextFont *font );

gint gtk_paragraph_position_compare( Paragraph *paragraph, ParagraphPosition a, ParagraphPosition b );
#define gtk_paragraph_position_equal( pos, a, b) ((a).word == (b).word && \
                                                  gtk_word_position_equal( (Word *) (a).word->data, (a).word_ptr, (b).word_ptr ))
ParagraphPosition gtk_paragraph_decrement_position( Paragraph *paragraph, ParagraphPosition old_pos, gint distance );
ParagraphPosition gtk_paragraph_increment_position( Paragraph *paragraph, ParagraphPosition old_pos, gint distance );

gint gtk_paragraph_height_invalid( Paragraph *paragraph );
#define gtk_paragraph_height( x ) ( ((Paragraph *) (x))->valuesvalid ? ((Paragraph *) (x))->values->height : gtk_paragraph_height_invalid ( x ) )

gint gtk_paragraph_line_count_invalid( Paragraph *paragraph );
#define gtk_paragraph_line_count( x ) ( ((Paragraph *) (x))->valuesvalid ? ((Paragraph *) (x))->values->count : gtk_paragraph_line_count_invalid ( x ) )

void gtk_paragraph_draw( Paragraph *paragraph, gint xoff, gint yoff, GdkDrawable *drawable, ParagraphPosition paragraph_ptr, gboolean cursor, ParagraphPosition end_selection_ptr, gboolean end_selection, gboolean in_selection, GdkGC *gc, GdkGC *gcsel );
void gtk_paragraph_render( Paragraph *paragraph,
			   GtkRichTextRenderSystem *system);
ParagraphPosition gtk_paragraph_find_point( Paragraph *paragraph, int x, int y );
Paragraph *gtk_paragraph_break( Paragraph *paragraph, ParagraphPosition position );

gboolean gtk_paragraph_position_equals( ParagraphPosition posa, ParagraphPosition posb );

ParagraphPosition gtk_paragraph_position( Paragraph *paragraph, int position );

Paragraph *gtk_paragraph_new( GtkRichText *parent );

void gtk_paragraph_set_font( Paragraph *paragraph, ParagraphPosition start, ParagraphPosition end, GtkRichTextFont *font );

gboolean gtk_paragraph_a_first( Paragraph *paragraph, ParagraphPosition a, ParagraphPosition b);

void gtk_paragraph_destroy( Paragraph *paragraph );

void gtk_paragraph_get_chars( Paragraph *paragraph, gchar *chars, gint start, gint end );
ParagraphPosition gtk_paragraph_vertical_move( Paragraph *paragraph, ParagraphPosition old_pos, gint ideal, gint *distance );
gint gtk_paragraph_find_ideal( Paragraph *paragraph, ParagraphPosition old_pos );

ParagraphPosition gtk_paragraph_end_of_line( Paragraph *par, ParagraphPosition pos );
ParagraphPosition gtk_paragraph_move_backward_word( Paragraph *par, ParagraphPosition pos );
ParagraphPosition gtk_paragraph_move_forward_word( Paragraph *par, ParagraphPosition pos );
void gtk_paragraph_set_width( Paragraph *par, gint width );
gint gtk_paragraph_top_of_pos( Paragraph *par, ParagraphPosition pos );
gint gtk_paragraph_bottom_of_pos( Paragraph *par, ParagraphPosition pos );
void gtk_paragraph_position_coords( Paragraph *par, ParagraphPosition pos, gint *x, gint *y );
ParagraphPosition gtk_paragraph_concat( Paragraph *paragrapha, Paragraph *paragraphb );
gint gtk_paragraph_position_to_int( Paragraph *paragraph, ParagraphPosition pos );
void gtk_paragraph_set_params( Paragraph *par, ParagraphParams *params, ParagraphParamsValid *validity );
void gtk_paragraph_get_params( Paragraph *par, ParagraphParams *params, ParagraphParamsValid *validity );
void gtk_paragraph_get_params_match( Paragraph *par, ParagraphParams *params, ParagraphParamsValid *validity );


void gtk_paragraph_traverse( Paragraph *paragraph,
			     GtkRichTextTraversalCallbacks *callbacks,
			     gpointer data );

void gtk_paragraph_traverse_partial( Paragraph *paragraph,
				     gint start,
				     gint end,
				     GtkRichTextTraversalCallbacks *callbacks,
				     gpointer data );

gint gtk_paragraph_get_line_number( Paragraph *paragraph, ParagraphPosition pos );

GtkRichTextFont *gtk_paragraph_char_font( Paragraph *paragraph, ParagraphPosition pos );

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_WORD_H__ */

