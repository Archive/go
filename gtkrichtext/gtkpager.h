/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GTK_PAGER_H__
#define __GTK_PAGER_H__

#include <gdk/gdk.h>
#include <gtk/gtkadjustment.h>
#include <gtk/gtkwidget.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define GTK_PAGER(obj)          GTK_CHECK_CAST (obj, gtk_pager_get_type (), GtkPager)
#define GTK_PAGER_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, gtk_pager_get_type (), GtkPagerClass)
#define GTK_IS_PAGER(obj)       GTK_CHECK_TYPE (obj, gtk_pager_get_type ())


typedef struct _GtkPager        GtkPager;
typedef struct _GtkPagerClass   GtkPagerClass;

struct _GtkPager {
  GtkWidget widget;

  guint8 button;

  guint8 acceleration;

  GtkAdjustment *hadjust;
  GtkAdjustment *vadjust;

  gint hmove;
  gint vmove;
};

struct _GtkPagerClass {
  GtkWidgetClass parent_class;
};


GtkWidget*     gtk_pager_new                     (GtkAdjustment *hadjust, GtkAdjustment *vadjust);
guint          gtk_pager_get_type                (void);
GtkAdjustment* gtk_pager_get_hadjustment         (GtkPager      *pager);
GtkAdjustment* gtk_pager_get_vadjustment         (GtkPager      *pager);
void           gtk_pager_set_update_policy       (GtkPager      *pager, 
						       GtkUpdateType  policy);
void           gtk_pager_set_hadjustment         (GtkPager      *pager, 
						       GtkAdjustment *hadjust);
void           gtk_pager_set_vadjustment         (GtkPager      *pager,
						       GtkAdjustment *vadjust);
#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_PAGER_H__ */
