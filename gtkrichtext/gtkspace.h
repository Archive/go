/* gtkword.h
 * Copyright (C) 1998 Chris Lahey
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GTK_SPACE_H__
#define __GTK_SPACE_H__


#include <gdk/gdk.h>
#include <libhnj/hyphen.h>

typedef struct _Space              Space;
/* typedef gint                       WordPosition; */

struct _Space
{
  gchar *text;
  gint length;
};

void         gtk_word_init              (void);
Word *       gtk_word_new               (gchar               *text,
					 gint                 length,
					 GdkFont             *font);
WordPosition gtk_word_insert            (Word                *word,
					 WordPosition         position,
					 gint                 keyval);
void         gtk_word_delete            (Word                *word,
					 WordPosition         start,
					 WordPosition         end);
void         gtk_word_destroy           (Word                *word);
WordPosition gtk_word_increment_position(Word                *word,
					 WordPosition         old_pos,
					 gint                 distance);
WordPosition gtk_word_decrement_position(Word                *word,
					 WordPosition         old_pos, 
					 gint                 distance);
gint         gtk_word_position_compare  (Word                *word,
					 WordPosition         a,
					 WordPosition         b);
gboolean     gtk_word_position_equal    (Word                *word,
					 WordPosition         a,
					 WordPosition         b);
gint         gtk_word_length            (Word                *word);
WordPosition gtk_word_start             (Word                *word);
WordPosition gtk_word_end               (Word                *word);
Word *       gtk_word_concat            (Word                *worda,
					 Word                *wordb);

/* Returns a newly allocated word, consisting of the second half of the word.
Word        *gtk_word_break             (Word                *word,
                                         gint                 position);*/

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_WORD_H__ */

