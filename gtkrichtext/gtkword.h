/* gtkword.h
 * Copyright (C) 1998 Chris Lahey
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GTK_WORD_H__
#define __GTK_WORD_H__


#include <gdk/gdk.h>
#include "gtkrichtextcallbacks.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


typedef struct _Word               Word;
typedef gint                       WordPosition;
typedef struct _GtkWordFontSection GtkWordFontSection;

struct _GtkWordFontSection
{
  GtkRichTextFont *font;
  int start;
};
  
typedef enum
{
  GTK_SPACE_SPACE = 0,
  GTK_SPACE_TAB = 1
} GtkSpaceType;

struct _Word
{
  gchar *text;
  gint length;
  gint width;
  GtkRichTextFont *font;
  GtkRichText *parent;
  /* GList *fonts is of type GtkWordFontSection.  These describe font
     changes past the first.  They are guaranteed to be strongly
     increasing, but this isn't quite enforced yet. */
  GList *fonts;
  char *hyphenation;
  GtkSpaceType space;
};

void         gtk_word_init              (void);
Word *       gtk_word_new               (gchar               *text,
					 gint                 length,
					 GtkRichTextFont     *font,
					 GtkRichText         *parent);
WordPosition gtk_word_insert            (Word                *word,
					 WordPosition         position,
					 gchar               *vals,
					 gint                 length);
WordPosition gtk_word_insert_with_data  (Word                *word,
					 WordPosition         position,
					 gchar               *vals,
					 gint                 length,
					 GtkRichTextFont     *font);
void         gtk_word_delete            (Word                *word,
					 WordPosition         start,
					 WordPosition         end);
void         gtk_word_destroy           (Word                *word);
WordPosition gtk_word_increment_position(Word                *word,
					 WordPosition         old_pos,
					 gint                 distance);
WordPosition gtk_word_decrement_position(Word                *word,
					 WordPosition         old_pos, 
					 gint                 distance);
gint         gtk_word_position_compare  (Word                *word,
					 WordPosition         a,
					 WordPosition         b);
#define      gtk_word_position_equal( word, a, b ) ((a)==(b))
#define gtk_word_length(word) ((word)->length)
#define gtk_word_start(word) (0)
#define gtk_word_end(word) ((word)->length)
#define gtk_word_char_width(word, position) (gdk_char_width( gtk_word_char_font( (word), (position) )->font, (word)->text[(position)] ))

gboolean     gtk_word_test_start        (Word                *word,
					 WordPosition         position);
gboolean     gtk_word_test_end          (Word                *word,
					 WordPosition         position);
Word *       gtk_word_concat            (Word                *worda,
					 Word                *wordb);

/* Returns a newly allocated word, consisting of the second half of the word. */
Word        *gtk_word_break             (Word                *word,
					 WordPosition         position,
					 GtkSpaceType         space);

gboolean     gtk_word_can_insert_space  (Word                *word,
					 WordPosition         position);

#define gtk_word_position(word, integer) (integer)
#define gtk_word_position_to_int(word, position) (position)
#define gtk_word_a_first(word, a, b) ( (a) < (b) )

void         gtk_word_set_font         (Word                *word,
					 WordPosition         start,
					 WordPosition         end,
					 GtkRichTextFont     *font);

GtkRichTextFont *gtk_word_char_font     (Word                *word,
					 gint                 position);
gint         gtk_word_text_width        (Word                *word,
					 gint                 start,
					 gint                 end);
void         gtk_word_draw_text	        (Word                *word,
					 gint                 start,
					 gint                 end,
					 GdkDrawable         *drawable,
					 GdkGC         	     *gc,
					 gint    	      x,
					 gint		      y);
void         gtk_word_draw_text_with_selection
                                        (Word                *word,
					 gint                 start,
					 gint                 end,
					 GdkDrawable         *drawable,
					 GdkGC         	     *gc,
					 GdkGC         	     *gcsel,
					 gint    	      x,
					 gint		      y,
					 WordPosition         selstart,
					 WordPosition         selend,
					 gint                 negy,
					 gint                 posy);

void         gtk_word_get_chars         (Word                *word,
					 gchar               *buffer,
					 gint                 start,
					 gint                 end);
gint gtk_word_max_descent (Word *word, gint start, gint end);
gint gtk_word_max_ascent (Word *word, gint start, gint end);

void gtk_word_traverse( Word *word,
			GtkRichTextTraversalCallbacks *callbacks,
			gpointer data );

void gtk_word_traverse_partial( Word *word,
				WordPosition start,
				WordPosition end,
				GtkRichTextTraversalCallbacks *callbacks,
				gpointer data );

#define gtk_word_char_width_with_system(word, position, system) (system->char_width( system, gtk_word_char_font( (word), (position) ), (word)->text[(position)] ))
void         gtk_word_render (Word                *word,
			      gint                 start,
			      gint                 end,
			      GtkRichTextRenderSystem *system);
gdouble         gtk_word_text_width_with_system (Word                *word,
						 gint                 start,
						 gint                 end,
						 GtkRichTextRenderSystem *system);
gdouble gtk_word_max_descent_with_system (Word *word, gint start, gint end,
					  GtkRichTextRenderSystem *system);
gdouble gtk_word_max_ascent_with_system (Word *word, gint start, gint end,
					 GtkRichTextRenderSystem *system);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_WORD_H__ */

