/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include "gtkrichtext.h"
#include <gtk/gtkmain.h>
#include <gdk/gdk.h>
#include <gdk/gdkkeysyms.h>
#include <gtk/gtkselection.h>
#include <math.h>
#include <stdio.h>
#include <libhnj/hsjust.h>

#define SCALE 1
#define HYPHEN_PIXELS 20
#define HYPHEN_PENALTY ( (SCALE) * (SCALE) \
			 * (HYPHEN_PIXELS) * (HYPHEN_PIXELS) )
#define SCROLL_PIXELS            5
#define KEY_SCROLL_PIXELS        15

enum {
  FAILED_INSERT_TEXT,
  FAILED_DELETE_TEXT,
  SELECTION_CHANGED,
  LAST_SIGNAL
};


static GtkEditableClass *parent_class = NULL;
static guint rich_text_signals[LAST_SIGNAL] = { 0 };

/* #define DEBUG 1 */

#ifdef DEBUG
#define D(x) fprintf(stderr, x)
#else
#define D(x)
#endif

static void gtk_rich_text_move_beginning_of_line (GtkRichText *text);
static void gtk_rich_text_move_backward_character (GtkRichText *text);
static void gtk_rich_text_delete_forward_character (GtkRichText *text);
static void gtk_rich_text_move_end_of_line (GtkRichText *text);
static void gtk_rich_text_delete_backward_character (GtkRichText *text);
static void gtk_rich_text_delete_to_line_end (GtkRichText *text);
static void gtk_rich_text_move_forward_character (GtkRichText *text);
static void gtk_rich_text_move_next_line (GtkRichText *text);
static void gtk_rich_text_move_previous_line (GtkRichText *text);
static void gtk_rich_text_delete_line (GtkRichText *text);
static void gtk_rich_text_delete_backward_word (GtkRichText *text);
static void gtk_rich_text_move_backward_word (GtkRichText *text);
static void gtk_rich_text_delete_forward_word (GtkRichText *text);
static void gtk_rich_text_move_forward_word (GtkRichText *text);
static void gtk_rich_text_vertical_move_page (GtkRichText *text,
					      gint distance);
static void gtk_rich_text_move_beginning_of_buffer (GtkRichText *text);
static void gtk_rich_text_move_end_of_buffer (GtkRichText *text);
static void gtk_rich_text_locate_cursor( GtkRichText *text );
static void gtk_rich_text_cursor_coords( GtkRichText *text,
					 gint *x,
					 gint *y );
static void gtk_rich_text_select_word( GtkRichText *text );
static void gtk_rich_text_select_line( GtkRichText *text );
static void gtk_rich_text_set_font( GtkRichText *text,
				    Position start,
				    Position end,
				    GtkRichTextFont *font );

static ParagraphTree *find_next_paragraph( ParagraphTree *oldtree );
static ParagraphTree *find_previous_paragraph( ParagraphTree *oldtree );
static ParagraphTree *find_first_paragraph( ParagraphTree *oldtree );
static ParagraphTree *find_last_paragraph( ParagraphTree *oldtree );

static GtkTextFunction control_keys[26] =
{
  (GtkTextFunction)gtk_rich_text_move_beginning_of_line,    /* a */
  (GtkTextFunction)gtk_rich_text_move_backward_character,   /* b */
  (GtkTextFunction)gtk_editable_copy_clipboard,        /* c */
  (GtkTextFunction)gtk_rich_text_delete_forward_character,  /* d */
  (GtkTextFunction)gtk_rich_text_move_end_of_line,          /* e */
  (GtkTextFunction)gtk_rich_text_move_forward_character,    /* f */
  NULL,                                                /* g */
  (GtkTextFunction)gtk_rich_text_delete_backward_character, /* h */
  NULL,                                                /* i */
  NULL,                                                /* j */
  (GtkTextFunction)gtk_rich_text_delete_to_line_end,        /* k */
  NULL,                                                /* l */
  NULL,                                                /* m */
  (GtkTextFunction)gtk_rich_text_move_next_line,            /* n */
  NULL,                                                /* o */
  (GtkTextFunction)gtk_rich_text_move_previous_line,        /* p */
  NULL,                                                /* q */
  NULL,                                                /* r */
  NULL,                                                /* s */
  NULL,                                                /* t */
  (GtkTextFunction)gtk_rich_text_delete_line,               /* u */
  (GtkTextFunction)gtk_editable_paste_clipboard,       /* v */
  (GtkTextFunction)gtk_rich_text_delete_backward_word,      /* w */
  (GtkTextFunction)gtk_editable_cut_clipboard,         /* x */
  NULL,                                                /* y */
  NULL,                                                /* z */
};

static GtkTextFunction alt_keys[26] =
{
  NULL,                                                /* a */
  (GtkTextFunction)gtk_rich_text_move_backward_word,   /* b */
  NULL,                                                /* c */
  (GtkTextFunction)gtk_rich_text_delete_forward_word,  /* d */
  NULL,                                                /* e */
  (GtkTextFunction)gtk_rich_text_move_forward_word,    /* f */
  NULL,                                           /* g */
  NULL,                                           /* h */
  NULL,                                           /* i */
  NULL,                                           /* j */
  NULL,                                           /* k */
  NULL,                                           /* l */
  NULL,                                           /* m */
  NULL,                                           /* n */
  NULL,                                           /* o */
  NULL,                                           /* p */
  NULL,                                           /* q */
  NULL,                                           /* r */
  NULL,                                           /* s */
  NULL,                                           /* t */
  NULL,                                           /* u */
  NULL,                                           /* v */
  NULL,                                           /* w */
  NULL,                                           /* x */
  NULL,                                           /* y */
  NULL,                                           /* z */
};

static void gtk_rich_text_class_init    (GtkRichTextClass   *klass);
static void gtk_rich_text_init          (GtkRichText        *darea);
static void gtk_rich_text_realize       (GtkWidget          *widget);
static void gtk_rich_text_size_allocate (GtkWidget          *widget,
					 GtkAllocation      *allocation);
static void gtk_rich_text_send_configure (GtkRichText       *darea);
static gint gtk_rich_text_expose        (GtkWidget          *widget,
					 GdkEventExpose     *event);
static gint gtk_rich_text_button_press  (GtkWidget          *widget,
					 GdkEventButton     *event);
static gint gtk_rich_text_button_release(GtkWidget          *widget,
					 GdkEventButton     *event);
static gint gtk_rich_text_motion_notify (GtkWidget         *widget,
					 GdkEventMotion    *event);
/*
static gint gtk_rich_text_button_release(GtkWidget          *widget,
					 GdkEventButton     *event);
*/
static gint  gtk_rich_text_key_press         (GtkWidget         *widget,
                                         GdkEventKey       *event);
/*
static gint  gtk_rich_text_focus_in          (GtkWidget         *widget,
                                         GdkEventFocus     *event);
static gint  gtk_rich_text_focus_out         (GtkWidget         *widget,
                                         GdkEventFocus     *event);
*/
static void gtk_rich_text_draw          (GtkWidget          *widget,
					 GdkRectangle       *area);
static void expose_text                 (GtkRichText        *text, 
					 GdkRectangle       *area, 
					 gboolean            cursor);
static void calculate_selection         (GtkRichText        *text);
static void gtk_rich_text_adjustment    (GtkAdjustment      *adjustment,
					 GtkRichText        *text);
static void gtk_rich_text_disconnect    (GtkAdjustment      *adjustment,
					 GtkRichText        *text);
static void gtk_rich_text_destroy       (GtkObject          *object);
static void adjust_adjs                 (GtkRichText        *text);
static Position gtk_rich_text_find_point(GtkRichText        *text,
					 gint                x, 
					 gint                y);

static gchar *gtk_rich_text_get_chars  (GtkEditable       *editable,
					gint               start,
					gint               end);

static Position decrement_position(Position position,
				   gint distance);
static Position increment_position(Position position,
				   gint distance);
static Position vertical_move(GtkRichText *text,
			      Position position,
			      gint distance);

static Position gtk_rich_text_delete_internal(GtkRichText *text,
					      Position start,
					      Position end,
					      Position *maintain,
					      gint poscount);
static Position gtk_rich_text_insert_internal( GtkRichText *text,
					       Position position,
					       gchar *keyvals,
					       gint length,
					       Position *maintain,
					       gint poscount );

static void gtk_rich_text_set_position( GtkEditable *editable,
					int position );
static Position gtk_rich_text_position( GtkRichText *text,
					int position );
static void gtk_rich_text_insert_as_editable (GtkEditable   *editable,
					      const gchar   *new_text,
					      gint           new_text_length,
					      gint           *position);
static void gtk_rich_text_set_selection (GtkEditable        *editable,
					 gint                start,
					 gint                end);

static void gtk_rich_text_delete_as_editable (GtkEditable        *editable,
					      gint                start,
					      gint                end);
static void gtk_rich_text_update_text (GtkEditable        *editable,
				       gint                startpos,
				       gint                endpos);
static void gtk_rich_text_set_ideal( GtkRichText *text );
static gint motion_timeout( gpointer data );
static void gtk_rich_text_queue_redraw( GtkRichText *text );
static void gtk_rich_text_queue_redraw_with_rect( GtkRichText *text,
						  GdkRectangle *area);
static void gtk_rich_text_queue_redraw_between_positions( GtkRichText *text,
							  Position start,
							  Position end );
static gint redraw_timeout( gpointer data );
static void gtk_rich_text_insert_through_editable (GtkRichText *text,
						   gchar *vals,
						   gint length);
static void gtk_rich_text_set_editable_as_editable( GtkEditable   *editable,
						   gboolean is_editable );
static gint gtk_rich_text_top_of_position( GtkRichText *text,
					   Position pos );
static gint gtk_rich_text_top_of_cursor( GtkRichText *text);
static gint gtk_rich_text_bottom_of_position( GtkRichText *text,
					      Position pos );
static gint gtk_rich_text_bottom_of_cursor( GtkRichText *text);

static void
update_counts ( ParagraphTree *tree )
{
  while( tree )
    {
      tree->height =
	( tree->left ? tree->left->height : 0 ) +
	( tree->paragraph ?
	  gtk_paragraph_height( tree->paragraph ) :
	  0 ) +
	( tree->right ? tree->right->height : 0 );
      tree->count =
	( tree->left ? tree->left->count + 1 : 0 ) +
	( tree->paragraph ?
	  gtk_paragraph_length( tree->paragraph ) + 1 :
	  0 ) +
	( tree->right ? tree->right->count + 1 : 0 )
	- 1;
      tree->tree_depth = 1+
	MAX( (tree->left ? tree->left->tree_depth : -1),
	     (tree->right ? tree->right->tree_depth : -1 ) );
      tree = tree->parent;
    }
}

static void
update_all_counts( ParagraphTree *tree )
{
  if ( tree->left )
    update_all_counts( tree->left );
  if ( tree->right )
    update_all_counts( tree->right );
  
  tree->height =
    ( tree->left ? tree->left->height : 0 ) +
    ( tree->paragraph ?
      gtk_paragraph_height( tree->paragraph ) :
      0 ) +
    ( tree->right ? tree->right->height : 0 );
  tree->count =
    ( tree->left ? tree->left->count + 1 : 0 ) +
    ( tree->paragraph ?
      gtk_paragraph_length( tree->paragraph ) + 1 :
	  0 ) +
    ( tree->right ? tree->right->count + 1 : 0 )
    - 1;
  tree->tree_depth = 1+
    MAX( (tree->left ? tree->left->tree_depth : -1),
	 (tree->right ? tree->right->tree_depth : -1 ) );
}

static void
update_this_count( ParagraphTree *tree )
{
  tree->height =
    ( tree->left ? tree->left->height : 0 ) +
    ( tree->paragraph ?
      gtk_paragraph_height( tree->paragraph ) :
      0 ) +
    ( tree->right ? tree->right->height : 0 );
  tree->count =
    ( tree->left ? tree->left->count + 1 : 0 ) +
    ( tree->paragraph ?
      gtk_paragraph_length( tree->paragraph ) + 1 :
	  0 ) +
    ( tree->right ? tree->right->count + 1 : 0 )
    - 1;
  tree->tree_depth = 1+
    MAX( (tree->left ? tree->left->tree_depth : -1),
	 (tree->right ? tree->right->tree_depth : -1 ) );
}

static void
gtk_rich_text_real_set_position (GtkRichText *text,
				 Position pos)
{
  GtkEditable *editable = GTK_EDITABLE( text );
  editable->current_pos = gtk_rich_text_position_to_int( text, pos );
  gtk_editable_select_region( editable,
			      editable->current_pos, editable->current_pos );
  text->selection_end = text->cursor = pos;
  text->selection_active = FALSE;
  text->cursor_first = TRUE;
  if ( text->cursor_font != NULL )
    gtk_rich_text_font_unref( text->cursor_font );
  text->cursor_font = NULL;  
  gtk_rich_text_locate_cursor( text );
  if ( ! text->incremental_redraw_handled )
    gtk_rich_text_queue_redraw( text );
  gtk_signal_emit( GTK_OBJECT (text),
		   rich_text_signals[SELECTION_CHANGED],
		   text->cursor_first ? text->cursor : text->selection_end,
		   text->cursor_first ? text->selection_end : text->cursor );
}

static void
gtk_rich_text_real_set_position_with_int (GtkRichText *text,
					  Position pos,
					  gint with_int)
{
  GtkEditable *editable = GTK_EDITABLE( text );
  editable->current_pos = with_int;
  gtk_editable_select_region( editable,
			      editable->current_pos, editable->current_pos );
  text->selection_end = text->cursor = pos;
  text->selection_active = FALSE;
  text->cursor_first = TRUE;
  if ( text->cursor_font != NULL )
    gtk_rich_text_font_unref( text->cursor_font );
  text->cursor_font = NULL;  
  gtk_rich_text_locate_cursor( text );  
  gtk_rich_text_queue_redraw( text );
}

guint
gtk_rich_text_get_type (void)
{
  static guint rich_text_type = 0;

  if (!rich_text_type)
    {
      GtkTypeInfo rich_text_info =
      {
	"GtkRichText",
	sizeof (GtkRichText),
	sizeof (GtkRichTextClass),
	(GtkClassInitFunc) gtk_rich_text_class_init,
	(GtkObjectInitFunc) gtk_rich_text_init,
	/* reserved_1 */ NULL,
        /* reserved_2 */ NULL,
        (GtkClassInitFunc) NULL,
      };

      rich_text_type = gtk_type_unique (GTK_TYPE_EDITABLE, &rich_text_info);
    }

  return rich_text_type;
}

static void
gtk_rich_text_class_init (GtkRichTextClass *class)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;
  GtkEditableClass *editable_class;

  gtk_word_init();

  object_class = (GtkObjectClass*) class;
  widget_class = (GtkWidgetClass*) class;
  editable_class = (GtkEditableClass*) class;

  parent_class = gtk_type_class (GTK_TYPE_EDITABLE);
  
  class->failed_insert_text = NULL;
  class->failed_delete_text = NULL;
  class->selection_changed = NULL;

  widget_class->realize = gtk_rich_text_realize;
  widget_class->size_allocate = gtk_rich_text_size_allocate;
  widget_class->draw = gtk_rich_text_draw;
  widget_class->expose_event = gtk_rich_text_expose;
  widget_class->button_press_event = gtk_rich_text_button_press;
  widget_class->button_release_event = gtk_rich_text_button_release;
  widget_class->motion_notify_event = gtk_rich_text_motion_notify;
  widget_class->key_press_event = gtk_rich_text_key_press;
  ((GtkObjectClass*) class)->destroy = gtk_rich_text_destroy;

  editable_class->set_editable = gtk_rich_text_set_editable_as_editable;
  editable_class->insert_text = gtk_rich_text_insert_as_editable;
  editable_class->delete_text = gtk_rich_text_delete_as_editable;
      
  /*  editable_class->move_cursor = gtk_rich_text_move_cursor;
      editable_class->move_word = gtk_rich_text_move_word;
      editable_class->move_page = gtk_rich_text_move_page;
      editable_class->move_to_row = gtk_rich_text_move_to_row;
      editable_class->move_to_column = gtk_rich_text_move_to_column;
    
      editable_class->kill_char = gtk_rich_text_kill_char;
      editable_class->kill_word = gtk_rich_text_kill_word;
      editable_class->kill_line = gtk_rich_text_kill_line;
  */
  editable_class->update_text = gtk_rich_text_update_text; 
  editable_class->get_chars = gtk_rich_text_get_chars;
  editable_class->set_selection = gtk_rich_text_set_selection;
  editable_class->set_position = gtk_rich_text_set_position;


  rich_text_signals[FAILED_INSERT_TEXT] =
    gtk_signal_new ("failed_insert_text",
		    GTK_RUN_LAST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GtkRichTextClass, failed_insert_text),
		    gtk_marshal_NONE__POINTER_INT_POINTER,
		    GTK_TYPE_NONE,
		    3,
		    GTK_TYPE_STRING,
		    GTK_TYPE_INT,
		    GTK_TYPE_POINTER);

  rich_text_signals[FAILED_DELETE_TEXT] =
    gtk_signal_new ("failed_delete_text",
		    GTK_RUN_LAST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GtkRichTextClass, failed_delete_text),
		    gtk_marshal_NONE__INT_INT,
		    GTK_TYPE_NONE,
		    2,
		    GTK_TYPE_INT,
		    GTK_TYPE_INT);

  rich_text_signals[SELECTION_CHANGED] =
    gtk_signal_new ("selection_changed",
		    GTK_RUN_LAST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GtkRichTextClass, selection_changed),
		    gtk_marshal_NONE__POINTER_POINTER,
		    GTK_TYPE_NONE,
		    2,
		    GTK_TYPE_POINTER,
		    GTK_TYPE_POINTER);

  gtk_object_class_add_signals (object_class, rich_text_signals, LAST_SIGNAL);
#if 0
  void (* changed)      (GtkEditable    *editable);

  /* Bindings actions */
  void (* activate)        (GtkEditable *editable);
  void (* set_editable)    (GtkEditable *editable,
			    gboolean	 is_editable);
  void (* move_cursor)     (GtkEditable *editable,
			    gint         x,
			    gint         y);
  void (* move_word)       (GtkEditable *editable,
			    gint         n);
  void (* move_page)       (GtkEditable *editable,
			    gint         x,
			    gint         y);
  void (* move_to_row)     (GtkEditable *editable,
			    gint         row);
  void (* move_to_column)  (GtkEditable *editable,
			    gint         row);
  void (* kill_char)       (GtkEditable *editable,
			    gint         direction);
  void (* kill_word)       (GtkEditable *editable,
			    gint         direction);
  void (* kill_line)       (GtkEditable *editable,
			    gint         direction);
  void (* cut_clipboard)   (GtkEditable *editable);
  void (* copy_clipboard)  (GtkEditable *editable);
  void (* paste_clipboard) (GtkEditable *editable);

  /* Virtual functions. get_chars is in paricular not a signal because
   * it returns malloced memory. The others are not signals because
   * they would not be particularly useful as such. (All changes to
   * selection and position do not go through these functions)
   */
  void (* update_text)  (GtkEditable    *editable,
			 gint            start_pos,
			 gint            end_pos);
#endif
}

static void
gtk_rich_text_update_text( GtkEditable *editable, gint startpos, gint endpos )
{
  gtk_rich_text_queue_redraw( GTK_RICH_TEXT( editable ) );
}

static void
gtk_rich_text_init (GtkRichText *darea)
{
}

GtkWidget*
gtk_rich_text_new (GtkAdjustment *hadj,
		   GtkAdjustment *vadj)
{
  GtkWidget *new_widget =
    GTK_WIDGET (gtk_type_new (gtk_rich_text_get_type ()));
  GtkRichText *text = GTK_RICH_TEXT( new_widget );
  Position initpos;
  gint i;

  text->is_editable = TRUE;
  text->line_wrap = TRUE;
  text->word_wrap = TRUE;
  text->frozen = FALSE;

  text->draw_data = NULL;
  text->minimum_width = 0;
  text->width = -1;
  text->hadj = NULL;
  text->vadj = NULL;
  text->paragraphs = g_new( ParagraphTree, 1 );
  text->paragraphs->count = text->paragraphs->height = 0;
  text->paragraphs->left = text->paragraphs->right = text->paragraphs->parent = NULL;
  text->paragraphs->paragraph = gtk_paragraph_new( text );
  text->paragraphs->tree_depth = 0;
  gtk_paragraph_set_width( (Paragraph *) text->paragraphs->paragraph, 200 );

  text->redraw_area.x = 0;
  text->redraw_area.y = 0;
  text->redraw_area.width = 0;
  text->redraw_area.height = 0;
  
  text->incremental_redraw_handled = FALSE;
  
  text->cursor_font = NULL;

  gtk_rich_text_set_adjustments ( text, hadj, vadj);

  initpos.paragraph = text->paragraphs;
  initpos.paragraph_ptr = gtk_paragraph_start( text->paragraphs->paragraph );
  gtk_rich_text_real_set_position( text, initpos );
  text->point_valid = FALSE;
  text->ideal_valid = FALSE;
  
  gtk_widget_set_events(new_widget,
			gtk_widget_get_events(new_widget) |
			GDK_BUTTON_PRESS_MASK |
			GDK_BUTTON1_MOTION_MASK |
			GDK_BUTTON2_MOTION_MASK |
			GDK_BUTTON3_MOTION_MASK |
			GDK_BUTTON_RELEASE_MASK |
			GDK_KEY_PRESS_MASK);

  GTK_WIDGET_SET_FLAGS( new_widget, GTK_CAN_FOCUS );

  return new_widget;
}

guint
gtk_rich_text_get_length      (GtkRichText       *text)
{
  return text->paragraphs->count;
}

static void
gtk_rich_text_set_ideal( GtkRichText *text )
{
  if ( text->ideal_valid == FALSE )
    {
      text->ideal =
	gtk_paragraph_find_ideal( text->cursor.paragraph->paragraph,
				  text->cursor.paragraph_ptr );
      text->ideal_valid = TRUE;
    }
}

void
gtk_rich_text_freeze          (GtkRichText       *text)
{
  text->frozen = TRUE;
}

void
gtk_rich_text_thaw            (GtkRichText       *text)
{
  text->frozen = FALSE;
  gtk_rich_text_queue_redraw( text );
  adjust_adjs( text );
}

void
gtk_rich_text_set_point       (GtkRichText       *text,
			       guint              index)
{
  D( "Call to gtk_rich_text_set_point.\n" );
  text->point = gtk_rich_text_position( text, index );
  text->point_valid = TRUE;
}

guint
gtk_rich_text_get_point       (GtkRichText       *text)
{
  if ( text->point_valid )
    return gtk_rich_text_position_to_int( text, text->point );
  else
    return 0;
}

void
gtk_rich_text_set_editable    (GtkRichText       *text,
			       gboolean           editable)
{
  text->is_editable = editable;
}

static void
gtk_rich_text_set_editable_as_editable( GtkEditable   *editable,
					gboolean is_editable )
{
  gtk_rich_text_set_editable( GTK_RICH_TEXT( editable ), is_editable );
}

void
gtk_rich_text_set_word_wrap   (GtkRichText       *text,
			       gint               word_wrap)
{
  text->word_wrap = word_wrap;
}

void
gtk_rich_text_set_line_wrap   (GtkRichText       *text,
			       gint               line_wrap)
{
  text->line_wrap = line_wrap;
}

static void
gtk_rich_text_set_position( GtkEditable *editable, int position )
{
  GtkRichText *text = GTK_RICH_TEXT (editable);
  g_return_if_fail (editable != NULL);
  g_return_if_fail (GTK_IS_RICH_TEXT (text));

  D( "Entering gtk_rich_text_set_position.\n" );

  gtk_rich_text_real_set_position_with_int( text,
					    gtk_rich_text_position( text,
								    position ),
					    position );
  text->ideal_valid = FALSE;
  
  D( "Leaving gtk_rich_text_set_position.\n" );
}

static Position
gtk_rich_text_position( GtkRichText *text, int position )
{
  ParagraphTree *paragraph = text->paragraphs;
  Position rval;
  while ( paragraph )
    {
      if ( paragraph->left )
	{
	  if ( position <= paragraph->left->count )
	    {
	      paragraph = paragraph->left;
	      continue;
	    }
	  else
	    {
	      position -= paragraph->left->count + 1;
	    }
	}
      if ( paragraph->paragraph )
	{
	  if ( position <= gtk_paragraph_length ( paragraph->paragraph ) )
	    {
	      rval.paragraph = paragraph;
	      rval.paragraph_ptr = gtk_paragraph_position( paragraph->paragraph, position );
	      return rval;
	    }
	  else
	    {
	      position -= gtk_paragraph_length ( paragraph->paragraph ) + 1;
	    }
	}
      paragraph = paragraph->right;
    }
  rval.paragraph = NULL;
  return rval;
}

gint
gtk_rich_text_position_to_int( GtkRichText *text, Position pos )
{
  ParagraphTree *paragraph;
  ParagraphTree *last_paragraph = NULL;
  gint rval;

  if ( pos.paragraph == NULL )
    return 0;
  
  paragraph = pos.paragraph;
  rval = gtk_paragraph_position_to_int( pos.paragraph->paragraph, pos.paragraph_ptr );
  if ( paragraph->left )
    rval += paragraph->left->count + 1;
  last_paragraph = paragraph;
  paragraph = paragraph->parent;

  while ( paragraph )
    {
      if ( paragraph->right == last_paragraph )
	{
	  if ( paragraph->paragraph )
	    {
	      rval += gtk_paragraph_length( paragraph->paragraph ) + 1;
	    }
	  if ( paragraph->left )
	    {
	      rval += paragraph->left->count + 1;
	    }
	}
      last_paragraph = paragraph;
      paragraph = paragraph->parent;
    }
  return rval;
}

void
gtk_rich_text_set_adjustments (GtkRichText   *text,
			       GtkAdjustment *hadj,
			       GtkAdjustment *vadj)
{
  g_return_if_fail (text != NULL);
  g_return_if_fail (GTK_IS_RICH_TEXT (text));

  D( "Entering gtk_rich_text_set_adjustments.\n" );
  
  if (text->hadj && (text->hadj != hadj))
    {
      gtk_signal_disconnect_by_data (GTK_OBJECT (text->hadj), text);
      gtk_object_unref (GTK_OBJECT (text->hadj));
    }
  
  if (text->vadj && (text->vadj != vadj))
    {
      gtk_signal_disconnect_by_data (GTK_OBJECT (text->vadj), text);
      gtk_object_unref (GTK_OBJECT (text->vadj));
    }
  
  if (!hadj)
    hadj = GTK_ADJUSTMENT (gtk_adjustment_new (0.0, 0.0, 0.0, 0.0, 0.0, 0.0));
  
  if (!vadj)
    vadj = GTK_ADJUSTMENT (gtk_adjustment_new (0.0, 0.0, 0.0, 0.0, 0.0, 0.0));
  
  if (text->hadj != hadj)
    {
      text->hadj = hadj;
      gtk_object_ref (GTK_OBJECT (text->hadj));
      gtk_object_sink (GTK_OBJECT (text->hadj));
      
      gtk_signal_connect (GTK_OBJECT (text->hadj), "changed",
			  (GtkSignalFunc) gtk_rich_text_adjustment,
			  text);
      gtk_signal_connect (GTK_OBJECT (text->hadj), "value_changed",
			  (GtkSignalFunc) gtk_rich_text_adjustment,
			  text);
      gtk_signal_connect (GTK_OBJECT (text->hadj), "disconnect",
			  (GtkSignalFunc) gtk_rich_text_disconnect,
			  text);
    }
  
  if (text->vadj != vadj)
    {
      text->vadj = vadj;
      gtk_object_ref (GTK_OBJECT (text->vadj));
      gtk_object_sink (GTK_OBJECT (text->vadj));
      
      gtk_signal_connect (GTK_OBJECT (text->vadj), "changed",
			  (GtkSignalFunc) gtk_rich_text_adjustment,
			  text);
      gtk_signal_connect (GTK_OBJECT (text->vadj), "value_changed",
			  (GtkSignalFunc) gtk_rich_text_adjustment,
			  text);
      gtk_signal_connect (GTK_OBJECT (text->vadj), "disconnect",
			  (GtkSignalFunc) gtk_rich_text_disconnect,
			  text);
    }
  D( "Leaving gtk_rich_text_set_adjustments.\n" );
}

static void
gtk_rich_text_adjustment (GtkAdjustment *adjustment,
			  GtkRichText       *text)
{
  g_return_if_fail (adjustment != NULL);
  g_return_if_fail (GTK_IS_ADJUSTMENT (adjustment));
  g_return_if_fail (text != NULL);
  g_return_if_fail (GTK_IS_RICH_TEXT (text));

  D( "Entering gtk_rich_text_adjustment.\n" );
  
  /* Just ignore it if we haven't been size-allocated yet, or
   * if something weird has happened */
  if ((GTK_WIDGET (text)->allocation.height <= 1) || 
      (GTK_WIDGET (text)->allocation.width <= 1))
    return;
  
  if (adjustment == text->hadj)
    {
      if ( text->xoff != ((gint)adjustment->value) )
	{
	  text->xoff = ((gint)adjustment->value);
	  gtk_rich_text_queue_redraw(text);
	}
    }
  else
    {
      if ( text->yoff != ((gint)adjustment->value) )
	{
	  text->yoff = ((gint)adjustment->value);
	  gtk_rich_text_queue_redraw(text);
	}
    }
  
  D( "Leaving gtk_rich_text_adjustment.\n" );
  
}

static void
gtk_rich_text_disconnect (GtkAdjustment *adjustment,
			  GtkRichText       *text)
{
  g_return_if_fail (adjustment != NULL);
  g_return_if_fail (GTK_IS_ADJUSTMENT (adjustment));
  g_return_if_fail (text != NULL);
  g_return_if_fail (GTK_IS_RICH_TEXT (text));

  D( "Entering gtk_rich_text_disconnect.\n" );
  
  if (adjustment == text->hadj)
    text->hadj = NULL;
  if (adjustment == text->vadj)
    text->vadj = NULL;

  D( "Leaving gtk_rich_text_disconnect.\n" );
}

static void
gtk_rich_text_paragraph_set_width (GtkRichText *text,
				   ParagraphTree *tree)
{
  while ( 1 )
    {
      if ( tree->paragraph )
	{
	  if ( text->width == -1 )
	    gtk_paragraph_set_width( tree->paragraph,
				     MAX
				     ( GTK_WIDGET(text)->allocation.width * SCALE,
				       text->minimum_width ) );
	  else
	    gtk_paragraph_set_width( tree->paragraph, text->width );
	}
      
      if ( tree->left )
	gtk_rich_text_paragraph_set_width ( text, tree->left );
      if ( ! tree->right )
	break;
      tree = tree->right;
    }
  update_counts( tree );
}

void
gtk_rich_text_set_minimum_width (GtkRichText *text,
				 gint         min_width)
{
  GList *list;
  Paragraph *par;

  D( "Entering gtk_rich_text_set_minimum_width.\n" );

  if ( text->minimum_width != min_width )
    {
      text->minimum_width = min_width;
      
      gtk_rich_text_paragraph_set_width ( text, text->paragraphs );
     
      adjust_adjs(text);
      gtk_rich_text_queue_redraw(text);
    }
  
  D( "Leaving gtk_rich_text_set_minimum_width.\n" );
}

void
gtk_rich_text_set_width (GtkRichText *text,
			 gint         width)
{
  GList *list;
  Paragraph *par;

  D( "Entering gtk_rich_text_set_width.\n" );

  if ( width != text->width )
    {
      text->width = width;

      gtk_rich_text_paragraph_set_width ( text, text->paragraphs );

      adjust_adjs(text);
      gtk_rich_text_queue_redraw(text);
    }
  
  D( "Leaving gtk_rich_text_set_width.\n" );
}

gint
gtk_rich_text_get_width (GtkRichText *text)
{
  return text->width;
}

gint
gtk_rich_text_forward_delete(GtkRichText *text, guint nchars)
{
  Position end = increment_position( text->point, nchars );
  Position poss[2];

  D( "Entering gtk_rich_text_forward_delete.\n" );

  if ( ! text->point_valid )
    {
      text->point = gtk_rich_text_position( text, 0 );
      text->point_valid = TRUE;
    }

  poss[0] = text->cursor;
  poss[1] = text->selection_end;
   
  text->point = gtk_rich_text_delete_internal( text, text->point, end,
					       poss, 2 );
  text->ideal_valid = FALSE;
 
  text->cursor = poss[0];
  text->selection_end = poss[1];
  GTK_EDITABLE( text )->current_pos =
    gtk_rich_text_position_to_int( text, text->cursor );
   
  if ( ! text->frozen )
    {
      adjust_adjs( text );
      gtk_rich_text_locate_cursor( text );
      gtk_rich_text_queue_redraw( text );
    }

  D( "Leaving gtk_rich_text_forward_delete.\n" );
  
  return TRUE;
}

gint
gtk_rich_text_backward_delete(GtkRichText *text, guint nchars )
{
  Position start = decrement_position( text->cursor, nchars );
  
  Position poss[2];
   
  D( "Entering gtk_rich_text_backward_delete.\n" );

  if ( ! text->point_valid )
    {
      text->point = gtk_rich_text_position( text, 0 );
      text->point_valid = TRUE;
    }

  poss[0] = text->cursor;
  poss[1] = text->selection_end;
  
  text->point = gtk_rich_text_delete_internal( text, start, text->cursor,
					       poss, 2 );
  text->ideal_valid = FALSE;
 
  text->cursor = poss[0];
  text->selection_end = poss[1];
  GTK_EDITABLE( text )->current_pos =
    gtk_rich_text_position_to_int( text, text->cursor );
  
  if ( ! text->frozen )
    {
      adjust_adjs( text );
      gtk_rich_text_locate_cursor( text );
      gtk_rich_text_queue_redraw( text );
    }

  D( "Leaving gtk_rich_text_backward_delete.\n" );
  
  return TRUE;
}

static void
gtk_rich_text_realize (GtkWidget *widget)
{
  GtkRichText *text;
  GdkWindowAttr attributes;
  gint attributes_mask;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_RICH_TEXT (widget));

  D( "Entering gtk_rich_text_realize.\n" );

  text = GTK_RICH_TEXT (widget);
  GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);

  attributes.window_type = GDK_WINDOW_CHILD;
  attributes.x = widget->allocation.x;
  attributes.y = widget->allocation.y;
  attributes.width = widget->allocation.width;
  attributes.height = widget->allocation.height;
  attributes.wclass = GDK_INPUT_OUTPUT;
  attributes.visual = gtk_widget_get_visual (widget);
  attributes.colormap = gtk_widget_get_colormap (widget);
  attributes.event_mask = gtk_widget_get_events (widget) | GDK_EXPOSURE_MASK;

  attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;

  widget->window = gdk_window_new (gtk_widget_get_parent_window (widget),
				   &attributes, attributes_mask);
  gdk_window_set_user_data (widget->window, text);

  widget->style = gtk_style_attach (widget->style, widget->window);
  gtk_style_set_background (widget->style, widget->window, GTK_STATE_NORMAL);

  gtk_rich_text_send_configure (GTK_RICH_TEXT (widget));

  ( (Paragraph *) text->paragraphs->paragraph)->params.hnjparams.max_neg_space
    = 0 * SCALE;
  ( (Paragraph *) text->paragraphs->paragraph)->params.hnjparams.tab_width
    = 36 * SCALE;
  adjust_adjs( text );

  D( "Leaving gtk_rich_text_realize.\n" );
}

static void
gtk_rich_text_size_allocate (GtkWidget     *widget,
				GtkAllocation *allocation)
{
  GList *list;
  Paragraph *par;
  GtkRichText *text;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_RICH_TEXT (widget));
  g_return_if_fail (allocation != NULL);

  D( "Entering gtk_rich_text_size_allocate.\n" );

  text = GTK_RICH_TEXT( widget );

  widget->allocation = *allocation;

  if (GTK_WIDGET_REALIZED (widget))
    {
      gdk_window_move_resize (widget->window,
			      allocation->x, allocation->y,
			      allocation->width, allocation->height);

      gtk_rich_text_send_configure (GTK_RICH_TEXT (widget));
    }

  gtk_rich_text_paragraph_set_width( text, text->paragraphs );
  
  adjust_adjs( GTK_RICH_TEXT( widget ) );

  D( "Leaving gtk_rich_text_size_allocate.\n" );
}

static void
gtk_rich_text_send_configure (GtkRichText *darea)
{
  GtkWidget *widget;
  GdkEventConfigure event;

  D( "Entering gtk_rich_text_send_configure.\n" );

  widget = GTK_WIDGET (darea);

  event.type = GDK_CONFIGURE;
  event.window = widget->window;
  event.x = widget->allocation.x;
  event.y = widget->allocation.y;
  event.width = widget->allocation.width;
  event.height = widget->allocation.height;
  
  gtk_widget_event (widget, (GdkEvent*) &event);

  D( "Leaving gtk_rich_text_send_configure.\n" );
}

static void
gtk_rich_text_draw (GtkWidget    *widget,
		    GdkRectangle *area)
{
  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_RICH_TEXT (widget));
  g_return_if_fail (area != NULL);

  D( "Entering gtk_rich_text_draw.\n" );
  
  if (GTK_WIDGET_DRAWABLE (widget))
    {
      gtk_rich_text_queue_redraw_with_rect (GTK_RICH_TEXT (widget), area);
      gtk_widget_draw_focus (widget);
    }

  D( "Leaving gtk_rich_text_draw.\n" );
}

static gint
gtk_rich_text_expose (GtkWidget    *widget,
		      GdkEventExpose *event)
{
  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_RICH_TEXT (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);

  D( "Entering gtk_rich_text_expose.\n" );

  gtk_rich_text_queue_redraw_with_rect (GTK_RICH_TEXT (widget), &event->area);

  D( "Leaving gtk_rich_text_expose.\n" );
  
  return FALSE;
}

static void
calculate_selection (GtkRichText *text)
{
  ParagraphTree *cursor_tree;
  ParagraphTree *selection_tree;
  ParagraphTree *last_tree;
  
  D( "Entering calculate_selection.\n" );

  if ( GTK_WIDGET_REALIZED( GTK_WIDGET( text ) ) )
    {
      cursor_tree = text->cursor.paragraph;
      selection_tree = text->selection_end.paragraph;
      if ( cursor_tree == selection_tree )
	{
	  Paragraph *paragraph = text->cursor.paragraph->paragraph;
	  if ( gtk_paragraph_position_equal
	       ( paragraph,
		 text->cursor.paragraph_ptr,
		 text->selection_end.paragraph_ptr ) )
	    text->cursor_first = text->selection_active = FALSE;
	  else
	    text->cursor_first
	      = gtk_paragraph_a_first( paragraph,
				       text->cursor.paragraph_ptr,
				       text->selection_end.paragraph_ptr );
	  return;
	}
      if ( selection_tree )
	{
	  last_tree = selection_tree;
	  selection_tree = selection_tree->parent;
	}
      while( selection_tree )
	{
	  if ( selection_tree == cursor_tree )
	    {
	      text->cursor_first = ( last_tree == selection_tree->right );
	      return;
	    }
	  last_tree = selection_tree;
	  selection_tree = selection_tree->parent;
	}
      /* This algorithm might be sped up by using a hash table to
         store the set of ParagraphTree *s that are parents of
         text->selection_end.paragraph. That would take it from a
         log^2(n) to a log(n), but might increase the constant.  If
         you add this, profile it.*/
      if ( cursor_tree )
	{
	  last_tree = cursor_tree;
	  cursor_tree = cursor_tree->parent;
	}
      while ( cursor_tree )
	{
	  selection_tree = text->selection_end.paragraph;
	  while( selection_tree )
	    {
	      if ( selection_tree == cursor_tree )
		{
		  text->cursor_first = ( last_tree == cursor_tree->left );
		  return;
		}
	      selection_tree = selection_tree->parent;
	    }
	  last_tree = cursor_tree;
	  cursor_tree = cursor_tree->parent;
	}
    }

  D( "Leaving calculate_selection.\n" );
}

/* ParentList is a structure to keep the list of parents of a
   particular ParagraphTree.  It is mostly meant to be used for
   ParagraphTrees stored in Positions.  It's meant for use during
   expose_text, but might be adapted usefully for calculate_selection,
   especially if ParentLists get cached.  */
typedef struct _ParentList ParentList;

struct _ParentList
{
  gint left_child_bits;
  gint length;
};

static ParentList
build_parent_list( ParagraphTree *tree )
{
  ParentList list;
  ParagraphTree *last = NULL;
  list.length = 0;
  list.left_child_bits = 0;
  last = tree;
  tree = tree->parent;
  while( tree )
    {
      list.length ++;
      list.left_child_bits <<= 1;
      if ( last == tree->left )
	list.left_child_bits ++;
      last = tree;
      tree = tree->parent;
    }
  return list;
};

static void
expose_text_paragraph (GtkRichText* text,
		       ParagraphTree *tree,
		       gint y,
		       GdkPixmap *pixmap,
		       gboolean in_selection,
		       ParentList firstlist,
		       ParentList secondlist,
		       GdkRectangle *area,
		       gboolean cursor)
{
  Position first
    = text->cursor_first ? text->cursor : text->selection_end;
  Position second
    = text->cursor_first ? text->selection_end : text->cursor;
  GdkGC *gc
    = GTK_WIDGET( text )->style->text_gc[ GTK_WIDGET( text )->state ];
  GdkGC *gcsel
    = GTK_WIDGET( text )->style->dark_gc[ GTK_WIDGET( text )->state ];
  while ( tree
	  && ( ( tree->left ? tree->left->height : 0 ) +
	       ( tree->paragraph ? gtk_paragraph_height( tree->paragraph ) : 0 )
	       + y
	       <= text->yoff + area->y ) )
    {
      y += ( tree->left ? tree->left->height : 0 ) +
	( tree->paragraph ? gtk_paragraph_height( tree->paragraph ) : 0 );
      if ( firstlist.length > 0 )
	{
	  if ( firstlist.left_child_bits & 0x1 )
	    {
	      in_selection = TRUE;
	      firstlist.length = 0; /* Will be decremented. */
	    }
	  firstlist.length --;
	  firstlist.left_child_bits >>= 1;
	}
      else if ( firstlist.length == 0 )
	{
	  in_selection = TRUE;
	  firstlist.length = -1;
	}
      if ( secondlist.length > 0 )
	{
	  if ( secondlist.left_child_bits & 0x1 )
	    {
	      in_selection = FALSE;
	      secondlist.length = 0; /* Will be decremented. */
	    }
	  secondlist.length --;
	  secondlist.left_child_bits >>= 1;
	}
      else if ( secondlist.length == 0 )
	{
	  in_selection = FALSE;
	  secondlist.length = -1;
	}
      tree = tree->right;	
    }
  while ( tree )
    {
      if ( tree->left )
	{
	  if ( y < text->yoff + area->y + area->height
	       && y + tree->left->height
	       > text->yoff + area->y ) 
	    {
	      ParentList nextfirst = firstlist;
	      ParentList nextsecond = secondlist;
	      if ( nextfirst.length > 0 )
		{
		  if ( !( nextfirst.left_child_bits & 0x1 ) )
		    nextfirst.length = 0; /* Will be decremented. */
		  nextfirst.length --;
		  nextfirst.left_child_bits >>= 1;
		}
	      else if ( nextfirst.length == 0 )
		nextfirst.length = -1;
	      if ( nextsecond.length > 0 )
		{
		  if ( !( nextsecond.left_child_bits & 0x1 ) )
		    nextsecond.length = 0; /* Will be decremented. */
		  nextsecond.length --;
		  nextsecond.left_child_bits >>= 1;
		}
	      else if ( nextsecond.length == 0 )
		nextsecond.length = -1;
	      expose_text_paragraph( text, tree->left, y, pixmap,
				     in_selection,
				     nextfirst, nextsecond,
				     area, cursor );
	    }
	  y += tree->left->height;
	}
      if ( firstlist.length > 0 )
	{
	  if ( firstlist.left_child_bits & 0x1 )
	    {
	      in_selection = TRUE;
	      firstlist.length = 0; /* Will be decremented. */
	    }
	  firstlist.length --;
	  firstlist.left_child_bits >>= 1;
	}
      else if ( firstlist.length == 0 )
	firstlist.length = -1;
      if ( secondlist.length > 0 )
	{
	  if ( secondlist.left_child_bits & 0x1 )
	    {
	      in_selection = FALSE;
	      secondlist.length = 0; /* Will be decremented. */
	    }
	  secondlist.length --;
	  secondlist.left_child_bits >>= 1;
	}
      else if ( secondlist.length == 0 )
	secondlist.length = -1;

      if ( tree->paragraph )
	{
	  gint jump = ((tree == first.paragraph ) ? 1 : 0 )
	    + (( tree == second.paragraph ) ? 2 : 0 );
	  if ( y < text->yoff + area->y + area->height
	       && y + gtk_paragraph_height( tree->paragraph )
	       > text->yoff + area->y )
	    {
	      Paragraph *paragraph = tree->paragraph;
	      switch( jump )
		{
		case 0:
		  gtk_paragraph_draw( paragraph, - text->hadj->value - area->x,
				      y - text->yoff - area->y,
				      pixmap,
				      first.paragraph_ptr, FALSE,
				      second.paragraph_ptr, FALSE,
				      in_selection, gc, gcsel );
		  break;
		case 1:
		  gtk_paragraph_draw( paragraph, - text->hadj->value - area->x,
				      y - text->yoff - area->y,
				      pixmap,
				      first.paragraph_ptr, cursor,
				      second.paragraph_ptr, FALSE,
				      in_selection, gc, gcsel );
		  in_selection = TRUE;
		  break;
		case 2:
		  gtk_paragraph_draw( paragraph, - text->hadj->value - area->x,
				      y - text->yoff - area->y,
				      pixmap,
				      first.paragraph_ptr, FALSE,
				      second.paragraph_ptr, cursor,
				      in_selection, gc, gcsel );
		  in_selection = FALSE;
		  break;
		case 3:
		  gtk_paragraph_draw( paragraph, - text->hadj->value - area->x,
				      y - text->yoff - area->y,
				      pixmap,
				      first.paragraph_ptr, cursor,
				      second.paragraph_ptr, cursor,
				      in_selection, gc, gcsel );
		  break;
		}
	    }
	  else
	    {
	      switch( jump )
		{
		case 1:
		  in_selection = TRUE;
		  break;
		case 2:
		  in_selection = FALSE;
		  break;
		}
	    }
	  y += gtk_paragraph_height( tree->paragraph );
	}
      
      if ( tree->right )
	{
	  if ( ! ( y < text->yoff + area->y + area->height
		   && y + tree->right->height >
		   text->yoff + area->y ) )
	    return;
	}
      tree = tree->right;
    }
}

static void
expose_text (GtkRichText* text, GdkRectangle *area, gboolean cursor)
{
  GdkPixmap *pixmap;
  int y;
  GList *current_paragraph; /* Of type Paragraph *. */

  D( "Entering expose_text.\n" );

  if ( GTK_WIDGET_REALIZED( GTK_WIDGET( text ) ) )
    {
      GdkGC *gc
	= GTK_WIDGET( text )->style->text_gc[ GTK_WIDGET( text )->state ];
      GdkGC *gcsel
	= GTK_WIDGET( text )->style->dark_gc[ GTK_WIDGET( text )->state ];
      Position first
	= text->cursor_first ? text->cursor : text->selection_end;
      Position second
	= text->cursor_first ? text->selection_end : text->cursor;
      ParentList firstlist = build_parent_list( first.paragraph );
      ParentList secondlist = build_parent_list( second.paragraph );

      pixmap = gdk_pixmap_new( GTK_WIDGET( text )->window,
			       area->width, area->height,
			       gtk_widget_get_visual
			       ( GTK_WIDGET( text ) )->depth );
  
      gdk_draw_rectangle (pixmap,
			  GTK_WIDGET(text)->style->white_gc,
			  TRUE,
			  0, 0,
			  area->width, area->height);

      y = 0;

      

      expose_text_paragraph( text,
			     text->paragraphs,
			     0,
			     pixmap,
			     FALSE,
			     firstlist,
			     secondlist,
			     area,
			     cursor );

      
      
      /* Copy the pixmap to the window and clean up */
      gdk_draw_pixmap ( GTK_WIDGET( text )->window,
			gc,
			pixmap,
			0, 0,
			area->x,
			area->y,
			area->width, area->height );
      
      gdk_pixmap_unref (pixmap);
    }

  D( "Leaving expose_text.\n" );
}

static void
gtk_rich_text_paragraph_destroy( ParagraphTree *tree )
{
  ParagraphTree *last_tree;
  while ( tree )
    {
      if ( tree->left )
	gtk_rich_text_paragraph_destroy( tree->left );
      if ( tree->paragraph )
	gtk_paragraph_destroy( tree->paragraph );
      
      last_tree = tree;
      tree = tree->right;
      g_free( last_tree );
    }
}

static void
gtk_rich_text_destroy(GtkObject *object)
{
  GtkRichText *text = GTK_RICH_TEXT( object );

  g_return_if_fail (object != NULL);
  g_return_if_fail (GTK_IS_RICH_TEXT (object));

  D( "Entering gtk_rich_text_destroy.\n" );

  if (text->hadj)
    {
      gtk_object_unref (GTK_OBJECT (text->hadj));
      text->hadj = NULL;
    }
  if (text->vadj)
    {
      gtk_object_unref (GTK_OBJECT (text->vadj));
      text->vadj = NULL;
    }

  gtk_rich_text_paragraph_destroy( text->paragraphs );
  
  GTK_OBJECT_CLASS( parent_class )->destroy (object);

  D( "Leaving gtk_rich_text_destroy.\n" );
}

static void
adjust_adjs( GtkRichText *text )
{
  gint y = 0;
  gint width, height;
  GList *current_paragraph; /* Of type Paragraph *. */

  D( "Entering adjust_adjs.\n" );

  y = text->paragraphs->height;
      
  width = GTK_WIDGET( text )->allocation.width;
  height = GTK_WIDGET( text )->allocation.height;
  
  text->vadj->upper          = y;
  text->vadj->step_increment = MIN (text->vadj->upper, (float) SCROLL_PIXELS);
  text->vadj->page_increment = MIN (text->vadj->upper,
				    height - (float) KEY_SCROLL_PIXELS);
  text->vadj->page_size      = MIN (text->vadj->upper, height);
  text->vadj->value          = MIN (text->vadj->value,
				    text->vadj->upper - text->vadj->page_size);
  text->vadj->value          = MAX (text->vadj->value, 0.0);
  
  gtk_signal_emit_by_name (GTK_OBJECT (text->vadj), "changed");

  if ( text->width == -1 )
    text->hadj->upper        = MAX( width * SCALE, text->minimum_width );
  else
    text->hadj->upper        = text->width;
  text->hadj->step_increment = MIN (text->hadj->upper, (float) SCROLL_PIXELS);
  text->hadj->page_increment = MIN (text->hadj->upper,
				    width - (float) KEY_SCROLL_PIXELS);
  text->hadj->page_size      = MIN (text->hadj->upper, width);
  text->hadj->value          = MIN (text->hadj->value,
				    text->hadj->upper - text->hadj->page_size);
  text->hadj->value          = MAX (text->hadj->value, 0.0);
  
  gtk_signal_emit_by_name (GTK_OBJECT (text->hadj), "changed");

  D( "Leaving adjust_adjs.\n" );
}

/* Takes coordinates from the upper left hand corner of the entire
   block of text. */
Position
gtk_rich_text_find_point( GtkRichText *text, int x, int y )
{
  Position newpos;
  ParagraphTree *tree;

  newpos.paragraph = NULL;
  
  if ( GTK_WIDGET_REALIZED( GTK_WIDGET( text ) ) )
    {
      tree = text->paragraphs;
      while ( tree )
	{
	  if ( tree->left )
	    {
	      if ( y <= tree->left->height )
		{
		  tree = tree->left;
		  continue;
		}
	      y -= tree->left->height;
	    }
	  if ( tree->paragraph )
	    {
	      if ( y <= gtk_paragraph_height( tree->paragraph ) )
		{
		  newpos.paragraph = tree;
		  newpos.paragraph_ptr
		    = gtk_paragraph_find_point( newpos.paragraph->paragraph,
						x, y );
		  return newpos;
		}
	      y -= gtk_paragraph_height( tree->paragraph );
	    }
	  tree = tree->right;
	}
  
      newpos.paragraph = find_last_paragraph( text->paragraphs );
      y += gtk_paragraph_height( newpos.paragraph->paragraph );
      newpos.paragraph_ptr
	= gtk_paragraph_find_point( newpos.paragraph->paragraph,
				    x, y );
      return newpos;
    }
  return newpos;
}

static void
gtk_rich_text_select_word   (GtkRichText         *text)
{
}

static void
gtk_rich_text_select_line   (GtkRichText         *text)
{
}

static gint
gtk_rich_text_button_press  (GtkWidget           *widget,
			     GdkEventButton      *event)
{
  GtkRichText *text;
  GtkEditable *editable;
  gint x;
  gint y;
  static GdkAtom ctext_atom = GDK_NONE;
  
  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_RICH_TEXT (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);
  
  if (ctext_atom == GDK_NONE)
    ctext_atom = gdk_atom_intern ("COMPOUND_TEXT", FALSE);

  text = GTK_RICH_TEXT( widget );
  editable = GTK_EDITABLE( widget );

  x = event->x + text->xoff;
  y = event->y + text->yoff;

  text->last_button = event->button;

  if (!GTK_WIDGET_HAS_FOCUS (widget))
    gtk_widget_grab_focus (widget);

  if (event->button == 1)
    {
      switch (event->type)
	{
	case GDK_BUTTON_PRESS:
	  text->last_count = 1;
	  gtk_rich_text_real_set_position( text,
					   gtk_rich_text_find_point( text,
								     x, y ) );
	  gtk_grab_add (widget);
	  break;
	  
	case GDK_2BUTTON_PRESS:
	  text->last_count = 2;
	  gtk_rich_text_select_word (text);
	  break;
	  
	case GDK_3BUTTON_PRESS:
	  text->last_count = 3;
	  gtk_rich_text_select_line (text);
	  break;
	  
	default:
	  break;
	}
    }
  else if (event->button == 2)
    {
      gtk_rich_text_real_set_position( text, gtk_rich_text_find_point( text,
								       x, y ) );
      gtk_selection_convert (widget, GDK_SELECTION_PRIMARY,
			     ctext_atom, event->time);
    }
  text->ideal_valid = FALSE;
  
  gtk_rich_text_queue_redraw(text);
  
  D( "Leaving gtk_rich_text_button_press.\n" );
  return FALSE;
}

static gint
gtk_rich_text_button_release (GtkWidget      *widget,
			      GdkEventButton *event)
{
  GtkRichText *text;
  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_RICH_TEXT (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);
  
  text = GTK_RICH_TEXT (widget);
  
  gtk_grab_remove (widget);
  
  if (text->last_button != event->button)
    return FALSE;
  
  text->last_button = 0;
#if 0
  if (text->timer)
    {
      gtk_timeout_remove (text->timer);
      text->timer = 0;
    }
#endif

  gtk_grab_remove (widget);
  
  return FALSE;
}

static gint
gtk_rich_text_motion_notify  (GtkWidget           *widget,
			      GdkEventMotion      *event)
{
  GtkRichText *text = GTK_RICH_TEXT( widget );
  gint x = event->x + text->xoff;
  gint y = event->y + text->yoff;
  D( "Entering gtk_rich_text_motion_notify.\n" );

  text->queuedx = x;
  text->queuedy = y;
  
  if ( text->motion_callback == 0 )
    {
      text->motion_callback = gtk_timeout_add( 0,
					       motion_timeout,
					       (gpointer) text );
    }

  D( "Leaving gtk_rich_text_motion_notify.\n" );
  
  return FALSE;
}

static gint
redraw_timeout( gpointer data )
{
  GtkRichText *text = GTK_RICH_TEXT( data );
  GdkRectangle rect;
  gint x, y;
  gint width, height;

  gdk_window_get_geometry ( GTK_WIDGET( text )->window,
			    &x, &y, &width, &height,
			    NULL );

  if ( text->redraw_area.width == 65535 )
    text->redraw_area.width = width - text->redraw_area.x;
  if ( text->redraw_area.height == 65535 )
    text->redraw_area.height = height - text->redraw_area.y;

  expose_text(text, &text->redraw_area, TRUE);

  text->redraw_area.x = 0;
  text->redraw_area.y = 0;
  text->redraw_area.width = 0;
  text->redraw_area.height = 0;

  text->redraw_callback = 0;
  return FALSE;
}

static void
gtk_rich_text_queue_redraw  (GtkRichText *text)
{
  text->redraw_area.x = 0;
  text->redraw_area.y = 0;
  text->redraw_area.width = 65535;
  text->redraw_area.height = 65535;
  if ( text->redraw_callback == 0 )
    {
      text->redraw_callback
	= gtk_timeout_add( 0, redraw_timeout, (gpointer) text );
    }
}

static void
gtk_rich_text_queue_redraw_with_rect (GtkRichText *text,
				      GdkRectangle *area)
{
  if ( text->redraw_area.width == 0 && text->redraw_area.height == 0 )
    {
      text->redraw_area = *area;
    }
  else
    {
      GdkRectangle result;
      if ( text->redraw_area.width != 65535 || text->redraw_area.height != 65535 )
	{
	  gdk_rectangle_union( area, &text->redraw_area, &result );
	}
      if ( text->redraw_area.width != 65535 )
	{
	  text->redraw_area.x = result.x;
	  text->redraw_area.width = result.width;
	}
      else
	text->redraw_area.x = MIN( text->redraw_area.x, area->x );
      if ( text->redraw_area.height != 65535 )
	{
	  text->redraw_area.y = result.y;
	  text->redraw_area.height = result.height;
	}
      else
	text->redraw_area.y = MIN( text->redraw_area.y, area->y );
    }
  if ( text->redraw_callback == 0 )
    {
      text->redraw_callback
	= gtk_timeout_add( 0, redraw_timeout, (gpointer) text );
    }
}

static void
gtk_rich_text_queue_redraw_between_positions (GtkRichText *text,
					      Position start,
					      Position end)
{
  gint start_top = gtk_rich_text_top_of_position( text, start );
  gint end_top = gtk_rich_text_top_of_position( text, end );
  GdkRectangle rect;

  rect.x = 0;
  rect.width = 65535;
  if ( start_top < end_top )
    {
      rect.y = start_top;
      rect.height = gtk_rich_text_bottom_of_position( text, end ) - rect.y;
    }
  else
    {
      rect.y = end_top;
      rect.height = gtk_rich_text_bottom_of_position( text, start ) - rect.y;
    }
  rect.y -= text->yoff;
  gtk_rich_text_queue_redraw_with_rect( text, &rect );
}

static gint
motion_timeout( gpointer data )
{
  GtkRichText *text = GTK_RICH_TEXT( data );
  GtkEditable *editable = GTK_EDITABLE( text );
  gboolean cursor_first_before = text->cursor_first;
  Position oldpos = text->cursor;

  text->cursor
    = gtk_rich_text_find_point( text, text->queuedx, text->queuedy );
  editable->current_pos = gtk_rich_text_position_to_int( text, text->cursor );
  text->selection_active = TRUE;
  
  calculate_selection( text );
  
  if ( cursor_first_before )
    {
      if ( text->cursor_first )
	{	
	  editable->selection_start_pos =
	    editable->current_pos;
	}
      else
	{
	  editable->selection_start_pos =
	    editable->selection_end_pos;
	  editable->selection_end_pos =
	    editable->current_pos;
	}
    }
  else
    {
      if ( text->cursor_first )
	{
	  editable->selection_end_pos =
	    editable->selection_start_pos;
	  editable->selection_start_pos =
	    editable->current_pos;
	}
      else
	{
	  editable->selection_end_pos =
	    editable->current_pos;
	}
    }
  gtk_rich_text_queue_redraw_between_positions(text, oldpos, text->cursor);
  text->motion_callback = 0;
  gtk_signal_emit( GTK_OBJECT (text),
		   rich_text_signals[SELECTION_CHANGED],
		   text->cursor_first ? text->cursor : text->selection_end,
		   text->cursor_first ? text->selection_end : text->cursor );  

  return FALSE;
}

static void
gtk_rich_text_insert_through_editable (GtkRichText *text,
				       gchar *vals,
				       gint length)
{
  GtkEditable *editable = GTK_EDITABLE( text );
  gint position = editable->current_pos;
  if( text->is_editable )
    {
      gtk_editable_insert_text( editable, vals, length, &position );
    }
  else
    {
      gtk_signal_emit (GTK_OBJECT (text),
		       rich_text_signals[FAILED_INSERT_TEXT],
		       vals, length, &position);
    }
}

static void
gtk_rich_text_delete_through_editable (GtkRichText *text,
				       Position startpos,
				       Position endpos)
{
  gint start = gtk_rich_text_position_to_int( text, startpos );
  gint end = gtk_rich_text_position_to_int( text, endpos );
  if( text->is_editable )
    {
      GtkEditable *editable = GTK_EDITABLE( text );
      gtk_editable_delete_text( editable, start, end );
      text->ideal_valid = FALSE;
    }
  else
    {
      gtk_signal_emit (GTK_OBJECT (text),
		       rich_text_signals[FAILED_DELETE_TEXT], start, end);
    }
}

static gint
gtk_rich_text_key_press (GtkWidget            *widget,
			 GdkEventKey          *event)
{
  GtkRichText *text = GTK_RICH_TEXT(widget);
  GtkEditable *editable = GTK_EDITABLE( widget );
  gint key = event->keyval;
  gboolean return_val = FALSE;
  gint extend_selection;
  gint selected_before;
  gint extend_start;
  gchar value = key;

  ParagraphTree *starttree;
  ParagraphTree *lasttree = NULL;
  gint paragraph_height = 0;
  gboolean handling_redraw;

  handling_redraw = ! text->incremental_redraw_handled;
  text->incremental_redraw_handled = TRUE;
  starttree = text->cursor.paragraph;
  if ( starttree != text->selection_end.paragraph )
    starttree = NULL;
  else
    {
      paragraph_height = gtk_paragraph_height( text->cursor.paragraph->paragraph );
      lasttree = find_last_paragraph( starttree );
    }
  
  extend_selection = event->state & GDK_SHIFT_MASK;
  extend_start = FALSE;

  selected_before =
    ! ( text->cursor.paragraph == text->selection_end.paragraph &&
	gtk_paragraph_position_equal
	( (Paragraph *) text->cursor.paragraph->data,
	  text->cursor.paragraph_ptr,
	  text->selection_end.paragraph_ptr )
	);
  

  if ( selected_before )
    {
      switch ( key )
	{
	case GDK_Left:
	case GDK_KP_Left:
	  if (event->state & GDK_CONTROL_MASK)
	    gtk_rich_text_move_backward_word (text);
	  else
	    gtk_rich_text_move_backward_character( text );
	  return_val = TRUE;
	  break;
	case GDK_Right:
	case GDK_KP_Right:
	  if (event->state & GDK_CONTROL_MASK)
	    gtk_rich_text_move_forward_word (text);
	  else
	    gtk_rich_text_move_forward_character( text );
	  return_val = TRUE;
	  break;
	case GDK_Up:
	case GDK_KP_Up:
	  if ( event->state & GDK_CONTROL_MASK )
	    gtk_rich_text_vertical_move_page( text, -1 );
	  else
	    gtk_rich_text_move_previous_line( text );
	  return_val = TRUE;
	  break;
	case GDK_Down:
	case GDK_KP_Down:
	  if ( event->state & GDK_CONTROL_MASK )
	    gtk_rich_text_vertical_move_page( text, 1 );
	  else
	    gtk_rich_text_move_next_line( text );
	  return_val = TRUE;
	  break;

	case GDK_Home:
	case GDK_KP_Home:
	  return_val = TRUE;
	  if (event->state & GDK_CONTROL_MASK)
	    gtk_rich_text_move_beginning_of_buffer( text );
	  else
	    gtk_rich_text_move_beginning_of_line( text );
	  break;

	case GDK_End:
	case GDK_KP_End:
	  return_val = TRUE;
	  if (event->state & GDK_CONTROL_MASK)
	    gtk_rich_text_move_end_of_buffer( text );
	  else
	    gtk_rich_text_move_end_of_line( text );
	  break;

	case GDK_Page_Down:
	case GDK_KP_Page_Down:
	  return_val = TRUE;
	  gtk_rich_text_vertical_move_page( text, 1 );
	  break;
      
	case GDK_Page_Up:
	case GDK_KP_Page_Up:
	  return_val = TRUE;
	  gtk_rich_text_vertical_move_page( text, -1 );
	  break;
	  
	case GDK_BackSpace:
	case GDK_Delete:
	case GDK_KP_Delete:
	  
	case GDK_exclam:
	case GDK_quotedbl:
	case GDK_numbersign:
	case GDK_dollar:
	case GDK_percent:
	case GDK_ampersand:
	case GDK_apostrophe:
	  /*    case GDK_quoteright:*/
	case GDK_parenleft:
	case GDK_parenright:
	case GDK_asterisk:
	case GDK_plus:
	case GDK_comma:
	case GDK_minus:
	case GDK_period:
	case GDK_slash:
	  
	case GDK_colon:
	case GDK_semicolon:
	case GDK_less:
	case GDK_equal:
	case GDK_greater:
	case GDK_question:
	case GDK_at:
	  
	case GDK_bracketleft:
	case GDK_backslash:
	case GDK_bracketright:
	case GDK_asciicircum:
	case GDK_underscore:
	case GDK_grave:
	  /*    case GDK_quoteleft: */
	  
	case GDK_braceleft:
	case GDK_bar:
	case GDK_braceright:
	case GDK_asciitilde:
	  
	case GDK_space:
	case GDK_KP_Space:

	case GDK_Tab:
	  
	case GDK_KP_Add:
	case GDK_KP_Subtract:
	case GDK_KP_Divide:
	case GDK_KP_Decimal:
	case GDK_KP_Multiply:
	  
	  return_val = TRUE;
	  gtk_editable_delete_selection( editable );
	  break;
	  
	default:
	  if (!((event->state & GDK_CONTROL_MASK)
		|| (event->state & GDK_MOD1_MASK)))
	      {
		if(( key >= GDK_a && key <= GDK_z ) ||
		   ( key >= GDK_A && key <= GDK_Z ) ||
		   ( key >= GDK_0 && key <= GDK_9 ) ||
		   ( key >= GDK_KP_0 && key <= GDK_KP_9 ))
		  {
		    return_val = TRUE;
		    gtk_editable_delete_selection( editable );
		  }
	      }
	  break;
	}
    }
  else
    {
      switch( key )
	{
	case GDK_BackSpace:
	  return_val = TRUE;
	  if (event->state & GDK_CONTROL_MASK)
	    gtk_rich_text_delete_backward_word( text );
	  else
	    gtk_rich_text_delete_backward_character( text );
	  break;
	case GDK_Delete:
	case GDK_KP_Delete:
	  return_val = TRUE;
	  if (event->state & GDK_CONTROL_MASK)
	    gtk_rich_text_delete_forward_word (text);
	  else if (event->state & GDK_SHIFT_MASK)
	    {
	      /*	  extend_selection = FALSE; */
	      gtk_editable_cut_clipboard (editable);
	    }
	  else
	    gtk_rich_text_delete_forward_character (text);
	  break;
	case GDK_Left:
	case GDK_KP_Left:
	  if (event->state & GDK_CONTROL_MASK)
	    gtk_rich_text_move_backward_word (text);
	  else
	    gtk_rich_text_move_backward_character( text );
	  return_val = TRUE;
	  break;
	case GDK_Right:
	case GDK_KP_Right:
	  if (event->state & GDK_CONTROL_MASK)
	    gtk_rich_text_move_forward_word (text);
	  else
	    gtk_rich_text_move_forward_character( text );
	  return_val = TRUE;
	  break;
	case GDK_Up:
	case GDK_KP_Up:
	  if ( event->state & GDK_CONTROL_MASK )
	    gtk_rich_text_vertical_move_page( text, -1 );
	  else
	    gtk_rich_text_move_previous_line( text );
	  return_val = TRUE;
	  break;
	case GDK_Down:
	case GDK_KP_Down:
	  if ( event->state & GDK_CONTROL_MASK )
	    gtk_rich_text_vertical_move_page( text, 1 );
	  else
	    gtk_rich_text_move_next_line( text );
	  return_val = TRUE;
	  break;

	case GDK_Home:
	case GDK_KP_Home:
	  return_val = TRUE;
	  if (event->state & GDK_CONTROL_MASK)
	    gtk_rich_text_move_beginning_of_buffer( text );
	  else
	    gtk_rich_text_move_beginning_of_line( text );
	  break;

	case GDK_End:
	case GDK_KP_End:
	  return_val = TRUE;
	  if (event->state & GDK_CONTROL_MASK)
	    gtk_rich_text_move_end_of_buffer( text );
	  else
	    gtk_rich_text_move_end_of_line( text );
	  break;

	case GDK_Page_Down:
	case GDK_KP_Page_Down:
	  return_val = TRUE;
	  gtk_rich_text_vertical_move_page( text, 1 );
	  break;
      
	case GDK_Page_Up:
	case GDK_KP_Page_Up:
	  return_val = TRUE;
	  gtk_rich_text_vertical_move_page( text, -1 );
	  break;
	  
	default:
	  /* Do nothing. */
	  break;
	}
    }
  switch ( key )
    {
    case GDK_exclam:
    case GDK_quotedbl:
    case GDK_numbersign:
    case GDK_dollar:
    case GDK_percent:
    case GDK_ampersand:
    case GDK_apostrophe:
      /*    case GDK_quoteright:*/
    case GDK_parenleft:
    case GDK_parenright:
    case GDK_asterisk:
    case GDK_plus:
    case GDK_comma:
    case GDK_minus:
    case GDK_period:
    case GDK_slash:

    case GDK_colon:
    case GDK_semicolon:
    case GDK_less:
    case GDK_equal:
    case GDK_greater:
    case GDK_question:
    case GDK_at:
      
    case GDK_bracketleft:
    case GDK_backslash:
    case GDK_bracketright:
    case GDK_asciicircum:
    case GDK_underscore:
    case GDK_grave:
      /*    case GDK_quoteleft: */

    case GDK_braceleft:
    case GDK_bar:
    case GDK_braceright:
    case GDK_asciitilde:

    case GDK_space:
    case GDK_KP_Space:
      return_val = TRUE;
      gtk_rich_text_insert_through_editable( text, &value, 1 );
      break;

    case GDK_KP_Add:
      return_val = TRUE;
      value = '+';
      gtk_rich_text_insert_through_editable( text, &value, 1 );
      break;

    case GDK_KP_Subtract:
      return_val = TRUE;
      value = '-';
      gtk_rich_text_insert_through_editable( text, &value, 1 );
      break;

    case GDK_KP_Divide:
      return_val = TRUE;
      value = '/';
      gtk_rich_text_insert_through_editable( text, &value, 1 );
      break;

    case GDK_KP_Decimal:
      return_val = TRUE;
      value = '.';
      gtk_rich_text_insert_through_editable( text, &value, 1 );
      break;

    case GDK_KP_Multiply:
      return_val = TRUE;
      value = '*';
      gtk_rich_text_insert_through_editable( text, &value, 1 );
      break;
      
    case GDK_Tab:
      return_val = TRUE;
      gtk_rich_text_insert_through_editable( text, "\t", 1 );
      break;

    case GDK_Return:
    case GDK_KP_Enter:
      if (event->state & GDK_CONTROL_MASK)
	gtk_signal_emit_by_name (GTK_OBJECT (text), "activate");
      else
	{
	  return_val = TRUE;
	  gtk_rich_text_insert_through_editable( text, "\n", 1 );
	}
      break;
      
    default:
      if (event->state & GDK_CONTROL_MASK)
	{
	  if ((key >= 'A') && (key <= 'Z'))
	    key -= 'A' - 'a';
	      
	  if ((key >= 'a') && (key <= 'z') && control_keys[(int) (key - 'a')])
	    {
	      (* control_keys[(int) (key - 'a')]) (editable, event->time);
	      return_val = TRUE;
	      starttree = NULL;
	    }
	  break;
	}
      else if (event->state & GDK_MOD1_MASK)
	{
	  if ((key >= 'A') && (key <= 'Z'))
	    key -= 'A' - 'a';
	  
	  if ((key >= 'a') && (key <= 'z') && alt_keys[(int) (key - 'a')])
	    {
	      (* alt_keys[(int) (key - 'a')]) (editable, event->time);
	      return_val = TRUE;
	      starttree = NULL;
	    }
	}
      else
	{
	  if(( key >= GDK_a && key <= GDK_z ) ||
	     ( key >= GDK_A && key <= GDK_Z ) ||
	     ( key >= GDK_0 && key <= GDK_9 ))
	    {
	      return_val = TRUE;
	      gtk_rich_text_insert_through_editable( text, &value, 1 );
	    }
	  else if ( key >= GDK_KP_0 && key <= GDK_KP_9 )
	    {
	      return_val = TRUE;
	      value = key - GDK_KP_0 + GDK_0;
	      gtk_rich_text_insert_through_editable( text, &value, 1 );
	    }
	}
      break;
    }

  if ( ! text->frozen )
    {
      adjust_adjs( text );
      gtk_rich_text_locate_cursor( text );
      starttree = text->cursor.paragraph;
      if ( handling_redraw )
	{
	  if ( text->cursor.paragraph == starttree &&
	       text->selection_end.paragraph == starttree &&
	       paragraph_height == gtk_paragraph_height( text->cursor.paragraph->paragraph ) &&
	       lasttree == find_last_paragraph( starttree )
	       )
	    {
	      Position start;
	      Position end;
	  
	      start.paragraph = end.paragraph = starttree;
	      start.paragraph_ptr = gtk_paragraph_start( start.paragraph->paragraph );
	      end.paragraph_ptr = gtk_paragraph_end( start.paragraph->paragraph );
	      gtk_rich_text_queue_redraw_between_positions( text, start, end );
	    }
	  else
	    gtk_rich_text_queue_redraw( text );
	  text->incremental_redraw_handled = FALSE;
	}
   }

  if ( text->cursor.paragraph == text->selection_end.paragraph &&
       gtk_paragraph_position_equal( (Paragraph *)text->cursor.paragraph->data,
				     text->cursor.paragraph_ptr,
				     text->selection_end.paragraph_ptr ) )
    {
      text->selection_active = FALSE;
    }

  D( "Leaving gtk_rich_text_key_press.\n" );
  return return_val;
}

static ParagraphTree *
rotate_right( ParagraphTree *tree )
{
  ParagraphTree *parent = tree->parent;
  if( parent )
    {
      if ( parent->right == tree )
	parent->right = tree->left;
      else
	parent->left = tree->left;
    }
  tree->parent = tree->left;
  tree->left->parent = parent;
  tree->left = tree->left->right;
  tree->parent->right = tree;
  if ( tree->left )
    tree->left->parent = tree;
  update_this_count( tree );
  update_this_count( tree->parent );
  return tree->parent;
}

static ParagraphTree *
rotate_left( ParagraphTree *tree )
{
  ParagraphTree *parent = tree->parent;
  if( parent )
    {
      if ( parent->right == tree )
	parent->right = tree->right;
      else
	parent->left = tree->right;
    }
  tree->parent = tree->right;
  tree->right->parent = parent;
  tree->right = tree->right->left;
  tree->parent->left = tree;
  if ( tree->right )
    tree->right->parent = tree;
  update_this_count( tree );
  update_this_count( tree->parent );
  return tree->parent;
}

static ParagraphTree *
double_rotate_right( ParagraphTree *tree )
{
  ParagraphTree *k1 = tree->left;
  ParagraphTree *k2 = tree->left->right;
  ParagraphTree *k3 = tree;
  ParagraphTree *parent = tree->parent;
  if ( parent )
    {
      if ( parent->right == tree )
	parent->right = k2;
      else
	parent->left = k2;
    }
  k2->parent = parent;
  k1->right = k2->left;
  k3->left = k2->right;
  k2->left = k1;
  k2->right = k3;
  k1->parent = k3->parent = k2;
  if ( k1->right )
    k1->right->parent = k1;
  if ( k3->left )
    k3->left->parent = k3;
  update_this_count( k3 );
  update_this_count( k1 );
  update_this_count( k2 );
  return k2;
}

static ParagraphTree *
double_rotate_left( ParagraphTree *tree )
{
  ParagraphTree *k1 = tree;
  ParagraphTree *k2 = tree->right->left;
  ParagraphTree *k3 = tree->right;
  ParagraphTree *parent = tree->parent;
  if ( parent )
    {
      if ( parent->right == tree )
	parent->right = k2;
      else
	parent->left = k2;
    }
  k2->parent = parent;
  k1->right = k2->left;
  k3->left = k2->right;
  k2->left = k1;
  k2->right = k3;
  k1->parent = k3->parent = k2;
  if ( k1->right )
    k1->right->parent = k1;
  if ( k3->left )
    k3->left->parent = k3;
  update_this_count( k3 );
  update_this_count( k1 );
  update_this_count( k2 );
  return k2;
}

static void
balance_tree( GtkRichText *text, ParagraphTree *tree )
{
  gint left;
  gint right;
  while ( tree )
    {
      left = tree->left ? tree->left->tree_depth : -1;
      right = tree->right ? tree->right->tree_depth : -1;
      if ( left + 1 < right )
	{
	  left = tree->right->left ? tree->right->left->tree_depth : -1;
	  right = tree->right->right ? tree->right->right->tree_depth : -1;
	  if ( left > right )
	    tree = double_rotate_left( tree );
	  else
	    tree = rotate_left( tree );
	}
      else if ( right + 1 < left)
	{
	  left = tree->left->left ? tree->left->left->tree_depth : -1;
	  right = tree->left->right ? tree->left->right->tree_depth : -1;
	  if ( left < right )
	    tree = double_rotate_right( tree );
	  else
	    tree = rotate_right( tree );
	}
      tree = tree->parent;
    }
  while ( text->paragraphs->parent )
    text->paragraphs = text->paragraphs->parent;
}

static Position
gtk_rich_text_delete_internal( GtkRichText *text,
			       Position start,
			       Position end,
			       Position *maintain,
			       gint poscount )
{
  gint i;
  gint *oldposs = NULL;
  gint startpos, endpos;
  ParagraphTree *tree;
  ParagraphPosition para_pos = start.paragraph_ptr;
  Position final_pos = start;

  if ( poscount > 0 )
    {
      startpos = gtk_rich_text_position_to_int( text, start );
      endpos = gtk_rich_text_position_to_int( text, end );
      oldposs = g_new( gint, poscount );
      for( i = 0; i < poscount; i++ )
	{
	  oldposs[i] = gtk_rich_text_position_to_int( text, maintain[i] );
	  oldposs[i] = MIN( oldposs[i], MAX( oldposs[i] - (endpos - startpos),
					     startpos ) );
	}
    }

  tree = start.paragraph;
  while ( tree && tree != end.paragraph )
    {
      ParagraphTree *nextpar;
      final_pos.paragraph = tree;
      final_pos.paragraph_ptr
	= gtk_paragraph_delete( tree->paragraph,
				para_pos,
				gtk_paragraph_end( tree->paragraph ) );
      nextpar = find_next_paragraph( tree );
      if ( nextpar != tree )
	{
	  ParagraphTree *newnextpar;

	  final_pos.paragraph_ptr = gtk_paragraph_concat( tree->paragraph,
							  nextpar->paragraph );
	  /*	  nextpar->paragraph = NULL; */
	  if ( end.paragraph == nextpar )
	    end.paragraph = tree;
	  
	  if ( nextpar->left == NULL )
	    newnextpar = nextpar->right;
	  else if ( nextpar->right == NULL )
	    newnextpar = nextpar->left;
	  else
	    newnextpar = nextpar;

	  if ( newnextpar != nextpar )
	    {
	      if ( newnextpar )
		newnextpar->parent = nextpar->parent;
	      if ( nextpar->parent )
		{
		  if ( nextpar->parent->right == nextpar )
		    {
		      nextpar->parent->right = newnextpar;
		    }
		  else
		    {
		      nextpar->parent->left = newnextpar;
		    }
		  update_counts( nextpar->parent );
		  balance_tree( text, nextpar->parent );
		}
	      else if ( nextpar == text->paragraphs )
		text->paragraphs = newnextpar;
	      g_free( nextpar );
	    }
	  else
	    {
	      ParagraphTree *parent;

	      newnextpar = nextpar->right;
	      while( newnextpar->left )
		newnextpar = newnextpar->left;
	      if ( newnextpar->parent != nextpar )
		parent = newnextpar->parent;
	      else
		parent = newnextpar;
	      
	      if (newnextpar->right )
		newnextpar->right->parent = newnextpar->parent;
	      if ( newnextpar->parent->left == newnextpar )
		newnextpar->parent->left = newnextpar->right;
	      else
		newnextpar->parent->right = newnextpar->right;

	      newnextpar->right = nextpar->right;
	      if ( newnextpar->right )
		newnextpar->right->parent = newnextpar;
	      newnextpar->left = nextpar->left;
	      newnextpar->left->parent = newnextpar;
	      if ( nextpar->parent )
		{
		  if ( nextpar->parent->right == nextpar )
		    nextpar->parent->right = newnextpar;
		  else
		    nextpar->parent->left = newnextpar;
		}
	      else if ( nextpar == text->paragraphs )
		text->paragraphs = newnextpar;
	      newnextpar->parent = nextpar->parent;
	      g_free( nextpar );
	      update_counts( parent );
	      balance_tree( text, parent );
	    }
	  para_pos = final_pos.paragraph_ptr;
	  if ( end.paragraph_ptr.word == final_pos.paragraph_ptr.word )
	    end.paragraph_ptr.word_ptr += final_pos.paragraph_ptr.word_ptr;
	}
      else
	{
	  update_counts( tree );
	  tree = NULL;
	}
    }
  if( tree )
    {
      final_pos.paragraph = tree;
      final_pos.paragraph_ptr
	= gtk_paragraph_delete( tree->paragraph,
				para_pos, end.paragraph_ptr );
      update_counts( tree ); 
   }
  if ( poscount > 0 )
    {
      for ( i = 0; i < poscount; i++ )
	{
	  maintain[i] = gtk_rich_text_position( text, oldposs[i] );
	}
    }
  return final_pos;
}

/* Scanning code to be used later.
   Looks for some byte with the value 0x42.
   x = ((int *)p)[0] ^ 0x42424242;
   if ( (x - 0x01010101) & ~x & 0x80808080 )
   {
       some byte has the value 0x42.
   }
*/


static Position
gtk_rich_text_insert_internal( GtkRichText *text,
			       Position start,
			       gchar *vals,
			       gint length,
			       Position *maintain,
			       gint poscount )
{
  gint i;
  gint laststart = 0;
  gint *oldposs = NULL;
  gint startpos;
  
  D( "Entering gtk_rich_text_insert_internal.\n" );

  if ( poscount > 0 )
    {
      startpos = gtk_rich_text_position_to_int( text, start );
      oldposs = g_new( gint, poscount );
      for( i = 0; i < poscount; i++ )
	{
	  oldposs[i] = gtk_rich_text_position_to_int( text, maintain[i] );
	  if (oldposs[i] >= startpos )
	    oldposs[i] += length;
	}
    }
  
  for ( i = 0; i < length; i++ )
    {
      if ( vals[i] == '\n')
	{
	  ParagraphTree *temp;
	  if ( laststart != i )
	    {
	      if ( text->cursor_font != NULL )
		start.paragraph_ptr =
		  gtk_paragraph_insert_with_data( start.paragraph->paragraph,
						  start.paragraph_ptr,
						  vals + laststart,
						  i - laststart,
						  text->cursor_font );
	      else
		start.paragraph_ptr =
		  gtk_paragraph_insert( start.paragraph->paragraph,
					start.paragraph_ptr,
					vals + laststart,
					i - laststart );
	    }
	  if ( start.paragraph->right )
	    {
	      temp = start.paragraph->right;
	      while ( temp->left )
		temp = temp->left;
	      temp->left = g_new( ParagraphTree, 1 );
	      temp->left->parent = temp;
	      temp = temp->left;
	    }
	  else
	    {
	      start.paragraph->right = g_new( ParagraphTree, 1 );
	      start.paragraph->right->parent = start.paragraph;
	      temp = start.paragraph->right;
	    }
	  temp->paragraph
	    = gtk_paragraph_break( start.paragraph->paragraph, start.paragraph_ptr );
	  temp->left = temp->right = NULL;
	  temp->tree_depth = 0;
	  update_counts( start.paragraph );
	  update_counts( temp );
	  balance_tree( text, temp->parent );
	  start.paragraph = temp;
	  start.paragraph_ptr = gtk_paragraph_start( start.paragraph->paragraph );
	  laststart = i + 1;
	}
    }
  if ( laststart != i )
    {
      if ( text->cursor_font != NULL )
	start.paragraph_ptr =
	  gtk_paragraph_insert_with_data( start.paragraph->paragraph,
					  start.paragraph_ptr,
					  vals + laststart,
					  i - laststart,
					  text->cursor_font );
      else
	start.paragraph_ptr =
	  gtk_paragraph_insert( start.paragraph->paragraph,
				start.paragraph_ptr,
				vals + laststart,
				i - laststart );
      update_counts( start.paragraph );
    }
  if ( poscount > 0 )
    {
      for ( i = 0; i < poscount; i++ )
	{
	  maintain[i] = gtk_rich_text_position( text, oldposs[i] );
	}
    }
  gtk_signal_emit( GTK_OBJECT (text),
		   rich_text_signals[SELECTION_CHANGED],
		   text->cursor_first ? text->cursor : text->selection_end,
		   text->cursor_first ? text->selection_end : text->cursor );
  
  D( "Leaving gtk_rich_text_insert_internal.\n" );
  return start;
}

void
gtk_rich_text_insert( GtkRichText       *text,
		      GtkRichTextFont   *font,
		      GdkColor          *fore,
		      GdkColor          *back,
		      const char        *chars,
		      gint               length )
{
  Position poss[2];
  Position temp;
  gint posscount = 0;
  gint possvalid[2] = { 0, 0 };

  D( "Entering gtk_rich_text_insert.\n" );

  if ( ! text->point_valid )
    {
      text->point = gtk_rich_text_position( text, 0 );
      text->point_valid = TRUE;
    }

  if ( text->cursor.paragraph == text->point.paragraph )
    {
      if ( gtk_paragraph_position_equal
	   ( (Paragraph *) text->cursor.paragraph->data,
	     text->cursor.paragraph_ptr,
	     text->point.paragraph_ptr ) )
	possvalid[0] = 2;
      else
	{
	  poss[posscount] = text->cursor;
	  possvalid[0] = 1;
	  posscount ++;
	}
    }
  if ( text->selection_end.paragraph == text->point.paragraph )
    {
      if ( gtk_paragraph_position_equal
	   ( (Paragraph *) text->selection_end.paragraph->data,
	     text->selection_end.paragraph_ptr,
	     text->point.paragraph_ptr ) )
	possvalid[1] = 2;
      else
	{
	  poss[posscount] = text->selection_end;
	  possvalid[1] = TRUE;
	  posscount ++;
	}
    }

  temp = text->point;
  
  if ( length == -1 )
    length = strlen( chars );
  text->point = gtk_rich_text_insert_internal( text, text->point,
					       (char *) chars, length,
					       poss, posscount );

  if ( font != NULL )
    {
      gtk_rich_text_set_font( text, temp, text->point, font );
    }

  if ( possvalid[1] == 1 )
    {
      posscount --;
      text->selection_end = poss[posscount];
    }
  else if ( possvalid[1] == 2 )
    text->selection_end = text->point;
  if ( possvalid[0] == 1 )
    {
      posscount --;
      text->cursor = poss[posscount];
    }
  else if ( possvalid[0] == 2 )
    text->cursor = text->point;
  
  GTK_EDITABLE( text )->current_pos
    = gtk_rich_text_position_to_int( text, text->cursor );
  
  if ( ! text->frozen )
    {
      adjust_adjs( text );
      gtk_rich_text_locate_cursor( text );
      gtk_rich_text_queue_redraw( text );
    }

  D( "Leaving gtk_rich_text_insert.\n" );
}

gboolean
gtk_rich_text_position_equal( GtkRichText *text, Position a, Position b )
{
  return a.paragraph == b.paragraph &&
    gtk_paragraph_position_equal( (Paragraph *) a.paragraph->data,
				  a.paragraph_ptr,
				  b.paragraph_ptr );
}

static ParagraphTree *
find_first_paragraph( ParagraphTree *root )
{
  ParagraphTree *first;
  while ( root )
    {
      if ( root->left )
	{
	  first = find_first_paragraph( root->left );
	  if ( first != NULL )
	    return first;
	}
      if ( root->paragraph )
	return root;
      root = root->right;
    }
  return NULL;
}

static ParagraphTree *
find_next_paragraph( ParagraphTree *oldtree )
{
  ParagraphTree *original_tree = oldtree;
  ParagraphTree *lasttree;
  while ( oldtree )
    {
      if ( oldtree->right )
	{
	  ParagraphTree *newtree = find_first_paragraph( oldtree->right );
	  if ( newtree )
	    return newtree;
	}
      lasttree = oldtree;
      oldtree = oldtree->parent;
      while ( oldtree && lasttree == oldtree->right )
	{
	  lasttree = oldtree;
	  oldtree = oldtree->parent;
	}
      if ( oldtree && oldtree->paragraph )
	return oldtree;
    }
  return original_tree;
}

static ParagraphTree *
find_last_paragraph( ParagraphTree *root )
{
  ParagraphTree *last;
  while ( root )
    {
      if ( root->right )
	{
	  last = find_last_paragraph( root->right );
	  if ( last != NULL )
	    return last;
	}
      if ( root->paragraph )
	return root;
      root = root->left;
    }
  return NULL;
}

static ParagraphTree *
find_previous_paragraph( ParagraphTree *oldtree )
{
  ParagraphTree *original_tree = oldtree;
  ParagraphTree *lasttree;
  while ( oldtree )
    {
      if ( oldtree->left )
	{
	  ParagraphTree *newtree = find_last_paragraph( oldtree->left );
	  if ( newtree )
	    return newtree;
	}
      lasttree = oldtree;
      oldtree = oldtree->parent;
      while ( oldtree && lasttree == oldtree->left )
	{
	  lasttree = oldtree;
	  oldtree = oldtree->parent;
	}
      if ( oldtree && oldtree->paragraph )
	return oldtree;
    }
  return original_tree;
}

static Position
decrement_position( Position old_pos, gint distance )
{
  D( "Entering decrement_position.\n" );
  for ( ; distance != 0; distance -- )
    {
      Paragraph *prg = old_pos.paragraph->paragraph;

      if (gtk_paragraph_position_equal (prg,
					old_pos.paragraph_ptr,
					gtk_paragraph_start (prg)))
	{
	  ParagraphTree *newpar = find_previous_paragraph( old_pos.paragraph );
	  if ( newpar != old_pos.paragraph )
	    {
	      old_pos.paragraph = newpar;
	      old_pos.paragraph_ptr =
		gtk_paragraph_end( old_pos.paragraph->paragraph );
	    }
	}
      else
	{
	  old_pos.paragraph_ptr =
	    gtk_paragraph_decrement_position (prg, old_pos.paragraph_ptr, 1);
	}
    }

  D( "Leaving decrement_position.\n" );
  return old_pos;
}

static void
gtk_rich_text_set_font( GtkRichText *text,
			Position start,
			Position end,
			GtkRichTextFont *font )
{
  ParagraphTree *tree;
  ParagraphTree *last = NULL;
  ParagraphPosition parstart = start.paragraph_ptr;
  if ( font != NULL )
    {
      for ( tree = start.paragraph;
	    tree != last && tree != end.paragraph;
	    last = tree,
	    tree = find_next_paragraph( tree ),
	      (tree ?
	       parstart = gtk_paragraph_start( tree->paragraph ) :
	       parstart ) )
	{
	  gtk_paragraph_set_font( tree->paragraph,
				  parstart, gtk_paragraph_end( tree->paragraph ),
				  font );
	  update_counts( tree );
	}
      
      if ( tree )
	{
	  gtk_paragraph_set_font( tree->paragraph,
				  parstart, end.paragraph_ptr,
				  font );
	  update_counts( tree );
	}
    }

  if ( ! text->frozen )
    {
      gtk_rich_text_locate_cursor( text );
      gtk_rich_text_queue_redraw(text);
    }
}

static Position
increment_position( Position old_pos, gint distance )
{
  D( "Entering increment_position.\n" );
  for ( ; distance > 0; distance -- )
    {
      Paragraph *prg = old_pos.paragraph->paragraph;

      if ( gtk_paragraph_position_equal (prg,
					 old_pos.paragraph_ptr,
					 gtk_paragraph_end(prg)))
	{
	  ParagraphTree *newpar = find_next_paragraph( old_pos.paragraph );
	  if ( newpar != old_pos.paragraph )
	    {
	      old_pos.paragraph = newpar;
	      old_pos.paragraph_ptr =
		gtk_paragraph_start( old_pos.paragraph->paragraph );
	    }
	}
      else
	{
	  old_pos.paragraph_ptr =
	    gtk_paragraph_increment_position (prg, old_pos.paragraph_ptr, 1 );
	}
    }

  D( "Leaving increment_position.\n" );
  return old_pos;
}

static Position
vertical_move( GtkRichText *text, Position old_pos, gint distance )
{
  ParagraphTree *newpar;
  ParagraphTree *lastpar;
  gtk_rich_text_set_ideal( text );
  old_pos.paragraph_ptr =
    gtk_paragraph_vertical_move( old_pos.paragraph->paragraph,
				 old_pos.paragraph_ptr,
				 text->ideal, &distance );
  while ( distance > 0 )
    {
      newpar = find_next_paragraph( old_pos.paragraph );
      if ( newpar != old_pos.paragraph )
	{
	  old_pos.paragraph = newpar;
	  old_pos.paragraph_ptr =
	    gtk_paragraph_start( old_pos.paragraph->paragraph );
	  distance --;
	  old_pos.paragraph_ptr =
	    gtk_paragraph_vertical_move( old_pos.paragraph->paragraph,
					 old_pos.paragraph_ptr,
					 text->ideal, &distance );
	}
      else
	break;
    }
  while ( distance < 0 )
    {
      newpar = find_previous_paragraph( old_pos.paragraph );
      if ( newpar != old_pos.paragraph )
	{
	  old_pos.paragraph = newpar;
	  old_pos.paragraph_ptr =
	    gtk_paragraph_end( old_pos.paragraph->paragraph );
	  distance ++;
	  old_pos.paragraph_ptr =
	    gtk_paragraph_vertical_move( old_pos.paragraph->paragraph,
					 old_pos.paragraph_ptr,
					 text->ideal, &distance );
	}
      else
	break;
    }
  return old_pos;
}

void
gtk_rich_text_set_cursor_font( GtkRichText *text, GtkRichTextFont *font )
{
  if ( text->cursor_font != NULL )
    gtk_rich_text_font_unref( text->cursor_font );
  text->cursor_font = font;
  gtk_rich_text_font_ref( text->cursor_font );
}

GtkRichTextFont *
gtk_rich_text_get_cursor_font( GtkRichText *text )
{
  if ( text->cursor_font != NULL )
    return text->cursor_font;
  else
    {
      Position start = text->cursor_first ? text->cursor : text->selection_end;
      return gtk_paragraph_char_font( start.paragraph->paragraph,
				      start.paragraph_ptr );
    }
}

static void
gtk_rich_text_paragraph_get_chars( GtkRichText *text,
				   ParagraphTree *tree,
				   gchar *buffer,
				   gint start,
				   gint end )
{
  while( tree )
    {
      if ( tree->left )
	{
	  if ( start < tree->left->count && end > 0 )
	    {
	      gtk_rich_text_paragraph_get_chars( text,
						 tree->left,
						 buffer,
						 start,
						 end );
	    }
	  if ( start <= tree->left->count && end > tree->left->count )
	    {
	      buffer[ tree->left->count - start ] = '\n';
	    }
	  start -= tree->left->count + 1;
	  end -= tree->left->count + 1;
	  buffer += tree->left->count + 1;
	  start = MAX( start, 0 );
	  end = MAX( end, 0 );
	}
      if ( tree->paragraph )
	{
	  if ( start < gtk_paragraph_length( tree->paragraph ) && end > 0 )
	    {
	      gtk_paragraph_get_chars( tree->paragraph,
				       buffer,
				       start,
				       end );
	    }
	  if ( start <= gtk_paragraph_length( tree->paragraph ) && end > gtk_paragraph_length( tree->paragraph ) )
	    {
	      buffer[ gtk_paragraph_length( tree->paragraph ) - start ] = '\n';
	    }
	  start -= gtk_paragraph_length( tree->paragraph ) + 1;
	  end -= gtk_paragraph_length( tree->paragraph ) + 1;
	  buffer += gtk_paragraph_length( tree->paragraph ) + 1;
	  start = MAX( start, 0 );
	  end = MAX( end, 0 );
	}
      tree = tree->right;
    }
}

static gchar *
gtk_rich_text_get_chars  (GtkEditable       *editable,
			  gint               start,
			  gint               end)
{
  GtkRichText *text = GTK_RICH_TEXT( editable );
  GList *current_paragraph;
  gchar *buffer;
  int position = 0;
  int length = gtk_rich_text_get_length( text );
  if ( end == -1 ) end = length;
  start = MAX( start, 0 );
  end = MIN( end, length );
  buffer = g_malloc0( end - start + 1 );

  gtk_rich_text_paragraph_get_chars( text,
				     text->paragraphs,
				     buffer,
				     start,
				     end );
  
  buffer[end - start] = 0;
  return buffer;
}

static void
gtk_rich_text_move_beginning_of_line (GtkRichText *text)
{
  text->ideal_valid = TRUE;
  text->ideal = 0;
  gtk_rich_text_real_set_position( text,
				   vertical_move( text, text->cursor, 0 ) );
  text->ideal_valid = FALSE;
}

static void
gtk_rich_text_move_backward_character (GtkRichText *text)
{
  gtk_rich_text_real_set_position( text,
				   decrement_position( text->cursor, 1 ) );
  text->ideal_valid = FALSE;
}

static void
gtk_rich_text_delete_forward_character (GtkRichText *text)
{
  Position end = increment_position( text->cursor, 1 );
  gtk_rich_text_delete_through_editable( text, text->cursor, end );
}

static void
gtk_rich_text_move_end_of_line (GtkRichText *text)
{
  Position pos = text->cursor;
  pos.paragraph_ptr = gtk_paragraph_end_of_line( text->cursor.paragraph->paragraph,
						 text->cursor.paragraph_ptr);
  gtk_rich_text_real_set_position( text, pos );
  text->ideal_valid = FALSE;
}

static void
gtk_rich_text_delete_backward_character (GtkRichText *text)
{
  Position beginning = decrement_position( text->cursor, 1 );
  gtk_rich_text_delete_through_editable( text, beginning, text->cursor);
}

static void
gtk_rich_text_delete_to_line_end (GtkRichText *text)
{
  Position end = text->cursor;
  end.paragraph_ptr = gtk_paragraph_end_of_line( text->cursor.paragraph->paragraph,
						 text->cursor.paragraph_ptr);
  gtk_rich_text_delete_through_editable( text, text->cursor, end );
}

static void
gtk_rich_text_move_forward_character (GtkRichText *text)
{
  gtk_rich_text_real_set_position( text,
				   increment_position( text->cursor, 1 ) );
  text->ideal_valid = FALSE;
}

static void
gtk_rich_text_move_next_line (GtkRichText *text)
{
  gtk_rich_text_real_set_position( text,
				   vertical_move( text, text->cursor, 1 ) );
}

static void
gtk_rich_text_move_previous_line (GtkRichText *text)
{
  gtk_rich_text_real_set_position( text,
				   vertical_move( text, text->cursor, -1 ) );
}

static void
gtk_rich_text_delete_line (GtkRichText *text)
{
}

static void
gtk_rich_text_delete_backward_word (GtkRichText *text)
{
  Position end = text->cursor;
  ParagraphTree *newpar;
  ParagraphPosition pos = end.paragraph_ptr;
  end.paragraph_ptr =
    gtk_paragraph_move_backward_word( text->cursor.paragraph->paragraph,
				      text->cursor.paragraph_ptr );
  if ( gtk_paragraph_position_equal( end.paragraph->paragraph,
				     pos, end.paragraph_ptr ) )
    {
      newpar = find_previous_paragraph( end.paragraph );
      if ( newpar != end.paragraph )
	{
	  end.paragraph = newpar;
	  end.paragraph_ptr = gtk_paragraph_end( end.paragraph->paragraph );
	  end.paragraph_ptr =
	    gtk_paragraph_move_backward_word( end.paragraph->paragraph,
					      end.paragraph_ptr );
	}
    }
  text->ideal_valid = FALSE;
  gtk_rich_text_delete_through_editable( text, end, text->cursor );
}

static void
gtk_rich_text_move_backward_word (GtkRichText *text)
{
  Position pos = text->cursor;
  Position newpos = pos;
  ParagraphTree *newpar;
  newpos.paragraph_ptr =
    gtk_paragraph_move_backward_word( newpos.paragraph->paragraph,
				      newpos.paragraph_ptr );
  if ( gtk_paragraph_position_equal( newpos.paragraph->paragraph,
				     pos.paragraph_ptr, newpos.paragraph_ptr )
       )
    {
      newpar = find_previous_paragraph( pos.paragraph );
      if ( newpar != pos.paragraph )
	{
	  newpos.paragraph = newpar;
	  newpos.paragraph_ptr = gtk_paragraph_end( newpos.paragraph->paragraph );
	  newpos.paragraph_ptr =
	    gtk_paragraph_move_backward_word( newpos.paragraph->paragraph,
					      newpos.paragraph_ptr );
	}
    }
  text->ideal_valid = FALSE;
  gtk_rich_text_real_set_position( text, newpos );
}

static void
gtk_rich_text_delete_forward_word (GtkRichText *text)
{
  Position end = text->cursor;
  ParagraphPosition pos = end.paragraph_ptr;
  ParagraphTree *newpar;
  end.paragraph_ptr =
    gtk_paragraph_move_forward_word( text->cursor.paragraph->paragraph,
				     text->cursor.paragraph_ptr );
  if ( gtk_paragraph_position_equal( (Paragraph *) end.paragraph->data,
				     pos, end.paragraph_ptr )
       )
    {
      newpar = find_next_paragraph( end.paragraph );
      if ( newpar != end.paragraph )
	{
	  end.paragraph = newpar;
	  end.paragraph_ptr = gtk_paragraph_start( end.paragraph->paragraph );
	  end.paragraph_ptr =
	    gtk_paragraph_move_forward_word( end.paragraph->paragraph,
					     end.paragraph_ptr );
	}
    }
  gtk_rich_text_delete_through_editable( text, text->cursor, end );
}

static void
gtk_rich_text_move_forward_word (GtkRichText *text)
{
  Position pos = text->cursor;
  Position newpos = pos;
  newpos.paragraph_ptr =
    gtk_paragraph_move_forward_word( newpos.paragraph->paragraph,
				     newpos.paragraph_ptr );
  if ( gtk_paragraph_position_equal( newpos.paragraph->paragraph,
				     pos.paragraph_ptr, newpos.paragraph_ptr )
       )
    {
      
      ParagraphTree *newpar = find_next_paragraph( pos.paragraph );
      if ( newpar != pos.paragraph )
	{
	  newpos.paragraph = newpar;
	  newpos.paragraph_ptr =
	    gtk_paragraph_start( newpos.paragraph->paragraph );
	  newpos.paragraph_ptr =
	    gtk_paragraph_move_forward_word( newpos.paragraph->paragraph,
					     newpos.paragraph_ptr );
	}
    }
  text->ideal_valid = FALSE;
  gtk_rich_text_real_set_position( text, newpos );
}

static void
gtk_rich_text_vertical_move_page (GtkRichText *text, gint distance)
{
  gint x, y;
  gtk_rich_text_cursor_coords( text, NULL, &y );
  gtk_rich_text_set_ideal( text );
  x = text->ideal;
  y += text->vadj->page_increment * distance;
  gtk_rich_text_real_set_position( text,
				   gtk_rich_text_find_point( text, x, y ) );
}

static void gtk_rich_text_move_end_of_buffer (GtkRichText *text)
{
  Position pos;
  pos.paragraph = text->paragraphs;
  while( pos.paragraph->right )
    pos.paragraph = pos.paragraph->right;
  pos.paragraph_ptr = gtk_paragraph_end( pos.paragraph->paragraph );
  gtk_rich_text_real_set_position( text, pos );
}

static void gtk_rich_text_move_beginning_of_buffer (GtkRichText *text)
{
  Position pos;
  pos.paragraph = text->paragraphs;
  while( pos.paragraph->left )
    pos.paragraph = pos.paragraph->left;
  pos.paragraph_ptr = gtk_paragraph_start( pos.paragraph->paragraph );
  gtk_rich_text_real_set_position( text, pos );
}

static gint
gtk_rich_text_top_of_position( GtkRichText *text, Position pos )
{
  gint top;
  ParagraphTree *tree;
  ParagraphTree *last;
  
  top = gtk_paragraph_top_of_pos( pos.paragraph->paragraph,
				  pos.paragraph_ptr );
  tree = pos.paragraph;
  last = NULL;
  if ( tree->left )
    top += tree->left->height;
  last = tree;
  tree = tree->parent;
  while( tree )
    {
      if ( last == tree->right )
	{
	  if ( tree->paragraph )
	    top += gtk_paragraph_height( tree->paragraph );
	  if ( tree->left )
	    top += tree->left->height;
	}
      last = tree;
      tree = tree->parent;
    }
  return top;
}

static gint
gtk_rich_text_top_of_cursor( GtkRichText *text )
{
  return gtk_rich_text_top_of_position( text, text->cursor );
}

static gint
gtk_rich_text_bottom_of_position( GtkRichText *text,
				  Position pos )
{
  gint bottom;
  ParagraphTree *tree;
  ParagraphTree *last;
  
  bottom = gtk_paragraph_bottom_of_pos( pos.paragraph->paragraph,
					pos.paragraph_ptr );
  tree = pos.paragraph;
  last = NULL;
  if ( tree->left )
    bottom += tree->left->height;
  last = tree;
  tree = tree->parent;
  while( tree )
    {
      if ( last == tree->right )
	{
	  if ( tree->paragraph )
	    bottom += gtk_paragraph_height( tree->paragraph );
	  if ( tree->left )
	    bottom += tree->left->height;
	}
      last = tree;
      tree = tree->parent;
    }
  return bottom;
}

static gint
gtk_rich_text_bottom_of_cursor( GtkRichText *text )
{
  return gtk_rich_text_bottom_of_position( text, text->cursor );
}

static void
gtk_rich_text_locate_cursor( GtkRichText *text )
{
  gint height;

  D( "Entering adjust_adjs.\n" );

  height = GTK_WIDGET( text )->allocation.height;
  
  text->vadj->value = MIN( text->vadj->value,
			   gtk_rich_text_top_of_cursor( text ) );
  text->vadj->value = MAX( text->vadj->value,
			   gtk_rich_text_bottom_of_cursor( text ) - height );
  
  gtk_signal_emit_by_name (GTK_OBJECT (text->vadj), "changed");
}

static void
gtk_rich_text_cursor_coords( GtkRichText *text, gint *x, gint *y )
{
  ParagraphTree *tree;
  ParagraphTree *last;

  if ( text->cursor.paragraph == NULL )
    {
      if ( x != NULL )
	*x = 0;
      if ( y != NULL )
	*y = 0;
      return;
    }

  gtk_paragraph_position_coords( text->cursor.paragraph->paragraph,
				 text->cursor.paragraph_ptr,
				 x, y );
  tree = text->cursor.paragraph;
  last = NULL;
  if ( y != NULL )
    {
      if ( tree->left )
	(*y) += tree->left->height;
      last = tree;
      tree = tree->parent;
      while( tree )
	{
	  if ( last == tree->right )
	    {
	      if ( tree->paragraph )
		(*y) += gtk_paragraph_height( tree->paragraph );
	      if ( tree->left )
		(*y) += tree->left->height;
	    }
	  last = tree;
	  tree = tree->parent;
	}
    }
}

static void
gtk_rich_text_insert_as_editable (GtkEditable       *editable,
				  const gchar       *new_text,
				  gint               new_text_length,
				  gint               *position)
{
  GtkRichText *text = GTK_RICH_TEXT( editable );
  Position poss[3];
  Position insertpoint = gtk_rich_text_position( text, *position );

  poss[0] = text->cursor;
  poss[1] = text->selection_end;
  if ( text->point_valid )
    {
      poss[2] = text->point;
    }
 
  insertpoint =
    gtk_rich_text_insert_internal( text, insertpoint,
				   (gchar *) new_text, new_text_length,
				   poss,
				   text->point_valid ? 3 : 2 );
  
  text->cursor = poss[0];
  text->selection_end = poss[1];
  if ( text->point_valid )
    {
      text->point = poss[2];
    }
  GTK_EDITABLE( text )->current_pos =
    gtk_rich_text_position_to_int( text, text->cursor );

  *position = gtk_rich_text_position_to_int( text, insertpoint );

  if ( ! text->frozen )
    {
      adjust_adjs( text );
      gtk_rich_text_locate_cursor( text );
      if ( ! text->incremental_redraw_handled )
	gtk_rich_text_queue_redraw( text );
    }
}

static void
gtk_rich_text_delete_as_editable (GtkEditable       *editable,
				  gint               start,
				  gint               end)
{
  GtkRichText *text = GTK_RICH_TEXT( editable );
  Position a = gtk_rich_text_position( text, start );
  Position b = gtk_rich_text_position( text, end );
  Position poss[3];

  poss[0] = text->cursor;
  poss[1] = text->selection_end;
  if ( text->point_valid )
    {
      poss[2] = text->point;
    }
 
  gtk_rich_text_delete_internal( text, a, b, poss,
				 text->point_valid ? 3 : 2 );

  text->cursor = poss[0];
  text->selection_end = poss[1];
  if ( text->point_valid )
    {
      text->point = poss[2];
    }
  editable->current_pos = gtk_rich_text_position_to_int( text, text->cursor );
  if ( text->cursor_first )
    {
      editable->selection_start_pos = editable->current_pos;
      editable->selection_end_pos =
	gtk_rich_text_position_to_int( text, text->selection_end );
    }
  else
    {
      editable->selection_start_pos =
	gtk_rich_text_position_to_int( text, text->selection_end );
      editable->selection_end_pos = editable->current_pos;
    }
}

static void
gtk_rich_text_set_selection (GtkEditable        *editable,
			     gint                start_pos,
			     gint                end_pos)
{
  GtkRichText *text = GTK_RICH_TEXT( editable );
  if( start_pos > end_pos )
    {
      gint temp = start_pos;
      start_pos = end_pos;
      end_pos = temp;
    }
  editable->selection_start_pos = start_pos;
  editable->selection_end_pos = end_pos;
  text->cursor = gtk_rich_text_position( text, start_pos );
  if ( start_pos == end_pos )
    {
      text->selection_end = text->cursor;
      text->selection_active = FALSE;
    }
  else
    {
      text->selection_end = gtk_rich_text_position( text, end_pos );
      text->selection_active = TRUE;
      text->cursor_first = TRUE;
    }
  if ( ! text->incremental_redraw_handled )
    gtk_rich_text_queue_redraw( text );
}

static void
gtk_rich_text_set_paragraph_params (GtkRichText *text,
				    Position a,
				    Position b,
				    ParagraphParams *params,
				    ParagraphParamsValid *validity)
{
  ParagraphTree *tree;
  ParagraphTree *last = NULL;
  for ( tree = a.paragraph;
	tree != last && tree != b.paragraph;
	last = tree,
	tree = find_next_paragraph( tree ) )
    {
      gtk_paragraph_set_params( tree->paragraph, params, validity );
      update_counts( tree );
    }
  if ( tree )
    {
      gtk_paragraph_set_params( tree->paragraph, params, validity );
      update_counts( tree );
    }
  adjust_adjs( text );
  gtk_rich_text_queue_redraw( text );
}

void
gtk_rich_text_set_selection_paragraph_params (GtkRichText *text,
					      ParagraphParams *params,
					      ParagraphParamsValid *validity)
{
  Position first = text->cursor_first ? text->cursor : text->selection_end;
  Position second = text->cursor_first ? text->selection_end : text->cursor;
  gtk_rich_text_set_paragraph_params( text, first, second, params, validity );
}

void
gtk_rich_text_set_location_paragraph_params (GtkRichText *text,
					     gint location,
					     ParagraphParams *params,
					     ParagraphParamsValid *validity)
{
  Position first;
  Position second;
  first = second = gtk_rich_text_position( text, location );
  gtk_rich_text_set_paragraph_params( text, first, second, params, validity );
}

void
gtk_rich_text_set_range_paragraph_params (GtkRichText *text,
					  gint start,
					  gint end,
					  ParagraphParams *params,
					  ParagraphParamsValid *validity)
{
  Position first;
  Position second;
  first = gtk_rich_text_position( text, start );
  second = gtk_rich_text_position( text, end );
  gtk_rich_text_set_paragraph_params( text, first, second, params, validity );
}

void
gtk_rich_text_get_paragraph_params (GtkRichText *text,
				    Position a,
				    Position b,
				    ParagraphParams *params,
				    ParagraphParamsValid *validity)
{
  ParagraphTree *tree;
  ParagraphTree *last = NULL;
  tree = a.paragraph;
  gtk_paragraph_get_params( tree->paragraph, params, validity );
  for ( tree = a.paragraph;
	tree != last && tree != b.paragraph;
	last = tree,
	tree = find_next_paragraph( tree ) )
    {
      gtk_paragraph_get_params_match( tree->paragraph, params, validity );
    }
  if ( tree )
    gtk_paragraph_get_params_match( tree->paragraph, params, validity );
}

void
gtk_rich_text_get_selection_paragraph_params (GtkRichText *text,
					     ParagraphParams *params,
					     ParagraphParamsValid *validity)
{
  Position a = text->cursor_first ? text->cursor : text->selection_end;
  Position b = text->cursor_first ? text->selection_end : text->cursor;
  gtk_rich_text_get_paragraph_params( text, a, b, params, validity );
}

void
gtk_rich_text_set_selection_character_params (GtkRichText *text,
					      CharacterParams *params,
					      CharacterParamsValid *validity)
{
  Position first = text->cursor_first ? text->cursor : text->selection_end;
  Position second = text->cursor_first ? text->selection_end : text->cursor;
  GtkRichTextFont *font = params->font;
  if ( validity->font_valid )
    gtk_rich_text_set_font( text, first, second, font );
}

void
gtk_rich_text_set_location_character_params (GtkRichText *text,
					     gint location,
					     CharacterParams *params,
					     CharacterParamsValid *validity)
{
  Position first;
  Position second;
  GtkRichTextFont *font = params->font;
  if ( validity->font_valid )
    {
      first = second = gtk_rich_text_position( text, location );
      gtk_rich_text_set_font( text, first, second, font );
    }
}

void
gtk_rich_text_set_range_character_params (GtkRichText *text,
					  gint start,
					  gint end,
					  CharacterParams *params,
					  CharacterParamsValid *validity)
{
  Position first;
  Position second;
  GtkRichTextFont *font = params->font;
  if ( validity->font_valid )
    {
      first = gtk_rich_text_position( text, start );
      second = gtk_rich_text_position( text, end );
      gtk_rich_text_set_font( text, first, second, font );
    }
}

/* This function could be rewritten to be a top down recursive
   function.  This might be more efficient. */
void
gtk_rich_text_traverse( GtkRichText *text,
			GtkRichTextTraversalCallbacks *callbacks,
			gpointer data )
{
  ParagraphTree *tree;
  ParagraphTree *last = NULL;
  for ( tree = find_first_paragraph( text->paragraphs );
	tree != last;
	last = tree, tree = find_next_paragraph( tree ) )
    {
      gtk_paragraph_traverse( tree->paragraph,
			      callbacks, data );
    }
}

/* This function could be rewritten to be a top down recursive
   function.  It would almost definitely be more efficient. */
void
gtk_rich_text_traverse_partial( GtkRichText *text,
				gint start,
				gint end,
				GtkRichTextTraversalCallbacks *callbacks,
				gpointer data )
{
  ParagraphTree *tree;
  ParagraphTree *last = NULL;
  int position = 0;
  int length = gtk_rich_text_get_length( text );
  if ( end == -1 ) end = length;
  start = MAX( start, 0 );
  end = MIN( end, length );

  for ( tree = find_first_paragraph( text->paragraphs );
	tree != last && position <= end;
	last = tree, tree = find_next_paragraph( tree ) )
    {
      Paragraph *paragraph = tree->paragraph;
      int parend = position + gtk_paragraph_length( paragraph );
      if ( position <= end && parend >= start )
	{
	  gint thisstart = MAX( start, position );
	  gint thisend = MIN( end, parend );
	  gtk_paragraph_traverse_partial( paragraph,
					  thisstart - position,
					  thisend - position,
					  callbacks, data );
	}
      position = parend;
      position += 1;
    }
}

void
gtk_rich_text_traverse_selection( GtkRichText *text,
				  GtkRichTextTraversalCallbacks *callbacks,
				  gpointer data )
{
  gint start =
    gtk_rich_text_position_to_int( text,
				   text->cursor_first ?
				   text->cursor :
				   text->selection_end );
  gint end =
    gtk_rich_text_position_to_int( text,
				   text->cursor_first ?
				   text->selection_end :
				   text->cursor );
  gtk_rich_text_traverse_partial( text,
				  start,
				  end,
				  callbacks,
				  data );
}

GtkRichTextFont *
gtk_rich_text_font_new( gchar *desc )
{
  GtkRichTextFont *return_val = g_new( GtkRichTextFont, 1 );
  if ( desc == NULL || *desc == 0 )
    desc = "fixed";
  return_val->font = gdk_font_load( desc );
  if ( return_val->font != NULL )
    {
      gdk_font_ref( return_val->font );
      return_val->desc = g_strdup( desc );
      return_val->ref_count = 0;
    }
  else
    {
      g_free( return_val );
      return_val = NULL;
    }
  return_val->data = NULL;
  return return_val;
}

GtkRichTextFont *
gtk_rich_text_font_adopt( GdkFont *font,
			  gchar *desc,
			  gpointer data )
{
  if ( font != NULL )
    {
      GtkRichTextFont *return_val = g_new( GtkRichTextFont, 1 );
      return_val->font = font;
      gdk_font_ref( return_val->font );
      if ( desc != NULL )
	return_val->desc = g_strdup( desc );
      else
	return_val->desc = g_strdup( "" );
      return_val->ref_count = 0;
      return_val->data = data;
      return return_val;
    }
  else
    {
      return NULL;
    }
}

void
gtk_rich_text_font_ref( GtkRichTextFont *font )
{
  font->ref_count ++;
}

void
gtk_rich_text_font_unref( GtkRichTextFont *font )
{
  font->ref_count --;
  if ( font->ref_count <= 0 )
    {
      gdk_font_unref( font->font );
      g_free( font->desc );
      g_free( font );
    }
}

gint
gtk_rich_text_get_position_line_number( GtkRichText *text )
{
  gint line_number = 0;
  ParagraphTree *tree;
  ParagraphTree *last = NULL;
  for ( tree = find_first_paragraph( text->paragraphs );
	tree != last &&
	tree != text->cursor.paragraph;
	last = tree,
	tree = find_next_paragraph( tree ) )
    {
      line_number += gtk_paragraph_line_count( tree->paragraph );
    }
  return line_number
    + gtk_paragraph_get_line_number( tree->paragraph,
				     text->cursor.paragraph_ptr );
}

void
gtk_rich_text_render( GtkRichText *text,
		      GtkRichTextRenderSystem *system )
{
  ParagraphTree *tree;
  ParagraphTree *last = NULL;
  for ( tree = find_first_paragraph( text->paragraphs );
	tree != last;
	last = tree,
	tree = find_next_paragraph( tree ) )
    {
      Paragraph *paragraph = tree->paragraph;
      gtk_paragraph_render( paragraph,
			    system );
    }
  system->output_page( system );
}
