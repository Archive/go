/* gtkword.c
 * Copyright (C) 1998 Chris Lahey
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include "gtkword.h"
#include "gtkrichtext.h"
#include <string.h>
#include <libhnj/hyphen.h>

static HyphenDict *dict;

static GtkWordFontSection *
new_font_section( GtkRichTextFont *font, gint start )
{
  GtkWordFontSection *section = g_new( GtkWordFontSection, 1 );
  section->font = font;
  gtk_rich_text_font_ref( section->font );
  section->start = start;
  return section;
}

static GList *
duplicate_font_sections( GList *fonts, int offset )
{
  GList *item = NULL;
  for ( ; fonts != NULL; fonts = fonts->next )
    {
      GtkWordFontSection *section =
	new_font_section( ((GtkWordFontSection *) fonts->data)->font,
			  ((GtkWordFontSection *) fonts->data)->start + offset );
      item = g_list_append( item, section );
    }
  return item;
}

static GList *
insert_font_sections( GList *fonts, int offset, int amount )
{
  GList *start = fonts;
  for ( ; fonts != NULL && ((GtkWordFontSection *) fonts->data)->start < offset; fonts = fonts->next )
    /* Empty Statement. */ ;

  for ( ; fonts != NULL; fonts = fonts->next )
    {
      GtkWordFontSection *section = fonts->data;
      section->start += amount;
    }
  return start;
}

static void
insert_font_sections_in_word_with_font( Word *word, int offset, int amount, GtkRichTextFont *font )
{
  word->fonts = insert_font_sections( word->fonts, offset, amount );
  if ( offset == 0 )
    {
      if ( font != word->font )
	{
	  if ( amount != word->length )
	    word->fonts = g_list_prepend( word->fonts, new_font_section( word->font, amount ) );
	  gtk_rich_text_font_unref( word->font );
	  word->font = font;
	  gtk_rich_text_font_ref( font );
	}
    }
  else
    {
      GList *fonts = word->fonts;
      if ( fonts )
	{
	  if ( fonts->next == NULL )
	    {
	      if ( ((GtkWordFontSection *)fonts->data)->start >= offset + amount )
		{
		  if ( font != word->font )
		    {
		      GtkWordFontSection *newsection = new_font_section( font, offset );
		      word->fonts = g_list_prepend( word->fonts, newsection );
		    }
		}
	      else
		{
		  if ( font != ((GtkWordFontSection *)word->fonts->data)->font )
		    {
		      GtkWordFontSection *newsection = new_font_section( font, offset );
		      word->fonts = g_list_append( word->fonts, newsection );
		    }
		}
	    }
	  else
	    {
	      while( fonts->next != NULL && ((GtkWordFontSection *)fonts->next->data)->start < offset + amount )
		fonts = fonts->next;
	      if ( ( fonts->next != NULL && ((GtkWordFontSection *) fonts->next->data)->start == offset + amount ) || offset == gtk_word_end( word ) )
		{
		  GtkWordFontSection *newsection = new_font_section( font, offset );
		  word->fonts = g_list_first( g_list_insert( fonts, newsection, 1 ) );
		}
	      else
		{
		  GtkWordFontSection *newsection = new_font_section( ((GtkWordFontSection *) fonts->data)->font, offset + amount );
		  word->fonts = g_list_first( g_list_insert( fonts, newsection, 1 ) );
	      
		  newsection = new_font_section( font, offset );
		  word->fonts = g_list_first( g_list_insert( fonts, newsection, 1 ) );
		}
	    }
	}
      else
	{
	  GtkWordFontSection *newsection = new_font_section( font, offset );
	  word->fonts = g_list_append( word->fonts, newsection );

	  if( offset + amount < word->length )
	    {
	      newsection = new_font_section( word->font, offset + amount );
	      word->fonts = g_list_append( word->fonts, newsection );
	    }
	}
    }
}

static GList *
delete_font_sections( GList *fonts, int offset, int amount )
{
  GList *item = NULL;
  GList *start = fonts;
  for ( ; fonts != NULL && ((GtkWordFontSection *) fonts->data)->start < offset; fonts = fonts->next )
    /* Empty Statement. */ ;

  if ( fonts )
    {
      while ( fonts->next != NULL && ((GtkWordFontSection *) fonts->next->data)->start <= offset + amount )
	{
	  item = fonts->next;
	  gtk_rich_text_font_unref( ((GtkWordFontSection *) fonts->data)->font );
	  g_free( fonts->data );
	  start = g_list_remove_link( start, fonts );
	  g_list_free_1( fonts );
	  fonts = item;
	}
      if ( ((GtkWordFontSection *) fonts->data)->start <= offset + amount )
	{
	  ((GtkWordFontSection *)fonts->data)->start = offset;
	  fonts = fonts->next;
	}
      for ( ; fonts != NULL; fonts = fonts->next )
	{
	  GtkWordFontSection *section = fonts->data;
	  section->start -= amount;
	}
    }
  return start;
}

GtkRichTextFont *
gtk_word_char_font( Word *word, gint position )
{
  GList *list = word->fonts;
  GtkRichTextFont *font = word->font;
  while ( list )
    {
      GtkWordFontSection *section = list->data;
      if ( section->start > position )
	break;
      font = section->font;
      list = list->next;
    }
  return font;
}

gint
gtk_word_text_width( Word *word, gint start, gint end )
{
  GList *list = word->fonts;
  GtkRichTextFont *font = word->font;
  gint thisstart, thisend;
  int i = 0;
  int width = 0;
  while ( list )
    {
      GtkWordFontSection *section = list->data;
      thisstart = MIN( MAX( i, start ), end );
      thisend = MAX( MIN( section->start, end ), start );
      if ( thisstart != thisend )
	width += gdk_text_width( font->font, word->text + thisstart, thisend - thisstart );
      font = section->font;
      i = section->start;
      list = list->next;
    }
  thisstart = MIN( MAX( i, start ), end );
  thisend = MAX( MIN( word->length, end ), start );
  if ( thisstart != thisend )
    width += gdk_text_width( font->font, word->text + thisstart, thisend - thisstart );
  return width;
}

void
gtk_word_draw_text	 (Word         *word,
			  gint          start,
			  gint          end,
			  GdkDrawable  *drawable,
			  GdkGC	       *gc,
			  gint		x,
			  gint		y)
{
  GList *list = word->fonts;
  GtkRichTextFont *font = word->font;
  gint thisstart, thisend;
  int i = 0;

  while ( list )
    {
      GtkWordFontSection *section = list->data;
      thisstart = MIN( MAX( i, start ), end );
      thisend = MAX( MIN( section->start, end ), start );
      if ( thisstart != thisend )
	{
	  gdk_draw_text( drawable, font->font, gc, x, y, word->text + thisstart, thisend - thisstart );
	  x += gdk_text_width( font->font, word->text + thisstart, thisend - thisstart );
	}
      font = section->font;
      i = section->start;
      list = list->next;
    }
  thisstart = MIN( MAX( i, start ), end );
  thisend = MAX( MIN( word->length, end ), start );
  if ( thisstart != thisend )
    {
      gdk_draw_text( drawable, font->font, gc, x, y, word->text + thisstart, thisend - thisstart );
      x += gdk_text_width( font->font, word->text + thisstart, thisend - thisstart );
    }
}

void gtk_word_draw_text_with_selection	 (Word         *word,
					  gint          start,
					  gint          end,
					  GdkDrawable  *drawable,
					  GdkGC	       *gc,
					  GdkGC	       *gcsel,
					  gint		x,
					  gint		y,
					  WordPosition  selstart,
					  WordPosition  selend,
					  gint          negy,
					  gint          posy )
{
  GList *list = word->fonts;
  GtkRichTextFont *font = word->font;
  gint thisstart, thisend;
  int i = 0;

  gdk_draw_rectangle( drawable,
		      gcsel,
		      TRUE,
		      x + gtk_word_text_width( word, start, selstart ),
		      y - negy,
		      gtk_word_text_width( word, selstart, selend ),
		      negy + posy );

  while ( list )
    {
      GtkWordFontSection *section = list->data;
      thisstart = MIN( MAX( i, start ), end );
      thisend = MAX( MIN( section->start, end ), start );
      if ( thisstart != thisend )
	{
	  gdk_draw_text( drawable, font->font, gc, x, y, word->text + thisstart, thisend - thisstart );
	  x += gdk_text_width( font->font, word->text + thisstart, thisend - thisstart );
	}
      font = section->font;
      i = section->start;
      list = list->next;
    }
  thisstart = MIN( MAX( i, start ), end );
  thisend = MAX( MIN( word->length, end ), start );
  if ( thisstart != thisend )
    {
      gdk_draw_text( drawable, font->font, gc, x, y, word->text + thisstart, thisend - thisstart );
    }
}


void
gtk_word_init( void )
{
  dict = hnj_hyphen_load ( DATADIR "/hypn/hyphen.mashed");
  if ( dict == NULL )
    {
      g_error( "Please make install with GtkRichText enabled before running.\nIf you have make installed (with GtkRichText), you've found a bug.\nPlease report it to clahey@umich.edu.\n" );
    }
}

static gint
gtk_word_width( Word *word )
{
  GList *list = word->fonts;
  GtkRichTextFont *font = word->font;
  int i = 0;
  int width = 0;
  while ( list )
    {
      GtkWordFontSection *section = list->data;
      width += gdk_text_width( font->font, word->text + i, section->start - i );
      font = section->font;
      i = section->start;
      list = list->next;
    }
  width += gdk_text_width( font->font, word->text + i, word->length - i );
  return width;
}

Word *
gtk_word_new (gchar *text, gint length, GtkRichTextFont *font, GtkRichText *parent)
{
  Word *new_word = g_new( Word, 1 );
  new_word->parent = parent;
  new_word->text = g_strndup( text, length );
  new_word->length = length;
  new_word->space = GTK_SPACE_SPACE;
  if ( font != NULL )
    new_word->font = font;
  else
    new_word->font = gtk_rich_text_font_new( "fixed" );
  gtk_rich_text_font_ref( new_word->font );
  new_word->fonts = NULL;

  new_word->width = gtk_word_width( new_word );

  if ( new_word->length > 0 )
    {
      new_word->hyphenation = g_new( gchar, new_word->length + 4);
      hnj_hyphen_hyphenate (dict, 
			    new_word->text, 
			    new_word->length,
			    new_word->hyphenation);
    }
  else
    new_word->hyphenation = NULL;
  return new_word;
}

void
gtk_word_destroy (Word *word)
{
  g_list_foreach( word->fonts, (GFunc) g_free, NULL );
  g_list_free( word->fonts );
  g_free( word->text );
  g_free( word->hyphenation );
}

Word *
gtk_word_concat (Word *worda, Word *wordb)
{
  Word *new_word = g_new( Word, 1 );
  GtkWordFontSection *newsection;

  new_word->parent = worda->parent;
  new_word->length = worda->length + wordb->length;
  new_word->text = g_malloc0( new_word->length + 1 );
  new_word->space = wordb->space;
  g_snprintf( new_word->text, new_word->length + 1, "%s%s", worda->text, wordb->text );

  /* Deal with concatenating font info. */
  new_word->font = worda->font;
  new_word->fonts = duplicate_font_sections( worda->fonts, 0 );
  newsection = g_new( GtkWordFontSection, 1 );
  newsection->font = wordb->font;
  newsection->start = worda->length;
  new_word->fonts = g_list_append( new_word->fonts, newsection);
  new_word->fonts = g_list_concat( new_word->fonts, duplicate_font_sections( wordb->fonts, worda->length ) );

  new_word->width = gtk_word_width( new_word );
    

  if ( new_word->length > 0 )
    {
      new_word->hyphenation = g_new( gchar, new_word->length + 4);
      hnj_hyphen_hyphenate (dict, 
			    new_word->text, 
			    new_word->length,
			    new_word->hyphenation);
    }
  else
    new_word->hyphenation = NULL;
  return new_word;
}

void
gtk_word_set_font( Word *word, WordPosition start, WordPosition end, GtkRichTextFont *font )
{
  if ( font != NULL )
    {
      word->fonts = delete_font_sections( word->fonts, start, end - start );
      insert_font_sections_in_word_with_font( word, start, end - start, font );
      word->width = gtk_word_width( word );
    }
}

Word *
gtk_word_break( Word *word, WordPosition position, GtkSpaceType space )
{
  Word *new_word = g_new( Word, 1 );
  gchar *text;
  GList *list;

  new_word->parent = word->parent;
  new_word->length = word->length - position;
  new_word->text = g_malloc0( new_word->length + 1 );
  new_word->space = word->space;
  word->space = space;
  g_snprintf( new_word->text, new_word->length + 1, "%s", word->text + position );
  
  if( word->fonts )
    {
      list = word->fonts;
      if ( ((GtkWordFontSection *)list->data)->start < position )
	{
	  while( list->next && ((GtkWordFontSection *) list->next->data)->start < position )
	    {
	      list = list->next;
	    }
	  if ( list->next && ((GtkWordFontSection *)list->next->data)->start == position )
	    {
	      GList *fonts = list->next;
	      new_word->font = ((GtkWordFontSection *) fonts->data)->font; /* A ref and an unref. */
	      g_free( fonts->data );
	      word->fonts = g_list_remove_link( word->fonts, fonts );
	      g_list_free_1( fonts );
	    }
	  else
	    new_word->font = ((GtkWordFontSection *) list->data)->font;
	  gtk_rich_text_font_ref( new_word->font );
	      
	  new_word->fonts = list->next;
	  if ( list->next != NULL )
	    list->next->prev = NULL;
	  list->next = NULL;
	  new_word->fonts = delete_font_sections( new_word->fonts, 0, position );
	}
      else if ( ((GtkWordFontSection *)list->data)->start == position )
	{
	  GList *fonts = list;
	  list = fonts->next;
	  new_word->font = ((GtkWordFontSection *) fonts->data)->font; /* A ref and an unref. */
	  g_free( fonts->data );
	  g_list_free_1( fonts );
	  word->fonts = NULL;
	  new_word->fonts = list;
	  if ( list != NULL )
	    list->prev = NULL;
	  new_word->fonts = delete_font_sections( new_word->fonts, 0, position );
	}
      else
	{
	  new_word->font = word->font;
	  gtk_rich_text_font_ref( new_word->font );
	  new_word->fonts = word->fonts;
	  word->fonts = NULL;
	  new_word->fonts = delete_font_sections( new_word->fonts, 0, position );
	}
    }
  else
    {
      new_word->font = word->font;
      gtk_rich_text_font_ref( new_word->font );
      new_word->fonts = NULL;
    }
  
  new_word->width = gtk_word_width( new_word );

  if ( new_word->length > 0 )
    {
      new_word->hyphenation = g_new( gchar, new_word->length + 4);
      hnj_hyphen_hyphenate (dict, 
			    new_word->text, 
			    new_word->length,
			    new_word->hyphenation);
    }
  else
    new_word->hyphenation = NULL;

  word->length = position;
  text = g_new( gchar, word->length + 1 );
  word->text[position] = 0;
  g_snprintf( text, word->length + 1, "%s", word->text );

  word->width = gtk_word_width( word );

  g_free( word->text );
  word->text = text;

  g_free( word->hyphenation );

  if( word->length > 0 )
    {
      word->hyphenation = g_new( gchar, word->length + 4);
      hnj_hyphen_hyphenate (dict, 
			    word->text, 
			    word->length,
			    word->hyphenation);
    }
  else
    word->hyphenation = NULL;

  return new_word;
}

WordPosition
gtk_word_decrement_position( Word *word, WordPosition old_pos, gint distance )
{
  if ( distance >= old_pos )
    return 0;
  else
    return old_pos - distance;
}

gint
gtk_word_position_compare( Word *word, WordPosition a, WordPosition b )
{
  if ( a < b ) return -1;
  if ( a > b ) return 1;
  return 0;
}

WordPosition
gtk_word_insert( Word *word, WordPosition start, gchar *vals, gint length )
{
  Word new_word;

  if ( start < gtk_word_start( word ) )
    start = gtk_word_start( word );

  new_word.length = word->length + length;
  new_word.text = g_malloc( new_word.length + length );
  strncpy( new_word.text, word->text, start );
  strncpy( new_word.text + start, vals, length );
  strncpy( new_word.text + start + length, word->text + start, word->length - start );
  new_word.text[ new_word.length ] = 0;
  new_word.font = word->font;
  new_word.fonts = insert_font_sections( word->fonts, start, length );
  new_word.width = gtk_word_width( &new_word );
  new_word.space = word->space;
  new_word.parent = word->parent;

  if ( new_word.length > 0 )
    {
      new_word.hyphenation = g_new( gchar, new_word.length + 4 );
      hnj_hyphen_hyphenate (dict, 
			    new_word.text, 
			    new_word.length,
			    new_word.hyphenation);
    }
  else
    new_word.hyphenation = NULL;
  g_free( word->text );
  g_free( word->hyphenation );
  *word = new_word;
  return start + length;
}

WordPosition
gtk_word_insert_with_data( Word *word, WordPosition start, gchar *vals, gint length, GtkRichTextFont *font )
{
  WordPosition end = gtk_word_insert( word, start, vals, length );
  gtk_word_set_font( word, start, end, font );
  return end;
}

void
gtk_word_delete( Word *word, WordPosition start, WordPosition end )
{
  Word new_word;

  if ( start < gtk_word_start( word ) )
    start = gtk_word_start( word );
  if ( end > gtk_word_end( word ) )
    end = gtk_word_end( word );

  new_word.length = word->length - end + start;
  new_word.text = g_malloc( new_word.length + 1 );
  strncpy( new_word.text, word->text, start );
  strncpy( new_word.text + start, word->text + end, word->length - end );
  new_word.text[ new_word.length ] = 0;
  new_word.font = word->font;
  new_word.fonts = delete_font_sections( word->fonts, start, end - start );
  new_word.width = gtk_word_width( &new_word );
  new_word.space = word->space;
  new_word.parent = word->parent;

  if ( new_word.length > 0 )
    {
      new_word.hyphenation = g_new( gchar, new_word.length + 4 );
      hnj_hyphen_hyphenate (dict, 
			    new_word.text, 
			    new_word.length,
			    new_word.hyphenation);
    }
  else
    new_word.hyphenation = NULL;
  g_free( word->text );
  g_free( word->hyphenation );
  *word = new_word;
}
#if 0
gint
gtk_word_length( Word *word )
{
  return word->length;
}

WordPosition
gtk_word_start( Word *word )
{
  return 0;
}

WordPosition
gtk_word_end( Word *word )
{
  return word->length;
}

WordPosition
gtk_word_position( Word *word, int position )
{
  return position;
}

gint gtk_word_position_to_int (Word *word, WordPosition pos)
{
  return pos;
}

gboolean
gtk_word_a_first( Word *word, WordPosition a, WordPosition b )
{
  return a < b;
}
#endif

WordPosition
gtk_word_increment_position( Word *word, WordPosition old_pos, gint distance )
{
  WordPosition pos = old_pos + distance;
  if ( pos > word->length )
    pos = word->length;
  return pos;
}

void
gtk_word_get_chars (Word *word, gchar *buffer, gint start, gint end )
{
  start = MAX( start, 0 );
  end = MIN( end, gtk_word_length( word ) );
  strncpy( buffer, word->text + start, end - start );
}

gint
gtk_word_max_ascent (Word *word, gint start, gint end)
{
  GList *list = word->fonts;
  GtkRichTextFont *font = word->font;
  gint thisstart, thisend;
  int i = 0;
  int ascent = 0;
  if ( start == end )
    {
      if ( word->length > 0 )
	end += 1;
      else
	ascent = word->font->font->ascent;
    }
  while ( list )
    {
      GtkWordFontSection *section = list->data;
      thisstart = MIN( MAX( i, start ), end );
      thisend = MAX( MIN( section->start, end ), start );
      if ( thisstart != thisend )
	ascent = MAX( ascent, font->font->ascent );
      font = section->font;
      i = section->start;
      list = list->next;
    }
  thisstart = MIN( MAX( i, start ), end );
  thisend = MAX( MIN( word->length, end ), start );
  if ( thisstart != thisend )
    ascent = MAX( ascent, font->font->ascent );
  return ascent;  
}

gint
gtk_word_max_descent (Word *word, gint start, gint end)
{
  GList *list = word->fonts;
  GtkRichTextFont *font = word->font;
  gint thisstart, thisend;
  int i = 0;
  int descent = 0;
  if ( start == end )
    {
      if ( word->length > 0 )
	end += 1;
      else
	descent = word->font->font->descent;
    }
  while ( list )
    {
      GtkWordFontSection *section = list->data;
      thisstart = MIN( MAX( i, start ), end );
      thisend = MAX( MIN( section->start, end ), start );
      if ( thisstart != thisend )
	descent = MAX( descent, font->font->descent );
      font = section->font;
      i = section->start;
      list = list->next;
    }
  thisstart = MIN( MAX( i, start ), end );
  thisend = MAX( MIN( word->length, end ), start );
  if ( thisstart != thisend )
    descent = MAX( descent, font->font->descent );
  return descent;  
}

void gtk_word_traverse( Word *word,
			GtkRichTextTraversalCallbacks *callbacks,
			gpointer data )
{
  GList *list = word->fonts;
  CharacterParams params;
  GtkWordFontSection *section;
  gint thisstart, thisend;
  int i = 0;
  params.font = word->font;

  while ( list )
    {
      section = list->data;
      thisstart = i;
      thisend = section->start;
      if ( thisstart != thisend )
	{
	  (*(callbacks->return_text)) ( word->text + thisstart, thisend - thisstart, &params, data );
	}
      params.font = section->font;
      i = section->start;
      list = list->next;
    }
  thisstart = i;
  thisend = word->length;
  if ( thisstart != thisend )
    {
      (*(callbacks->return_text)) ( word->text + thisstart, thisend - thisstart, &params, data );
    }
}

void gtk_word_traverse_partial( Word *word,
				gint start,
				gint end,
				GtkRichTextTraversalCallbacks *callbacks,
				gpointer data )
{
  GList *list = word->fonts;
  CharacterParams params;
  GtkWordFontSection *section;
  gint thisstart, thisend;
  int i = 0;
  params.font = word->font;

  while ( list )
    {
      section = list->data;
      thisstart = MIN( MAX( i, start ), end );
      thisend = MAX( MIN( section->start, end ), start );
      if ( thisstart != thisend )
	{
	  (*(callbacks->return_text)) ( word->text + thisstart, thisend - thisstart, &params, data );
	}
      params.font = section->font;
      i = section->start;
      list = list->next;
    }
  thisstart = MIN( MAX( i, start ), end );
  thisend = MAX( MIN( word->length, end ), start );
  if ( thisstart != thisend )
    {
      (*(callbacks->return_text)) ( word->text + thisstart, thisend - thisstart, &params, data );
    }
}

void
gtk_word_render (Word                *word,
		 gint                 start,
		 gint                 end,
		 GtkRichTextRenderSystem *system)
{
  GList *list = word->fonts;
  GtkRichTextFont *font = word->font;
  gint thisstart, thisend;
  int i = 0;

  while ( list )
    {
      GtkWordFontSection *section = list->data;
      thisstart = MIN( MAX( i, start ), end );
      thisend = MAX( MIN( section->start, end ), start );
      if ( thisstart != thisend )
	{
	  system->draw_text( system, font, word->text + thisstart, thisend - thisstart );
	}
      font = section->font;
      i = section->start;
      list = list->next;
    }
  thisstart = MIN( MAX( i, start ), end );
  thisend = MAX( MIN( word->length, end ), start );
  if ( thisstart != thisend )
    {
      system->draw_text( system, font, word->text + thisstart, thisend - thisstart );
    }
}

gdouble
gtk_word_text_width_with_system (Word                *word,
				 gint                 start,
				 gint                 end,
				 GtkRichTextRenderSystem *system)
{
  GList *list = word->fonts;
  GtkRichTextFont *font = word->font;
  gint thisstart, thisend;
  int i = 0;
  gdouble width = 0;
  while ( list )
    {
      GtkWordFontSection *section = list->data;
      thisstart = MIN( MAX( i, start ), end );
      thisend = MAX( MIN( section->start, end ), start );
      if ( thisstart != thisend )
	width += system->text_width( system, font, word->text + thisstart, thisend - thisstart );
      font = section->font;
      i = section->start;
      list = list->next;
    }
  thisstart = MIN( MAX( i, start ), end );
  thisend = MAX( MIN( word->length, end ), start );
  if ( thisstart != thisend )
    width += system->text_width( system, font, word->text + thisstart, thisend - thisstart );
  return width;
}

gdouble
gtk_word_max_descent_with_system (Word *word, gint start, gint end,
				  GtkRichTextRenderSystem *system)
{
  GList *list = word->fonts;
  GtkRichTextFont *font = word->font;
  gint thisstart, thisend;
  int i = 0;
  gdouble descent = 0;
  if ( start == end )
    {
      if ( word->length > 0 )
	end += 1;
      else 
	descent = system->descent( system, word->font );
    }
  while ( list )
    {
      GtkWordFontSection *section = list->data;
      thisstart = MIN( MAX( i, start ), end );
      thisend = MAX( MIN( section->start, end ), start );
      if ( thisstart != thisend )
	descent = MAX( descent, system->descent( system, font ) );
      font = section->font;
      i = section->start;
      list = list->next;
    }
  thisstart = MIN( MAX( i, start ), end );
  thisend = MAX( MIN( word->length, end ), start );
  if ( thisstart != thisend )
    descent = MAX( descent, system->descent( system, font ) );
  return descent;  
}

gdouble
gtk_word_max_ascent_with_system (Word *word, gint start, gint end,
				 GtkRichTextRenderSystem *system)
{
  GList *list = word->fonts;
  GtkRichTextFont *font = word->font;
  gint thisstart, thisend;
  int i = 0;
  gdouble ascent = 0;
  if ( start == end )
    {
      if ( word->length > 0 )
	end += 1;
      else
	ascent = system->ascent( system, word->font );
    }
  while ( list )
    {
      GtkWordFontSection *section = list->data;
      thisstart = MIN( MAX( i, start ), end );
      thisend = MAX( MIN( section->start, end ), start );
      if ( thisstart != thisend )
	ascent = MAX( ascent, system->ascent( system, font ) );
      font = section->font;
      i = section->start;
      list = list->next;
    }
  thisstart = MIN( MAX( i, start ), end );
  thisend = MAX( MIN( word->length, end ), start );
  if ( thisstart != thisend )
    ascent = MAX( ascent, system->ascent( system, font ) );
  return ascent;  
}
