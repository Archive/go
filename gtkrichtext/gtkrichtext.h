/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GTK_RICH_TEXT_H__
#define __GTK_RICH_TEXT_H__


#include <gdk/gdk.h>
#include <gtk/gtkwidget.h>
#include <gtk/gtkadjustment.h>
#include <gtk/gtksignal.h>
#include <gtk/gtkeditable.h>
#include "gtkparagraph.h"
#include "gtkword.h"
#include "gtkrichtextcallbacks.h"


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
#define GTK_RICH_TEXT(obj)          GTK_CHECK_CAST (obj, \
						    gtk_rich_text_get_type (), \
						    GtkRichText)
#define GTK_RICH_TEXT_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, \
                                                    gtk_rich_text_get_type (), \
						        GtkRichTextClass)
#define GTK_IS_RICH_TEXT(obj)       GTK_CHECK_TYPE (obj, \
						    gtk_rich_text_get_type ())

typedef struct _GtkRichTextClass   GtkRichTextClass;
typedef struct _Position           Position;
typedef struct _ParagraphTree      ParagraphTree;

struct _ParagraphTree
{
  ParagraphTree *left;
  ParagraphTree *right;
  gint height;
  gint count;
  ParagraphTree *parent;
  Paragraph *paragraph;

  /* tree balancing info */  
  gint tree_depth;
};

struct _Position
{
  ParagraphPosition paragraph_ptr;
  ParagraphTree *paragraph;
};

typedef struct
{
  gint pos_val;
  Position pos;
  gboolean valid;
} GtkRichTextPositionCacheEntry;

struct _GtkRichText
{
  GtkEditable editable;

  gpointer draw_data;

  gint minimum_width;
  gint width;
  
  GtkAdjustment *hadj;
  GtkAdjustment *vadj;

  int xoff;
  int yoff;

  gboolean is_editable;
  gboolean word_wrap;
  gboolean line_wrap;
  gboolean frozen;

  ParagraphTree *paragraphs;

  Position cursor;
  
  Position selection_end;
  gboolean selection_active;
  gboolean cursor_first;

  Position point;
  gboolean point_valid;

  int cursor_timeout;
  gboolean cursor_on;

  gint ideal;
  gboolean ideal_valid;

  GtkRichTextFont *cursor_font;

  gint queuedx, queuedy;
  gint motion_callback;
  GdkRectangle redraw_area;
  gint redraw_callback;

  gint last_button;
  gint last_count;
  gboolean incremental_redraw_handled;
};

struct _GtkRichTextClass
{
  GtkEditableClass parent_class;

  void (* failed_insert_text)  (GtkRichText    *editable,
				const gchar    *text,
				gint            length,
				gint           *position);
  void (* failed_delete_text)  (GtkRichText    *editable,
				gint            start_pos,
				gint            end_pos);
  void (* selection_changed)   (GtkRichText    *text,
				Position       *start_pos,
				Position       *end_pos);
};


guint      gtk_rich_text_get_type   (void);
GtkWidget* gtk_rich_text_new         (GtkAdjustment *hadj,
				     GtkAdjustment *vadj);
void       gtk_rich_text_render( GtkRichText *text,
				 GtkRichTextRenderSystem *system );
void       gtk_rich_text_set_minimum_width (GtkRichText *rtext,
					    gint         min_width);
void       gtk_rich_text_set_width (GtkRichText *rtext,
				    gint         width);
gint       gtk_rich_text_get_width (GtkRichText *rtext);
void       gtk_rich_text_set_adjustments (GtkRichText *rtext,
					  GtkAdjustment *hadj,
					  GtkAdjustment *vadj);
void       gtk_rich_text_insert          (GtkRichText       *text,
					  GtkRichTextFont   *font,
					  GdkColor          *fore,
					  GdkColor          *back,
					  const char        *chars,
					  gint               length);
gint       gtk_rich_text_backward_delete (GtkRichText       *text,
					  guint              nchars);
gint       gtk_rich_text_forward_delete  (GtkRichText       *text,
					  guint              nchars);


void       gtk_rich_text_set_editable    (GtkRichText       *text,
					  gboolean           editable);
void       gtk_rich_text_set_word_wrap   (GtkRichText       *text,
					  gint               word_wrap);
void       gtk_rich_text_set_line_wrap   (GtkRichText       *text,
					  gint               line_wrap);
void       gtk_rich_text_set_point       (GtkRichText       *text,
					  guint              index);
guint      gtk_rich_text_get_point       (GtkRichText       *text);
guint      gtk_rich_text_get_length      (GtkRichText       *text);
void       gtk_rich_text_freeze          (GtkRichText       *text);
void       gtk_rich_text_thaw            (GtkRichText       *text);
void       gtk_rich_text_set_cursor_font (GtkRichText       *text,
					  GtkRichTextFont   *font);
GtkRichTextFont *gtk_rich_text_get_cursor_font (GtkRichText       *text);
gboolean   gtk_rich_text_position_equal  (GtkRichText       *text,
					  Position           a,
					  Position           b);

void gtk_rich_text_set_selection_paragraph_params (GtkRichText *text,
						   ParagraphParams *params,
						ParagraphParamsValid *validity);
void gtk_rich_text_set_location_paragraph_params (GtkRichText *text,
						  gint location,
						  ParagraphParams *params,
					       ParagraphParamsValid *validity);
void gtk_rich_text_set_range_paragraph_params (GtkRichText *text,
					       gint start,
					       gint end,
					       ParagraphParams *params,
					       ParagraphParamsValid *validity);

void gtk_rich_text_get_paragraph_params (GtkRichText *text,
					 Position start,
					 Position end,
					 ParagraphParams *params, /* Output */
					 ParagraphParamsValid *validity); /* Output */

void gtk_rich_text_get_selection_paragraph_params (GtkRichText *text,
						   ParagraphParams *params, /* Output */
						   ParagraphParamsValid *validity); /* Output */

void gtk_rich_text_set_selection_character_params (GtkRichText *text,
						   CharacterParams *params,
					        CharacterParamsValid *validity);
void gtk_rich_text_set_location_character_params (GtkRichText *text,
						  gint location,
						  CharacterParams *params,
					        CharacterParamsValid *validity);
void gtk_rich_text_set_range_character_params (GtkRichText *text,
					       gint start,
					       gint end,
					       CharacterParams *params,
					       CharacterParamsValid *validity);

void gtk_rich_text_traverse( GtkRichText *text,
			     GtkRichTextTraversalCallbacks *callbacks,
			     gpointer data );

void gtk_rich_text_traverse_partial( GtkRichText *text,
				     gint start,
				     gint end,
				     GtkRichTextTraversalCallbacks *callbacks,
				     gpointer data );

void gtk_rich_text_traverse_selection( GtkRichText *text,
				       GtkRichTextTraversalCallbacks *callbacks,
				       gpointer data );

gint gtk_rich_text_position_to_int( GtkRichText *text, Position pos );

/* This function returns a zero indexed line number. */
gint gtk_rich_text_get_position_line_number( GtkRichText *text );

#define GTK_RICH_TEXT_INDEX(t, index)  0
/*\
  ((index) < (t)->gap_position ? (t)->text[index] : \
  (t)->text[(index) + (t)->gap_size])
*/

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_RICH_TEXT_H__ */
