/* gtkparagraph.h
 * Copyright (C) 1998 Chris Lahey
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GTK_RICH_TEXT_CALLBACKS_H__
#define __GTK_RICH_TEXT_CALLBACKS_H__

#include <gdk/gdk.h>
#include <gtk/gtkenums.h>
#include <libhnj/hsjust.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef struct _GtkRichText        GtkRichText;
typedef struct _GtkRichTextRenderSystem GtkRichTextRenderSystem;

typedef struct
{
  GdkFont *font;  /* The actual font. */
  gchar *desc;  /* A description of the font. */
  gpointer data;
  gint ref_count; /* Ref count. */
} GtkRichTextFont;


struct _GtkRichTextRenderSystem
{
  gdouble (* char_width) ( GtkRichTextRenderSystem *system, GtkRichTextFont *font, gchar character );
  gdouble (* text_width) ( GtkRichTextRenderSystem *system, GtkRichTextFont *font, gchar *chars, gint length );
  gdouble (* ascent) ( GtkRichTextRenderSystem *system, GtkRichTextFont *font ); 
  gdouble (* descent) ( GtkRichTextRenderSystem *system, GtkRichTextFont *font ); 
  /* draw_text should also increment x by text_width. */
  void (* draw_text) ( GtkRichTextRenderSystem *system, GtkRichTextFont *font, gchar *chars, gint length );
  void (* output_page) ( GtkRichTextRenderSystem *system );
  gdouble x;
  gdouble y;
  gdouble height;
  gdouble width;
  gdouble margin;
  gboolean uses_cursor;
  gpointer data;
};

typedef struct
{
  HnjParams hnjparams;
  gint prespace;
  gint inspace;
  gint postspace;
  gint first_indent;
  gint left_indent;
  gint right_indent;
  GtkJustification justification;
} ParagraphParams;

typedef struct
{
  gint hnjparams_valid;
  gint prespace_valid;
  gint inspace_valid;
  gint postspace_valid;
  gint first_indent_valid;
  gint left_indent_valid;
  gint right_indent_valid;
  gint justification_valid;
} ParagraphParamsValid;

#define paragraph_params_none_valid { 0, 0, 0, 0, 0, 0, 0, 0 }
#define paragraph_params_all_valid { 1, 1, 1, 1, 1, 1, 1, 1 }

typedef struct
{
  GtkRichTextFont *font;
} CharacterParams;

typedef struct
{
  gint font_valid;
} CharacterParamsValid;

#define character_params_none_valid { 0 }
#define character_params_all_valid { 1 }

typedef struct
{
  void (*start_paragraph) ( ParagraphParams *params, gpointer data );
  void (*return_text) ( gchar *text, gint length, CharacterParams *params, gpointer data );
} GtkRichTextTraversalCallbacks;


GtkRichTextFont *gtk_rich_text_font_new( gchar *desc );
GtkRichTextFont *gtk_rich_text_font_adopt( GdkFont *font, gchar *desc, gpointer data );
void gtk_rich_text_font_ref( GtkRichTextFont *font );
void gtk_rich_text_font_unref( GtkRichTextFont *font );
gchar *gtk_rich_text_font_desc( GtkRichTextFont *font );


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_RICH_TEXT_CALLBACKS_H__ */  
