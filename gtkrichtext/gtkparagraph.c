
/* gtkword.c
 * Copyright (C) 1998 Chris Lahey
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gtk/gtk.h>
#include <math.h>
#include <string.h>
#include "gtkparagraph.h"
#include "gtkrichtext.h"
#include <libhnj/hsjust.h>

static WordPosition gtk_rich_text_word_find_point (Word     *word,
						   gint      x,
						   gint      startchar,
						   gint      lastchar);

#define SCALE 1
#define HYPHEN_PIXELS 20
#define HYPHEN_PENALTY ( (SCALE) * (SCALE) * (HYPHEN_PIXELS) * (HYPHEN_PIXELS) )
#define SCROLL_PIXELS            5
#define KEY_SCROLL_PIXELS        10

GtkRichTextFont *
gtk_paragraph_char_font( Paragraph *paragraph, ParagraphPosition pos )
{
  return gtk_word_char_font( (Word *) pos.word->data, pos.word_ptr );
}

Paragraph *
gtk_paragraph_new( GtkRichText *parent )
{
  Paragraph *paragraph = g_new( Paragraph, 1 );
  paragraph->parent = parent;
  paragraph->words = g_list_append( NULL, gtk_word_new( "", 0, NULL, parent ) );
  paragraph->values = NULL;
  paragraph->valuesvalid = FALSE;
  paragraph->length_valid = FALSE;
  paragraph->params.left_indent = paragraph->params.right_indent =
    paragraph->params.first_indent = paragraph->params.inspace =
    paragraph->params.prespace = paragraph->params.postspace = 0;
  paragraph->params.justification = GTK_JUSTIFY_FILL;
  return paragraph;
}

void
gtk_paragraph_set_params( Paragraph *par,
			  ParagraphParams *params,
			  ParagraphParamsValid *validity )
{
  if ( validity->hnjparams_valid )
    par->params.hnjparams = params->hnjparams;
  if ( validity->prespace_valid )
    par->params.prespace = params->prespace;
  if ( validity->inspace_valid )
    par->params.inspace = params->inspace;
  if ( validity->postspace_valid )
    par->params.postspace = params->postspace;
  if ( validity->first_indent_valid )
    par->params.first_indent = params->first_indent;
  if ( validity->left_indent_valid )
    par->params.left_indent = params->left_indent;
  if ( validity->right_indent_valid )
    par->params.right_indent = params->right_indent;
  if ( validity->justification_valid )
    par->params.justification = params->justification;
  par->params.hnjparams.set_width
    = (par->width - par->params.left_indent - par->params.right_indent) * SCALE;
  par->valuesvalid = FALSE;
}

void
gtk_paragraph_get_params( Paragraph *par,
			  ParagraphParams *params,
			  ParagraphParamsValid *validity )
{
  if ( validity != NULL )
    {
      validity->hnjparams_valid = TRUE;
      validity->prespace_valid = TRUE;
      validity->inspace_valid = TRUE;
      validity->postspace_valid = TRUE;
      validity->first_indent_valid = TRUE;
      validity->left_indent_valid = TRUE;
      validity->right_indent_valid = TRUE;
      validity->justification_valid = TRUE;
    }
  if ( params != NULL )
    {
      params->hnjparams = par->params.hnjparams;
      params->prespace = par->params.prespace;
      params->inspace = par->params.inspace;
      params->postspace = par->params.postspace;
      params->first_indent = par->params.first_indent;
      params->left_indent = par->params.left_indent;
      params->right_indent = par->params.right_indent;
      params->justification = par->params.justification;
    }
}

void
gtk_paragraph_get_params_match( Paragraph *par,
				ParagraphParams *params,
				ParagraphParamsValid *validity )
{
  if ( validity != NULL )
    {
      if ( params->prespace != par->params.prespace )
	validity->prespace_valid = FALSE;
  
      if ( params->inspace != par->params.inspace )
	validity->inspace_valid = FALSE;
  
      if ( params->postspace != par->params.postspace )
	validity->postspace_valid = FALSE;
  
      if ( params->first_indent != par->params.first_indent )
	validity->first_indent_valid = FALSE;
  
      if ( params->left_indent != par->params.left_indent )
	validity->left_indent_valid = FALSE;
  
      if ( params->right_indent != par->params.right_indent )
	validity->right_indent_valid = FALSE;
  
      if ( params->justification != par->params.justification )
	validity->justification_valid = FALSE;
    }
}

static void
gtk_paragraph_free_values( Paragraph *paragraph, ParagraphValues values )
{
  g_free( values.words );
  g_free( values.js );
  g_free( values.ascents );
  g_free( values.descents );
  g_free( values.word_counts );
  g_free( values.extra_space );
  g_free( values.space_space );
  g_free( values.tab_count );
}

void
gtk_paragraph_destroy( Paragraph *paragraph )
{
  if ( paragraph->values != NULL )
    {
      gtk_paragraph_free_values( paragraph, *paragraph->values );
      g_free( paragraph->values );
    }
  g_list_foreach( paragraph->words, (GFunc) gtk_word_destroy, NULL );
  g_list_free( paragraph->words );
  g_free( paragraph );
}

ParagraphPosition
gtk_paragraph_end( Paragraph *paragraph )
{
  ParagraphPosition para_ptr;
  para_ptr.word = g_list_last( paragraph->words );
  if ( para_ptr.word )
    para_ptr.word_ptr = gtk_word_end( (Word *) para_ptr.word->data );
  return para_ptr;
}

ParagraphPosition
gtk_paragraph_start( Paragraph *paragraph )
{
  ParagraphPosition para_ptr;
  para_ptr.word = paragraph->words;
  if ( para_ptr.word )
    para_ptr.word_ptr = gtk_word_start( (Word *) para_ptr.word->data );
  return para_ptr;
}

gint
gtk_paragraph_length_invalid( Paragraph *paragraph )
{
  GList *word; /* Of type Word */
  gint length;
  
  length = 0;
  for ( word = paragraph->words; word != NULL; word = g_list_next( word ) )
    {
      length += gtk_word_length( (Word *) word->data );
      length += 1;
    }
  length -= 1;
  paragraph->length = length;
  paragraph->length_valid = TRUE;

  return paragraph->length;
}

ParagraphPosition
gtk_paragraph_delete( Paragraph *paragraph,
		      ParagraphPosition start, ParagraphPosition end )
{
  GList *word; /* Of type Word */
  WordPosition word_pos = start.word_ptr;
  ParagraphPosition final_pos = end;
  for ( word = start.word; word && word != end.word; )
    {
      gtk_word_delete( (Word *) word->data, word_pos,
		       gtk_word_end( (Word *) word->data ) );
      if ( g_list_next( word ) )
	{
	  Word *temp = gtk_word_concat( (Word *) word->data,
					(Word *) g_list_next( word )->data );
	  GList *templist = g_list_next( word );
	  final_pos.word_ptr = gtk_word_end( (Word *) word->data );
	  word_pos = final_pos.word_ptr;
	  gtk_word_destroy( word->data );
	  gtk_word_destroy( templist->data );
	  templist->data = temp;
	  paragraph->words = g_list_remove_link( paragraph->words, word );
	  word = templist;
	  final_pos.word = word;
	  if ( end.word == word )
	    end.word_ptr += word_pos;
	}
      else
	{
	  /* We don't need info about the next paragraph here.  That's
             dealt with in gtkrichtext.c. */
	  final_pos.word = word;
	  final_pos.word_ptr = gtk_word_end( (Word *) word->data );
	  word = g_list_next( word );
	}
    }
  if( word )
    {
      gtk_word_delete( (Word *) word->data, word_pos, end.word_ptr );
      final_pos.word = word;
      final_pos.word_ptr = word_pos;
    }
  paragraph->valuesvalid = FALSE;
  paragraph->length_valid = FALSE;
  return final_pos;
}

ParagraphPosition
gtk_paragraph_concat( Paragraph *paragrapha,
		      Paragraph *paragraphb )
{
  ParagraphPosition return_val;
  GList *worda, *wordb;
  Word *temp;
  
  paragrapha->valuesvalid = FALSE;
  paragrapha->length_valid = FALSE;
  paragrapha->words = g_list_concat( paragrapha->words, paragraphb->words );
  wordb = paragraphb->words;
  worda = wordb->prev;
  temp = gtk_word_concat( (Word *) worda->data,
			  (Word *) wordb->data );

  return_val.word = wordb;
  return_val.word_ptr = gtk_word_length( (Word *) worda->data );
  
  paragrapha->words = g_list_remove_link( paragrapha->words, worda );
  gtk_word_destroy( worda->data );
  g_list_free_1( worda );
  gtk_word_destroy( wordb->data );
  wordb->data = temp;
  
  paragraphb->words = NULL;
  gtk_paragraph_destroy( paragraphb );

  return return_val;
}

void
gtk_paragraph_set_font( Paragraph *paragraph,
			ParagraphPosition start,
			ParagraphPosition end,
			GtkRichTextFont *font )
{
  GList *list;
  WordPosition wordstart = start.word_ptr;
  if ( font != NULL )
    {
      for ( list = start.word;
	    list != NULL && list != end.word;
	    list = list->next,
	      (list ? wordstart = gtk_word_start( list->data ) : wordstart ) )
	{
	  gtk_word_set_font( list->data,
			     wordstart, gtk_word_end( (Word *) list->data ),
			     font );
	}
      
      if ( list )
	{
	  gtk_word_set_font( list->data, wordstart, end.word_ptr, font );
	}
    }
  paragraph->valuesvalid = FALSE;
}

ParagraphPosition
gtk_paragraph_insert( Paragraph *paragraph,
		      ParagraphPosition start,
		      gchar *vals,
		      gint length )
{
  gint i;
  gint laststart = 0;
  for ( i = 0; i < length; i++ )
    {
      if ( vals[i] == GDK_space || vals[i] == '\t' )
	{
	  GList *temp;
	  if ( laststart != i )
	    {
	      if ( start.word )
		start.word_ptr =
		  gtk_word_insert( (Word *) start.word->data,
				   start.word_ptr,
				   vals + laststart,
				   i - laststart);
	      else
		{
		  paragraph->words = g_new( GList, 1 );
		  paragraph->words->data
		    = gtk_word_new( vals + laststart,
				    i - laststart, NULL, paragraph->parent );
		  paragraph->words->next = paragraph->words->prev = NULL;
		  start.word = paragraph->words;
		  start.word_ptr = gtk_word_end( (Word *) start.word->data );
		}
	    }
	  temp = start.word->next;
	  start.word->next = g_new( GList, 1 );
	  start.word->next->data
	    = gtk_word_break( start.word->data,
			      start.word_ptr,
			      vals[i] == GDK_space
			      ? GTK_SPACE_SPACE
			      : GTK_SPACE_TAB );
	  start.word->next->prev = start.word;
	  start.word = start.word->next;
	  start.word->next = temp;
	  if ( start.word->next != NULL )
	    start.word->next->prev = start.word;
	  start.word_ptr = gtk_word_start( start.word->data );
	  laststart = i + 1;
	}
    }
  if ( laststart != i )
    {
      if ( start.word )
	start.word_ptr =
	  gtk_word_insert( (Word *) start.word->data,
			   start.word_ptr,
			   vals + laststart,
			   i - laststart);
      else
	{
	  paragraph->words = g_new( GList, 1 );
	  paragraph->words->data = gtk_word_new( vals + laststart,
						 i - laststart,
						 NULL,
						 paragraph->parent );
	  paragraph->words->next = paragraph->words->prev = NULL;
	  start.word = paragraph->words;
	  start.word_ptr = gtk_word_end( (Word *) start.word->data );
	}
    }
  paragraph->valuesvalid = FALSE;
  paragraph->length_valid = FALSE;
  return start;  
}

ParagraphPosition
gtk_paragraph_insert_with_data( Paragraph *paragraph,
				ParagraphPosition start,
				gchar *vals,
				gint length,
				GtkRichTextFont *font )
{
  gint i;
  gint laststart = 0;
  for ( i=0; i < length; i++ )
    {
      if ( vals[i] == GDK_space || vals[i] == '\t' )
	{
	  GList *temp;
	  if ( laststart != i )
	    {
	      if ( start.word )
		start.word_ptr =
		  gtk_word_insert_with_data( (Word *) start.word->data,
					     start.word_ptr,
					     vals + laststart,
					     i - laststart,
					     font );
	      else
		{
		  paragraph->words = g_new( GList, 1 );
		  paragraph->words->data = gtk_word_new( vals + laststart, i - laststart, NULL, paragraph->parent );
		  paragraph->words->next = paragraph->words->prev = NULL;
		  start.word = paragraph->words;
		  start.word_ptr = gtk_word_end( (Word *) start.word->data );
		}
	    }	
	  temp = start.word->next;
	  start.word->next = g_new( GList, 1 );
	  start.word->next->data = gtk_word_break( start.word->data, start.word_ptr, vals[i] == GDK_space ? GTK_SPACE_SPACE : GTK_SPACE_TAB );
	  start.word->next->prev = start.word;
	  start.word = start.word->next;
	  start.word->next = temp;
	  if ( start.word->next != NULL )
	    start.word->next->prev = start.word;
	  start.word_ptr = gtk_word_start( start.word->data );
	  laststart = i + 1;
	}
    }
  if ( laststart != i )
    {
      if ( start.word )
	start.word_ptr =
	  gtk_word_insert_with_data( (Word *) start.word->data,
				     start.word_ptr,
				     vals + laststart,
				     i - laststart,
				     font );
      else
	{
	  paragraph->words = g_new( GList, 1 );
	  paragraph->words->data = gtk_word_new( vals + laststart, i - laststart, NULL, paragraph->parent );
	  paragraph->words->next = paragraph->words->prev = NULL;
	  start.word = paragraph->words;
	  start.word_ptr = gtk_word_end( (Word *) start.word->data );
	}
    }
  paragraph->valuesvalid = FALSE;
  paragraph->length_valid = FALSE;
  return start;  
}

ParagraphPosition
gtk_paragraph_decrement_position( Paragraph *paragraph,
				  ParagraphPosition old_pos, gint distance )
{
  for( ; distance != 0; distance -- )
    {
      if (gtk_word_position_equal((Word *) old_pos.word->data,
				  old_pos.word_ptr,
				  gtk_word_start((Word *) old_pos.word->data)))
	{
	  if ( old_pos.word && old_pos.word->prev )
	    {
	      old_pos.word = old_pos.word->prev;
	      old_pos.word_ptr = gtk_word_end( (Word *) old_pos.word->data );
	    }
	}
      else
	{
	  old_pos.word_ptr =
	    gtk_word_decrement_position ((Word *) old_pos.word->data,
					 old_pos.word_ptr, 1);
	}
    }

  return old_pos;
}

ParagraphPosition
gtk_paragraph_increment_position( Paragraph *paragraph,
				  ParagraphPosition old_pos, gint distance )
{
  for( ; distance != 0; distance -- )
    {
      if (gtk_word_position_equal((Word *) old_pos.word,
				  old_pos.word_ptr,
				  gtk_word_end((Word *) old_pos.word->data )))
	{
	  if ( old_pos.word && old_pos.word->next )
	    {
	      old_pos.word = old_pos.word->next;
	      old_pos.word_ptr = gtk_word_start( (Word *) old_pos.word->data );
	    }
	}
      else
	{
	  old_pos.word_ptr =
	    gtk_word_increment_position( (Word *) old_pos.word->data,
					 old_pos.word_ptr, 1 );
	}
    }
  return old_pos;
}

static ParagraphValues
gtk_paragraph_calculate_values_actual( Paragraph *paragraph )
{
  HnjBreak *breaks;
  gint *result;
  GList **words;
  gint *js;
  gint *ascents;
  gint *descents;
  gint *extra_space;
  gint *word_counts;
  gint *space_space;
  gint *tab_count;
  gint n_breaks = 0, n_actual_breaks = 0;
  gint i;
  gint l;
  gint j;
  gint line_number;
  GList *current_word;
  gint x = paragraph->params.first_indent * SCALE;
  ParagraphValues return_val;

  /* find possible line breaks. */
  for ( current_word = paragraph->words; current_word; current_word = g_list_next( current_word ) )
    {
      Word *word = (Word *) current_word->data;
      l = word->length;
      for (j = 0; j < l; j++)
	{
	  if ( word->text[j] == '-' )
	    n_breaks++;
	  else if (word->hyphenation[j] & 1)
	    n_breaks++;
	}
      n_breaks++;
    }

  breaks = g_new( HnjBreak, n_breaks );
  result = g_new( gint, n_breaks );
  words = g_new( GList *, n_breaks );
  js = g_new( gint, n_breaks );
  n_breaks = 0;
  /* find possible line breaks. */
  for ( current_word = paragraph->words; current_word; current_word = g_list_next( current_word ) )
    {
      Word *word = (Word *) current_word->data;
      l = word->length;
      for (j = 0; j < l; j++)
	{
	  x += gtk_word_char_width( word, j ) * SCALE;
	  if ( word->text[j] == '-' )
	    {
	      breaks[n_breaks].x0 = x;
	      breaks[n_breaks].x1 = x;
	      breaks[n_breaks].penalty = HYPHEN_PENALTY;
	      breaks[n_breaks].flags = HNJ_JUST_FLAG_ISHYPHEN;
	      words[n_breaks] = current_word;
	      js[n_breaks] = j + 1;
	      n_breaks++;
	    }
	  else if (word->hyphenation[j] & 1)
	    {
	      breaks[n_breaks].x0 = x + gdk_char_width( gtk_word_char_font( word, j )->font, '-' ) * SCALE;
	      breaks[n_breaks].x1 = x;
	      breaks[n_breaks].penalty = HYPHEN_PENALTY;
	      breaks[n_breaks].flags = HNJ_JUST_FLAG_ISHYPHEN;
	      words[n_breaks] = current_word;
	      js[n_breaks] = j + 1;
	      n_breaks++;
	    }
	}
      if ( word->space == GTK_SPACE_SPACE )
	{
	  breaks[ n_breaks ].x0 = x;
	  x += gdk_char_width( gtk_word_char_font( word, word->length )->font, ' ' ) * SCALE;
	  breaks[ n_breaks ].x1 = x;
	  breaks[ n_breaks ].penalty = 0;
	  breaks[ n_breaks ].flags = HNJ_JUST_FLAG_ISSPACE;
	  words[ n_breaks ] = current_word;
	  js[ n_breaks ] = l;
	}
      else
	{
	  breaks[n_breaks].x0 = x;
	  breaks[n_breaks].x1 = x;
	  breaks[n_breaks].penalty = 0;
	  breaks[n_breaks].flags = HNJ_JUST_FLAG_ISTAB;
	  words[n_breaks] = current_word;
	  js[n_breaks] = l;
	}
      n_breaks++;
    }
  breaks[ n_breaks - 1 ].flags = 0;

  /* Calculate optimal line breaks. */
  n_actual_breaks = hnj_hs_just (breaks, n_breaks,
				 &(paragraph->params.hnjparams), result);

  ascents = g_new( gint, n_actual_breaks );
  descents = g_new( gint, n_actual_breaks );
  extra_space = g_new( gint, n_actual_breaks );
  word_counts = g_new( gint, n_actual_breaks );
  space_space = g_new( gint, n_actual_breaks );
  tab_count = g_new( gint, n_actual_breaks );

  return_val.count = n_actual_breaks;
  return_val.words = g_new( GList *, return_val.count );
  return_val.js = g_new( gint, return_val.count );

  for ( i = 0; i < return_val.count; i++ )
    {
      return_val.words[i] = words[ result[ i ] ];
      return_val.js[i] = js[ result[ i ] ];
    }


  /* Calculate full justification information and text height. */
  current_word = paragraph->words;
  return_val.height = 0;
  j = 0;
  for ( line_number = 0; line_number < return_val.count; line_number++ )
    {
      Word *word;
      tab_count[ line_number ] = 0;
      ascents[ line_number ] = descents[ line_number ] = space_space[ line_number ] = extra_space[ line_number ] = word_counts[ line_number ] = 0;
      if ( line_number == 0 )
	x = paragraph->params.first_indent;
      else
	x = 0;
      for( ; current_word != NULL && current_word != return_val.words[ line_number ]; current_word = g_list_next( current_word ) )
	{
	  word = (Word *) current_word->data;
	  x += gtk_word_text_width( word, j, word->length );
	  ascents[ line_number ] = MAX( ascents[ line_number ], gtk_word_max_ascent( word, j, word->length ) );
	  descents[ line_number ] = MAX( descents[ line_number ], gtk_word_max_descent( word, j, word->length ) );
	  word_counts[ line_number ] ++;
	  if ( word->space == GTK_SPACE_SPACE )
	    {
	      gint delta = gdk_char_width( gtk_word_char_font( word, word->length )->font, ' ' );
	      space_space[ line_number ] += delta;
	      x += delta;
	    }
	  else
	    {
	      word_counts[ line_number ] = 1;
	      tab_count[ line_number ] ++;
	    }
	  j = 0;
	}
      word = (Word *) current_word->data;
      word_counts[ line_number ] ++;
      if ( return_val.js[ line_number ] != word->length)
	{
	  char nextchar;
	  int oldj = j;
	  j = return_val.js[ line_number ];
	  nextchar = word->text[j];
	  word->text[j] = 0;
	  x += gtk_word_text_width( word, oldj, j );
	  x += gdk_char_width( gtk_word_char_font( word, j )->font, '-' );
	  ascents[ line_number ] = MAX( ascents[ line_number ], gtk_word_max_ascent( word, oldj, j ) );
	  descents[ line_number ] = MAX( descents[ line_number ], gtk_word_max_descent( word, oldj, j ) );
	  word->text[j] = nextchar;
	}
      else
	{
	  x += gtk_word_text_width( word, j, word->length );
	  ascents[ line_number ] = MAX( ascents[ line_number ], gtk_word_max_ascent( word, j, word->length ) );
	  descents[ line_number ] = MAX( descents[ line_number ], gtk_word_max_descent( word, j, word->length ) );
	  j = 0;
	  current_word = g_list_next( current_word );
	}
      word_counts[ line_number ] --;
      extra_space[ line_number ] = paragraph->params.hnjparams.set_width / SCALE - x + space_space[ line_number ];
      return_val.height += ascents[ line_number ] + descents[ line_number ];
    }

  return_val.height += (return_val.count - 1) * paragraph->params.inspace;
  return_val.height += paragraph->params.prespace;
  return_val.height += paragraph->params.postspace;

  return_val.ascents = ascents;
  return_val.descents = descents;
  return_val.word_counts = word_counts;
  return_val.extra_space = extra_space;
  return_val.space_space = space_space;
  return_val.tab_count = tab_count;

  g_free( breaks );
  g_free( js );
  g_free( words );
  g_free( result );

  return return_val;
}

static ParagraphValues
gtk_paragraph_calculate_values_invalid( Paragraph *paragraph )
{
  if ( ! paragraph->valuesvalid )
    {
      if ( paragraph->values != NULL )
	{
	  gtk_paragraph_free_values( paragraph, *paragraph->values );
	}
      else
	{
	  paragraph->values = g_new( ParagraphValues, 1 );
	}
      *(paragraph->values) = gtk_paragraph_calculate_values_actual( paragraph );
      paragraph->valuesvalid = TRUE;
    }
  return *paragraph->values;
}

#define gtk_paragraph_calculate_values( x ) ( (x)->valuesvalid ? ( *( ( x )->values ) ) : gtk_paragraph_calculate_values_invalid( x ) )

gint
gtk_paragraph_height_invalid( Paragraph *paragraph )
{
  ParagraphValues values = gtk_paragraph_calculate_values( paragraph );
  return values.height;
}

gint
gtk_paragraph_line_count_invalid( Paragraph *paragraph )
{
  ParagraphValues values = gtk_paragraph_calculate_values( paragraph );
  return values.count;
}

static void
gtk_paragraph_draw_word ( Paragraph           *paragraph,
			  ParagraphValues     *values,
			  Word                *word,
			  gint                 start,
			  gint                 end,
			  GdkDrawable         *drawable,
			  GdkGC               *gc,
			  GdkGC               *gcsel,
			  gint    	       x,
			  gint		       y,
			  ParagraphPosition    selstart,
			  gboolean             startval,
			  ParagraphPosition    selend,
			  gboolean             endval,
			  gboolean            *in_selection,
			  gint                 line_number)
{
  if ( *in_selection )
    {
      if ( endval && selend.word && ((Word *) (selend.word->data)) == word && selend.word_ptr >= start && selend.word_ptr <= end )
	{
	  gtk_word_draw_text_with_selection( word, start, end, drawable, gc, gcsel, x, y, start, selend.word_ptr,
					     values->ascents[ line_number ], values->descents[ line_number ] );
	  *in_selection = FALSE;
	}
      else
	gtk_word_draw_text_with_selection( word, start, end, drawable, gc, gcsel, x, y, start, end,
					   values->ascents[ line_number ], values->descents[ line_number ] );

    }
  else
    {
      if ( startval && selstart.word && ((Word *) (selstart.word->data)) == word && selstart.word_ptr >= start && selstart.word_ptr <= end)
	{
	  if ( gtk_paragraph_position_equal( paragraph, selstart, selend ) )
	    {
	      gtk_word_draw_text( word, start, end, drawable, gc, x, y );
	      if ( selstart.word_ptr == word->length )
		x--;
	      x += gtk_word_text_width( word, start, selstart.word_ptr );
	      gdk_draw_rectangle( drawable, gc, TRUE,
				  x, y - values->ascents[ line_number ],
				  1, values->ascents[ line_number ] + values->descents[ line_number ] );
	      x -= gtk_word_text_width( word, start, selstart.word_ptr );
	      if ( selstart.word_ptr == word->length )
		x++;
	    }
	  else
	    {
	      *in_selection = TRUE;
	      if ( endval && selend.word && ((Word *) (selend.word->data)) == word && selend.word_ptr >= start && selend.word_ptr <= end )
		{
		  gtk_word_draw_text_with_selection( word, start, end, drawable, gc, gcsel, x, y, selstart.word_ptr, selend.word_ptr,
						     values->ascents[ line_number ], values->descents[ line_number ] );
		  *in_selection = FALSE;
		}
	      else
		gtk_word_draw_text_with_selection( word, start, end, drawable, gc, gcsel, x, y, selstart.word_ptr, end,
						   values->ascents[ line_number ], values->descents[ line_number ] );
							   
	    }
	}
      else
	gtk_word_draw_text( word, start, end, drawable, gc, x, y );
    }

}

void
gtk_paragraph_draw( Paragraph *paragraph, gint xoff, gint yoff,
		    GdkDrawable *drawable,
		    ParagraphPosition paragraph_ptr, gboolean cursor,
		    ParagraphPosition end_selection_ptr, gboolean end_selection,
		    gboolean in_selection, GdkGC *gc, GdkGC *gcsel )
{
  ParagraphValues values = gtk_paragraph_calculate_values( paragraph );
  gint x, y, j, oldx;
  GList *current_word;
  gint line_number;
  gint tab_width = paragraph->params.hnjparams.tab_width;
  Word *word;
  float space;
  float xf = 0.0;
  gint tabs_so_far = 0;

#if 0
  y = yoff;
  line_number = 0;
  if ( paragraph->parent->line_wrap || paragraph->parent->word_wrap )
    {
      switch( paragraph->params.justification )
	{
	case GTK_JUSTIFY_LEFT:
	case GTK_JUSTIFY_FILL:
	  /* Add nothing. */
	  break;
	case GTK_JUSTIFY_RIGHT:
	  x += values.extra_space[ line_number ] - values.space_space[ line_number ];
	  break;
	case GTK_JUSTIFY_CENTER:
	  x += ( values.extra_space[ line_number ] - values.space_space[ line_number ] ) / 2;
	  break;
	}
    }

  if (values.word_counts[ line_number ] > 0)
    space = ((float) values.extra_space[ line_number ] ) /
      values.word_counts[ line_number ];
  else
    space = (float) values.extra_space[ line_number ];
  
  x = xoff + paragraph->params.first_indent + paragraph->params.left_indent;
  y += values.ascents[ line_number ];
  
  for ( current_word = paragraph->words; current_word; current_word = g_list_next( current_word ) )
    {
      Word *word = (Word *) current_word;
      gint j = 0;
      if ( paragraph->parent->line_wrap || paragraph->parent->word_wrap )
	{
	  while ( current_word == values.words[ line_number ] && line_number < values.count )
	    {
	      if ( values.js[ line_number ] != word->length )
		{
		  char nextchar;
		  int oldj = j;
		  j = values.js[ line_number ];
		  nextchar = word->text[j];
		  word->text[j] = 0;
		  
		  gtk_paragraph_draw_word( paragraph, &values, word, oldj, j,
					   drawable, gc, gcsel,
					   x, y, paragraph_ptr, cursor, end_selection_ptr, end_selection,
					   &in_selection, line_number );	    
		  x += gtk_word_text_width( word, oldj, j);
	      
		  gdk_draw_text( drawable, gtk_word_char_font( word, j )->font, gc, x, y, "-", 1 );
		  word->text[j] = nextchar;
		}
	      else
		{
		  gtk_paragraph_draw_word( paragraph, &values, word, j, gtk_word_end( word ),
					   drawable, gc, gcsel,
					   x, y, paragraph_ptr, cursor, end_selection_ptr, end_selection,
					   &in_selection, line_number );	    
		}
	      y += values.descents[ line_number ];
	  
	      /* Initialize next line. */
	      line_number ++;

	      y += values.ascents[ line_number ];
	      switch( paragraph->params.justification )
		{
		case GTK_JUSTIFY_LEFT:
		case GTK_JUSTIFY_FILL:
		  /* Add nothing. */
		  break;
		case GTK_JUSTIFY_RIGHT:
		  x += values.extra_space[ line_number ] - values.space_space[ line_number ];
		  break;
		case GTK_JUSTIFY_CENTER:
		  x += ( values.extra_space[ line_number ] - values.space_space[ line_number ] ) / 2;
		  break;
		}
	  
	      if (values.word_counts[ line_number ] > 0)
		space = ((float) values.extra_space[ line_number ] ) /
		  values.word_counts[ line_number ];
	      else
		space = (float) values.extra_space[ line_number ];

	      xf = 0.0;
	      x = xoff + paragraph->params.left_indent;
	      tabs_so_far = 0;
	    }
	}
      if ( j != word->length && line_number < values.count)
	{
	  gint oldx = x;
	  if ( word->space == GTK_SPACE_SPACE )
	    {
	      switch( paragraph->params.justification )
		{
		case GTK_JUSTIFY_LEFT:
		case GTK_JUSTIFY_RIGHT:
		case GTK_JUSTIFY_CENTER:
		  x += gdk_char_width( gtk_word_char_font( word, word->length )->font, ' ' );
		  break;
		case GTK_JUSTIFY_FILL:
		  if ( ( line_number == values.count - 1 ) || ( values.tab_count[ line_number ] - tabs_so_far > 0 ) )
		    {
		      x += gdk_char_width( gtk_word_char_font( word, word->length )->font, ' ' );
		    }
		  else
		    {
		      xf += space;
		      x += (int) xf;
		      xf -= floor( xf );
		      break;
		    }
		}
	    }
	  else if ( word->space == GTK_SPACE_TAB )
	    {
	      x = ( x / tab_width + 1 ) * tab_width;
	      tabs_so_far ++;
	    }
	  if ( in_selection )
	    gdk_draw_rectangle( drawable,
				gcsel, TRUE,
				oldx, y - values.ascents[ line_number ],
				x - oldx, values.ascents[ line_number ] + values.descents[ line_number ] );
	  j = 0;
	}
    }
#else
  /* Do actual drawing. */
  j = 0;
  current_word = paragraph->words;
  y = yoff;
  y += paragraph->params.prespace;
  for ( line_number = 0; line_number < values.count; line_number++, y += paragraph->params.inspace )
    {
      Word *word;
      float space;
      float xf = 0.0;
      gint tabs_so_far = 0;

      if (values.word_counts[ line_number ] > 0)
	space = ((float) values.extra_space[ line_number ] ) /
	  values.word_counts[ line_number ];
      else
	space = (float) values.extra_space[ line_number ];

      if ( line_number == 0 )
	x = xoff + paragraph->params.first_indent + paragraph->params.left_indent;
      else
	x = xoff + paragraph->params.left_indent;
      switch( paragraph->params.justification )
	{
	case GTK_JUSTIFY_LEFT:
	case GTK_JUSTIFY_FILL:
	  /* Add nothing. */
	  break;
	case GTK_JUSTIFY_RIGHT:
	  x += values.extra_space[ line_number ] - values.space_space[ line_number ];
	  break;
	case GTK_JUSTIFY_CENTER:
	  x += ( values.extra_space[ line_number ] - values.space_space[ line_number ] ) / 2;
	  break;
	}
      y += values.ascents[ line_number ];
      for( ; current_word != NULL && current_word != values.words[ line_number ]; current_word = g_list_next( current_word ) )
	{
	  word = (Word *) current_word->data;

	  gtk_paragraph_draw_word( paragraph, &values, word, j, word->length,
				   drawable, gc, gcsel,
				   x, y, paragraph_ptr, cursor, end_selection_ptr, end_selection,
				   &in_selection, line_number );

	  if ( j == 0 )
	    x += word->width;
	  else
	    x += gtk_word_text_width( word, j, word->length );

	  oldx = x;
	  if ( word->space == GTK_SPACE_SPACE )
	    {
	      switch( paragraph->params.justification )
		{
		case GTK_JUSTIFY_LEFT:
		case GTK_JUSTIFY_RIGHT:
		case GTK_JUSTIFY_CENTER:
		  x += gdk_char_width( gtk_word_char_font( word, word->length )->font, ' ' );
		  break;
		case GTK_JUSTIFY_FILL:
		  if ( ( line_number == values.count - 1 ) || ( values.tab_count[ line_number ] - tabs_so_far > 0 ) )
		    {
		      x += gdk_char_width( gtk_word_char_font( word, word->length )->font, ' ' );
		    }
		  else
		    {
		      xf += space;
		      x += (int) xf;
		      xf -= floor( xf );
		      break;
		    }
		}
	    }
	  else if ( word->space == GTK_SPACE_TAB )
	    {
	      x = ( x / tab_width + 1 ) * tab_width;
	      tabs_so_far ++;
	    }
	  if ( in_selection )
	    gdk_draw_rectangle( drawable,
				gcsel, TRUE,
				oldx, y - values.ascents[ line_number ],
				x - oldx, values.ascents[ line_number ] + values.descents[ line_number ] );
	  j = 0;
	}
      word = (Word *) current_word->data;
      if ( values.js[ line_number ] != word->length)
	{
	  char nextchar;
	  int oldj = j;
	  j = values.js[ line_number ];
	  nextchar = word->text[j];
	  word->text[j] = 0;
	  
	  gtk_paragraph_draw_word( paragraph, &values, word, oldj, j,
				   drawable, gc, gcsel,
				   x, y, paragraph_ptr, cursor, end_selection_ptr, end_selection,
				   &in_selection, line_number );	    
	  x += gtk_word_text_width( word, oldj, j);
	  
	  gdk_draw_text( drawable, gtk_word_char_font( word, j )->font, gc, x, y, "-", 1 );
	  word->text[j] = nextchar;
	}
      else
	{
	  gtk_paragraph_draw_word( paragraph, &values, word, j, gtk_word_end( word ),
				   drawable, gc, gcsel,
				   x, y, paragraph_ptr, cursor, end_selection_ptr, end_selection,
				   &in_selection, line_number );	    
	  j = 0;
	  current_word = g_list_next( current_word );
	}
      y += values.descents[ line_number ];
    }
#endif
}


WordPosition gtk_rich_text_word_find_point( Word *word, int x, int startchar, int lastchar )
{
  int i;
  for( i = startchar; i < lastchar; i++ )
    {
      x -= gtk_word_char_width( word, i ) / 2;
      if ( x < 0 )
	break;
      x -= ( gtk_word_char_width( word, i ) + 1 ) / 2;
    }
  return i;
}

/* Takes coordinates from the upper left hand corner of the entire block of text. */
ParagraphPosition gtk_paragraph_find_point( Paragraph *paragraph, int x, int y )
{
  int j;
  int line_number;
  GList *current_word;
  gint xpos = 0;
  gint ypos = 0;
  ParagraphPosition newpos;
  ParagraphValues values = gtk_paragraph_calculate_values( paragraph );
  gint tab_width = paragraph->params.hnjparams.tab_width;

  xpos = 0;
      
  /* Do actual calculation. */
  j = 0;
  ypos = paragraph->params.prespace;
  
  /* Make sure you stop by the last line. */
  for( line_number = 0; line_number < values.count - 1; line_number++, ypos += paragraph->params.inspace)
    {
      ypos += values.descents[ line_number ];
      ypos += values.ascents[ line_number ];
      if ( y <= ypos )
	break;
    }

  if ( line_number > 0 )
    {
      xpos = paragraph->params.left_indent;
      current_word = values.words[ line_number - 1 ];
      if ( values.js[ line_number - 1 ] != ( (Word *) (current_word->data) )->length )
	j = values.js[ line_number - 1 ];
      else
	{
	  j = 0;
	  if ( current_word->next != NULL )
	    current_word = current_word->next;
	}
    }
  else
    {
      xpos = paragraph->params.first_indent + paragraph->params.left_indent;
      j = 0;
      current_word = paragraph->words;
    }
  switch( paragraph->params.justification )
    {
    case GTK_JUSTIFY_LEFT:
    case GTK_JUSTIFY_FILL:
      /* Add nothing. */
      break;
    case GTK_JUSTIFY_RIGHT:
      xpos += values.extra_space[ line_number ] - values.space_space[ line_number ];
      break;
    case GTK_JUSTIFY_CENTER:
      xpos += ( values.extra_space[ line_number ] - values.space_space[ line_number ] ) / 2;
      break;
    }
  {
    Word *word;
    float space;
    float xf = 0.0;
    int oldxpos = xpos;
    gint tabs_so_far = 0;

    if (values.word_counts[ line_number ] > 0)
      space = ((float) values.extra_space[ line_number ] ) /
	values.word_counts[ line_number ];
    else
      space = (float) values.extra_space[ line_number ];

    for( ; current_word->next != NULL && current_word != values.words[ line_number ]; current_word = g_list_next( current_word ) )
      {
	gint delta = 0;
	oldxpos = xpos;
	word = (Word *) current_word->data;
	xpos += gtk_word_text_width( word, j, word->length );
	
	if ( word->space == GTK_SPACE_SPACE )
	  {
	    switch( paragraph->params.justification )
	      {
	      case GTK_JUSTIFY_LEFT:
	      case GTK_JUSTIFY_RIGHT:
	      case GTK_JUSTIFY_CENTER:
		delta = gdk_char_width( gtk_word_char_font( word, word->length )->font, ' ' );
		break;
	      case GTK_JUSTIFY_FILL:
		if ( ( line_number == values.count - 1 ) || ( values.tab_count[ line_number ] - tabs_so_far > 0 ) )
		  {
		    delta= gdk_char_width( gtk_word_char_font( word, word->length )->font, ' ' );
		  }
		else
		  {
		    xf += space;
		    delta = (int) xf;
		    xf -= floor( xf );
		    break;
		  }
	      }
	  }
	else if ( word->space == GTK_SPACE_TAB )
	  {
	    delta = ( x / tab_width + 1 ) * tab_width - x;
	    tabs_so_far ++;
	  }
	else delta = 0;
	
	xpos += delta / 2;
	if ( x <= xpos )
	  {
	    xpos = oldxpos;
	    break;
	  }
	xpos += ( delta + 1 ) / 2;
	
	j = 0;
      }
  }
  {
    Word *word = (Word *) current_word->data;
    int oldj = j;
    newpos.word = current_word;
    if( current_word == values.words[ line_number ] )
      j = values.js[ line_number ];
    else
      j = word->length;
    newpos.word_ptr = gtk_rich_text_word_find_point( word, x - xpos, oldj, j );
  }
  return newpos;
}

Paragraph *gtk_paragraph_break( Paragraph *paragraph, ParagraphPosition position )
{
  Paragraph *new_par = g_new( Paragraph, 1 );
  Word *new_word = gtk_word_break( position.word->data, position.word_ptr, GTK_SPACE_SPACE );
  GList *new_word_ptr = g_new( GList, 1 );

  new_par->params = paragraph->params;
  new_par->words = new_word_ptr;
  new_par->valuesvalid = FALSE;
  new_par->length_valid = FALSE;
  new_par->values = NULL;
  new_par->parent = paragraph->parent;
  new_par->width = paragraph->width;
  
  new_word_ptr->data = new_word;
  new_word_ptr->prev = NULL;
  new_word_ptr->next = position.word->next;
  if ( new_word_ptr->next )
    new_word_ptr->next->prev = new_word_ptr;

  position.word->next = NULL;
  paragraph->valuesvalid = FALSE;
  paragraph->length_valid = FALSE;
  
  return new_par;
}

gboolean gtk_paragraph_position_equals( ParagraphPosition posa, ParagraphPosition posb )
{
  return posa.word == posb.word && posa.word_ptr == posb.word_ptr;
}

gint
gtk_paragraph_get_line_number( Paragraph *paragraph, ParagraphPosition pos )
{
  gboolean found = FALSE;
  GList *list = pos.word;
  ParagraphValues values = gtk_paragraph_calculate_values( paragraph );
  gint line_number = 0;
  while( ! found && list )
    {
      int i;
      for ( i = 0; i < values.count; i++ )
	{
	  if ( list == values.words[i] )
	    {
	      line_number = i;
	      if ( list == pos.word && values.js[i] >= pos.word_ptr )
		line_number --;
	      found = TRUE;
	      break;  
	    }
	}
      list = list->prev;
    }
  if ( found )
    {
      return line_number + 1;
    }
  else
    {
      return 0;
    }
}

gint
gtk_paragraph_find_ideal( Paragraph *paragraph, ParagraphPosition pos )
{
  ParagraphValues values = gtk_paragraph_calculate_values( paragraph );
  gint line_number = gtk_paragraph_get_line_number( paragraph, pos );
  int j = 0;
  int xpos;
  float space = 0.0;
  float xf = 0.0;
  GList *list;
  Word *word;
  gint tabs_so_far = 0;
  gint tab_width = paragraph->params.hnjparams.tab_width;

  if ( line_number == 0 )
    {
      xpos = paragraph->params.first_indent + paragraph->params.left_indent;
      list = paragraph->words;
    }
  else
    {
      xpos = paragraph->params.left_indent;
      list = values.words[ line_number - 1 ];
      word = (Word *) list->data;
      if ( values.js[ line_number - 1 ] == word->length )
	list = list->next;
      else
	j = values.js[ line_number - 1 ];
    }
  
  switch( paragraph->params.justification )
    {
    case GTK_JUSTIFY_LEFT:
    case GTK_JUSTIFY_FILL:
      /* Add nothing. */
      break;
    case GTK_JUSTIFY_RIGHT:
      xpos += values.extra_space[ line_number ] - values.space_space[ line_number ];
      break;
    case GTK_JUSTIFY_CENTER:
      xpos += ( values.extra_space[ line_number ] - values.space_space[ line_number ] ) / 2;
      break;
    }

  word = list->data;
  
  if (values.word_counts[ line_number ] > 0)
    space = ((float) values.extra_space[ line_number ] ) /
      values.word_counts[ line_number ];
  else
    space = (float) values.extra_space[ line_number ];

  for( ; list != NULL && list != pos.word; list = g_list_next( list ) )
    {
      word = (Word *) list->data;
      if ( j != 0 )
	xpos += gtk_word_text_width( word, j, word->length );
      else
	xpos += word->width;
      if ( word->space == GTK_SPACE_SPACE )
	{
	  switch( paragraph->params.justification )
	    {
	    case GTK_JUSTIFY_LEFT:
	    case GTK_JUSTIFY_RIGHT:
	    case GTK_JUSTIFY_CENTER:
	      xpos += gdk_char_width( gtk_word_char_font( word, word->length )->font, ' ' );
	      break;
	    case GTK_JUSTIFY_FILL:
	      if ( ( line_number == values.count - 1 ) || ( values.tab_count[ line_number ] - tabs_so_far > 0 ) )
		{
		  xpos += gdk_char_width( gtk_word_char_font( word, word->length )->font, ' ' );
		}
	      else
		{
		  xf += space;
		  xpos += (int) xf;
		  xf -= floor( xf );
		  break;
		}
	    }
	}
      else if ( word->space == GTK_SPACE_TAB )
	{
	  xpos = ( xpos / tab_width + 1 ) * tab_width;
	  tabs_so_far ++;
	}
      j = 0;
    }
  word = (Word *) list->data;
  xpos += gtk_word_text_width( word, j, pos.word_ptr );
  return xpos;
}

ParagraphPosition
gtk_paragraph_vertical_move( Paragraph *paragraph, ParagraphPosition old_pos, gint ideal, gint *distance )
{
  gint old_line_number = gtk_paragraph_get_line_number( paragraph, old_pos );
  gint height = 0;
  ParagraphValues values = gtk_paragraph_calculate_values( paragraph );
  gint i;
  gint line_number = old_line_number + *distance;
  
  line_number = MAX( line_number, 0 );
  line_number = MIN( line_number, values.count - 1 );
  (*distance) -= ( line_number - old_line_number );
  height += paragraph->params.prespace;
  for( i = 0; TRUE ; i++ )
    {
      height += values.ascents[ i ];
      if ( i == line_number )
	break;
      height += values.descents[ i ];
      height += paragraph->params.inspace;
    }
  return gtk_paragraph_find_point( paragraph, ideal, height );
}

#if 0
static ParagraphPosition gtk_paragraph_vertical_move( Paragraph *paragraph, ParagraphPosition old_pos, gint *distance )
{
  HnjBreak breaks[60000];
  int result[60000];
  GList *words[60000];
  int js[60000];
  int n_breaks = 0, n_actual_breaks = 0;
  int l;
  int j;
  int line_number;
  int current_line_number = 0;
  int current_word_number = 0;
  int ascents[60000];
  int descents[60000];
  int extra_space[60000];
  int word_counts[60000];
  GList *current_word;
  gint xpos = 0;
  gint ypos = 0;
  gint x = 0;
  ParagraphPosition newpos;

  n_breaks = 0;
  xpos = 0;
  
  /* find possible line breaks. */
  for ( current_word = paragraphs->words; current_word; current_word = g_list_next( current_word ) )
    {
      Word *word = (Word *) current_word->data;
      l = word->length;
      for (j = 0; j < l; j++)
	{
	  xpos += gtk_word_char_width( word, j ) * SCALE;
	  if ( word->text[j] == '-' )
	    {
	      breaks[n_breaks].x0 = xpos;
	      breaks[n_breaks].x1 = xpos;
	      breaks[n_breaks].penalty = HYPHEN_PENALTY;
	      breaks[n_breaks].flags = HNJ_JUST_FLAG_ISHYPHEN;
	      words[n_breaks] = current_word;
	      js[n_breaks] = j + 1;
	      n_breaks++;
	    }
	  else if (word->hyphenation[j] & 1)
	    {
	      breaks[n_breaks].x0 = xpos + gdk_char_width( gtk_word_char_font( word, j )->font, '-' ) * SCALE;
	      breaks[n_breaks].x1 = xpos;
	      breaks[n_breaks].penalty = HYPHEN_PENALTY;
	      breaks[n_breaks].flags = HNJ_JUST_FLAG_ISHYPHEN;
	      words[n_breaks] = current_word;
	      js[n_breaks] = j + 1;
	      n_breaks++;
	    }
	}
      breaks[ n_breaks ].x0 = xpos;
      xpos += gdk_char_width( gdk_word_char_font( word, word->length ), ' ' ) * SCALE;
      breaks[ n_breaks ].x1 = xpos;
      breaks[ n_breaks ].penalty = 0;
      breaks[ n_breaks ].flags = HNJ_JUST_FLAG_ISSPACE;
      words[ n_breaks ] = current_word;
      js[ n_breaks ] = l;
      n_breaks++;
    }
  breaks[ n_breaks - 1 ].flags = 0;

  /* Calculate optimal line breaks. */
  n_actual_breaks = hnj_hs_just (breaks, n_breaks,
				 &(paragraph->params.hnjparams), result);

  /* Calculate text height and current line number. */
  current_word = paragraph->words;
  j = 0;
  for ( line_number = 0; line_number < n_actual_breaks; line_number++ )
    {
      Word *word;
      ascents[ line_number ] = descents[ line_number ] = extra_space[ line_number ] = word_counts[ line_number ] = 0;
      xpos = 0;
      for( ; current_word != NULL && current_word != words[ result[ line_number ] ]; current_word = g_list_next( current_word ) )
	{
	  word = (Word *) current_word->data;
	  if ( current_word == old_pos.paragraph_ptr.word && old_pos.paragraph_ptr.word_ptr >= j)
	    {
	      current_line_number = line_number;
	      current_word_number = word_counts[ line_number ];
	      x = xpos + gtk_word_text_width( word, j, old_pos.paragraph_ptr.word_ptr );
	    }
	  if( j == 0 )
	    xpos += word->width;
	  else
	    xpos += gtk_word_text_width( word, j, word->length );
	  ascents[ line_number ] = MAX( ascents[ line_number ], word->font->font->ascent );
	  descents[ line_number ] = MAX( descents[ line_number ], word->font->font->descent );
	  word_counts[ line_number ] ++;
	  j = 0;
	}
      word = (Word *) current_word->data;
      ascents[ line_number ] = MAX( ascents[ line_number ], word->font->font->ascent );
      descents[ line_number ] = MAX( descents[ line_number ], word->font->font->descent );
      if ( js[ result[ line_number ] ] != word->length)
	{
	  char nextchar;
	  int oldj = j;
	  j = js[ result[ line_number ] ];
	  if ( current_word == old_pos.paragraph_ptr.word && old_pos.paragraph_ptr.word_ptr < j && old_pos.paragraph_ptr.word_ptr >= oldj )
	    {
	      current_line_number = line_number;
	      current_word_number = word_counts[ line_number ];
	      x = xpos + gtk_word_text_width( word, oldj, old_pos.paragraph_ptr.word_ptr );
	    }
	  nextchar = word->text[j];
	  word->text[j] = 0;
	  xpos += gtk_word_text_width( word, oldj, j );
	  xpos += gdk_char_width( gtk_word_char_font( word, j )->font, '-' );
	  word->text[j] = nextchar;
	}
      else
	{
	  if ( current_word == old_pos.paragraph_ptr.word && old_pos.paragraph_ptr.word_ptr >= j )
	    {
	      x = xpos + gtk_word_text_width( word, j, old_pos.paragraph_ptr.word_ptr );
	      current_word_number = word_counts[ line_number ];
	      current_line_number = line_number;
	    }
	  if ( j == 0 )
	    xpos += word->width;
	  else
	    xpos += gtk_word_text_width( word, j, word->length );
	  j = 0;
	  current_word = g_list_next( current_word );
	}
      extra_space[ line_number ] = paragraph->params.hnjparams.set_width - xpos;
    }

  /* Add proper distance. */
  current_line_number += *distance;
  if ( current_line_number < 0 )
    {
      *distance -= current_line_number;
      current_line_number = 0;
    }
  if ( current_line_number >= n_actual_breaks )
    {
      *distance -= current_line_number - n_actual_breaks + 1;
      current_line_number = n_actual_breaks - 1;
    }

  /* Add x amount. */

  x += ( (double) extra_space[ current_line_number ] * (double) current_word_number ) / (double) word_counts[ current_line_number ];
      
  /* Calculate ypos. */
  ypos = ascents[0];
  for ( line_number = 0; line_number < current_line_number; )
    {
      ypos += descents[ line_number ];
      line_number ++;
      ypos += ascents[ line_number ];
    }
  newpos = gtk_paragraph_find_point( paragraph, x, ypos );
  return newpos;
}
#endif

ParagraphPosition
gtk_paragraph_position( Paragraph *par, int position )
{
  ParagraphPosition rval;
  GList *current_word;
  
  for ( current_word = par->words; current_word; current_word = g_list_next( current_word ) )
    {
      Word *word = (Word *) current_word->data;
      if ( position <= gtk_word_length( word ) )
	{
	  rval.word = current_word;
	  rval.word_ptr = gtk_word_position( word, position );
	  break;
	}
      else
	{
	  position -= gtk_word_length( word );
	  position -= 1;
	}
    }

  return rval;
}

gint
gtk_paragraph_position_to_int( Paragraph *paragraph, ParagraphPosition pos )
{
  GList *current_word;
  gint rval = 0;

  for ( current_word = paragraph->words; current_word; current_word = g_list_next( current_word ) )
    {
      Word *word = (Word *) current_word->data;
      if ( pos.word == current_word )
	{
	  rval += gtk_word_position_to_int( (Word *) word, pos.word_ptr );
	  break;
	}
      else
	rval += gtk_word_length( word ) + 1;
    }

  return rval;
}

gboolean
gtk_paragraph_a_first (Paragraph *par, ParagraphPosition a, ParagraphPosition b)
{
  GList *current_word; /* Of type Word *. */

  for ( current_word = par->words; current_word; current_word = g_list_next( current_word ) )
    {
      Word *word = (Word *) current_word->data;
      gint jump = ((current_word == a.word) ? 1 : 0 ) + ((current_word == b.word) ? 2 : 0 );
      switch( jump )
	{
	case 0:
	  break;
	case 1:
	  return TRUE;
	  break;
	case 2:
	  return FALSE;
	  break;
	case 3:
	  return gtk_word_a_first( word, a.word_ptr, b.word_ptr );
	  break;
	}
    }
  return FALSE;
}

void
gtk_paragraph_get_chars (Paragraph *par, gchar *buffer, gint start, gint end )
{
  int position = 0;
  GList *current_word;
  start = MAX( start, 0 );
  end = MIN( end, gtk_paragraph_length( par ) );
  
  for ( current_word = par->words; current_word; current_word = g_list_next( current_word ) )
    {
      Word *word = (Word *) current_word->data;
      int parend = position + gtk_word_length( word );
      if ( position < end && parend >= start )
	{
	  gint thisstart = MAX( start, position );
	  gint thisend = MIN( end, parend );
	  gtk_word_get_chars( word, buffer + thisstart - start, thisstart - position, thisend - position);
	}
      position = parend;
      if ( position < end && position >= start)
	if ( word->space == GTK_SPACE_SPACE )
	  buffer[position - start] = ' ';
	else
	  buffer[position - start] = '\t';
      position += 1;
    }
}

ParagraphPosition
gtk_paragraph_end_of_line( Paragraph *par, ParagraphPosition pos )
{
  gint line_number = gtk_paragraph_get_line_number( par, pos );
  ParagraphValues values = gtk_paragraph_calculate_values( par );
  ParagraphPosition returnval;
  returnval.word = values.words[ line_number ];
  returnval.word_ptr = values.js[ line_number ];
  return returnval;
}

ParagraphPosition
gtk_paragraph_move_backward_word( Paragraph *par, ParagraphPosition pos )
{
  if ( pos.word_ptr == gtk_word_start( pos.word->data ) && pos.word->prev != NULL )
    pos.word = pos.word->prev;
  pos.word_ptr = gtk_word_start( pos.word->data );
  return pos;
}

ParagraphPosition
gtk_paragraph_move_forward_word( Paragraph *par, ParagraphPosition pos )
{
  if ( pos.word_ptr == gtk_word_end( (Word *) pos.word->data ) && pos.word->next != NULL )
    pos.word = pos.word->next;
  pos.word_ptr = gtk_word_end( (Word *) pos.word->data );
  return pos;
}

void
gtk_paragraph_set_width( Paragraph *par, gint width )
{
  par->width = width;
  par->params.hnjparams.set_width = par->width - par->params.left_indent - par->params.right_indent;
  par->valuesvalid = FALSE;
}

gint
gtk_paragraph_top_of_pos( Paragraph *par, ParagraphPosition pos )
{
  gint line_number = gtk_paragraph_get_line_number( par, pos );
  ParagraphValues values = gtk_paragraph_calculate_values( par );
  gint i;
  gint top = 0;
  top += par->params.prespace;
  for( i = 0; i < line_number; i++, top += par->params.inspace )
    top += values.ascents[ i ] + values.descents[ i ];
  return top;
}

gint
gtk_paragraph_bottom_of_pos( Paragraph *par, ParagraphPosition pos )
{
  gint line_number = gtk_paragraph_get_line_number( par, pos );
  ParagraphValues values = gtk_paragraph_calculate_values( par );
  gint i;
  gint top = 0;
  top += par->params.prespace;
  for( i = 0; i < line_number + 1; i++, top += par->params.inspace )
    top += values.ascents[ i ] + values.descents[ i ];
  return top;
}

void gtk_paragraph_position_coords( Paragraph *par, ParagraphPosition pos, gint *x, gint *y )
{
  gint line_number = gtk_paragraph_get_line_number( par, pos );
  ParagraphValues values = gtk_paragraph_calculate_values( par );
  gint i;
  if ( y != NULL )
    {
      (*y) = par->params.prespace;
      for( i = 0; TRUE; i++, (*y) += par->params.inspace )
	{
	  (*y) += values.ascents[ i ];
	  if ( i >= line_number )
	    break;
	  (*y) += values.descents[ i ];
	}
    }
  if ( x != NULL )
    (*x) = gtk_paragraph_find_ideal( par, pos );
}

void
gtk_paragraph_traverse( Paragraph *paragraph,
			GtkRichTextTraversalCallbacks *callbacks,
			gpointer data )
{
  GList *current_word;
  Word *word;
  CharacterParams params;
  (*(callbacks->start_paragraph)) ( &(paragraph->params), data );
  current_word = paragraph->words;
  while( 1 )
    {
      word = (Word *) current_word->data;
      gtk_word_traverse( word, callbacks, data );
      current_word = g_list_next( current_word );
      if ( current_word == NULL )
	break;
      params.font = gtk_word_char_font( word, word->length );
      (*(callbacks->return_text)) ( " ", 1, &params, data );
    }
}

void
gtk_paragraph_traverse_partial (Paragraph *par,
				gint start,
				gint end,
				GtkRichTextTraversalCallbacks *callbacks,
				gpointer data )
{
  int position = 0;
  GList *current_word;
  start = MAX( start, 0 );
  end = MIN( end, gtk_paragraph_length( par ) );

  (*(callbacks->start_paragraph)) ( &(par->params), data );

  for ( current_word = par->words; current_word && position < end; current_word = g_list_next( current_word ) )
    {
      Word *word = (Word *) current_word->data;
      CharacterParams params;
      int parend = position + gtk_word_length( word );
      if ( position < end && parend >= start )
	{
	  gint thisstart = MAX( start, position );
	  gint thisend = MIN( end, parend );
	  gtk_word_traverse_partial( word, thisstart - position, thisend - position, callbacks, data );
	}
      position = parend;
      if ( position < end && position >= start)
	{
	  params.font = gtk_word_char_font( word, word->length );
	  (*(callbacks->return_text)) ( " ", 1, &params, data );
	}
      position += 1;
    }
}


typedef struct
{
  gint count;
  GList **words;
  gint *js;
  gdouble *ascents;
  gdouble *descents;
  gdouble *extra_space;
  gdouble *space_space;
  gint *word_counts;
  gint *tab_count;
  gint current_x_line;
  gint current_y_char;
  gdouble height;
} ParagraphValuesWithSystem;

static ParagraphValuesWithSystem
gtk_paragraph_calculate_values_with_system( Paragraph *paragraph,
					    GtkRichTextRenderSystem *system )
{
  HnjBreak breaks[100000];
  gint result[100000];
  GList *words[100000];
  gint js[100000];
  gdouble ascents[100000];
  gdouble descents[100000];
  gdouble extra_space[100000];
  gint word_counts[100000];
  gdouble space_space[100000];
  gint tab_count[100000];
  gint n_breaks = 0, n_actual_breaks = 0;
  gint i;
  gint l;
  gint j;
  gint line_number;
  GList *current_word;
  gdouble x = paragraph->params.first_indent * SCALE;
  ParagraphValuesWithSystem return_val;
  HnjParams hnjparams = paragraph->params.hnjparams;

  hnjparams.set_width = ( system->width - paragraph->params.left_indent - paragraph->params.right_indent ) * SCALE;

  /* find possible line breaks. */
  for ( current_word = paragraph->words;
	current_word;
	current_word = g_list_next( current_word ) )
    {
      Word *word = (Word *) current_word->data;
      l = word->length;
      for (j = 0; j < l; j++)
	{
	  x += gtk_word_char_width_with_system( word, j, system ) * SCALE;
	  if ( word->text[j] == '-' )
	    {
	      breaks[n_breaks].x0 = x;
	      breaks[n_breaks].x1 = x;
	      breaks[n_breaks].penalty = HYPHEN_PENALTY;
	      breaks[n_breaks].flags = HNJ_JUST_FLAG_ISHYPHEN;
	      words[n_breaks] = current_word;
	      js[n_breaks] = j + 1;
	      n_breaks++;
	    }
	  else if (word->hyphenation[j] & 1)
	    {
	      breaks[n_breaks].x0 = x + system->char_width( system, gtk_word_char_font( word, j ), '-' ) * SCALE;
	      breaks[n_breaks].x1 = x;
	      breaks[n_breaks].penalty = HYPHEN_PENALTY;
	      breaks[n_breaks].flags = HNJ_JUST_FLAG_ISHYPHEN;
	      words[n_breaks] = current_word;
	      js[n_breaks] = j + 1;
	      n_breaks++;
	    }
	}
      if ( word->space == GTK_SPACE_SPACE )
	{
	  breaks[ n_breaks ].x0 = x;
	  x += system->char_width( system, gtk_word_char_font( word, word->length ), ' ' ) * SCALE;
	  breaks[ n_breaks ].x1 = x;
	  breaks[ n_breaks ].penalty = 0;
	  breaks[ n_breaks ].flags = HNJ_JUST_FLAG_ISSPACE;
	  words[ n_breaks ] = current_word;
	  js[ n_breaks ] = l;
	}
      else
	{
	  breaks[n_breaks].x0 = x;
	  breaks[n_breaks].x1 = x;
	  breaks[n_breaks].penalty = 0;
	  breaks[n_breaks].flags = HNJ_JUST_FLAG_ISTAB;
	  words[n_breaks] = current_word;
	  js[n_breaks] = l;
	}
      n_breaks++;
    }
  breaks[ n_breaks - 1 ].flags = 0;

  /* Calculate optimal line breaks. */
  n_actual_breaks = hnj_hs_just (breaks, n_breaks,
				 &(hnjparams), result);

  return_val.count = n_actual_breaks;
  return_val.words = g_new( GList *, return_val.count );
  return_val.js = g_new( gint, return_val.count );

  for ( i = 0; i < return_val.count; i++ )
    {
      return_val.words[i] = words[ result[ i ] ];
      return_val.js[i] = js[ result[ i ] ];
    }


  /* Calculate full justification information and text height. */
  current_word = paragraph->words;
  return_val.height = 0;
  j = 0;
  for ( line_number = 0; line_number < return_val.count; line_number++ )
    {
      Word *word;
      tab_count[ line_number ] = word_counts[ line_number ] = 0;
      ascents[ line_number ] = descents[ line_number ]
	= space_space[ line_number ] = extra_space[ line_number ] = 0.0;
      if ( line_number == 0 )
	x = paragraph->params.first_indent;
      else
	x = 0;
      for( ;
	   current_word != NULL
	     && current_word != return_val.words[ line_number ];
	   current_word = g_list_next( current_word ) )
	{
	  word = (Word *) current_word->data;
	  x += gtk_word_text_width_with_system( word, j, word->length, system );
	  ascents[ line_number ]
	    = MAX( ascents[ line_number ],
		   gtk_word_max_ascent_with_system( word,
						    j, word->length,
						    system ) );
	  descents[ line_number ]
	    = MAX( descents[ line_number ],
		   gtk_word_max_descent_with_system( word,
						     j, word->length,
						     system ) );
	  word_counts[ line_number ] ++;
	  if ( word->space == GTK_SPACE_SPACE )
	    {
	      gint delta
		= system->char_width( system,
				      gtk_word_char_font( word, word->length ), ' ' );
	      space_space[ line_number ] += delta;
	      x += delta;
	    }
	  else
	    {
	      word_counts[ line_number ] = 1;
	      tab_count[ line_number ] ++;
	    }
	  j = 0;
	}
      word = (Word *) current_word->data;
      word_counts[ line_number ] ++;
      if ( return_val.js[ line_number ] != word->length)
	{
	  char nextchar;
	  int oldj = j;
	  j = return_val.js[ line_number ];
	  nextchar = word->text[j];
	  word->text[j] = 0;
	  x += gtk_word_text_width_with_system( word, oldj, j, system );
	  x += system->char_width( system, gtk_word_char_font( word, j ), '-' );
	  ascents[ line_number ] = MAX( ascents[ line_number ], gtk_word_max_ascent_with_system( word, oldj, j, system ) );
	  descents[ line_number ] = MAX( descents[ line_number ], gtk_word_max_descent_with_system( word, oldj, j, system ) );
	  word->text[j] = nextchar;
	}
      else
	{
	  x += gtk_word_text_width_with_system( word, j, word->length, system );
	  ascents[ line_number ] = MAX( ascents[ line_number ], gtk_word_max_ascent_with_system( word, j, word->length, system ) );
	  descents[ line_number ] = MAX( descents[ line_number ], gtk_word_max_descent_with_system( word, j, word->length, system ) );
	  j = 0;
	  current_word = g_list_next( current_word );
	}
      word_counts[ line_number ] --;
      extra_space[ line_number ] = hnjparams.set_width / SCALE - x + space_space[ line_number ];
      return_val.height += ascents[ line_number ] + descents[ line_number ];
    }

  return_val.height += (return_val.count - 1) * paragraph->params.inspace;
  return_val.height += paragraph->params.prespace;
  return_val.height += paragraph->params.postspace;

  return_val.ascents = g_new( gdouble, return_val.count );
  return_val.descents = g_new( gdouble, return_val.count );
  return_val.word_counts = g_new( gint, return_val.count );
  return_val.extra_space = g_new( gdouble, return_val.count );
  return_val.space_space = g_new( gdouble, return_val.count );
  return_val.tab_count = g_new( gint, return_val.count );

  for ( i = 0; i < return_val.count; i++ )
    {
      return_val.ascents[i] = ascents[i];
      return_val.descents[i] = descents[i];
      return_val.word_counts[i] = word_counts[i];
      return_val.extra_space[i] = extra_space[i];
      return_val.space_space[i] = space_space[i];
      return_val.tab_count[i] = tab_count[ i ];
    }

  return return_val;
}

void
gtk_paragraph_render( Paragraph *paragraph,
		      GtkRichTextRenderSystem *system )
{
  ParagraphValuesWithSystem values = gtk_paragraph_calculate_values_with_system( paragraph, system );
  gint j;
  GList *current_word;
  gint line_number;
  gint tab_width = paragraph->params.hnjparams.tab_width;
  Word *word;

  /* Do actual drawing. */
  j = 0;
  current_word = paragraph->words;
  system->y += paragraph->params.prespace;
  for ( line_number = 0;
	line_number < values.count;
	line_number++, system->y += paragraph->params.inspace )
    {
      Word *word;
      float space = 0.0;
      gint tabs_so_far = 0;

      if (values.word_counts[ line_number ] > 0)
	space = ((float) values.extra_space[ line_number ] ) /
	  values.word_counts[ line_number ];
      else
	space = (float) values.extra_space[ line_number ];

      if ( line_number == 0 )
	system->x = paragraph->params.first_indent + paragraph->params.left_indent;
      else
	system->x = paragraph->params.left_indent;
      switch( paragraph->params.justification )
	{
	case GTK_JUSTIFY_LEFT:
	case GTK_JUSTIFY_FILL:
	  /* Add nothing. */
	  break;
	case GTK_JUSTIFY_RIGHT:
	  system->x += values.extra_space[ line_number ] - values.space_space[ line_number ];
	  break;
	case GTK_JUSTIFY_CENTER:
	  system->x += ( values.extra_space[ line_number ] - values.space_space[ line_number ] ) / 2;
	  break;
	}
      if ( system->height != -1 && system->y + values.ascents[ line_number ] + values.descents[ line_number ] > system->height )
	{
	  system->output_page( system );
	  system->y = 0;
	}
      system->y += values.ascents[ line_number ];
      for( ;
	   current_word != NULL && current_word != values.words[ line_number ];
	   current_word = g_list_next( current_word ) )
	{
	  word = (Word *) current_word->data;

	  gtk_word_render( word, j, word->length, system );

	  if ( word->space == GTK_SPACE_SPACE )
	    {
	      switch( paragraph->params.justification )
		{
		case GTK_JUSTIFY_LEFT:
		case GTK_JUSTIFY_RIGHT:
		case GTK_JUSTIFY_CENTER:
		  system->x += system->char_width( system, gtk_word_char_font( word, word->length ), ' ' );
		  break;
		case GTK_JUSTIFY_FILL:
		  if ( ( line_number == values.count - 1 && values.extra_space[line_number] - values.space_space[line_number] >= 0 ) || ( values.tab_count[ line_number ] - tabs_so_far > 0 ) )
		    {
		      system->x += system->char_width( system, gtk_word_char_font( word, word->length ), ' ' );
		    }
		  else
		    {
		      system->x += space;
		      break;
		    }
		}
	    }
	  else if ( word->space == GTK_SPACE_TAB )
	    {
	      system->x = ( floor( system->x / tab_width ) + 1 ) * tab_width;
	      tabs_so_far ++;
	    }
	  j = 0;
	}
      word = (Word *) current_word->data;
      if ( values.js[ line_number ] != word->length)
	{
	  char nextchar;
	  int oldj = j;
	  j = values.js[ line_number ];
	  nextchar = word->text[j];
	  word->text[j] = 0;
	  gtk_word_render( word, oldj, j, system );
	  
	  system->draw_text( system, gtk_word_char_font( word, j ), "-", 1 );
	  word->text[j] = nextchar;
	}
      else
	{
	  gtk_word_render( word, j, gtk_word_end( word ), system );
	  j = 0;
	  current_word = g_list_next( current_word );
	}
      system->y += values.descents[ line_number ];
    }
}
