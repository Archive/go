/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GTK_RICH_TEXT_H__
#define __GTK_RICH_TEXT_H__


#include <gdk/gdk.h>
#include <gtk/gtkwidget.h>
#include <gtk/gtkadjustment.h>
#include <gtk/gtksignal.h>
#include <gtk/gtkeditable.h>
#include <libhnj/hsjust.h>
#include "gtkparagraph.h"
#include "gtkword.h"
#include "gtkrichtextcallbacks.h"


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
#define GTK_RICH_TEXT(obj)          GTK_CHECK_CAST (obj, gtk_rich_text_get_type (), GtkRichText)
#define GTK_RICH_TEXT_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, gtk_rich_text_get_type (), GtkRichTextClass)
#define GTK_IS_RICH_TEXT(obj)       GTK_CHECK_TYPE (obj, gtk_rich_text_get_type ())

typedef struct _GtkRichText        GtkRichTextModel;
typedef struct _GtkRichTextClass   GtkRichTextClass;
typedef struct _Position           Position;

struct _Position
{
  ParagraphPosition paragraph_ptr;
  GList *paragraph; /* Of type Paragraph */
};

struct _GtkRichTextModel
{
  
  gpointer draw_data;

  gint minimum_width;
  gint width;

  GList *paragraphs; /* Of type Paragraph. */

  Position point;
  gboolean point_valid;

  int cursor_timeout;
  gboolean cursor_on;

  gint ideal;
  gboolean ideal_valid;

  gint queuedx, queuedy;
  gint motion_callback;
  gint redraw_callback;

  gint last_button;
  gint last_count;
};

struct _GtkRichTextClass
{
  GtkEditableClass parent_class;
};


void       gtk_rich_text_model_set_minimum_width (GtkRichTextModel *rtext,
					    gint         min_width);
void       gtk_rich_text_model_set_width (GtkRichTextModel *rtext,
				    gint         width);
gint       gtk_rich_text_model_get_width (GtkRichTextModel *rtext);
void       gtk_rich_text_model_insert          (GtkRichTextModel  *text,
						GtkRichTextFont   *font,
						GdkColor          *fore,
						GdkColor          *back,
						const char        *chars,
						gint               length);
gint       gtk_rich_text_model_backward_delete (GtkRichTextModel       *text,
						guint              nchars);
gint       gtk_rich_text_model_forward_delete  (GtkRichTextModel       *text,
						guint              nchars);


/* Just stubs so far. */
void       gtk_rich_text_model_set_editable    (GtkRichTextModel  *text,
						gboolean           editable);
void       gtk_rich_text_model_set_word_wrap   (GtkRichTextModel  *text,
						gint               word_wrap);
void       gtk_rich_text_model_set_point       (GtkRichTextModel  *text,
						guint              index);
guint      gtk_rich_text_model_get_point       (GtkRichTextModel  *text);
guint      gtk_rich_text_model_get_length      (GtkRichTextModel  *text);
void       gtk_rich_text_model_freeze          (GtkRichTextModel  *text);
void       gtk_rich_text_model_thaw            (GtkRichTextModel  *text);
gboolean   gtk_rich_text_model_position_equal  (GtkRichTextModel  *text,
						Position           a,
						Position           b);

void gtk_rich_text_model_set_selection_paragraph_params (GtkRichTextModel *text,
							 ParagraphParams *params,
							 ParagraphParamsValid *validity);
void gtk_rich_text_model_set_location_paragraph_params (GtkRichTextModel *text,
							gint location,
							ParagraphParams *params,
							ParagraphParamsValid *validity);
void gtk_rich_text_model_set_range_paragraph_params (GtkRichTextModel *text,
						     gint start,
						     gint end,
						     ParagraphParams *params,
						     ParagraphParamsValid *validity);

void gtk_rich_text_model_set_selection_character_params (GtkRichTextModel *text,
							 CharacterParams *params,
							 CharacterParamsValid *validity);
void gtk_rich_text_model_set_location_character_params (GtkRichTextModel *text,
							gint location,
							CharacterParams *params,
							CharacterParamsValid *validity);
void gtk_rich_text_model_set_range_character_params (GtkRichTextModel *text,
						     gint start,
						     gint end,
						     CharacterParams *params,
						     CharacterParamsValid *validity);

void gtk_rich_text_model_traverse( GtkRichTextModel *text,
				   GtkRichTextTraversalCallbacks *callbacks,
				   gpointer data );

void gtk_rich_text_model_traverse_partial( GtkRichTextModel *text,
					   gint start,
					   gint end,
					   GtkRichTextTraversalCallbacks *callbacks,
					   gpointer data );

gint gtk_rich_text_model_position_to_int( GtkRichTextModel *text, Position pos );

#define GTK_RICH_TEXT_MODEL_INDEX(t, index)  0
/*\
  ((index) < (t)->gap_position ? (t)->text[index] : \
  (t)->text[(index) + (t)->gap_size])
*/

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_RICH_TEXT_MODEL_H__ */
