/* window.h - Header file for window manipulations.
 *
 * Copyright (C) 1998 Chris Lahey.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __WINDOW_H__
#define __WINDOW_H__

#include "config.h"

#include <gtk/gtk.h>
#include "undo.h"
#include <gnome.h>
#include "gtkrichtext.h"
#include "ruler.h"
#include "go_types.h"

#define DEF_WIDTH 768
#define DEF_HEIGHT 576

typedef struct int_gchar_pair
{
  int x;
  gchar *title;
} int_gchar_pair;

GOWindow *make_window( char *title );
GOWindow *make_window_with_dimensions( int x, int y, int width, int height, char *title );
void window_set_title( GOWindow *window, gchar *title );
void window_close( GOWindow *window );
void window_cleanup( GOWindow *window );
int count_title( gchar *title );
gchar *g_malloc_title( GOWindow *window, gchar *prefix );
void GO_raise_window( GtkWidget *widget, gpointer data );
gint reset_usize( gpointer data );
void add_plugin_info_to_plugin_list( gpointer data, gpointer user_data );
void setup_window_stuff( GOWindow *window, gchar *title );
void add_window_to_menu( gpointer data, gpointer user_data );
void remove_window_from_menu( gpointer data, gpointer user_data );
void set_char_params_callback( GOWindow *window, gint start, gint end, CharacterParams *params, CharacterParamsValid *validity );
void set_par_params_callback( GOWindow *window, gint start, gint end, ParagraphParams *params, ParagraphParamsValid *validity );
void toggle_editable_callback( GtkWidget *widget, gpointer data );
gint toggle_editable( GOWindow *window );
void embed_widget( GOWindow *window, GtkWidget *widget, gint width, gint height );
void embed_widget_at_window_location( GOWindow *window, GtkWidget *widget, gint x, gint y, gint width, gint height );
#endif
