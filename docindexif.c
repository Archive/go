
#include "config.h"
#include "window.h"
#include "menu.h"
#include "file.h"
#include "util.h"
#include "toolbar.h"
#include "preferences.h"
#include "docindex.h"
#include "docindexif.h"

extern GList *idea_list;   /* of gchar *. */
extern GList *windowlist;   /* of GOWindow *. */

void
open_or_raise( gchar *file_name )
{
  struct bool_char_pair pair;
  
  pair.boole = FALSE;
  pair.string = file_name;
  
  g_list_foreach( windowlist, raise_if_match, &pair );
  
  if ( ! pair.boole )
    {
      open_file( file_name );
    }
}

gboolean
exit_from_go( void )
{
  if ( preferences->quit_on_idea_close || ! windowlist )
    file_exit_callback( NULL, NULL );
  return preferences->quit_on_idea_close || ! windowlist;
}

void
raise_if_match( gpointer data, gpointer user_data )
{
  GOWindow *window = (GOWindow *) data;
  struct bool_char_pair *pair = (struct bool_char_pair *) user_data;
  if ( ! pair->boole )
    if ( strcmp( pair->string, window->title ) == 0 )
      {
	pair->boole = TRUE;
	GO_raise_window( NULL, window );
      }
}


void
make_idea_window( int x, int y )
{
  GtkWidget *main_vbox, *menu;
  GtkWidget *scrolled_win;
  GtkWidget *toolbar;
  GtkAccelGroup *accel;

  /* malloc idea_manager */
  ideas = g_malloc0( sizeof( idea_manager ) );

  /* Setup tree */
  ideas->tree = gtk_tree_new();
  gtk_tree_set_selection_mode( GTK_TREE( ideas->tree ), GTK_SELECTION_BROWSE );
  
  /* Setup scrolled window */
  scrolled_win = gtk_scrolled_window_new( NULL, NULL );
  gtk_scrolled_window_set_policy( GTK_SCROLLED_WINDOW( scrolled_win ), GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS );
  gtk_scrolled_window_add_with_viewport( GTK_SCROLLED_WINDOW( scrolled_win ), ideas->tree );
  gtk_widget_show( ideas->tree );

  /* allocate the window and attach the menu */
  ideas->window = gnome_app_new( "Go", NULL );
  create_gnome_docindex_menu( ideas );

  /* Setup the status bar */
  ideas->status = gtk_statusbar_new();
  ideas->contextid = gtk_statusbar_get_context_id( GTK_STATUSBAR( ideas->status ), "main context" );

  create_gnome_docindex_toolbar( ideas );
  
  /* Setup a vbox to contain the menu */
  main_vbox = gtk_vbox_new( FALSE, 0 );
  gtk_box_pack_start( GTK_BOX( main_vbox ), scrolled_win, TRUE, TRUE, 0 ); 
  gtk_box_pack_start( GTK_BOX( main_vbox ), ideas->status, FALSE, FALSE, 0 );
  gtk_widget_show( scrolled_win );
  gtk_widget_show( ideas->status );

  /* Set the GOWindow title */
  ideas->title = g_strdup( _("Document Index") );

  /* Set the GtkWindow title */
  gtk_window_set_title( GTK_WINDOW( ideas->window ), ideas->title );

  /* Set the initial status message */
  gtk_statusbar_push( GTK_STATUSBAR( ideas->status ), ideas->contextid,  _("GTK successfully started") );

  /* Connect the signals */
  gtk_signal_connect( GTK_OBJECT( ideas->window ), "delete_event",
		      GTK_SIGNAL_FUNC( idea_window_delete_event_callback ),
		      NULL );
  
  /* Add the main vbox to the window */
  gnome_app_set_contents( GNOME_APP( ideas->window ), main_vbox );
  gtk_widget_show( main_vbox );

  docindex_configure_drop_on_widget(ideas->tree);

  /* Load and Show window */
  load_idea_manager( ideas );

  /* Set the position of the window if it was requested */
  if ( x >= 0 && y >= 0 )
    gtk_widget_set_uposition( ideas->window, x, y );
}
