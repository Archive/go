/* file.c - Does actions on files and in general implements the commands 
 * of the File menu.
 *
 * Copyright (C) 1998 Chris Lahey.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include "config.h"

#include <gtk/gtk.h>

#include <unistd.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <libgnomeprint/gnome-font.h>

#include "file.h"
#include "window.h"
#include "docindex.h"
#include "util.h"
#include "time.h"

#include "gtkrichtext.h"
#include <gnome-xml/tree.h>
#include <gnome-xml/parser.h>

extern GList *windowlist;
extern idea_manager *ideas;
extern gboolean no_autoload;

void save_changes( GOWindow *window );
void ok_save( GtkWidget *button, gpointer g_data );
void ok_load( GtkWidget *button, gpointer g_data );
void open_save_dialog( GtkWidget *widget, gpointer data );
void save_changes_callback( gpointer data, gpointer user_data );
void save_location_callback( gpointer data, gpointer user_data );

static gint save_as_go_ff( GOWindow *window, gchar *filename );
static void ok_save_as_go_ff( GtkWidget *button, gpointer g_data );
static void save_as_go_ff_start_paragraph( ParagraphParams *params, gpointer data );
static void save_as_go_ff_return_text( gchar *chars, gint length, CharacterParams *params, gpointer data );

static gint save_as_rtf( GOWindow *window, gchar *filename );
static void ok_save_as_rtf( GtkWidget *button, gpointer g_data );
static void save_as_rtf_start_paragraph( ParagraphParams *params, gpointer data );
static void save_as_rtf_return_text( gchar *chars, gint length, CharacterParams *params, gpointer data );

/* ok_save
 *  This function gets called when the user pushes the Save button in
 *  the Save as dialog.  FIXME. */
void ok_save( GtkWidget *button, gpointer g_data )
{
  gchar *data;
  FILE *fp;
  selector_window_pair *pair = ( selector_window_pair * ) g_data;
  GOWindow *window = pair->window;
  gchar *title = gtk_file_selection_get_filename( GTK_FILE_SELECTION( pair->selector ) );
  /*  gint x, y, width, height, sel_start, sel_end; */
  /*  gfloat scrollpos; */

  fp = fopen( title, "w" );
  if ( fp )
    {

      if( window->changed )
	save_changes( window );
      
      save_location( window );
      
      data = gtk_editable_get_chars( GTK_EDITABLE( window->textbox ),
				     0, -1 );
      fwrite( data, 1, strlen( data ), fp );
      g_free( data );
      fclose( fp );
      
      g_list_foreach( windowlist, remove_window_from_menu, window );
      
      undo_tree_destroy( window->undo );
      
      g_free( window->title );
      
      setup_window_stuff( window, title );

      windowlist = g_list_append( windowlist, ( gpointer ) window );
      
      /* Add ourselves to the tree */
      idea_add_in_position( title, 0 );
      
      window->named = TRUE;
      window->changed = FALSE;
    }
  gtk_widget_destroy( pair->selector );
  g_free( pair );
}

/* save_changes
 *  Saves changes and resets autosave callback functions. */
void save_changes( GOWindow *window )
{
  gchar *data;
  FILE *fp;
  if( window->named )
    {
      switch( window->format )
	{
	case GO_FILE_FORMAT_PLAIN:
	  fp = fopen( window->title, "w" );
	  if ( fp )
	    {
	      if( window->auto_save )
		{
		  gtk_timeout_remove( window->auto_save );
		  window->auto_save = 0;
		}
	      if( window->long_term_auto_save )
		{
		  gtk_timeout_remove( window->long_term_auto_save );
		  window->long_term_auto_save = 0;
		}
	      data = gtk_editable_get_chars( GTK_EDITABLE( window->textbox ), 0, -1 );
	      fwrite( data, 1, strlen( data ), fp );
	      g_free( data );
	      fclose( fp );
	      window->changed = FALSE;
	    }
	  break;
	case GO_FILE_FORMAT_GO:
	  save_as_go_ff( window, window->title );
	  break;
	case GO_FILE_FORMAT_RTF:
	  save_as_rtf( window, window->title );
	}
    }
  else
    {
      if( window->auto_save )
	{
	  gtk_timeout_remove( window->auto_save );
	  window->auto_save = 0;
	}
      if( window->long_term_auto_save )
	{
	  gtk_timeout_remove( window->long_term_auto_save );
	  window->long_term_auto_save = 0;
	}
    }
}


/*
 * Get a value for a node either carried as an attibute or as
 * the content of a child.
 */
static char *
xmlGetValue (xmlNodePtr node, const char *name)
{
	char *ret;
	xmlNodePtr child;

	ret = (char *) xmlGetProp (node, name);
	if (ret != NULL)
		return g_strdup (ret);
	child = node->childs;

	while (child != NULL) {
		if (!strcmp (child->name, name)) {
		        /*
			 * !!! Inefficient, but ...
			 */
			ret = xmlNodeGetContent(child);
			if (ret != NULL)
			    return (ret);
		}
		child = child->next;
	}

	return NULL;
}

/*
 * Get an integer value for a node either carried as an attibute or as
 * the content of a child.
 */
static int
xmlGetIntValue (xmlNodePtr node, const char *name, int *val)
{
	char *ret;
	int i;
	int res;

	ret = xmlGetValue (node, name);
	if (ret == NULL) return(0);
	res = sscanf (ret, "%d", &i);
	free(ret);
	
	if (res == 1) {
	        *val = i;
		return 1;
	}
	return 0;
}

/*
 * Get a double value for a node either carried as an attibute or as
 * the content of a child.
 */
static int
xmlGetDoubleValue (xmlNodePtr node, const char *name, double *val)
{
	int res;
	char *ret;
	xmlNodePtr child;
	float f;

	ret = xmlGetValue (node, name);
	if (ret == NULL) return(0);
	res = sscanf (ret, "%f", &f);
	free(ret);
	
	if (res == 1) {
	        *val = f;
		return 1;
	}
	return 0;
}

/*
 * Set a string value for a node either carried as an attibute or as
 * the content of a child.
 */
static void xmlSetValue (xmlNodePtr node, const char *name, const char *val)
{
	const char *ret;
	xmlNodePtr child;

	ret = xmlGetProp (node, name);
	if (ret != NULL){
		xmlSetProp (node, name, val);
		return;
	}
	child = node->childs;
	while (child != NULL){
		if (!strcmp (child->name, name)){
			xmlNodeSetContent (child, val);
			return;
		}
		child = child->next;
	}
	xmlSetProp (node, name, val);
}

/*
 * Set an integer value for a node either carried as an attibute or as
 * the content of a child.
 */
static void
xmlSetIntValue (xmlNodePtr node, const char *name, int val)
{
	const char *ret;
	xmlNodePtr child;
	char str[101];

	g_snprintf (str, 100, "%d", val);
	ret = xmlGetProp (node, name);
	if (ret != NULL){
		xmlSetProp (node, name, str);
		return;
	}
	child = node->childs;
	while (child != NULL){
		if (!strcmp (child->name, name)){
			xmlNodeSetContent (child, str);
			return;
		}
		child = child->next;
	}
	xmlSetProp (node, name, str);
}

/*
 * Set a double value for a node either carried as an attibute or as
 * the content of a child.
 */
static void
xmlSetDoubleValue (xmlNodePtr node, const char *name, double val)
{
	const char *ret;
	xmlNodePtr child;
	char str[101];

	g_snprintf (str, 100, "%f", (float) val);
	ret = xmlGetProp (node, name);
	if (ret != NULL){
		xmlSetProp (node, name, str);
		return;
	}
	child = node->childs;
	while (child != NULL){
		if (!strcmp (child->name, name)){
			xmlNodeSetContent (child, str);
			return;
		}
		child = child->next;
	}
	xmlSetProp (node, name, str);
}

typedef struct
{
  xmlDoc *doc;
  xmlNode *cur;
  xmlNs *ns;
  GtkRichTextFont *lastfont;
  xmlNode *lastnode;
  GtkRichTextFont *defaultfont;
} parseXmlContext;

static void
save_as_go_ff_start_paragraph( ParagraphParams *params, gpointer data )
{
  parseXmlContext *ctxt = (parseXmlContext *) data;
  xmlNode *paragraph = xmlNewChild( ctxt->doc->root, ctxt->ns, "paragraph", NULL );
  xmlSetIntValue( paragraph, "pre_space", params->prespace );
  xmlSetIntValue( paragraph, "in_space", params->inspace );
  xmlSetIntValue( paragraph, "post_space", params->postspace );
  xmlSetIntValue( paragraph, "first_indent", params->first_indent );
  xmlSetIntValue( paragraph, "left_indent", params->left_indent );
  xmlSetIntValue( paragraph, "right_indent", params->right_indent );
  xmlSetIntValue( paragraph, "justification", (int) params->justification );
  ctxt->cur = paragraph;
  ctxt->lastnode = NULL;
}

static void
save_as_go_ff_return_text( gchar *chars, gint length, CharacterParams *params, gpointer data )
{
  parseXmlContext *ctxt = (parseXmlContext *) data;
  gchar *charsloc;
  xmlNode *text;
  if ( ctxt->lastfont && gdk_font_equal( params->font->font, ctxt->lastfont->font ) && ctxt->lastnode != NULL )
    {
      gchar *lastcontent = xmlNodeGetContent( ctxt->lastnode );
      gint lastlength = strlen( lastcontent );
      charsloc = g_malloc0( lastlength + length + 1 );
      strcpy( charsloc, lastcontent );
      strncpy( charsloc + lastlength, chars, length );
      charsloc[lastlength + length] = 0;
      text = ctxt->lastnode;
      free( lastcontent );
    }
  else
    {
      charsloc = g_malloc0( length + 1 );
      strncpy( charsloc, chars, length );
      charsloc[length] = 0;
      text = xmlNewChild( ctxt->cur, ctxt->ns, "text", NULL );
      if ( params->font->data )
	{
	  GnomeFont *font = params->font->data;
	  if ( strcmp( font->fontmap_entry->familyname, "Times" ) )
	    xmlSetValue( text, "font_family", font->fontmap_entry->familyname );
	  if ( ((int) font->fontmap_entry->weight_code ) != 0 )
	    xmlSetIntValue( text, "weight", (int) font->fontmap_entry->weight_code );
	  if ( ((int) font->fontmap_entry->italic ) != FALSE )
	    xmlSetIntValue( text, "italic", (int) 1 );
	  if ( ((double) font->size ) != 12.0 )
	    xmlSetDoubleValue( text, "size", (double) font->size );
	}
      else
	{
	  xmlSetValue( text, "xfont", params->font->desc );
	}
    }
  xmlNodeSetContent( text, charsloc );
  ctxt->lastnode = text;
  ctxt->lastfont = params->font;
  g_free( charsloc );
}

/* Return value denotes success.  0 = sucess.  -1 = failure. */
static gint save_as_go_ff( GOWindow *window, gchar *filename )
{
  xmlDoc *doc;
  xmlNs *name_space;
  xmlNode *tree, *subtree;
  gint ret;
  GtkRichTextTraversalCallbacks callbacks;
  parseXmlContext ctxt;

  doc = xmlNewDoc( "1.0" );
  if ( doc == NULL )
    return -1;
  name_space = xmlNewGlobalNs( doc, "http://www.gnome.org/go/", "go" );

  doc->root = xmlNewDocNode( doc, name_space, "Document", NULL );
  xmlSetIntValue( doc->root, "width",
		  gtk_rich_text_get_width( GTK_RICH_TEXT(window->textbox) ) );
  if ( window->props->print_page_nums )
    xmlSetIntValue( doc->root, "print_page_nums", 1 );

  ctxt.doc = doc;
  ctxt.cur = doc->root;
  ctxt.ns = name_space;
  ctxt.lastfont = NULL;
  ctxt.lastnode = NULL;
  
  callbacks.return_text = save_as_go_ff_return_text;
  callbacks.start_paragraph = save_as_go_ff_start_paragraph;

  gtk_rich_text_traverse( GTK_RICH_TEXT( window->textbox ),
			  &callbacks,
			  (gpointer) &ctxt );
  
  
  xmlSetDocCompressMode (doc, 9);
  ret = xmlSaveFile (filename, doc);
  xmlFreeDoc (doc);
  window->changed = FALSE;
  if (ret < 0)
    return -1;
  return 0;
}


typedef struct _RtfContext RtfContext;
struct _RtfContext
{
  GtkRichTextFont *lastfont;
  gboolean started;
  FILE *fp;
};

static void
rtf_escaped_output( RtfContext *ctxt, gchar *astring )
{
  gint laststart = 0;
  gint i;
  for ( i = 0; astring[i]; i++ )
    {
      switch ( astring[i] )
	{
	case '\\':
	case '{':
	case '}':
	  fwrite( astring + laststart, 1, i - laststart, ctxt->fp );
	  fprintf( ctxt->fp, "\\%c", astring[i] );
	  laststart = i + 1;
	  break;
	}
    }
  if ( i - laststart > 0 )
    {
      fwrite( astring + laststart, 1, i - laststart, ctxt->fp );
    }
}

static void
rtf_output( RtfContext *ctxt, gchar *astring )
{
  fprintf( ctxt->fp, astring );
}

static void
rtf_number_output( RtfContext *ctxt, gint number )
{
  fprintf( ctxt->fp, "%d", number * 10 );
}

static void
save_as_rtf_start_paragraph( ParagraphParams *params, gpointer data )
{
  RtfContext *ctxt = (RtfContext *) data;
  if ( ctxt->lastfont )
    rtf_output( ctxt, "}" );
  if ( ctxt->started )
    rtf_output( ctxt, "\\par\n}" );
  else
    ctxt->started = TRUE;
  ctxt->lastfont = NULL;
  rtf_output( ctxt, "{" );
  switch ( params->justification )
    {
    case GTK_JUSTIFY_LEFT:
      break;
    case GTK_JUSTIFY_FILL:
      rtf_output( ctxt, "\\qj " );
      break;
    case GTK_JUSTIFY_RIGHT:
      rtf_output( ctxt, "\\qr " );
      break;
    case GTK_JUSTIFY_CENTER:
      rtf_output( ctxt, "\\qc " );
      break;
    }
  if ( params->prespace != 0 )
    {
      rtf_output( ctxt, "\\sb" );
      rtf_number_output( ctxt, params->prespace );
      rtf_output( ctxt, " " );
    }
  /*
  if ( params->inspace != 0 )
    {
      rtf_output( ctxt, "\\sl" );
      rtf_number_output( params->prespace );
      rtf_output( ctxt, " " );
    }
  */
  if ( params->postspace != 0 )
    {
      rtf_output( ctxt, "\\sa" );
      rtf_number_output( ctxt, params->postspace );
      rtf_output( ctxt, " " );
    }
  if ( params->first_indent != 0 )
    {
      rtf_output( ctxt, "\\fi" );
      rtf_number_output( ctxt, params->first_indent );
      rtf_output( ctxt, " " );
    }
  if ( params->left_indent != 0 )
    {
      rtf_output( ctxt, "\\li" );
      rtf_number_output( ctxt, params->left_indent );
      rtf_output( ctxt, " " );
    }
  if ( params->right_indent != 0 )
    {
      rtf_output( ctxt, "\\ri" );
      rtf_number_output( ctxt, params->right_indent );
      rtf_output( ctxt, " " );
    }
}

static void
save_as_rtf_return_text( gchar *chars, gint length, CharacterParams *params, gpointer data )
{
  RtfContext *ctxt = (RtfContext *) data;
  gchar *charsloc;
  if ( !( ctxt->lastfont && gdk_font_equal( params->font->font, ctxt->lastfont->font ) ) )
    {
      if ( ctxt->lastfont )
	{
	  rtf_output( ctxt, "}" );
	}
      rtf_output( ctxt, "{" );
      if ( params->font->data )
	{
	  GnomeFont *font = params->font->data;
	  if ( font->fontmap_entry->weight_code >= GNOME_FONT_SEMI )
	    rtf_output( ctxt, "\\b " );
	  if ( font->fontmap_entry->italic )
	    rtf_output( ctxt, "\\i " );
	  /*
	  if ( strcmp( font->fontmap_entry->familyname, "Times" ) )
	    xmlSetValue( text, "font_family", font->fontmap_entry->familyname );
	  if ( ((double) font->size ) != 12.0 )
	  xmlSetDoubleValue( text, "size", (double) font->size );*/
	}
    }
  ctxt->lastfont = params->font;

  charsloc = g_malloc0( length + 1 );
  strncpy( charsloc, chars, length );
  charsloc[length] = 0;
  rtf_escaped_output( ctxt, charsloc );
  g_free( charsloc );
}

/* Return value denotes success.  0 = sucess.  -1 = failure. */
static gint save_as_rtf( GOWindow *window, gchar *filename )
{
  gint ret;
  GtkRichTextTraversalCallbacks callbacks;
  RtfContext ctxt;

  
  ctxt.fp = fopen( filename, "w" );
  if ( ctxt.fp )
    {
      ctxt.lastfont = NULL;
      ctxt.started = FALSE;
      
      callbacks.return_text = save_as_rtf_return_text;
      callbacks.start_paragraph = save_as_rtf_start_paragraph;

      rtf_output( &ctxt, "{\\rtf1 \\ansi " );
      
      gtk_rich_text_traverse( GTK_RICH_TEXT( window->textbox ), &callbacks, (gpointer) &ctxt );
      if ( ctxt.lastfont )
	rtf_output( &ctxt, "}" );
      if ( ctxt.started )
	rtf_output( &ctxt, "}" );
      rtf_output( &ctxt, "}" );

      fclose( ctxt.fp );
      
      
      window->changed = FALSE;
      return 0;
    }
  else
    return -1;
}

/* ok_load
 *  Very small wrapper around function open_file to get the
 *  information from the given file selection widget. */
void ok_load( GtkWidget *button, gpointer g_data )
{
  gchar *newtitle;

  selector_window_pair *pair = ( selector_window_pair * ) g_data;
  GtkWidget *widget = pair->selector;

  newtitle =
    g_strdup( gtk_file_selection_get_filename( GTK_FILE_SELECTION( widget ) ) );
  gtk_widget_destroy( widget );

  open_file( newtitle );
  
  g_free( newtitle );
  g_free( pair );
}

void file_new_callback( GtkWidget *widget, gpointer data )
{
  new_file( "Untitled" );
}

/* open_file_with_dimensions
 *  Opens a file in the given position on the screen and inserts it at
 *  the beginning of the Document Index if neccessary. */
GOWindow *open_file_with_dimensions( gchar *title, gint x, gint y, gint width, gint height )
{
  return open_file_with_dimensions_in_position( title, x, y, width, height, 0 );
}

GOWindow *new_file_with_dimensions( gchar *title, gint x, gint y, gint width, gint height )
{
  return new_file_with_dimensions_in_position( title, x, y, width, height, 0 );
}


/* open_file_with_dimensions_in_position
 *  Opens a file in the given position on the screen and with the
 *  given position in the Document Index if it's needed. */
GOWindow *open_file_with_dimensions_in_position( gchar *filename, int x, int y, int width, int height, gint position )
{
  FILE *fp;
  GnomeDisplayFont *def_display_font;
  GtkRichTextFont *def_font;
  
  def_display_font =
    gnome_get_display_font( "Times", GNOME_FONT_BOOK, FALSE, 12.0, 1 );
  if ( def_display_font )
    def_font = gtk_rich_text_font_adopt( def_display_font->gdk_font, def_display_font->x_font_name, def_display_font->gnome_font );
  else
    def_font = gtk_rich_text_font_new( "fixed" );
  
  fp = fopen( filename, "r" );
  if ( fp )
    {
      gchar *data;
      GOWindow *window;

      window = make_window_with_dimensions( x, y, width, height, filename );

      /* Add ourselves to the tree */
      idea_add_in_position( filename, position );

      window->undo_count = 0;
      gtk_rich_text_freeze( GTK_RICH_TEXT( window->textbox ) );
      {
	gint count = 0;
	xmlDoc *doc = xmlParseFile( filename );
	if ( doc && doc->root && doc->root->name
	     && ( ! strcmp( doc->root->name, "Document" ) )
	     && doc->root->ns && doc->root->ns->href
	     && ( ! strcmp( doc->root->ns->href, "http://www.gnome.org/go/" ) )
	     )
	  {
	    xmlNode *paragraph;
	    gint width;
	    xmlGetIntValue( doc->root, "width", &width );
	    xmlGetIntValue( doc->root, "print_page_nums",
			    &(window->props->print_page_nums) );
	    gtk_rich_text_set_width( GTK_RICH_TEXT( window->textbox ), width );
	    for ( paragraph = doc->root->childs;
		  paragraph != NULL;
		  paragraph = paragraph->next )
	      {
		gint length = 0;
		gint tempjust;
		xmlNode *text;
		ParagraphParams params;
		ParagraphParamsValid validity = paragraph_params_none_valid;
		for ( text = paragraph->childs; text != NULL; text = text->next )
		  {
		    gchar *chars = xmlNodeGetContent( text );
		    if ( chars )
		      {
			gchar *family;
			gdouble weight;
			gint size;
			GnomeDisplayFont *display_font;
			GtkRichTextFont *font;
			gint italic;
			family = xmlGetValue( text, "font_family" );
			if ( family == NULL )
			  {
			    family = g_strdup( "Times" );
			  }
			if ( xmlGetDoubleValue( text, "weight", &weight ) == 0 )
			  weight = 0;
			if ( xmlGetIntValue( text, "size", &size ) == 0 )
			  size = 12.0;
			if ( xmlGetIntValue( text, "italic", &italic ) == 0 )
			  italic = 0;
			display_font = gnome_get_display_font( family,
							       weight,
							       italic != 0,
							       size,
							       1 );
			if ( display_font )
			  {
			    font = gtk_rich_text_font_adopt( display_font->gdk_font, display_font->x_font_name, display_font->gnome_font );
			    gtk_rich_text_insert( GTK_RICH_TEXT( window->textbox ),
						  font,
						  0, 0, chars, -1 );
			  }
			else
			  gtk_rich_text_insert( GTK_RICH_TEXT( window->textbox ),
						gtk_rich_text_font_new( xmlGetValue( text, "xfont" )),
						0, 0, chars, -1 );
			length += strlen( chars );
			free( chars );
			g_free( family );
		      }
		  }
		xmlGetIntValue( paragraph, "pre_space", &(params.prespace) );
		xmlGetIntValue( paragraph, "in_space", &(params.inspace) );
		xmlGetIntValue( paragraph, "post_space", &(params.postspace) );
		xmlGetIntValue( paragraph, "first_indent", &(params.first_indent) );
		xmlGetIntValue( paragraph, "left_indent", &(params.left_indent) );
		xmlGetIntValue( paragraph, "right_indent", &(params.right_indent) );
		xmlGetIntValue( paragraph, "justification", &(tempjust) );
		params.justification = (GtkJustification) tempjust;
		validity.prespace_valid = validity.inspace_valid = validity.postspace_valid =
		  validity.left_indent_valid = validity.right_indent_valid =
		  validity.first_indent_valid = validity.justification_valid = 1;
		gtk_rich_text_set_location_paragraph_params( GTK_RICH_TEXT( window->textbox ), count, &params, &validity );
		if ( paragraph->next )
		  {
		    gtk_rich_text_insert( GTK_RICH_TEXT( window->textbox ), def_font, 0, 0, "\n", -1 );
		    length += 1;
		  }
		count += length;
	      }
	    window->format = GO_FILE_FORMAT_GO;
	  }
	else
	  {
	    data = g_malloc0( 1024 );
	    while ( ! feof( fp ) )
	      {
		data[ fread( data, 1, 1023, fp ) ] = 0;
		gtk_rich_text_insert( GTK_RICH_TEXT( window->textbox ), def_font, 0, 0, data, -1 );
	      }
	    g_free( data );
	    window->format = GO_FILE_FORMAT_PLAIN;
	  }
	if ( doc != NULL )
	  xmlFreeDoc( doc );
      }
      gtk_rich_text_thaw( GTK_RICH_TEXT( window->textbox ) );
#if 0
      gtk_text_freeze( GTK_TEXT( window->textbox ) );
      data = g_malloc0( 1024 );
      while ( ! feof( fp ) )
	{
	  data[ fread( data, 1, 1023, fp ) ] = 0;
	  gtk_text_insert( GTK_TEXT( window->textbox ), def_font, 0, 0, data, -1 );
	}
      g_free( data );
      gtk_text_thaw( GTK_TEXT( window->textbox ) );
      window->format = GO_FILE_FORMAT_PLAIN;
#endif
      
      window->named = TRUE;
      window->changed = FALSE;
      fclose( fp );
      
      return window;
    }
  else
    {
      fp = fopen( filename, "a+" );
      if( fp )
	{
	  GOWindow *window;

	  window = make_window_with_dimensions( x, y, width, height, filename );

	  /* Add ourselves to the tree */
	  idea_add_in_position( filename, position );
	  
	  window->named = TRUE;
	  window->changed = FALSE;
	  fclose( fp );

	  return window;
	}
    }
  return NULL;
}

GOWindow *new_file_with_dimensions_in_position( gchar *filename, int x, int y, int width, int height, gint position )
{
  /*  FILE *fp; */

  GOWindow *window;
  
  gchar *home_dir, *dot_file_dir_name;
  gchar number[1024];
  static int incrementer = 0;
#if 0

  home_dir = getenv( "HOME" );
  
  /* create dot directory. */
  dot_file_dir_name = append2( home_dir, FALSE, "/.go/", FALSE  );
  create_directory( dot_file_dir_name );

  filename = append2( dot_file_dir_name, TRUE, filename, FALSE );
#endif

  sprintf( number, "<%d>", incrementer++ );

  filename = append2( filename, FALSE, number, FALSE );
  window = make_window_with_dimensions( x, y, width, height, filename );


  /* Add ourselves to the tree */
  /*  idea_add_in_position( filename, position );*/
  
  window->undo_count = 0;
  
  window->named = FALSE;
  window->changed = FALSE;
  
  return window;
}

/* open_file
 *  Opens the given file in its remembered position and puts it at the
 *  beginning of the Document Index if neccessary. */
GOWindow *open_file( gchar *title )
{
  return open_file_in_position( title, 0 );
}

GOWindow *new_file( gchar *title )
{
  return new_file_in_position( title, 0 );
}

/* open_file
 *  Opens the given file in its remembered position and puts it in the
 *  given position in the Document Index if neccessary. */
GOWindow *open_file_in_position( gchar *title, gint position )
{
  FILE *fp;
  gchar *home_dir;
  gchar *desktopfile;
  int x, y, width, height;
  int sel_start, sel_end;
  float scrollpos;
  
  home_dir = getenv( "HOME" );
  desktopfile = append2( home_dir, FALSE, "/.go/files", FALSE );
  desktopfile = append2( desktopfile, TRUE, title, FALSE );
  fp = fopen( desktopfile, "r" );
  g_free( desktopfile );

  if ( fp )
    {
      GOWindow *window;
      x = getinteger( fp );
      y = getinteger( fp );
      width = getinteger( fp );
      height = getinteger( fp );
      sel_start = getinteger( fp );
      sel_end = getinteger( fp );
      scrollpos = getfloat( fp );
      window = open_file_with_dimensions_in_position( title, x, y, width, height, position );
      if( window )
	{
	  gtk_adjustment_set_value( GTK_ADJUSTMENT( gtk_range_get_adjustment( GTK_RANGE( window->vscrollbar ) ) ), scrollpos + 1.0 );
	  if( sel_start == sel_end )
	    {
	      if( sel_start <= gtk_rich_text_get_length( GTK_RICH_TEXT( window->textbox ) ) )
		gtk_editable_set_position( GTK_EDITABLE( window->textbox ), sel_start );
	    }
	  else
	    {
	      gtk_editable_select_region( GTK_EDITABLE( window->textbox ), sel_start, sel_end );
	    }
	}
      clear_white( fp );
      return window;
    }
  else
    {
      return open_file_with_dimensions_in_position( title, -1, -1, DEF_WIDTH, DEF_HEIGHT, position );
    }
}

GOWindow *new_file_in_position( gchar *title, gint position )
{
  return new_file_with_dimensions_in_position( title, -1, -1, DEF_WIDTH, DEF_HEIGHT, position );
}

/* open_save_dialog
 *  Opens the Save As dialog for the given window. */
void open_save_dialog( GtkWidget *widget, gpointer data )
{
  GtkWidget *save_dialog;
  selector_window_pair *pair;

  pair = g_malloc0( sizeof( selector_window_pair ) );
  pair->selector = gtk_file_selection_new( "Save As" );
  save_dialog = pair->selector;
  pair->window = ( GOWindow * ) data;

  gtk_file_selection_set_filename( GTK_FILE_SELECTION( save_dialog ),
				   pair->window->title );

  /* Connect the ok_button to file_ok_sel function */
  gtk_signal_connect( GTK_OBJECT( GTK_FILE_SELECTION( save_dialog )->ok_button),
		      "clicked",
		      (GtkSignalFunc) ok_save,
		      (gpointer) pair );

  /* Connect the cancel_button to destroy the widget */
  gtk_signal_connect_object( GTK_OBJECT( GTK_FILE_SELECTION( save_dialog )
					 ->cancel_button),
			      "clicked",
			      (GtkSignalFunc) gtk_widget_destroy,
			      GTK_OBJECT( save_dialog ) );

  gtk_widget_show( save_dialog );
}

/* file_open_calback
 *  Handles the callback for Open in the File menu and in the
 *  toolbar. */
void file_open_callback( GtkWidget *widget, gpointer data )
{
  GtkWidget *open_dialog;
  selector_window_pair *pair;

  pair = g_malloc0( sizeof( selector_window_pair ) );
  pair->selector = gtk_file_selection_new( "Open" );
  open_dialog = pair->selector;
  pair->window = (GOWindow *) data;

  /* Connect the ok_button to file_ok_sel function */
  gtk_signal_connect( GTK_OBJECT( GTK_FILE_SELECTION( open_dialog )->ok_button),
		      "clicked",
		      (GtkSignalFunc) ok_load,
		      (gpointer) pair );

  /* Connect the cancel_button to destroy the widget */
  gtk_signal_connect_object( GTK_OBJECT( GTK_FILE_SELECTION( open_dialog )
					 ->cancel_button),
			     "clicked",
			     (GtkSignalFunc) gtk_widget_destroy,
			     GTK_OBJECT( open_dialog ) );

  gtk_widget_show( open_dialog );
}


#if 0
void file_save_callback( GtkWidget *widget, gpointer data )
{
  GOWindow *window = ( GOWindow * ) data;

  if ( window->named )
    {
      gchar *data;
      FILE *fp;

      data = gtk_editable_get_chars( GTK_EDITABLE( window->textbox ), 0, -1 );
      fp = fopen( window->title, "w" );
      if ( fp )
	{
	  fwrite( data, 1, strlen( data ), fp );
	  fclose( fp );
	  g_free( data );
	  window->changed = FALSE;
	}
    }
  else
    {
      open_save_dialog( widget, data );
    }
}
#endif


/* file_save_as_calback
 *  Handles the callback for Save As in the File menu and in the
 *  toolbar. */
void file_save_as_callback( GtkWidget *widget, gpointer data )
{
  open_save_dialog( widget, data );
}



/* ok_save
 *  This function gets called when the user pushes the Save button in
 *  the Save as dialog.  FIXME. */
void ok_save_as_go_ff( GtkWidget *button, gpointer g_data )
{
  gchar *data;
  FILE *fp;
  selector_window_pair *pair = ( selector_window_pair * ) g_data;
  GOWindow *window = pair->window;
  gchar *title = gtk_file_selection_get_filename( GTK_FILE_SELECTION( pair->selector ) );
  /*  gint x, y, width, height, sel_start, sel_end; */
  /*  gfloat scrollpos; */

  if( save_as_go_ff( window, title ) == 0 )
    {
      g_list_foreach( windowlist, remove_window_from_menu, window );
      windowlist = g_list_remove( windowlist, (gpointer) window );

      undo_tree_destroy( window->undo );

      g_free( window->title );
      
      setup_window_stuff( window, title );
      
      windowlist = g_list_append( windowlist, ( gpointer ) window );

      /* Add ourselves to the tree */
      idea_add_in_position( title, 0 );
      
      window->named = TRUE;
      window->changed = FALSE;
      window->format = GO_FILE_FORMAT_GO;
    }
  gtk_widget_destroy( pair->selector );
  g_free( pair );
}

/* open_save_dialog
 *  Opens the Save As dialog for the given window. */
static void open_save_as_ff_dialog( GtkWidget *widget, gpointer data );

void open_save_as_ff_dialog( GtkWidget *widget, gpointer data )
{
  GtkWidget *save_dialog;
  selector_window_pair *pair;

  pair = g_malloc0( sizeof( selector_window_pair ) );
  pair->selector = gtk_file_selection_new( "Save As Go File Format" );
  save_dialog = pair->selector;
  pair->window = ( GOWindow * ) data;

  gtk_file_selection_set_filename( GTK_FILE_SELECTION( save_dialog ),
				   pair->window->title );

  /* Connect the ok_button to file_ok_sel function */
  gtk_signal_connect( GTK_OBJECT( GTK_FILE_SELECTION( save_dialog )->ok_button),
		      "clicked",
		      (GtkSignalFunc) ok_save_as_go_ff,
		      (gpointer) pair );

  /* Connect the cancel_button to destroy the widget */
  gtk_signal_connect_object( GTK_OBJECT( GTK_FILE_SELECTION( save_dialog )
					 ->cancel_button),
			      "clicked",
			      (GtkSignalFunc) gtk_widget_destroy,
			      GTK_OBJECT( save_dialog ) );

  gtk_widget_show( save_dialog );
}
/* file_save_as_calback
 *  Handles the callback for Save As in the File menu and in the
 *  toolbar. */
void file_save_as_ff_callback( GtkWidget *widget, gpointer data )
{
  open_save_as_ff_dialog( widget, data );
}

/* ok_save
 *  This function gets called when the user pushes the Save button in
 *  the Save as dialog.  FIXME. */
void ok_save_as_rtf( GtkWidget *button, gpointer g_data )
{
  gchar *data;
  FILE *fp;
  selector_window_pair *pair = ( selector_window_pair * ) g_data;
  GOWindow *window = pair->window;
  gchar *title = gtk_file_selection_get_filename( GTK_FILE_SELECTION( pair->selector ) );
  /*  gint x, y, width, height, sel_start, sel_end; */
  /*  gfloat scrollpos; */

  if( save_as_rtf( window, title ) == 0 )
    {
      g_list_foreach( windowlist, remove_window_from_menu, window );
      windowlist = g_list_remove( windowlist, (gpointer) window );

      undo_tree_destroy( window->undo );

      g_free( window->title );
      
      setup_window_stuff( window, title );
      
      windowlist = g_list_append( windowlist, ( gpointer ) window );

      /* Add ourselves to the tree */
      idea_add_in_position( title, 0 );
      
      window->named = TRUE;
      window->changed = FALSE;
      window->format = GO_FILE_FORMAT_RTF;
    }
  gtk_widget_destroy( pair->selector );
  g_free( pair );
}

/* open_save_dialog
 *  Opens the Save As dialog for the given window. */
static void open_save_as_rtf_dialog( GtkWidget *widget, gpointer data );

void open_save_as_rtf_dialog( GtkWidget *widget, gpointer data )
{
  GtkWidget *save_dialog;
  selector_window_pair *pair;

  pair = g_malloc0( sizeof( selector_window_pair ) );
  pair->selector = gtk_file_selection_new( "Save As RTF File Format" );
  save_dialog = pair->selector;
  pair->window = ( GOWindow * ) data;

  gtk_file_selection_set_filename( GTK_FILE_SELECTION( save_dialog ),
				   pair->window->title );

  /* Connect the ok_button to file_ok_sel function */
  gtk_signal_connect( GTK_OBJECT( GTK_FILE_SELECTION( save_dialog )->ok_button),
		      "clicked",
		      (GtkSignalFunc) ok_save_as_rtf,
		      (gpointer) pair );

  /* Connect the cancel_button to destroy the widget */
  gtk_signal_connect_object( GTK_OBJECT( GTK_FILE_SELECTION( save_dialog )
					 ->cancel_button),
			      "clicked",
			      (GtkSignalFunc) gtk_widget_destroy,
			      GTK_OBJECT( save_dialog ) );

  gtk_widget_show( save_dialog );
}
/* file_save_as_calback
 *  Handles the callback for Save As in the File menu and in the
 *  toolbar. */
void file_save_as_rtf_callback( GtkWidget *widget, gpointer data )
{
  open_save_as_rtf_dialog( widget, data );
}

/* file_close_calback
 *  Handles the callback for Close in the File menu and in the
 *  toolbar. */
void file_close_callback( GtkWidget *widget, gpointer data )
{
  GOWindow *window = ( GOWindow * ) data;

  if( window->changed )
    save_changes( window );
  
  save_location( window );

  /* This calls destroy_callback.  It used to be that the work in
     destroy_callback was being done twice and this caused the Close
     menu item bug. */
  gtk_widget_destroy( window->app );
}

gboolean delete_event_callback( GtkWidget *widget, GdkEvent *event, gpointer data )
{
  GOWindow *window = ( GOWindow * ) data;

  if( window->changed )
    save_changes( window );
  
  save_location( window );
  
  return FALSE;
}

void destroy_callback( GtkWidget *widget, gpointer data )
{
  window_cleanup( ( GOWindow * ) data );
}

void save_changes_callback( gpointer data, gpointer user_data )
{
  if ( ( (GOWindow *) data )->changed )
    save_changes( (GOWindow *) data );
}

void save_location( GOWindow *window )
{
  FILE *fp;
  gchar *home_dir;
  gchar *desktopfile;
  int x, y, width, height;
  int sel_start, sel_end;
  gchar *chr;

  if( window->named )
    {
  
      home_dir = getenv( "HOME" );
      desktopfile = append2( home_dir, FALSE, "/.go/files", FALSE );
      desktopfile = append2( desktopfile, TRUE, window->title, FALSE );
      *( chr = strrchr( desktopfile, '/' ) ) = 0;
      create_directory( desktopfile );
      *chr = '/';
      fp = fopen( desktopfile, "w" );
      g_free( desktopfile );
      
      if ( fp )
	{
	  gdk_window_get_geometry( window->app->window, &x, &y, &width, &height, NULL );
	  gdk_window_get_origin( window->app->window, &x, &y );
	  sel_start = GTK_EDITABLE( window->textbox )->selection_start_pos;
	  sel_end = GTK_EDITABLE( window->textbox )->selection_end_pos;
	  if ( sel_start == sel_end )
	    {
	      sel_start = sel_end = gtk_editable_get_position( GTK_EDITABLE( window->textbox ) );
	    }
	  
	  fprintf( fp, "%d %d %d %d %d %d %f\n", x, y, width, height,
		   sel_start, sel_end,
		   GTK_ADJUSTMENT( gtk_range_get_adjustment( GTK_RANGE( window->vscrollbar ) ) ) -> value );
	  fclose( fp );
	}
    }
}

void save_location_callback( gpointer data, gpointer user_data )
{
  GOWindow *window = (GOWindow *) data;
  int x, y, width, height;
  int sel_start, sel_end;

  if ( window->named )
    {
      /* window->app->window = the gdk window of the gtk window of my window class. */
      gdk_window_get_geometry( window->app->window, &x, &y, &width, &height, NULL );
      gdk_window_get_origin( window->app->window, &x, &y );
      sel_start = GTK_EDITABLE( window->textbox )->selection_start_pos;
      sel_end = GTK_EDITABLE( window->textbox )->selection_end_pos;
      if ( sel_start == sel_end )
	{
	  sel_start = sel_end = gtk_editable_get_position( GTK_EDITABLE( window->textbox ) );
	}
  
      fprintf( (FILE *) user_data, "%d %d %d %d %d %d %f %d %s\n", x, y, width, height,
	       sel_start, sel_end,
	       GTK_ADJUSTMENT( gtk_range_get_adjustment( GTK_RANGE( window->vscrollbar ) ) ) -> value,
	       strlen( window->title ), window->title );
      save_location( window );
    }
}


#ifdef TEST_WINDOW_LAYERING

void add_reorder_callback( gpointer data, gpointer user_data )
{
  GOWindow *window = (GOWindow *) data;
  list_gdk_window_pair *pair = (list_gdk_window_pair *) user_data;
  GdkWindow *actualwindow = pair->window;
  GList **list = pair->list;

  printf( "%p, %p\n", window->app->window, actualwindow );

  if ( window->app->window == actualwindow )
    *list = g_list_append( *list, window );
}

void reorder_callback( gpointer data, gpointer user_data )
{
  GdkWindow *window = (GdkWindow *) data;
  GList **list = (GList **) user_data;
  list_gdk_window_pair pair;

  printf( "%p\n", data );
  pair.list = list;
  pair.window = window;
  g_list_foreach( windowlist, add_reorder_callback, &pair );
}

void add_children( gpointer data, gpointer user_data )
{
  GList *my_children = gdk_window_get_children( (GdkWindow *) data );
  GList **list = (GList **) user_data;
  
  printf( "%p, %p\n", data, user_data );
  printf( "%p\n", my_children );
  *list = g_list_concat( *list, my_children );
}

#endif


void file_exit_callback( GtkWidget *widget, gpointer data )
{
  FILE *fp;
  gchar *home_dir;
  gchar *desktopfile;
  
#ifdef TEST_WINDOW_LAYERING
  GList *fullwindowlist;
  GList *reallyfullwindowlist = 0;
  GList *newwindowlist = 0;
#endif
  g_list_foreach( windowlist, save_changes_callback, NULL );

  if ( !no_autoload )
    {
      home_dir = getenv( "HOME" );
      desktopfile = append2( home_dir, FALSE, "/.go/desktop", FALSE );
      fp = fopen( desktopfile, "w" );
      g_free( desktopfile );
      
      /* Write out persistant desktop information. */
      if( fp )
	{
	  
#ifdef TEST_WINDOW_LAYERING
	  
	  fullwindowlist = gdk_window_get_children( (GdkWindow *) &gdk_root_parent );
	  fullwindowlist = gdk_window_get_children( (GdkWindow *) ( fullwindowlist->data ) );
	  g_list_foreach( fullwindowlist, add_children, &reallyfullwindowlist );
	  fullwindowlist = reallyfullwindowlist;
	  g_list_foreach( fullwindowlist, reorder_callback, &newwindowlist );
	  windowlist = newwindowlist;
	  
#endif
	  
	  g_list_foreach( windowlist, save_location_callback, fp );
	  fclose( fp );
	}
    }
  save_idea_manager( ideas );
  
  gtk_main_quit();
}

void edit_select_all_callback( GtkWidget *widget, gpointer data )
{
  GOWindow *window = ( GOWindow * ) data;

  gtk_editable_select_region ( GTK_EDITABLE( window->textbox ),
			       0,
			       gtk_rich_text_get_length( GTK_RICH_TEXT( window->textbox ) ) );
}

void edit_copy_callback( GtkWidget *widget, gpointer data )
{
  GOWindow *window = ( GOWindow * ) data;

  gtk_rich_text_set_point( GTK_RICH_TEXT( window->textbox ), gtk_editable_get_position( GTK_EDITABLE( window->textbox ) ) );
  gtk_editable_copy_clipboard( GTK_EDITABLE( window->textbox ) );
}

void edit_cut_callback( GtkWidget *widget, gpointer data )
{
  GOWindow *window = ( GOWindow * ) data;

  gtk_rich_text_set_point( GTK_RICH_TEXT( window->textbox ), gtk_editable_get_position( GTK_EDITABLE( window->textbox ) ) );
  gtk_editable_cut_clipboard( GTK_EDITABLE( window->textbox ) );
}

void edit_paste_callback( GtkWidget *widget, gpointer data )
{
  GOWindow *window = ( GOWindow * ) data;

  gtk_rich_text_set_point( GTK_RICH_TEXT( window->textbox ), gtk_editable_get_position( GTK_EDITABLE( window->textbox ) ) );
  gtk_editable_paste_clipboard( GTK_EDITABLE( window->textbox ) );
}

void edit_clear_callback( GtkWidget *widget, gpointer data )
{
  GOWindow *window = ( GOWindow * ) data;

  gint sel_start = GTK_EDITABLE( window->textbox )->selection_start_pos;
  gint sel_end = GTK_EDITABLE( window->textbox )->selection_end_pos;

  if ( sel_start != sel_end )
  {
    gtk_editable_delete_selection( GTK_EDITABLE( window->textbox ) );
  }
}

gint auto_save_callback( gpointer data )
{
  GOWindow *window = (GOWindow *) data;

  window->auto_save = 0;
  save_changes( window );

  return FALSE;
}


gint long_auto_save_callback( gpointer data )
{
  GOWindow *window = (GOWindow *) data;

  window->long_term_auto_save = 0;
  save_changes( window );

  return FALSE;
}

gboolean create_directory( gchar *directory )
{
  gchar *full_name;
  gchar *position;
  gchar *current_dir = g_get_current_dir();
  struct stat info;
  gboolean return_value = TRUE;
  
  if( directory[0] == '/' )
    {
      full_name = g_malloc0( strlen( directory ) + 1 );
      strcpy( full_name, directory );
    }
  else
    {
      full_name = g_malloc0( strlen( directory ) + strlen( current_dir ) + 2 );
      sprintf( full_name, "%s/%s", current_dir, directory );
    }

  if( ( position = strrchr( full_name, '/' ) ) == full_name )
    {
      if ( stat( full_name, &info ) )
	{
	  switch ( errno )
	    {
	    case ENOENT:
	      if ( mkdir( full_name, 0777 ) )
		{
		  switch( errno )
		    {
		    default:
		      return_value = FALSE;
		      break;
		    }
		}
	      break;
	    default:
	      return_value = FALSE;
	      break;
	    }
	}
    }
  else
    {
      *position = 0;
      create_directory( full_name );
      *position = '/';
      if ( stat( full_name, &info ) )
	{
	  switch ( errno )
	    {
	    case ENOENT:
	      if ( mkdir( full_name, 0777 ) )
		{
		  switch( errno )
		    {
		    default:
		      return_value = FALSE;
		      break;
		    }
		}
	      break;
	    default:
	      return_value = FALSE;
	      break;
	    }
	}
    }

  g_free( current_dir );
  g_free( full_name );
  
  return( return_value );
}
