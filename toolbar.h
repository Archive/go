/* file.h - Header file for the toolbars.
 *
 * Copyright (C) 1998 Chris Lahey.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __TOOLBAR_H__
#define __TOOLBAR_H__
#include <gtk/gtk.h>

#include "window.h"
#include "docindex.h"

GtkWidget *create_toolbar( GOWindow *window );
void create_gnome_toolbar( GOWindow *window );
void create_gnome_docindex_toolbar( idea_manager *ideas );
GtkWidget *create_idea_toolbar( void );

#endif
