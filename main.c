/* main.c - entry point of execution for Go.
 *
 * Copyright (C) 1998 Chris Lahey.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */



#include "config.h"

#include <gnome.h>

#include <gtk/gtk.h>
#include <gdk/gdkprivate.h>

#define __USE_GNU 1

#include <unistd.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>

#include "docindex.h"
#include "main.h"
#include "menu.h"
#include "file.h"
#include "window.h"
#include "util.h"
#include "plugin.h"
#include "go_plugin_api.h"
#include "preferences.h"
#include "sig.h"
#include "undo_if.h"

#include "gtkrichtext.h"

#include <libgnorba/gnorba.h>
/*#include <bonobo/gnome-bonobo.h>*/

CORBA_Environment ev;
CORBA_ORB orb;

extern GList *windowlist;

gboolean no_autoload=0;
GSList *launch_plugins = NULL;
void setup_callbacks( plugin_callback_struct *callbacks );

static void
add_launch_plugin(poptContext ctx,
		  enum poptCallbackReason reason,
		  const struct poptOption *opt,
		  const char *arg, void *data)
{
  if(opt->shortName == 'p') {
    launch_plugins = g_slist_append(launch_plugins, (char *) arg);
  } /* else something's weird :) */
}

static void
launch_plugin(char *name)
{
  GString *fullname;
  plugin_callback_struct callbacks;
  plugin *plug;

  fullname = g_string_new(NULL);
  g_string_sprintf( fullname, "%s/%s%s", PLUGINDIR, name, "-plugin" );
	      
  plug = plugin_new( fullname->str );

  setup_callbacks( &callbacks );
	      
  plugin_register( plug, &callbacks, 0 );

  g_string_free( fullname, TRUE );
}

static const struct poptOption options[] = {
    { "no-autoload", 'n', POPT_ARG_NONE, &no_autoload, 0,
      N_("Don't use persistent desktop"), NULL },
      /* We want to use a callback for this launch-plugin thing so we
         can get multiple opts */
    { NULL, '\0', POPT_ARG_CALLBACK, &add_launch_plugin, 0 },
    { "launch-plugin", 'p', POPT_ARG_STRING, NULL, 0,
      N_("Launch a plugin at startup"), N_("PLUGIN-NAME") },
    { NULL, '\0', 0, NULL, 0 }
};

void setup_callbacks( plugin_callback_struct *callbacks )
{
  callbacks->document.create = go_plugin_document_create;
  callbacks->document.open = go_plugin_document_open;
  callbacks->document.close = go_plugin_document_close;
  callbacks->text.append = go_plugin_text_append;
  callbacks->text.insert = go_plugin_text_insert;
  callbacks->document.show = go_plugin_document_show;
  callbacks->document.current = go_plugin_document_current;
  callbacks->document.filename = go_plugin_document_filename;
  callbacks->text.get = go_plugin_text_get;
  callbacks->text.get_selected_text = go_plugin_text_get_selection_text;
  callbacks->text.set_selected_text = go_plugin_text_set_selection_text;
  callbacks->document.get_position = go_plugin_document_get_position;
  callbacks->document.get_selection = go_plugin_document_get_selection_range;
  callbacks->document.set_selection = go_plugin_document_set_selection_range;
  callbacks->program.reg = go_plugin_program_register;
  callbacks->program.quit = go_plugin_program_quit;
}

static void
open_file_from_desktop_file( FILE *fp )
{
  gint x, y, width, height, sel_start, sel_end, length;
  gfloat scrollpos;
  gchar *title;
  GOWindow *window;
  
  x = getinteger( fp );
  y = getinteger( fp );
  width = getinteger( fp );
  height = getinteger( fp );
  sel_start = getinteger( fp );
  sel_end = getinteger( fp );
  scrollpos = getfloat( fp );
  length = getinteger( fp );
  title = g_malloc0( length + 1 );
  
  title[fread( title, 1, length, fp )] = 0;
  window = open_file_with_dimensions( title, x, y, width, height );
  if( window )
    {
      gtk_adjustment_set_value
	( GTK_ADJUSTMENT( gtk_range_get_adjustment( GTK_RANGE( window->vscrollbar ) ) ),
	  MIN( GTK_ADJUSTMENT( gtk_range_get_adjustment( GTK_RANGE( window->vscrollbar ) ) )->upper,
	       scrollpos + 1.0 ) );
      sel_start = MIN( gtk_rich_text_get_length( GTK_RICH_TEXT( window->textbox ) ), sel_start );
      sel_end = MIN( gtk_rich_text_get_length( GTK_RICH_TEXT( window->textbox ) ), sel_end );
      sel_start = MAX( 0, sel_start );
      sel_end = MAX( 0, sel_end );
      if( sel_start == sel_end )
	{
	  gtk_editable_set_position( GTK_EDITABLE( window->textbox ), sel_start );
	}
      else
	{
	  gtk_editable_select_region( GTK_EDITABLE( window->textbox ), sel_start, sel_end );
	}
      window->named = TRUE;
      window->changed = FALSE;
    }
  g_free( title );
  clear_white( fp );
}

int main( int argc, char *argv[] )
{
  int i;
  FILE *fp;
  gchar *current_dir;
  gchar *home_dir;
  gchar *desktopfile;
  gchar *dot_file_dir_name;
  plugin_callback_struct callbacks;
#ifndef NOT_GNOME
  char **args;
  poptContext ctx;
#endif

#ifdef NOT_GNOME
  gtk_init( &argc, &argv );

#else
  bindtextdomain (PACKAGE, GNOMELOCALEDIR);
  textdomain (PACKAGE);

  go_init_undo();

  gnome_CORBA_init_with_popt_table( "Go", VERSION, &argc, argv, options, 0, &ctx, 0, &ev);
  orb = gnome_CORBA_ORB ();

  /*  if (bonobo_init (orb, NULL, NULL) == FALSE)*/
  /*g_error (_("Can not bonobo_init\n"));*/

  g_slist_foreach(launch_plugins, (GFunc) launch_plugin, NULL);
  g_slist_free(launch_plugins);

  args = poptGetArgs(ctx);

  for(i = 0; args && args[i]; i++) {
      if ( args[i][0] == '/' )
	{
	  open_file( args[i] );
	}
      else
	{
	  gchar *current_dir = g_get_current_dir();	
	  gchar *fullname = append2( current_dir, TRUE, "/", FALSE );
	  fullname = append2( fullname, TRUE, args[i], FALSE );
	  open_file( fullname );
	  g_free( fullname );
	}
  }

  poptFreeContext(ctx);

#endif

#if 0
  if ( ! gdk_threads_init() )
    {
      fprintf( stderr, "Could not initialize threads\n" );
      exit( 1 );
    }

  setup_signals();

  gdk_threads_enter();
#endif
  
  windowlist = 0;

  home_dir = getenv( "HOME" );
  
  /* create dot directory. */
  dot_file_dir_name = append2( home_dir, FALSE, "/.go/", FALSE  );
  create_directory( dot_file_dir_name );
  g_free( dot_file_dir_name );

  go_init_preferences();

  make_idea_window( -1, -1 );

  /*  current_dir = getenv( "PWD" );
      printf( "\n%s", current_dir );*/
  /*  if ( ! current_dir )*/
  current_dir = g_get_current_dir();
#ifdef NOT_GNOME
  /* Open all the files listed on the command line. */
  for ( i = 1; i < argc; i++ )
    {
      if( argv[i][0] == '-' && argv[i][1] == '-' )
	{
	  if( ! strcmp( argv[i], "--launch-plugin" ) )
	    {
	      GString *fullname;
	      plugin_callback_struct callbacks;
	      plugin *plug;
	      
	      i++;
	      fullname = g_string_new( NULL );
	      g_string_sprintf( fullname, "%s/%s%s",
				PLUGINDIR, argv[i], "-plugin" );
	      
	      i++;
	      plug =
		plugin_new_with_param( fullname->str, argc - i, argv + i );

	      setup_callbacks( &callbacks );
	      
	      plugin_register( plug, &callbacks, 0 );

	      g_string_free( fullname, TRUE );
	      break;
	    }
	  if( ! strcmp( argv[i], "--no-autoload" ) )
	    no_autoload = TRUE;
	}
      else if( argv[i][0] == '/' )
	{
	  open_file( argv[i] );
	}
      else
	{
	  gchar *fullname = append2( current_dir, FALSE, "/", FALSE );
	  fullname = append2( fullname, TRUE, argv[i], FALSE );
	  open_file( fullname );
	  g_free( fullname );
	}
    }
#endif
  if ( !no_autoload )
    {
      /* open persistant desktop file. */
      desktopfile = append2( home_dir, FALSE, "/.go/desktop", FALSE );
      fp = fopen( desktopfile, "r" );
      g_free( desktopfile );
      
      /* Read in persistant desktop information. */
      if ( fp )
	{
	  while ( ! feof( fp ) )
	    {
	      open_file_from_desktop_file( fp );
	    }
	  fclose( fp );
	}
    }
  /* Open an empty window if no arguments were passed or if no files
     could be opened. */
  if ( windowlist == 0 && !no_autoload )
    {
      new_file( "Untitled" );
    }

  gdk_window_raise( ideas->window->window );

  /*  if ( ! getenv( "PWD" ) )*/
  g_free( current_dir );

  setup_callbacks( &callbacks );
  plugin_query_all( &callbacks );

  gtk_main();
  
#if 0  
  gdk_threads_leave();
#endif

  CORBA_exception_free (&ev);

 /* Not reached. */
  return 0;
}

#define ABOUT_LINES ( sizeof( about_data ) / sizeof( char * ) )

static char *about_data[] =
{
  "  Welcome to GO.  ",
  "  Version " VERSION "  ",
  "  We think this stands for GTK Office.  ",
  "  If you have a better idea for the name, please suggest it.  ",
  "  Copyright 1998,  ",
  "  Christopher James Lahey <clahey@umich.edu>  "
};

void about_callback( GtkWidget *widget, gpointer data )
{
#ifdef NOTGNOME
  GtkWidget *dialog = gtk_dialog_new();
  GtkWidget *button = gtk_button_new_with_label( "OK" );
  GtkWidget *label;
  GtkWidget *wpixmap;
  GdkPixmap *pixmap;
  GdkBitmap *mask;
  int i;

  gtk_box_pack_start( GTK_BOX( GTK_DIALOG( dialog )->action_area ), button, TRUE, TRUE, 10 );

  gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
			     GTK_SIGNAL_FUNC (gtk_widget_destroy),
			     GTK_OBJECT (dialog));
  
  pixmap = gdk_pixmap_create_from_xpm( (GdkWindow *)&gdk_root_parent, &mask,
				       &button->style->bg[GTK_STATE_NORMAL],
				       DATADIR "/go/go.xpm" );
  wpixmap = gtk_pixmap_new( pixmap, mask );
  gtk_box_pack_start( GTK_BOX( GTK_DIALOG( dialog )->vbox ), wpixmap, FALSE, FALSE, 0 );
  
  for( i = 0; i < ABOUT_LINES - 2; i++ )
    {
      label = gtk_label_new( about_data[i] );
      gtk_box_pack_start( GTK_BOX( GTK_DIALOG( dialog )->vbox ), label, FALSE, FALSE, 0 );
    }
  
  for( ; i < ABOUT_LINES; i++ )
    {
      label = gtk_label_new( about_data[i] );
      gtk_box_pack_start( GTK_BOX( GTK_DIALOG( dialog )->vbox ), label, FALSE, FALSE, 0 );
    }
  gtk_widget_show_all( dialog );
#else
  
  const gchar *authors[] =
  {
    "Christopher James Lahey <clahey@umich.edu>",
    NULL
  };
     
  GtkWidget *about =
    gnome_about_new ( _("Go"), VERSION,
		      _("Copyright (C) 1998, Christopher James Lahey"),
		      authors,
		      _( "We used think that go stood for GTK Office.\n"
			 "It's just a word processor now though.\n"
			 "If you have a better acronym for the name, "
			 "please suggest it." ),
		      DATADIR "go/go.xpm");
  gtk_widget_show (about);                                            
#endif

}

void start_plugin( GtkWidget *widget, gpointer data )
{
  plugin_callback_struct callbacks;
  int_gchar_pair *pair = (int_gchar_pair *) data;
  plugin *plug = plugin_new( pair->title );

  setup_callbacks( &callbacks );
  
  plugin_register( plug, &callbacks, pair->x );
}
