#include "go_bonobo.h"
#include "window.h"
#include <gnome.h>

void insert_test( GtkWidget *widget, gpointer data )
{
  GOWindow *window = data;
  GtkWidget *button = gtk_button_new_with_label("Hello!");
  embed_widget_at_window_location( window, button, 100, 100, 50, 20 );
  gtk_widget_show( button );
}
