/* preferences.c - source file for preferences system for go.
 *
 * Copyright (C) 1998 Chris Lahey.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include "config.h"
#include <gnome.h>
#include "preferences.h"

prefs *preferences;

void
go_init_preferences()
{
  preferences = g_malloc0( sizeof( prefs ) );
  preferences->quit_on_idea_close = FALSE;
  preferences->reopen_idea_on_all_close = TRUE;
  preferences->idea_bring_to_top = TRUE;
}

typedef struct
{
  gint placeholder;
} prefs_dialog_data;

static void
delete_prefs_dialog_data( GtkWidget *widget, gpointer data )
{
  prefs_dialog_data *prefs_data = data;
  g_free( prefs_data );
}

static void
prefs_dialog_clicked( GtkWidget *widget, gint button, gpointer data )
{
  GnomeDialog *dialog = GNOME_DIALOG( widget );
  prefs_dialog_data *prefs_data = data;

  if ( button == 0 )
    {
    }
  
  gnome_dialog_close( dialog );
}

/* Here widget should be the menu item and data should be the GOWindow
   it was invoked on, but it doesn't seem like that should be needed
   for program wide preferences. */
void
open_prefs_dialog( GtkWidget *widget, gpointer data )
{
  GnomeDialog *dialog = GNOME_DIALOG ( gnome_dialog_new( _( "Set preferences" ),
							 GNOME_STOCK_BUTTON_OK,
							 GNOME_STOCK_BUTTON_CANCEL,
							 NULL ) );
  prefs_dialog_data *prefs_data = g_new( prefs_dialog_data, 1 );
  prefs_data->placeholder = 0;
  
  gtk_signal_connect( GTK_OBJECT( dialog ),
		      "clicked",
		      (GtkSignalFunc) prefs_dialog_clicked,
		      prefs_data );
  gtk_signal_connect( GTK_OBJECT( dialog ),
		      "destroy",
		      (GtkSignalFunc) delete_prefs_dialog_data,
		      prefs_data );
  /*  gnome_dialog_set_parent( dialog, GTK_WINDOW( window->app ) );  */
  gnome_dialog_set_default( dialog, 0 );

  gtk_widget_show_all( GTK_WIDGET( dialog ) );
}
