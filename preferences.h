/* preferences.h - header file for preferences system for go.
 *
 * Copyright (C) 1998 Chris Lahey.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __PREFERENCES_H__
#define __PREFERENCES_H__

#include <glib.h>

typedef struct
{
  gboolean quit_on_idea_close;
  gboolean reopen_idea_on_all_close;
  gboolean idea_bring_to_top;
} prefs;

extern prefs *preferences;

void go_init_preferences( void );
void open_prefs_dialog( GtkWidget *widget, gpointer data );

#endif
