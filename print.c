/* print.c - Prints stuff.
 *
 * Copyright (C) 1998 Chris Lahey.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#include "config.h"
#include <gtk/gtk.h>
#include <gnome.h>
#include "gtkrichtext.h"
#include <libgnomeprint/gnome-print.h>
#include <libgnomeprint/gnome-printer.h>
#include <libgnomeprint/gnome-font.h>
#include <libgnomeprint/gnome-printer-dialog.h>

#include "main.h"
#include "window.h"
#include "print.h"

typedef struct
{
  GOWindow *window;
  GnomePrintContext *ctxt;
  GnomeFont *font;
  gdouble lmargin;
  gdouble rmargin;
  gdouble tmargin;
  gdouble bmargin;
  gint page_no;
}
GoPrintContext;


static gdouble go_char_width ( GtkRichTextRenderSystem *system, GtkRichTextFont *font, gchar character );
static gdouble go_text_width ( GtkRichTextRenderSystem *system, GtkRichTextFont *font, gchar *chars, gint length );
static gdouble go_ascent ( GtkRichTextRenderSystem *system, GtkRichTextFont *font ); 
static gdouble go_descent ( GtkRichTextRenderSystem *system, GtkRichTextFont *font ); 
static void go_draw_text ( GtkRichTextRenderSystem *system, GtkRichTextFont *font, gchar *chars, gint length );
static void go_output_page ( GtkRichTextRenderSystem *system );

static void go_print( GOWindow *window, GnomePrinter *printer );

static gdouble
go_print_char_width ( GtkRichTextRenderSystem *system, GtkRichTextFont *font, gchar character )
{
  if ( font->data )
    return gnome_font_get_width( font->data, character );
  else
    return gnome_font_get_width( ((GoPrintContext *)system->data)->font, character );
}

static gdouble
go_print_text_width ( GtkRichTextRenderSystem *system, GtkRichTextFont *font, gchar *chars, gint length )
{
  gdouble return_val;
  gchar nextchar;

  nextchar = chars[ length ];
  if ( nextchar != 0 )
    chars[ length ] = 0;
  
  if ( font->data )
    return_val = gnome_font_get_width_string( font->data, chars );
  else
    return_val = gnome_font_get_width_string( ((GoPrintContext *)system->data)->font, chars );

  if ( nextchar != 0 )
    chars[ length ] = nextchar;
  return return_val;
}

static gdouble
go_print_ascent ( GtkRichTextRenderSystem *system, GtkRichTextFont *font )
{
  if ( font->data )
    return ((GnomeFont *)font->data)->size * 2.0 / 3.0;
  else
    return ((GnomeFont *) ((GoPrintContext *)system->data)->font)->size * 2.0 / 3.0;
}

static gdouble
go_print_descent ( GtkRichTextRenderSystem *system, GtkRichTextFont *font )
{
  if ( font->data )
    return ((GnomeFont *)font->data)->size * 1.0 / 3.0;
  else
    return ((GnomeFont *) ((GoPrintContext *)system->data)->font)->size * 1.0 / 3.0;
}

static void
go_print_draw_text ( GtkRichTextRenderSystem *system, GtkRichTextFont *font, gchar *chars, gint length )
{
  GoPrintContext *ctxt = system->data;
  GnomeFont *gnome_font;
  gchar nextchar;

  nextchar = chars[ length ];
  if ( nextchar != 0 )
    chars[ length ] = 0;
  
  if ( font->data )
    {
      gnome_font = font->data;
    }
  else
    {
      gnome_font = ctxt->font;
    }
  gnome_print_setfont( ctxt->ctxt, gnome_font );
  /* system->height doesn't include the margins, so add bmargin to the
     height to get the top of the page. */
  gnome_print_moveto( ctxt->ctxt,
		      system->x + ctxt->lmargin,
		      system->height + ctxt->bmargin - system->y );
  gnome_print_setrgbcolor( ctxt->ctxt, 0, 0, 0 );
  gnome_print_show( ctxt->ctxt, chars );

  system->x += gnome_font_get_width_string( gnome_font, chars );

  if ( nextchar != 0 )
    chars[ length ] = nextchar;
}

static void
go_print_output_page ( GtkRichTextRenderSystem *system )
{
  GoPrintContext *ctxt = system->data;
  GnomeFont *gnome_font;
  gchar *chars;
  if ( ctxt->window->props->print_page_nums )
    {
      chars = g_strdup_printf( "%d", (ctxt->page_no)++ );

      gnome_font = ctxt->font;
      gnome_print_setfont( ctxt->ctxt, gnome_font );
      /* system->height doesn't include the margins, so add bmargin to the
	 height to get the top of the page. */
      gnome_print_moveto( ctxt->ctxt,
			  ( system->width - gnome_font_get_width_string( gnome_font, chars ) ) / 2 + ctxt->lmargin,
			  ctxt->bmargin - 24 );
      gnome_print_setrgbcolor( ctxt->ctxt, 0, 0, 0 );
      gnome_print_show( ctxt->ctxt, chars );
      g_free( chars );
    }
  gnome_print_showpage( ctxt->ctxt );
}

static void
go_print( GOWindow *window, GnomePrinter *printer )
{
  GtkRichTextRenderSystem system;
  GoPrintContext ctxt;

  system.char_width = go_print_char_width;
  system.text_width = go_print_text_width;
  system.ascent = go_print_ascent;
  system.descent = go_print_descent;
  system.draw_text = go_print_draw_text;
  system.output_page = go_print_output_page;
  system.x = 0.0;
  system.y = 0.0;
  system.uses_cursor = FALSE;
  system.data = &ctxt;

  gnome_config_push_prefix( "/Go/Page Size/");
  ctxt.lmargin = gnome_config_get_float_with_default( "lmargin=72.0", NULL );
  ctxt.rmargin = gnome_config_get_float_with_default( "rmargin=72.0", NULL );
  ctxt.tmargin = gnome_config_get_float_with_default( "tmargin=72.0", NULL );
  ctxt.bmargin = gnome_config_get_float_with_default( "bmargin=72.0", NULL );
  system.width = gnome_config_get_float_with_default( "width=612.0", NULL ) - ctxt.lmargin - ctxt.rmargin;
  system.height = gnome_config_get_float_with_default( "height=792.0", NULL ) - ctxt.tmargin - ctxt.bmargin;
  gnome_config_pop_prefix();

  ctxt.page_no = 1;

  if ( system.width != 0.0 && system.height != 0.0 )
    {
      ctxt.ctxt = gnome_print_context_new( printer );
      
      ctxt.font = gnome_font_new_closest ("Times", GNOME_FONT_BOOK, 0, 12);

      gtk_rich_text_render( GTK_RICH_TEXT( window->textbox ), &system );

      gnome_print_context_close( ctxt.ctxt );
    }
}

static void
print_clicked( GtkWidget *widget, gint button, gpointer data )
{
  if ( button == 0 )
    {
      GnomePrinter *printer;
      GnomePrinterDialog *dialog = GNOME_PRINTER_DIALOG( widget );
      GOWindow *window = (GOWindow *) data;
      
      printer = gnome_printer_dialog_get_printer( dialog );
      if ( printer )
	go_print( window, printer );
    }
  
  gnome_dialog_close( GNOME_DIALOG( widget ) );
}

void
file_print_callback( GtkWidget *widget, gpointer data )
{
  GOWindow *window = data;
  GtkWidget *dialog;
  dialog = gnome_printer_dialog_new ();
  
  gnome_dialog_set_parent( GNOME_DIALOG( dialog ), GTK_WINDOW( window->app ) );
  
  gtk_signal_connect( GTK_OBJECT( dialog ),
		      "clicked",
		      (GtkSignalFunc) print_clicked,
		      data );

  gtk_widget_show_all( dialog );
}

static void
page_setup_clicked( GtkWidget *widget, gint button, gpointer data )
{
  if ( button == 0 )
    {
      GnomePaperSelector *paper = GNOME_PAPER_SELECTOR( data );
      gnome_config_push_prefix( "/Go/Page Size/");
      gnome_config_set_string( "name", gnome_paper_selector_get_name( paper ) );
      gnome_config_set_float( "width", gnome_paper_selector_get_width( paper ) );
      gnome_config_set_float( "height", gnome_paper_selector_get_height( paper ) );
      gnome_config_set_float( "lmargin", gnome_paper_selector_get_left_margin( paper ) );
      gnome_config_set_float( "rmargin", gnome_paper_selector_get_right_margin( paper ) );
      gnome_config_set_float( "tmargin", gnome_paper_selector_get_top_margin( paper ) );
      gnome_config_set_float( "bmargin", gnome_paper_selector_get_bottom_margin( paper ) );
      gnome_config_pop_prefix();
      gnome_config_sync();
    }
  gnome_dialog_close( GNOME_DIALOG( widget ) );
}

void
file_page_setup_callback( GtkWidget *widget, gpointer data )
{
  GtkWidget *dialog;
  GtkWidget *papersel;
  GOWindow *window;
  gchar *papername;
  gint width, height;
  gboolean def;

  window = (GOWindow *) data;

  dialog = gnome_dialog_new( _( "Page Setup" ),
			     GNOME_STOCK_BUTTON_OK,
			     GNOME_STOCK_BUTTON_CANCEL,
			     NULL );

  papersel = gnome_paper_selector_new();

  gtk_box_pack_start( GTK_BOX( GNOME_DIALOG( dialog )->vbox ), papersel, TRUE, TRUE, 0 );
  
  gnome_dialog_set_parent( GNOME_DIALOG( dialog ), GTK_WINDOW( window->app ) );
  gnome_dialog_set_default( GNOME_DIALOG( dialog ), 0 );
  
  gtk_signal_connect( GTK_OBJECT( dialog ),
		      "clicked",
		      (GtkSignalFunc) page_setup_clicked,
		      papersel );

  gnome_config_push_prefix( "/Go/Page Size/");
  
  papername = gnome_config_get_string_with_default( "name", &def );
  if ( ! def )
      gnome_paper_selector_set_name( GNOME_PAPER_SELECTOR( papersel ), papername );
  g_free( papername );
  
  width = gnome_config_get_float_with_default( "width=612.0", &def );
  if ( ! def )
    gnome_paper_selector_set_width( GNOME_PAPER_SELECTOR( papersel ), width );
  
  height = gnome_config_get_float_with_default( "height=792.0", &def );
  if ( ! def )
    gnome_paper_selector_set_height( GNOME_PAPER_SELECTOR( papersel ), height );
      
  gnome_config_pop_prefix();

  
  gtk_widget_show_all( dialog );
}
