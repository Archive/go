/* properties.h - properties for a specific document.
 *
 * Copyright (C) 1998 Chris Lahey.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __PROPERTIES_H__
#define __PROPERTIES_H__

#include <glib.h>
#include "go_types.h"

GoProperties *go_properties_new( void );
void go_properties_destroy( GoProperties *props );
GoProperties *go_properties_clone( GoProperties *props );
void open_props_dialog( GtkWidget *widget, gpointer data );

#endif
