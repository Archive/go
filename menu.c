/* menu.c - Creates the menus for GO Office.
 *
 * Copyright (C) 1998 Chris Lahey.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include "config.h"
#include <gtk/gtk.h>

#include "main.h"
#include "menu.h"
#include "file.h"
#include "window.h"
#include "undo_if.h"
#include "search.h"
#include "docindex.h"
#include "font.h"
#include "print.h"
#include "preferences.h"
#include "properties.h"
#include "go_bonobo.h"

#ifndef NOT_GNOME

GnomeUIInfo filemenu[] =
{
  GNOMEUIINFO_MENU_NEW_ITEM(N_("_New"), N_("Create a new document"),
			    file_new_callback, NULL),

  GNOMEUIINFO_MENU_OPEN_ITEM(file_open_callback, NULL),

  GNOMEUIINFO_MENU_SAVE_AS_ITEM(file_save_as_callback, NULL),

  { GNOME_APP_UI_ITEM, N_("Save As Go Document..."), N_("Save the current document in go format"), (gpointer) file_save_as_ff_callback, NULL, NULL, GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_SAVE_AS, 0, 0, NULL },

  { GNOME_APP_UI_ITEM, N_("Save As RTF Document..."), N_("Save the current document in rtf format"), (gpointer) file_save_as_rtf_callback, NULL, NULL, GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_SAVE_AS, 0, 0, NULL },

  GNOMEUIINFO_SEPARATOR,

  GNOMEUIINFO_MENU_PRINT_ITEM(file_print_callback, NULL),

  GNOMEUIINFO_MENU_PRINT_SETUP_ITEM(file_page_setup_callback, NULL),

  GNOMEUIINFO_SEPARATOR,

  GNOMEUIINFO_MENU_CLOSE_ITEM(file_close_callback, NULL),

  GNOMEUIINFO_MENU_EXIT_ITEM(file_exit_callback, NULL),

  GNOMEUIINFO_END
};

GnomeUIInfo editmenu[] =
{

  GNOMEUIINFO_MENU_UNDO_ITEM(undo_callback, NULL),

  GNOMEUIINFO_MENU_REDO_ITEM(redo_callback, NULL),

  GNOMEUIINFO_SEPARATOR,

  GNOMEUIINFO_MENU_CUT_ITEM(edit_cut_callback, NULL),

  GNOMEUIINFO_MENU_COPY_ITEM(edit_copy_callback, NULL),

  GNOMEUIINFO_MENU_PASTE_ITEM(edit_paste_callback, NULL),

  GNOMEUIINFO_MENU_CLEAR_ITEM(edit_clear_callback, NULL),

  GNOMEUIINFO_MENU_SELECT_ALL_ITEM(edit_select_all_callback, NULL),
  
  GNOMEUIINFO_SEPARATOR,

  { GNOME_APP_UI_ITEM, N_("_Search..."), NULL, (gpointer) search_search_callback, NULL },
  { GNOME_APP_UI_ITEM, N_("Search And _Replace..."), NULL, (gpointer) search_search_replace_callback, NULL },
  { GNOME_APP_UI_ITEM, N_("Regular _Expression Search..."), NULL, (gpointer) search_regexp_callback, NULL },
  { GNOME_APP_UI_ITEM, N_("Regular Expression Search And Replace..."), NULL, (gpointer) search_regexp_replace_callback, NULL },
  
  GNOMEUIINFO_END
};

GnomeUIInfo insertmenu[] =
{
  { GNOME_APP_UI_ITEM, N_("Insert _text"), N_("Insert a test text object"), (gpointer) insert_test, NULL, NULL, GNOME_APP_PIXMAP_NONE, 0, 0, 0, NULL },
  GNOMEUIINFO_END
};

GnomeUIInfo formatmenu[] =
{
  { GNOME_APP_UI_ITEM, N_("Set _Font"), N_("Select the font to be used"), (gpointer) open_font_dialog, NULL, NULL, GNOME_APP_PIXMAP_NONE, 0, 0, 0, NULL },
  { GNOME_APP_UI_ITEM, N_("Format _Paragraph"), N_("Set pre, in, and post spacing for selected paragraphs"), (gpointer) open_par_format_dialog, NULL, NULL, GNOME_APP_PIXMAP_NONE, 0, 0, 0, NULL },
  GNOMEUIINFO_MENU_PROPERTIES_ITEM( open_props_dialog, NULL ),
  GNOMEUIINFO_END
};

GnomeUIInfo windowmenu[] =
{
  GNOMEUIINFO_SEPARATOR,
  { GNOME_APP_UI_ITEM, N_("_Document Index"), N_("Raise the Document Index"), (gpointer) raise_idea_callback, NULL, NULL, GNOME_APP_PIXMAP_NONE, 0, 'I', GDK_CONTROL_MASK, NULL },
  GNOMEUIINFO_END
};

GnomeUIInfo pluginsmenu[] =
{
  GNOMEUIINFO_END
};

GnomeUIInfo settingsmenu[] =
{
  GNOMEUIINFO_MENU_PREFERENCES_ITEM( open_prefs_dialog, NULL ),
  GNOMEUIINFO_END
};

GnomeUIInfo helpmenu[] =
{
#if 0
  GNOMEUIINFO_HELP("help-browser"),
  GNOMEUIINFO_SEPARATOR,
#endif
  GNOMEUIINFO_MENU_ABOUT_ITEM(about_callback, NULL), 
  GNOMEUIINFO_END
};

GnomeUIInfo mainmenu[] = {
  GNOMEUIINFO_MENU_FILE_TREE(filemenu),
  GNOMEUIINFO_MENU_EDIT_TREE(editmenu),
  GNOMEUIINFO_SUBTREE(N_("_Insert"), insertmenu),
  GNOMEUIINFO_SUBTREE(N_("Forma_t"), formatmenu),
  GNOMEUIINFO_SUBTREE(N_("_Window"), windowmenu),
  GNOMEUIINFO_SUBTREE(N_("_Plugins"), pluginsmenu),

  GNOMEUIINFO_MENU_SETTINGS_TREE(settingsmenu),
  GNOMEUIINFO_MENU_HELP_TREE(helpmenu),
  GNOMEUIINFO_END
};

GnomeUIInfo docindexfilemenu[] =
{
  GNOMEUIINFO_MENU_NEW_ITEM(N_("_New"), N_("Create a new document"),
			    file_new_callback, NULL),

  GNOMEUIINFO_MENU_OPEN_ITEM(file_open_callback, NULL),

  GNOMEUIINFO_SEPARATOR,

  { GNOME_APP_UI_ITEM, N_("_Hide Index"), N_("Hide the document index."), (gpointer) idea_hide_callback, NULL, NULL, GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_CLOSE, 'W', GDK_CONTROL_MASK, NULL },

  GNOMEUIINFO_MENU_EXIT_ITEM(file_exit_callback, NULL),

  GNOMEUIINFO_END
};

GnomeUIInfo docindexhelpmenu[] =
{
#if 0
  GNOMEUIINFO_HELP("help-browser"),
  GNOMEUIINFO_SEPARATOR,
#endif
  GNOMEUIINFO_MENU_ABOUT_ITEM(about_callback, NULL), 
  GNOMEUIINFO_END
};

GnomeUIInfo docindexmainmenu[] = {
  GNOMEUIINFO_SUBTREE(N_("_File"), docindexfilemenu),
  GNOMEUIINFO_SUBTREE(N_("_Help"), docindexhelpmenu),
  GNOMEUIINFO_END
};


void
create_gnome_menu( GOWindow *window )
{
  gnome_app_create_menus_with_data( GNOME_APP( window->app ), mainmenu, (gpointer) window );
}


void
create_gnome_docindex_menu( idea_manager *ideas )
{
  gnome_app_create_menus_with_data( GNOME_APP( ideas->window ), docindexmainmenu, (gpointer) ideas );
}

#else
#define FILE_ITEMS (sizeof( menu ) / sizeof( GtkMenuEntry ))

static GtkMenuEntry menu [] =
{
  { "<Main>/File/New", "<control>N", file_new_callback, NULL },
  
#if 0
  { "<Main>/File/Save", "<control>S", file_save_callback, NULL },
#endif
  
  { "<Main>/File/Close", "<control>W", file_close_callback, NULL },
  { "<Main>/File/Open...", "<control>O", file_open_callback, NULL },
  { "<Main>/File/Save As...", NULL, file_save_as_callback, NULL },
  { "<Main>/File/<separator>", NULL, NULL, NULL },
  { "<Main>/File/Quit", "<control>Q", file_exit_callback, NULL },

  { "<Main>/Edit/Undo", "<control>Z", undo_callback, NULL },
  { "<Main>/Edit/Redo", "<control>R", redo_callback, NULL },
  { "<Main>/Edit/<separator>", NULL, NULL, NULL },
  { "<Main>/Edit/Cut", "<control>X", edit_cut_callback, NULL },
  { "<Main>/Edit/Copy", "<control>C", edit_copy_callback, NULL },
  { "<Main>/Edit/Paste", "<control>V", edit_paste_callback, NULL },
  { "<Main>/Edit/Clear", NULL, edit_clear_callback, NULL },
  { "<Main>/Edit/<separator>", NULL, NULL, NULL },
  { "<Main>/Edit/Select All", NULL, edit_select_all_callback, NULL },

  { "<Main>/Window/Document Index", "<control>I", raise_idea_callback, NULL },
  { "<Main>/Window/<separator>", NULL, NULL, NULL },

  /*
    { "<Main>/Font/Color/Red", NULL, font_color_callback, NULL },
    { "<Main>/Font/Color/Orange", NULL, font_color_callback, NULL },
    { "<Main>/Font/Color/Yellow", NULL, font_color_callback, NULL },
    { "<Main>/Font/Color/Green", NULL, font_color_callback, NULL },
    { "<Main>/Font/Color/Blue", NULL, font_color_callback, NULL },
    { "<Main>/Font/Color/Indigo", NULL, font_color_callback, NULL },
    { "<Main>/Font/Color/Violet", NULL, font_color_callback, NULL },
  */

  { "<Main>/Search/Search...", NULL, search_search_callback, NULL },
  { "<Main>/Search/Search And Replace...", NULL, search_search_replace_callback, NULL },
  { "<Main>/Search/Regular Expression Search...", NULL, search_regexp_callback, NULL },
  { "<Main>/Search/Regular Expression Search And Replace...", NULL, search_regexp_replace_callback, NULL },

  { "<Main>/Plugins/<separator>", NULL, NULL, NULL },

  { "<Main>/Help/About...", NULL, about_callback, NULL }
};    

GtkMenuFactory *create_menu( GOWindow *window )
{
  int i;
  GtkMenuFactory *factory;
  GtkMenuFactory *subfactory;
  GtkMenuEntry *current_menu_bar = g_malloc0( sizeof( menu ) );

  for ( i = 0; i < FILE_ITEMS; i++ )
    {
      current_menu_bar[i] = menu[i];
      current_menu_bar[i].callback_data = ( gpointer ) window;
    }

  factory = gtk_menu_factory_new( GTK_MENU_FACTORY_MENU_BAR );
  subfactory = gtk_menu_factory_new( GTK_MENU_FACTORY_MENU_BAR );
  gtk_menu_factory_add_subfactory( factory, subfactory, "<Main>" );
  gtk_menu_factory_add_entries( factory, current_menu_bar, FILE_ITEMS );

  return subfactory;
}
#endif

#define IDEA_FILE_ITEMS (sizeof( idea_menu ) / sizeof( GtkMenuEntry ))

static GtkMenuEntry idea_menu [] =
{
  { "<Main>/File/New", "<control>N", file_new_callback, NULL },
  { "<Main>/File/Open...", "<control>O", file_open_callback, NULL },
  { "<Main>/File/<separator>", NULL, NULL, NULL },
  { "<Main>/File/Hide Index", "<control>W",idea_hide_callback, NULL },
  { "<Main>/File/Quit", "<control>Q", file_exit_callback, NULL },
  { "<Main>/Help/About...", NULL, about_callback, NULL }
};    

GtkMenuFactory *create_idea_menu()
{
  int i;
  GtkMenuFactory *factory;
  GtkMenuFactory *subfactory;
  GtkMenuEntry *current_menu_bar = g_malloc0( sizeof( idea_menu ) );

  for ( i = 0; i < IDEA_FILE_ITEMS; i++ )
    {
      current_menu_bar[i] = idea_menu[i];
    }

  factory = gtk_menu_factory_new( GTK_MENU_FACTORY_MENU_BAR );
  subfactory = gtk_menu_factory_new( GTK_MENU_FACTORY_MENU_BAR );
  gtk_menu_factory_add_subfactory( factory, subfactory, "<Main>" );
  gtk_menu_factory_add_entries( factory, current_menu_bar, IDEA_FILE_ITEMS );

  return subfactory;
}
