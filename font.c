/* font.c - functions that do things to fonts.
 *
 * Copyright (C) 1998 Chris Lahey.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include "config.h"
#include <gtk/gtk.h>
#include <gnome.h>
#include <libgnomeprint/gnome-font.h>
#include <libgnomeprint/gnome-font-dialog.h>
#include "gtkrichtext.h"

#include "window.h"
#include "font.h"

typedef struct _JustDesc JustDesc;

struct _JustDesc
{
  gchar *name;
  GtkJustification just;
};

static gboolean just_descs_translated = FALSE;
static JustDesc just_descs[] =
{
  { N_("Left Justified"), GTK_JUSTIFY_LEFT },
  { N_("Centered"), GTK_JUSTIFY_CENTER },
  { N_("Right Justified"), GTK_JUSTIFY_RIGHT },
  { N_("Full Justified"), GTK_JUSTIFY_FILL }
};

static gint
first_pos( GtkRichText *text )
{
  Position first = text->cursor_first ? text->cursor : text->selection_end;
  return gtk_rich_text_position_to_int( text, first );
}

static gint
second_pos( GtkRichText *text )
{
  Position second = text->cursor_first ? text->selection_end : text->cursor;
  return gtk_rich_text_position_to_int( text, second );
}

#if 0
static void
update_selection( GtkWidget *widget, Position *start, Position *end, gpointer data )
{
  GoRuler *ruler = (GoRuler *) data;
  GOWindow *window = ruler->window;
  ParagraphParams params;
  ParagraphParamsValid validity;
  gtk_rich_text_get_selection_paragraph_params ( GTK_RICH_TEXT( window->textbox ),
						 &params,
						 &validity );

  ruler->first_indent_val = params.first_indent + params.left_indent;
  ruler->left_indent_val = params.left_indent;
  ruler->right_indent_val = params.right_indent;
  /*  place_indents( ruler ); */
}
#endif

static void
font_dialog_clicked( GtkWidget *widget, gint button, gpointer data )
{
  GnomeFontSelectionDialog *dialog = GNOME_FONT_SELECTION_DIALOG( widget );
  GOWindow *window = (GOWindow *) data;
  GtkRichText *textbox = GTK_RICH_TEXT( window->textbox );

  if( button == 0 )
    {
      GnomeDisplayFont *display_font = gnome_font_selection_dialog_get_font( dialog );
      GtkRichTextFont *font;

      if ( display_font != NULL && display_font->gnome_font != NULL )
	{
	  
	  font = gtk_rich_text_font_adopt( display_font->gdk_font, display_font->x_font_name, display_font->gnome_font );

	  if ( font != NULL )
	    {
	      if ( textbox->selection_active )
		{
		  CharacterParams params;
		  CharacterParamsValid validity = character_params_all_valid;
		  params.font = font;
		  set_char_params_callback( window, first_pos( textbox ), second_pos( textbox ), &params, &validity );
		  gtk_rich_text_set_selection_character_params( textbox, &params, &validity );
		}
	      else
		gtk_rich_text_set_cursor_font( textbox, font );
	    }
	}
    }

  gnome_dialog_close( GNOME_DIALOG( dialog ) );
}



static GnomeFont *
get_current_font( GtkRichText *text )
{
  GtkRichTextFont *font = gtk_rich_text_get_cursor_font( text );
  if ( font != NULL )
    return font->data;
  else
    return NULL;
}

void open_font_dialog( GtkWidget *widget, gpointer data )
{
  GtkWidget *dialog = gnome_font_selection_dialog_new( NULL );
  GOWindow *window = (GOWindow *) data;
  GtkRichText *textbox = GTK_RICH_TEXT( window->textbox );
  GnomeFont *current_font = get_current_font( textbox );
  
  if ( current_font )
    {
      gnome_font_selection_dialog_set_font
	( GNOME_FONT_SELECTION_DIALOG( dialog ),
	  current_font );
    }
  gtk_signal_connect( GTK_OBJECT( dialog ),
		      "clicked",
		      (GtkSignalFunc) font_dialog_clicked,
		      data );

  gtk_widget_show_all( dialog );
}



typedef struct
{
  GtkSpinButton *pre;
  GtkSpinButton *in;
  GtkSpinButton *post;
  GtkSpinButton *indent;
  GtkSpinButton *left_ind;
  GtkSpinButton *right_ind;
  GtkCombo *justification;
  GOWindow *window;
} par_format_data;

static void open_par_clicked( GtkWidget *widget, gint button, gpointer data )
{
  par_format_data *par_data = (par_format_data *) data;
  GnomeDialog *dialog = GNOME_DIALOG( widget );
  GOWindow *window = par_data->window;
  GtkRichText *text = GTK_RICH_TEXT( window->textbox );
  gint i;

  if ( button == 0 )
    {
      ParagraphParams params;
      ParagraphParamsValid validity = paragraph_params_none_valid;
      params.prespace = gtk_spin_button_get_value_as_int( par_data->pre );
      params.inspace = gtk_spin_button_get_value_as_int( par_data->in );
      params.postspace = gtk_spin_button_get_value_as_int( par_data->post );
      params.first_indent = gtk_spin_button_get_value_as_int( par_data->indent );
      params.left_indent = gtk_spin_button_get_value_as_int( par_data->left_ind );
      params.right_indent = gtk_spin_button_get_value_as_int( par_data->right_ind );
      params.justification = GTK_JUSTIFY_FILL;
      for ( i = 0; i < sizeof(just_descs) / sizeof(just_descs[0]); i++ )
	{
	  if ( ! strcmp( just_descs[i].name,
			 gtk_entry_get_text( GTK_ENTRY( GTK_COMBO( par_data->justification )->entry ) ) ) )
	    {
	      params.justification = just_descs[i].just;
	      break;
	    }
	}
      validity.prespace_valid = validity.inspace_valid = validity.postspace_valid =
	validity.left_indent_valid = validity.right_indent_valid =
	validity.first_indent_valid = validity.justification_valid = 1;
      set_par_params_callback( window, first_pos( text ), second_pos( text ), &params, &validity );
      gtk_rich_text_set_selection_paragraph_params( text, &params, &validity );
    }

  gnome_dialog_close( dialog );
}

static void delete_par_data( GtkWidget *widget, gpointer data )
{
  g_free( data );
}

void open_par_format_dialog( GtkWidget *widget, gpointer data )
{
  GOWindow *window = (GOWindow *) data;
  GtkWidget *spinbutton;
  GtkWidget *table;
  GtkWidget *label;
  GtkObject *adjustment;
  GtkWidget *alignment;
  GtkWidget *hbox, *vbox;
  GtkWidget *radiobutton;
  GtkWidget *frame;
  GtkWidget *combo;
  GtkWidget *listitem;
  ParagraphParams paramsin;
  gint i;
  GnomeDialog *dialog = GNOME_DIALOG ( gnome_dialog_new( _( "Set paragraph formatting" ),
							 GNOME_STOCK_BUTTON_OK,
							 GNOME_STOCK_BUTTON_CANCEL,
							 NULL ) );
  par_format_data *par_data = g_new( par_format_data, 1 );

  gtk_rich_text_get_selection_paragraph_params( GTK_RICH_TEXT( window->textbox ),
						&paramsin,
						NULL );

  gnome_dialog_set_parent( dialog, GTK_WINDOW( window->app ) );
  gnome_dialog_set_default( dialog, 0 );

  hbox = gtk_hbox_new( FALSE, 5 );

  frame = gtk_frame_new( _( "Indents" ) );

  table = gtk_table_new( 3, 2, FALSE );
  
  adjustment = gtk_adjustment_new( 0, -65535, 65535, 1, 0, 0 );
  spinbutton = gtk_spin_button_new( GTK_ADJUSTMENT( adjustment ), 1.0, 1 );
  gtk_spin_button_set_value( GTK_SPIN_BUTTON( spinbutton ), paramsin.first_indent );
  label = gtk_label_new( _( "First" ) );
  alignment = gtk_alignment_new( 1, .5, 0, 1 );
  gtk_container_add( GTK_CONTAINER( alignment ), label );
  gtk_table_attach( GTK_TABLE( table ), alignment, 0, 1, 0, 1,
		    GTK_EXPAND | GTK_FILL,
		    GTK_EXPAND | GTK_FILL,
		    5, 5);
  alignment = gtk_alignment_new( 0, .5, 1, 1 );
  gtk_container_add( GTK_CONTAINER( alignment ), spinbutton );
  gtk_table_attach( GTK_TABLE( table ), alignment, 1, 2, 0, 1,
		    GTK_EXPAND | GTK_FILL,
		    GTK_EXPAND | GTK_FILL,
		    5, 5);
  par_data->indent = GTK_SPIN_BUTTON( spinbutton );
  
  adjustment = gtk_adjustment_new( 0, 0, 65535, 1, 0, 0 );
  spinbutton = gtk_spin_button_new( GTK_ADJUSTMENT( adjustment ), 1.0, 1 );
  gtk_spin_button_set_value( GTK_SPIN_BUTTON( spinbutton ), paramsin.left_indent );
  label = gtk_label_new( _( "Left" ) );
  alignment = gtk_alignment_new( 1, .5, 0, 1 );
  gtk_container_add( GTK_CONTAINER( alignment ), label );
  gtk_table_attach( GTK_TABLE( table ), alignment, 0, 1, 1, 2,
		    GTK_EXPAND | GTK_FILL,
		    GTK_EXPAND | GTK_FILL,
		    5, 5);
  alignment = gtk_alignment_new( 0, .5, 1, 1 );
  gtk_container_add( GTK_CONTAINER( alignment ), spinbutton );
  gtk_table_attach( GTK_TABLE( table ), alignment, 1, 2, 1, 2,
		    GTK_EXPAND | GTK_FILL,
		    GTK_EXPAND | GTK_FILL,
		    5, 5);
  par_data->left_ind = GTK_SPIN_BUTTON( spinbutton );

  adjustment = gtk_adjustment_new( 0, 0, 65535, 1, 0, 0 );
  spinbutton = gtk_spin_button_new( GTK_ADJUSTMENT( adjustment ), 1.0, 1 );
  gtk_spin_button_set_value( GTK_SPIN_BUTTON( spinbutton ), paramsin.right_indent );
  label = gtk_label_new( _( "Right" ) );
  alignment = gtk_alignment_new( 1, .5, 0, 1 );
  gtk_container_add( GTK_CONTAINER( alignment ), label );
  gtk_table_attach( GTK_TABLE( table ), alignment, 0, 1, 2, 3,
		    GTK_EXPAND | GTK_FILL,
		    GTK_EXPAND | GTK_FILL,
		    5, 5);
  alignment = gtk_alignment_new( 0, .5, 1, 1 );
  gtk_container_add( GTK_CONTAINER( alignment ), spinbutton );
  gtk_table_attach( GTK_TABLE( table ), alignment, 1, 2, 2, 3,
		    GTK_EXPAND | GTK_FILL,
		    GTK_EXPAND | GTK_FILL,
		    5, 5);
  par_data->right_ind = GTK_SPIN_BUTTON( spinbutton );

  gtk_container_add( GTK_CONTAINER( frame ), table );
  gtk_box_pack_start( GTK_BOX( hbox ), frame, TRUE, TRUE, 0 );

  frame = gtk_frame_new( _( "Spacing" ) );

  table = gtk_table_new( 3, 2, FALSE );

  adjustment = gtk_adjustment_new( 0, 0, 65535, 1, 0, 0 );
  spinbutton = gtk_spin_button_new( GTK_ADJUSTMENT( adjustment ), 1.0, 1 );
  gtk_spin_button_set_value( GTK_SPIN_BUTTON( spinbutton ), paramsin.prespace );
  label = gtk_label_new( _( "Before Paragraph" ) ); 
  alignment = gtk_alignment_new( 1, .5, 0, 1 );
  gtk_container_add( GTK_CONTAINER( alignment ), label );
  gtk_table_attach( GTK_TABLE( table ), alignment, 0, 1, 0, 1,
		    GTK_EXPAND | GTK_FILL,
		    GTK_EXPAND | GTK_FILL,
		    5, 5);
  alignment = gtk_alignment_new( 0, .5, 1, 1 );
  gtk_container_add( GTK_CONTAINER( alignment ), spinbutton );
  gtk_table_attach( GTK_TABLE( table ), alignment, 1, 2, 0, 1,
		    GTK_EXPAND | GTK_FILL,
		    GTK_EXPAND | GTK_FILL,
		    5, 5);
  par_data->pre = GTK_SPIN_BUTTON( spinbutton );
  
  adjustment = gtk_adjustment_new( 0, 0, 65535, 1, 0, 0 );
  spinbutton = gtk_spin_button_new( GTK_ADJUSTMENT( adjustment ), 1.0, 1 );
  gtk_spin_button_set_value( GTK_SPIN_BUTTON( spinbutton ), paramsin.inspace );
  label = gtk_label_new( _( "Inside Paragraph" ) );
  alignment = gtk_alignment_new( 1, .5, 0, 1 );
  gtk_container_add( GTK_CONTAINER( alignment ), label );
  gtk_table_attach( GTK_TABLE( table ), alignment, 0, 1, 1, 2,
		    GTK_EXPAND | GTK_FILL,
		    GTK_EXPAND | GTK_FILL,
		    5, 5);
  alignment = gtk_alignment_new( 0, .5, 1, 1 );
  gtk_container_add( GTK_CONTAINER( alignment ), spinbutton );
  gtk_table_attach( GTK_TABLE( table ), alignment, 1, 2, 1, 2,
		    GTK_EXPAND | GTK_FILL,
		    GTK_EXPAND | GTK_FILL,
		    5, 5);
  par_data->in = GTK_SPIN_BUTTON( spinbutton );
  
  adjustment = gtk_adjustment_new( 0, 0, 65535, 1, 0, 0 );
  spinbutton = gtk_spin_button_new( GTK_ADJUSTMENT( adjustment ), 1.0, 1 );
  gtk_spin_button_set_value( GTK_SPIN_BUTTON( spinbutton ), paramsin.postspace );
  label = gtk_label_new( _( "After Paragraph" ) );
  alignment = gtk_alignment_new( 1, .5, 0, 1 );
  gtk_container_add( GTK_CONTAINER( alignment ), label );
  gtk_table_attach( GTK_TABLE( table ), alignment, 0, 1, 2, 3,
		    GTK_EXPAND | GTK_FILL,
		    GTK_EXPAND | GTK_FILL,
		    5, 5);
  alignment = gtk_alignment_new( 0, .5, 1, 1 );
  gtk_container_add( GTK_CONTAINER( alignment ), spinbutton );
  gtk_table_attach( GTK_TABLE( table ), alignment, 1, 2, 2, 3,
		    GTK_EXPAND | GTK_FILL,
		    GTK_EXPAND | GTK_FILL,
		    5, 5);
  par_data->post = GTK_SPIN_BUTTON( spinbutton );

  gtk_container_add( GTK_CONTAINER( frame ), table );
  gtk_box_pack_start( GTK_BOX( hbox ), frame, TRUE, TRUE, 0 );

  frame = gtk_frame_new( _( "Justification" ) );
  combo = gtk_combo_new();

  if ( ! just_descs_translated )
    {
      for ( i = 0; i < sizeof(just_descs) / sizeof(just_descs[0]); i++ )
	{
	  just_descs[i].name = _( just_descs[i].name );
	}
    }

  for ( i = 0; i < sizeof(just_descs) / sizeof(just_descs[0]); i++ )
    {
      listitem = gtk_list_item_new_with_label ( just_descs[i].name );
      gtk_widget_show( listitem );
      gtk_container_add( GTK_CONTAINER( GTK_COMBO( combo )->list ), listitem );
    }
  for ( i = 0; i < sizeof(just_descs) / sizeof(just_descs[0]); i++ )
    {
      if ( just_descs[i].just == paramsin.justification )
	{
	  gtk_entry_set_text( GTK_ENTRY( GTK_COMBO( combo )->entry ), just_descs[i].name );
	  break;
	}
    }
  gtk_combo_set_value_in_list( GTK_COMBO( combo ), TRUE, FALSE );
  GTK_WIDGET_UNSET_FLAGS( GTK_COMBO( combo )->entry, GTK_CAN_FOCUS );
  par_data->justification = GTK_COMBO( combo );

  gtk_container_add( GTK_CONTAINER( frame ), combo );
  gtk_box_pack_start( GTK_BOX( dialog->vbox ), frame, TRUE, TRUE, 0 );

  gtk_box_pack_start( GTK_BOX( dialog->vbox ), hbox, TRUE, TRUE, 0 );
  
  par_data->window = window;
  
  gtk_signal_connect( GTK_OBJECT( dialog ),
		      "clicked",
		      (GtkSignalFunc) open_par_clicked,
		      par_data );
  gtk_signal_connect( GTK_OBJECT( dialog ),
		      "destroy",
		      (GtkSignalFunc) delete_par_data,
		      par_data );
  gtk_widget_show_all( GTK_WIDGET( dialog ) );
}

void
set_paragraph_toolbar_settings( GtkWidget *widget, Position *a, Position *b, gpointer data )
{
#if 0
  GOWindow *window = (GOWindow *) data;
  GtkRichText *text = GTK_RICH_TEXT( window->textbox );
  ParagraphParams params;
  gtk_rich_text_get_paragraph_params( text, *a, *b, &params, NULL );
#endif
}

void
toolbar_fill_justified_callback( GtkWidget *widget, gpointer data )
{
}

void
toolbar_left_justified_callback( GtkWidget *widget, gpointer data )
{
}

void
toolbar_center_justified_callback( GtkWidget *widget, gpointer data )
{
}

void
toolbar_right_justified_callback( GtkWidget *widget, gpointer data )
{
}
