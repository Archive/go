#! /usr/bin/perl

die unless chdir( "$ENV{srcdir}/" );
open DIRLIST, "plugins/plugins.dir.list";
while ( <DIRLIST> )
{
    s/#.*\n?$//; next if /^\s*$/; s/\s*$//; s/^\s*//; push @direntry, $_ if length;    
}

open MAKEFILE, "> configure.in";
print MAKEFILE << 'EOF';
dnl This is a generated file.  Please see makeconfig.pl.
AC_INIT(go_plugin_api.c)

AM_CONFIG_HEADER(config.h)
AM_INIT_AUTOMAKE(go, 0.1.35)
AM_MAINTAINER_MODE
AM_ACLOCAL_INCLUDE(macros)

GNOME_INIT
AC_ISC_POSIX
AC_PROG_CC
AC_STDC_HEADERS
AC_ARG_PROGRAM
AM_PROG_LIBTOOL

AM_PATH_LIBHNJ(0.1.1, AC_DEFINE(HAVE_LIBHNJ))

## this should come after `AC_PROG_CC'
GNOME_COMPILE_WARNINGS
GNOME_XML_CHECK
GNOME_PRINT_CHECK
dnl ## AM_PATH_GNOME_PRINT(0.1.1, AC_DEFINE(HAVE_GNOME_PRINT))
GNOME_X_CHECKS
GNOME_ORBIT_CHECK
GNOME_GNORBA_CHECK

AC_SUBST(CFLAGS)
AC_SUBST(CPPFLAGS)
AC_SUBST(LDFLAGS)

dnl ## internationalization support
ALL_LINGUAS="de es fr no pigl"
AM_GNU_GETTEXT
AC_LINK_FILES($nls_cv_header_libgt, $nls_cv_header_intl)


AC_PROG_MAKE_SET

AM_PATH_GLIB
AM_PATH_GTK

dnl Checks for header files.
AC_HEADER_STDC
AC_CHECK_HEADERS(unistd.h)

dnl Checks for typedefs, structures, and compiler characteristics.
AC_C_CONST

dnl Checks for library functions.
AC_CHECK_FUNCS(mkdir)

AC_OUTPUT([
Makefile
macros/Makefile
intl/Makefile
po/Makefile.in
plugins/Makefile
EOF

if ( $#direntry + 1 > 0 )
{
    foreach $i (@direntry)
    {
	print MAKEFILE "plugins/$i/Makefile\n";
    }
}

print MAKEFILE << "EOF";
gtkrichtext/Makefile
go.spec
],[sed -e "/POTFILES =/r po/POTFILES" po/Makefile.in > po/Makefile])
EOF
