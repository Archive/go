/* go_plugin_api.h
 * Copyright (C) 1998 Chris Lahey
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __GO_PLUGIN_API_H__
#define __GO_PLUGIN_API_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "plugin.h"
  
  int go_plugin_document_create( gint context, gchar *title );
  int go_plugin_document_open( gint context, gchar *title );
  gboolean go_plugin_document_close( gint docid );
  void go_plugin_text_append( gint docid, gchar *buffer, gint length );
  void go_plugin_text_insert( gint docid, gchar *buffer, gint length, gint position );
  void go_plugin_document_show( gint docid );
  int go_plugin_document_current( gint context );
  char *go_plugin_document_filename( gint docid );
  char *go_plugin_text_get( gint docid );
  char *go_plugin_text_get_selection_text( gint docid );
  void go_plugin_text_set_selection_text( gint docid, char *text, gint length );
  gint go_plugin_document_get_position( gint docid );
  selection_range go_plugin_document_get_selection_range( gint docid );
  void go_plugin_document_set_selection_range( gint docid, selection_range range );
  void go_plugin_program_register( plugin_info *info );
  gboolean go_plugin_program_quit( void );

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GO_PLUGIN_API_H__ */
