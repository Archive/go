/* toolbar.c - Creates the toolbars used by GO Office.
 *
 * Copyright (C) 1998 Chris Lahey.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include "config.h"

#include <gtk/gtk.h>
#include <gdk/gdkprivate.h>
#include <string.h>
#include <gnome.h>

#include "window.h"
#include "file.h"
#include "docindex.h"
#include "toolbar.h"
#include "print.h"

GnomeUIInfo toolbar[] = {
  GNOMEUIINFO_ITEM_STOCK(N_("Close"), 
			 N_("Close the current document"),
			 (gpointer) file_close_callback,
			 GNOME_STOCK_PIXMAP_CLOSE),
  GNOMEUIINFO_ITEM_STOCK(N_("Open File"),
			 N_("Open a document"),
			 (gpointer) file_open_callback,
			 GNOME_STOCK_PIXMAP_OPEN),
  GNOMEUIINFO_ITEM_STOCK(N_("Save As"),
			 N_("Save the current document with a different name"),
			 (gpointer) file_save_as_callback,
			 GNOME_STOCK_PIXMAP_SAVE_AS),
  GNOMEUIINFO_ITEM_STOCK(N_("Print"),
			 N_("Print the current document"),
			 (gpointer) file_print_callback,
			 GNOME_STOCK_PIXMAP_PRINT ),
  GNOMEUIINFO_ITEM_STOCK(N_("Index"),
			 N_("Raise the Document Index"),
			 (gpointer) raise_idea_callback,
			 GNOME_STOCK_PIXMAP_INDEX ),
  GNOMEUIINFO_END
};


GnomeUIInfo docindextoolbar[] = {
  GNOMEUIINFO_ITEM_STOCK(N_("Open File"),
			 N_("Open a document"),
			 (gpointer) file_open_callback,
			 GNOME_STOCK_PIXMAP_OPEN),
  GNOMEUIINFO_ITEM_STOCK(N_("Up"),
			 N_("Move the selected entry up in the index"),
			 (gpointer) idea_up_callback,
			 GNOME_STOCK_PIXMAP_UP),
  GNOMEUIINFO_ITEM_STOCK(N_("Down"),
			 N_("Move the selected entry down in the index"),
			 (gpointer) idea_down_callback,
			 GNOME_STOCK_PIXMAP_DOWN),
  GNOMEUIINFO_ITEM_STOCK(N_("Remove"),
			 N_("Remove the selected entry from the index"),
			 (gpointer) idea_remove_callback,
			 GNOME_STOCK_PIXMAP_CLOSE),
  GNOMEUIINFO_ITEM_STOCK(N_("Hide"), 
			 N_("Hide the Document Index"),
			 (gpointer) idea_hide_callback,
			 GNOME_STOCK_PIXMAP_CLOSE),
  GNOMEUIINFO_ITEM_STOCK(N_("Quit"), 
			 N_("Duh"),
			 (gpointer) file_exit_callback,
			 GNOME_STOCK_PIXMAP_QUIT),
  GNOMEUIINFO_END
};

void
create_gnome_toolbar( GOWindow *window )
{
  gnome_app_create_toolbar_with_data( GNOME_APP( window->app ), toolbar, (gpointer) window );
}

void
create_gnome_docindex_toolbar( idea_manager *ideas )
{
  gnome_app_create_toolbar_with_data( GNOME_APP( ideas->window ), docindextoolbar, (gpointer) ideas );
}

#ifdef NOTGNOME
GtkWidget *create_toolbar( GOWindow *window )
{
  GtkWidget *wpixmap;
  GdkPixmap *pixmap;
  GdkBitmap *mask;
  GtkWidget *toolbar;

  toolbar = gtk_toolbar_new( GTK_ORIENTATION_HORIZONTAL, GTK_TOOLBAR_BOTH );
  gtk_widget_show( toolbar );
  gtk_toolbar_set_button_relief( GTK_TOOLBAR( toolbar ), GTK_RELIEF_NONE );
  
  pixmap = gdk_pixmap_create_from_xpm( (GdkWindow *)&gdk_root_parent, &mask,
				       &toolbar->style->bg[GTK_STATE_NORMAL],
				       DATADIR "/go/tb_exit.xpm" );
  wpixmap = gtk_pixmap_new( pixmap, mask );
  
  gtk_toolbar_append_item( GTK_TOOLBAR( toolbar ),
			   "Close", "Close this window", "Toolbar/Close",
			   wpixmap,
			   (GtkSignalFunc) file_close_callback, window );
  
  pixmap = gdk_pixmap_create_from_xpm( (GdkWindow *)&gdk_root_parent, &mask,
				       &toolbar->style->bg[GTK_STATE_NORMAL],
				       DATADIR "/go/tb_open.xpm" );
  wpixmap = gtk_pixmap_new( pixmap, mask );
  
  gtk_toolbar_append_item( GTK_TOOLBAR( toolbar ),
			   "Open File", "Open a file", "Toolbar/Open",
			   wpixmap,
			   (GtkSignalFunc) file_open_callback, window);

  pixmap = gdk_pixmap_create_from_xpm( (GdkWindow *)&gdk_root_parent, &mask,
				       &toolbar->style->bg[GTK_STATE_NORMAL],
				       DATADIR "/go/tb_save_as.xpm" );
  wpixmap = gtk_pixmap_new( pixmap, mask );
  
  gtk_toolbar_append_item( GTK_TOOLBAR( toolbar ),
			   "Save As", "Save this file with a different title", "Toolbar/Save As",
			   wpixmap,
			   (GtkSignalFunc) file_save_as_callback, window );

  pixmap = gdk_pixmap_create_from_xpm( (GdkWindow *)&gdk_root_parent, &mask,
				       &toolbar->style->bg[GTK_STATE_NORMAL],
				       DATADIR "/go/tb_index.xpm" );
  wpixmap = gtk_pixmap_new( pixmap, mask );
  
  gtk_toolbar_append_item( GTK_TOOLBAR( toolbar ),
			   "Index", "Raise Document Index", "Toolbar/Index",
			   wpixmap,
			   (GtkSignalFunc) raise_idea_callback, window );
  return toolbar;
}
#endif
GtkWidget *create_idea_toolbar()
{
  GtkWidget *wpixmap;
  GdkPixmap *pixmap;
  GdkBitmap *mask;
  GtkWidget *toolbar;

  toolbar = gtk_toolbar_new( GTK_ORIENTATION_HORIZONTAL, GTK_TOOLBAR_BOTH);
  gtk_widget_show( toolbar );
  gtk_toolbar_set_button_relief( GTK_TOOLBAR( toolbar ), GTK_RELIEF_NONE );
  
  pixmap = gdk_pixmap_create_from_xpm( (GdkWindow *)&gdk_root_parent, &mask,
				       &toolbar->style->bg[GTK_STATE_NORMAL],
				       DATADIR "/go/tb_open.xpm" );
  wpixmap = gtk_pixmap_new( pixmap, mask );
  
  gtk_toolbar_append_item( GTK_TOOLBAR( toolbar ),
			   "Open File", "Open a file", "Toolbar/Open",
			   wpixmap,
			   (GtkSignalFunc) file_open_callback, NULL);
  
  pixmap = gdk_pixmap_create_from_xpm( (GdkWindow *)&gdk_root_parent, &mask,
				       &toolbar->style->bg[GTK_STATE_NORMAL],
				       DATADIR "/go/tb_up_arrow.xpm" );
  wpixmap = gtk_pixmap_new( pixmap, mask );
  
  gtk_toolbar_append_item( GTK_TOOLBAR( toolbar ),
			   "Up", "Move the selected entry up in the index", "Toolbar/Up",
			   wpixmap,
			   (GtkSignalFunc) idea_up_callback, NULL);
  
  pixmap = gdk_pixmap_create_from_xpm( (GdkWindow *)&gdk_root_parent, &mask,
				       &toolbar->style->bg[GTK_STATE_NORMAL],
				       DATADIR "/go/tb_down_arrow.xpm" );
  wpixmap = gtk_pixmap_new( pixmap, mask );
  
  gtk_toolbar_append_item( GTK_TOOLBAR( toolbar ),
			   "Down", "Move the selected entry down in the index", "Toolbar/Down",
			   wpixmap,
			   (GtkSignalFunc) idea_down_callback, NULL );
  
  pixmap = gdk_pixmap_create_from_xpm( (GdkWindow *)&gdk_root_parent, &mask,
				       &toolbar->style->bg[GTK_STATE_NORMAL],
				       DATADIR "/go/cancel.xpm" );
  wpixmap = gtk_pixmap_new( pixmap, mask );
  
  gtk_toolbar_append_item( GTK_TOOLBAR( toolbar ),
			   "Remove", "Remove the selected entry from the index", "Toolbar/Remove",
			   wpixmap,
			   (GtkSignalFunc) idea_remove_callback, NULL );
  
  pixmap = gdk_pixmap_create_from_xpm( (GdkWindow *)&gdk_root_parent, &mask,
				       &toolbar->style->bg[GTK_STATE_NORMAL],
				       DATADIR "/go/tb_exit.xpm" );
  wpixmap = gtk_pixmap_new( pixmap, mask );
  
  gtk_toolbar_append_item( GTK_TOOLBAR( toolbar ),
			   "Hide", "Hide the Document Index", "Toolbar/Hide",
			   wpixmap,
			   (GtkSignalFunc) idea_hide_callback, NULL );
  
  pixmap = gdk_pixmap_create_from_xpm( (GdkWindow *)&gdk_root_parent, &mask,
				       &toolbar->style->bg[GTK_STATE_NORMAL],
				       DATADIR "/go/tb_exit.xpm" );
  wpixmap = gtk_pixmap_new( pixmap, mask );
  
  gtk_toolbar_append_item( GTK_TOOLBAR( toolbar ),
			   "Quit", "Duh", "Toolbar/Quit",
			   wpixmap,
			   (GtkSignalFunc) file_exit_callback, NULL );
  return toolbar;
}
