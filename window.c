/* window.c - Creates the windows used by GO Office and handles their
 * events.
 *
 * Copyright (C) 1998 Chris Lahey.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include "config.h"

#include <gtk/gtk.h>
#include <string.h>
#include <stdio.h>
#include <gnome.h>

#include "window.h"
#include "menu.h"
#include "file.h"
#include "docindex.h"
#include "toolbar.h"
#include "plugin.h"
#include "main.h"
#include "preferences.h"
#include "properties.h"
#include "util.h"
#include "undo_if.h"

#include "gtkrichtext.h"

extern GList *plugins;
GList *windowlist;
extern idea_manager *ideas;

typedef struct
{
  GList **list;
  gchar *title;
} list_char_pair;
static void add_window_to_win_list( gpointer data, gpointer user_data );

static void check_title( gpointer data, gpointer user_data );
static void insert_text_callback( GtkWidget *widget, gchar *text, gint text_length, gint *pos_p, gpointer data );
static void delete_text_callback( GtkWidget *widget, gint start, gint end, gpointer data );
static void add_window_to_menu_rev( gpointer data, gpointer user_data );
static void add_plugin_info_to_plugin_list_rev( gpointer data, gpointer user_data );

/* g_malloc_short_title  
 *  returns a pointer to a newly allocated piece of memory containing
 *  a copy of the file name (not path) of the given window, prefixed
 *  with the given prefix and affixed with <?> if count > 0 or the
 *  filename ends with >. */
static gchar *g_malloc_short_title( GOWindow *window, gchar *prefix )
{
  gchar *substring;
  gchar *title;
  gint count = window->count;
  gint length;
  
  substring = strrchr( window->title, '/' ) + 1;
  if ( substring == 0 + (char *) 1 )
    substring = window->title;

  length = strlen( substring ) + 1 + strlen( prefix ) +
    ( ( count != 0 || substring[ strlen( substring ) - 1 ] == '>' ) ? 3 : 0 );
  if ( substring[ strlen( substring ) - 1 ] == '>' && count <= 0 )
      length ++;
  if ( count < 0 )
    count = - count;
  while ( count )
    {
      count /= 10;
      length ++;
    }
  count = window->count;
  
  title = g_malloc0( length );

  if ( count != 0 || substring[ strlen( substring ) - 1 ] == '>' )
    sprintf( title, "%s%s <%d>", prefix, substring, window->count );
  else
    sprintf( title, "%s%s", prefix, substring );

  return title;
}

/* short_title_simple  
 *  returns a pointer to an already allocated piece of memory
 *  containing the file name (not path) of the given window with no
 *  prefixes or affixes. */
static gchar *short_title_simple( GOWindow *window )
{
  gchar *substring;
  
  substring = strrchr( window->title, '/' ) + 1;
  if ( substring == 0 + (char *) 1 )
    substring = window->title;
  return substring;
}

/* g_malloc_title
 *  returns a pointer to a newly allocated piece of memory containing
 *  a copy of the file name and path of the given window, prefixed
 *  with the given prefix and affixed with <?> if count > 0 or the
 *  filename ends with >. */
gchar *g_malloc_title( GOWindow *window, gchar *prefix )
{
  gchar *title;
  gint count = window->count;
  gint length;

  length = strlen( window->title ) + 1 + strlen( prefix ) +
    ( ( count != 0 || window->title[ strlen( window->title ) - 1 ] == '>' ) ? 3 : 0 );
  if( window->title[ strlen( window->title ) - 1 ] == '>' && count <= 0 )
      length ++;
  if ( count < 0 )
    count = - count;
  while ( count )
    {
      count /= 10;
      length ++;
    }
  count = window->count;
  
  title = g_malloc0( length );

  if ( count != 0 || window->title[ strlen( window->title ) - 1 ] == '>' )
    sprintf( title, "%s%s <%d>", prefix, window->title, count );
  else
    sprintf( title, "%s%s", prefix, window->title );

  return title;
}

/* window_set_title
 *  Sets the title of the window to a copy of the given string. */
void window_set_title( GOWindow *window, gchar *title )
{
  g_free( window->title );
  window->title = g_strdup( title );
  gtk_window_set_title( GTK_WINDOW( window->app ), window->title );
  window->named = 1;
}

/* remove_window_from_menu
 *  removes the second window from the window menu of the first
 *  menu. */
void remove_window_from_menu( gpointer data, gpointer user_data )
{
  GOWindow *this_window = (GOWindow *) data;
  GOWindow *window = (GOWindow *) user_data;
  gchar *prefix = append2( _("_Window"), 0, "/", 0 );
  gchar *path = g_malloc_short_title( window, prefix );

  gnome_app_remove_menus( GNOME_APP( this_window->app ), path, 1 );

  g_free( prefix );
  g_free( path );
}

/* window_close
 *  Closes the given window. */
void window_close( GOWindow *window )
{
  save_location( window );
  gtk_widget_destroy( window->app );
  window_cleanup( window );
}

/* window_close
 *  Closes the given window. */
void window_cleanup( GOWindow *window )
{
  windowlist = g_list_remove( windowlist, (gpointer) window );
  g_list_foreach( windowlist, remove_window_from_menu, window );
  g_free( window->title );
  undo_tree_destroy( window->undo );
  g_free( window );
  if ( windowlist == NULL && ideas == NULL )
    {
      if ( preferences->reopen_idea_on_all_close )
	open_idea_window();
      else
	file_exit_callback( NULL, NULL );
    }
}

static void
insert_text_callback( GtkWidget *widget, gchar *text, gint text_length, gint *pos_p, gpointer data )
{
  GOWindow *window = ( GOWindow * ) data;
  insert_param param;

  param.location = *pos_p;
  param.chars = text;
  param.length = text_length;
  
  append_to_undo_tree( window->undo, UNDO_TYPE_INSERT, &param );
}

static void
delete_text_callback( GtkWidget *widget, gint start, gint end, gpointer data )
{
  GOWindow *window = ( GOWindow * ) data;
  delete_param param;

  param.start = start;
  param.end = end;
  
  append_to_undo_tree( window->undo, UNDO_TYPE_DELETE, &param );
}

void set_char_params_callback( GOWindow *window, gint start, gint end, CharacterParams *params, CharacterParamsValid *validity )
{
  font_change_param param;

  param.start = start;
  param.end = end;
  param.params = *params;
  param.validity = *validity;

  append_to_undo_tree( window->undo, UNDO_TYPE_FONT, &param );
}

void set_par_params_callback( GOWindow *window, gint start, gint end, ParagraphParams *params, ParagraphParamsValid *validity )
{
  par_change_param param;

  param.start = start;
  param.end = end;
  param.params = *params;
  param.validity = *validity;

  append_to_undo_tree( window->undo, UNDO_TYPE_PARAGRAPH_FORMAT, &param );
}

/* reset_usize
 *  A callback so that the window can be resized smaller. */
gint reset_usize( gpointer data )
{
  gtk_widget_set_usize( GTK_WIDGET( data ), 0, 0 );
  return FALSE;
}

/* make_window
 *  Makes a randomly placed window with the given title. */
GOWindow *make_window( gchar *title )
{
  return make_window_with_dimensions( -1, -1, DEF_WIDTH, DEF_HEIGHT, title );
}

/* GO_raise_window
 *  A callback to raise a window and change its order in the list.  I
 *  wish I knew how to convince the wm to tell me when the user wants
 *  to raise a window using the wm.  */
void GO_raise_window( GtkWidget *widget, gpointer data )
{
  GOWindow *window = (GOWindow *) data;

  gdk_window_raise( window->app->window );
  windowlist = g_list_remove( windowlist, window );
  windowlist = g_list_append( windowlist, window );
}

/* add_window_to_menu
 *  Adds the second window to the first window's window menu. */
void add_window_to_menu( gpointer data, gpointer user_data )
{
  GOWindow *this_window = (GOWindow *) data;
  GOWindow *window = (GOWindow *) user_data;
  gchar *path = append2( _( "_Window" ), 0, "/", 0);
  gchar *menuname = g_malloc_short_title( window, "" );
  GnomeUIInfo new_menu_entry[] = { { GNOME_APP_UI_ITEM, NULL, N_( "Open this window" ), (gpointer) GO_raise_window, NULL, NULL, GNOME_APP_PIXMAP_NONE, 0, 0, 0, NULL }, GNOMEUIINFO_END };

  new_menu_entry[0].label = menuname;
  
  gnome_app_insert_menus_with_data( GNOME_APP( this_window->app ), path, new_menu_entry, window );
  g_free( menuname );
  g_free( path );
}

/* add_window_to_win_list
 *  Adds the first window->undo to the second list_char_pair. */
void add_window_to_win_list( gpointer data, gpointer user_data )
{
  list_char_pair *pair = (list_char_pair *) user_data;
  GOWindow *window = (GOWindow *) data;
  
  if ( strcmp( window->title, pair->title ) == 0 )
    {
      *(pair->list) = g_list_append( *(pair->list), (gpointer) window->undo );
    }
}

/* check_title
 *  Increase the count of the given pair if the window names match and
 *  the count isn't high enough yet. */
void check_title( gpointer data, gpointer user_data )
{
  GOWindow *window = (GOWindow *) data;
  int_gchar_pair *pair = (int_gchar_pair *) user_data;
  gint *count = &(pair->x);
  const gchar *title = pair->title;
  
  if ( strcmp( title, short_title_simple( window ) ) == 0 )
    {
      *count = *count > window->count + 1  ?  *count  :  window->count + 1;
    }
}

/* add_window_to_menu_rev
 *  Adds the first window to the second window's window menu. */
void add_window_to_menu_rev( gpointer data, gpointer user_data )
{
  add_window_to_menu( user_data, data );
}

void add_plugin_info_to_plugin_list( gpointer data, gpointer user_data )
{
  GOWindow *window = (GOWindow *) data;
  plugin_info *info = (plugin_info *) user_data;
  int_gchar_pair *pair = g_malloc0( sizeof( int_gchar_pair ) );
  gchar *path = append2( _("_Plugins"), 0, "/", 0);
  gchar *menuname = info->menu_location;
  GnomeUIInfo new_menu_entry[] = { { GNOME_APP_UI_ITEM, NULL, N_( "Open this window" ), (gpointer) start_plugin, NULL, NULL, GNOME_APP_PIXMAP_NONE, 0, 0, 0, NULL }, GNOMEUIINFO_END };

  new_menu_entry[0].label = menuname;
  
  if( info->suggested_accelerator != NULL && *info->suggested_accelerator != 0 )
    {
      gtk_accelerator_parse( info->suggested_accelerator,
			     &new_menu_entry[0].accelerator_key,
			     &new_menu_entry[0].ac_mods );
    }

  pair->title = g_strdup( info->plugin_name );
  pair->x = GPOINTER_TO_INT( window );
  gnome_app_insert_menus_with_data( GNOME_APP( window->app ), path, new_menu_entry, pair );

  g_free( path );
}

void add_plugin_info_to_plugin_list_rev( gpointer data, gpointer user_data )
{
  add_plugin_info_to_plugin_list( user_data, data );
}

void setup_window_stuff( GOWindow *window, gchar *title )
{
  int_gchar_pair pair;

  /* Set the GOWindow title */
  window->title = g_strdup( title );

  /* Check if the name is taken yet */
  pair.x = 0;
  pair.title = short_title_simple( window );
  
  g_list_foreach( windowlist, check_title, &pair );

  /* Set value if the name is taken */
  window->count = pair.x;

  /* Set the GtkWindow title */
  gtk_window_set_title( GTK_WINDOW( window->app ), g_malloc_title( window, "" ) );

  /* Add window to other people's window menus */
  g_list_foreach( windowlist, add_window_to_menu, window );

  {
    list_char_pair pair;
    window->windowlist = NULL;
    pair.list = &(window->windowlist);
    pair.title = window->title;
    g_list_foreach( windowlist, add_window_to_win_list, &pair );

    /* Setup the undo tree */
    window->undo = undo_tree_new( undo_system, window, window->windowlist );
    window->windowlist = NULL;
  }
}

typedef struct
{
  GOWindow *window;
  GtkWidget *widget;
  GnomeCanvasItem *item;
  gint x;
  gint y;
}
window_widget_position_set;

static void
set_x_adjustment( window_widget_position_set *set, GtkAdjustment *adjustment )
{
  gdouble *val;
  gtk_object_get( set->item,
		  "x", val,
		  NULL );
  if ( *val != set->x - adjustment->value )
      gnome_canvas_item_set( set->item,
			   "x", (double) (set->x - adjustment->value),
			   NULL );
}

static void
set_y_adjustment( window_widget_position_set *set, GtkAdjustment *adjustment )
{
  gdouble *val;
  gtk_object_get( set->item,
		  "y", val,
		  NULL );
  if ( *val != set->y - adjustment->value )
    gnome_canvas_item_set( set->item,
			   "y", (double) (set->y - adjustment->value),
			   NULL );
}

static void
go_window_adjustment( GtkAdjustment *adj, GOWindow *window )
{
  if ( adj == GTK_RICH_TEXT( window->textbox )->hadj )
    g_list_foreach( window->embedded_list, set_x_adjustment, adj );
  else
    g_list_foreach( window->embedded_list, set_y_adjustment, adj );
}

void
embed_widget( GOWindow *window, GtkWidget *widget, gint width, gint height )
{
  embed_widget_at_window_location( window, widget, 0, 0, width, height );
}

void
embed_widget_at_window_location( GOWindow *window, GtkWidget *widget, gint x, gint y, gint width, gint height )
{
  window_widget_position_set *newset = g_malloc( sizeof(window_widget_position_set) );
  newset->widget = widget;
  newset->window = window;
  newset->x = x + GTK_RICH_TEXT( window->textbox )->hadj->value;
  newset->y = y;
  g_list_prepend( window->embedded_list, newset );
  newset->item =
    gnome_canvas_item_new( gnome_canvas_root( GNOME_CANVAS( window->canvas) ),
			   gnome_canvas_widget_get_type(),
			   "widget", widget,
			   "x", (double) (x + GTK_RICH_TEXT( window->textbox )->hadj->value),
			   "y", (double) (y + GTK_RICH_TEXT( window->textbox )->vadj->value),
#if 0
			   "width", (double) width,
			   "height", (double) height,
#endif
			   "anchor", GTK_ANCHOR_NW,
			   NULL);
}

static void
canvas_resize_callback( GtkWidget *widget, GtkAllocation *alloc, GOWindow *window )
{
  if ( alloc->width > 50 && alloc->height > 50 )
    {
      gnome_canvas_set_scroll_region ( GNOME_CANVAS( window->canvas ),
				       0, 0,
				       alloc->width, alloc->height );
      gnome_canvas_item_set( window->canvas_item,
			     "width", (double) alloc->width,
			     "height", (double) alloc->height,
			     NULL );
    }
}

/* make_window_with_dimensions
 *  Actually does the work to create an editor window. */
GOWindow *make_window_with_dimensions( gint x, gint y, gint width, gint height, gchar *title )
{
  GOWindow *window;
  GtkWidget *text_hbox, *main_vbox;
  GtkWidget *table;
  gchar *statusmess;
  GtkObject *adj;
  
  /* malloc GOWindow */
  window = g_new( GOWindow, 1 );
  
  /* Setup textbox widget */
  window->textbox = gtk_rich_text_new( 0, 0 );
  gtk_rich_text_set_editable( GTK_RICH_TEXT( window->textbox ), TRUE );
  gtk_rich_text_set_word_wrap( GTK_RICH_TEXT( window->textbox ), TRUE );
  
  /* Setup scrollbar */
  window->vscrollbar = gtk_vscrollbar_new( GTK_RICH_TEXT( window->textbox )->vadj );
  window->hscrollbar = gtk_hscrollbar_new( GTK_RICH_TEXT( window->textbox )->hadj );
  window->ruler = create_ruler( window );

  window->canvas = gnome_canvas_new();
  gnome_canvas_set_scroll_region ( GNOME_CANVAS( window->canvas ),
				   0, 0,
				   100, 100 );

  table = gtk_table_new( 2, 3, FALSE );
  gtk_table_attach( GTK_TABLE( table ), GTK_WIDGET( window->ruler->canvas ), 0, 1, 0, 1, GTK_FILL | GTK_EXPAND, 0, 0, 0 );
  gtk_table_attach( GTK_TABLE( table ), window->canvas, 0, 1, 1, 2, GTK_FILL | GTK_EXPAND, GTK_FILL | GTK_EXPAND, 0, 0 );
  gtk_table_attach( GTK_TABLE( table ), window->vscrollbar, 1, 2, 1, 2, 0, GTK_FILL | GTK_EXPAND, 0, 0 );
  gtk_table_attach( GTK_TABLE( table ), window->hscrollbar, 0, 1, 2, 3, GTK_FILL | GTK_EXPAND, 0, 0, 0 );
  gtk_widget_show( window->vscrollbar );
  gtk_widget_show( window->hscrollbar );
  gtk_widget_show( window->canvas );
  gtk_widget_show( GTK_WIDGET( window->ruler->canvas ) );
  GTK_WIDGET_UNSET_FLAGS( window->vscrollbar, GTK_CAN_FOCUS );
  GTK_WIDGET_UNSET_FLAGS( window->hscrollbar, GTK_CAN_FOCUS );


  window->canvas_item =
    gnome_canvas_item_new( gnome_canvas_root( GNOME_CANVAS( window->canvas) ),
			   gnome_canvas_widget_get_type(),
			   "widget", window->textbox,
			   "x", (double) 0,
			   "y", (double) 0,
			   "width", (double) 200,
			   "height", (double) 200,
			   "anchor", GTK_ANCHOR_NW,
			   NULL);
  
  gtk_widget_show( window->textbox );

  gtk_signal_connect_after( GTK_OBJECT( window->canvas ), "size_allocate",
			    GTK_SIGNAL_FUNC( canvas_resize_callback ),
			    ( gpointer ) window );

  window->embedded_list = NULL;

  adj = GTK_RICH_TEXT( window->textbox )->hadj;

  gtk_object_ref (GTK_OBJECT( adj ));
  
  gtk_signal_connect (GTK_OBJECT (adj), "changed",
		      (GtkSignalFunc) go_window_adjustment,
		      window);
  gtk_signal_connect (GTK_OBJECT (adj), "value_changed",
		      (GtkSignalFunc) go_window_adjustment,
		      window);
#if 0
  gtk_signal_connect (GTK_OBJECT (adj), "disconnect",
		      (GtkSignalFunc) go_window_adjustment_disconnect,
		      window);
#endif

  adj = GTK_RICH_TEXT( window->textbox )->vadj;

  gtk_object_ref (GTK_OBJECT( adj ));
  
  gtk_signal_connect (GTK_OBJECT (adj), "changed",
		      (GtkSignalFunc) go_window_adjustment,
		      window);
  gtk_signal_connect (GTK_OBJECT (adj), "value_changed",
		      (GtkSignalFunc) go_window_adjustment,
		      window);

  /* allocate the window and attach the menu */
  window->app = gnome_app_new("Go", NULL );

  create_gnome_menu( window );

  /* Setup the status bar */
  window->status = gtk_statusbar_new();
  window->contextid = gtk_statusbar_get_context_id( GTK_STATUSBAR( window->status ), "main context" );
  GTK_WIDGET_UNSET_FLAGS( window->status, GTK_CAN_FOCUS );

  /* Setup the toolbar */
  create_gnome_toolbar( window );
  
  /* Setup a vbox to contain the menu */
  main_vbox = gtk_vbox_new( FALSE, 0 );

  gtk_box_pack_start( GTK_BOX( main_vbox ), table, TRUE, TRUE, 0 );
  gtk_widget_show( table );

  gtk_box_pack_start( GTK_BOX( main_vbox ), window->status, FALSE, FALSE, 0 );
  gtk_widget_show( window->status );

  setup_window_stuff( window, title );

  /* Set the initial status message */
  statusmess = g_malloc0( strlen( "Opened " ) + strlen( GTK_WINDOW( window->app )->title ) + 1 );
  sprintf( statusmess, "%s%s", "Opened ", GTK_WINDOW( window->app )->title );
  gtk_statusbar_push( GTK_STATUSBAR( window->status ), window->contextid, statusmess );

  /* Connect the signals */
  gtk_signal_connect( GTK_OBJECT( window->app ), "delete_event",
		      GTK_SIGNAL_FUNC( delete_event_callback ),
		      ( gpointer ) window );
  gtk_signal_connect( GTK_OBJECT( window->app ), "destroy",
		      GTK_SIGNAL_FUNC( destroy_callback ),
		      ( gpointer ) window );
  gtk_signal_connect( GTK_OBJECT( window->textbox ), "insert_text",
		      GTK_SIGNAL_FUNC( insert_text_callback ), ( gpointer ) window );
  gtk_signal_connect( GTK_OBJECT( window->textbox ), "delete_text",
		      GTK_SIGNAL_FUNC( delete_text_callback ), ( gpointer ) window );
  
  /* Add the main vbox to the window */
  gnome_app_set_contents( GNOME_APP( window->app ), main_vbox );
  gtk_widget_show( main_vbox );

  /* Window might not have a name */
  window->named = TRUE;

  /* Set window size but let it be resized */
  gtk_widget_set_usize( window->app, width, height );
  gtk_idle_add( reset_usize, window->app );

  g_list_foreach( plugins, add_plugin_info_to_plugin_list_rev, window );

  /* Add ourselves to the list of windows */
  windowlist = g_list_append( windowlist, ( gpointer ) window );

  /* Setup auto_save */
  window->auto_save = 0;

  /* Setup editability */
  window->is_editable = TRUE;
  gtk_rich_text_set_editable( GTK_RICH_TEXT( window->textbox ), TRUE );

  /* Add other windows to our window list */
  g_list_foreach( windowlist, add_window_to_menu_rev, window );

  /* Show window */
  gtk_widget_show( window->app );

  /* Set the focus on the textbox (This needs to work better) */
  gtk_window_set_focus( GTK_WINDOW( window->app ), window->textbox );

  /* Set the position of the window if it was requested */
  if ( x >= 0 && y >= 0 )
    gtk_widget_set_uposition( window->app, x, y );

  window->format = GO_FILE_FORMAT_PLAIN;
  window->props = go_properties_new();

  return window;
}

gint toggle_editable( GOWindow *window )
{
  window->is_editable = ! window->is_editable;
  gtk_rich_text_set_editable( GTK_RICH_TEXT( window->textbox ), window->is_editable );
  return window->is_editable;
}

void toggle_editable_callback( GtkWidget *widget, gpointer data )
{
  GOWindow *window = (GOWindow *) data;
  toggle_editable( window );
}

/* count_title
 *  This function returns one greater than the highest count it can
 *  find on a window that matches the title given.  0 if none exist
 *  yet.  */
int count_title( gchar *title )
{
  int_gchar_pair pair;
  gchar *substring;
  
  substring = strrchr( title, '/' ) + 1;
  if ( substring == 0 + (char *) 1 )
    substring = title;

  pair.x = 0;
  pair.title = substring;
  
  g_list_foreach( windowlist, check_title, &pair );

  return pair.x;
}
